import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
	},

	note: {
		flexGrow: .2,
	},

	info: {
		color: Colors.black.palette(2, .6),
	},

	dropdown: {
		marginLeft: 8,
		marginRight: 4,
	},

})
