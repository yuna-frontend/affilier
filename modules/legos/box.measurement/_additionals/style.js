import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	row: {
		marginBottom: 16,
	},

	content: {
		marginTop: -16,
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 16,
		paddingRight: 16,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		justifyContent: 'space-between',
	},
})
