import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import CommonHelper from 'coeur/helpers/common';
import ConnectHelper from 'coeur/helpers/connect';
import StringHelper from 'coeur/helpers/string';

import UtilitiesContext from 'coeur/contexts/utilities';

import PurchaseService from 'app/services/purchase';
import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import BoxLego from 'modules/legos/box';
import LoaderLego from 'modules/legos/loader';
import NotificationLego from 'modules/legos/notification';
import SelectionStylesheetInventoryLego from 'modules/legos/selection.stylesheet.inventory';

import PurchaseRequestUpdatePagelet from 'modules/pagelets/purchase.request.update'
import StylistNotePagelet from 'modules/pagelets/stylist.note'
import QuickEditStylesheetInventoryPagelet from 'modules/pagelets/quick.edit.stylesheet.inventory';

import AddPart from './_add';
import ValuePart from './_value';

import Styles from './style';

import { isEmpty, without } from 'lodash'


export default ConnectHelper(
	class StylesheetInventoryListPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,

				readonly: PropTypes.bool,
				title: PropTypes.string,
				userName: PropTypes.string,
				userNote: PropTypes.string,
				stylistNote: PropTypes.string,

				// TODO: item category validation

				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? StylesheetService.getStyleSheetShallow(oP.id, state.me.token) : null
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nP.data) && !nS.isLoaded) {
				return {
					readonly: nP.data.status !== 'STYLING' || nP.readonly,
					isLoaded: true,
					inventoryIds: nP.data.stylesheet_inventory_ids,
				}
			}

			return null
		}

		static defaultProps = {
			title: 'Items',
		}

		constructor(p) {
			super(p, {
				readonly: p.readonly,
				isLoaded: false,
				isLoading: false,
				isChanging: false,
				isValid: true,
				isRemovingIndex: [],
				inventoryIds: [],
				inventories: {},
				totalRetailValue: 0,
				totalRealValue: 0,
			})

			this.inventories = {}
		}

		componentDidUpdate(pP, pS) {
			if(
				pS.inventoryIds !== this.state.inventoryIds
				|| pS.inventories !== this.state.inventories
			) {
				// eslint-disable-next-line react/no-did-update-set-state
				this.setState(this.state.inventoryIds.map(inventoryId => {
					const inventory = this.state.inventories[inventoryId]
					return {
						retail_price: inventory ? inventory.retail_price : 0,
						price: inventory ? inventory.price : 0,
					}
				}).reduce((sum, curr) => {
					return {
						totalRetailValue: sum.totalRetailValue + curr.retail_price,
						totalRealValue: sum.totalRealValue + curr.price,
					}
				}, {
					totalRetailValue: 0,
					totalRealValue: 0,
				}))
			}
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onAdd = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddPart id={this.props.id} onUpdate={this.onAddedIntoInventories} />
				),
			})
		}

		onAddedIntoInventories = id => {
			this.setState({
				inventoryIds: [...this.state.inventoryIds, id],
			})
		}

		onRemove = id => {
			this.setState({
				isRemovingIndex: [...this.state.isRemovingIndex, id],
			})

			return StylesheetService.onRemoveInventoryFromStylesheet(id, this.props.token).then(res => {
				if(res) {
					this.props.utilities.notification.show({
						title: 'Removal Success',
						message: 'Success removing inventory.',
						type: 'SUCCESS',
					})

					delete this.inventories[id]

					this.setState({
						isRemovingIndex: without(this.state.isRemovingIndex, id),
						inventoryIds: without(this.state.inventoryIds, id),
					})
				} else {
					throw new Error('No result, or deletion failed')
				}
			}).catch(err => {
				this.warn(err)

				this.props.utilities.notification.show({
					title: 'Oops…',
					message: `Something went wrong. (${ err && (err.detail || err.message) || err })`,
				})

				this.setState({
					isRemovingIndex: without(this.state.isRemovingIndex, id),
				})
			})
		}

		onStartStyling = () => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					return StylesheetService.setStylesheetStatus(this.props.id, 'STYLING', undefined, this.props.token)
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Go…Go…Go…',
						message: `Happy styling, ${this.props.me} 🤗`,
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChanged &&
						this.props.onChanged('STYLING', undefined)
					})
				}).catch(this.onError)
			})
		}

		onPublishStylesheet = () => {
			this.props.utilities.alert.modal({
				component: (
					<StylistNotePagelet
						client={ this.props.userName }
						note={ this.props.userNote }
						value={ this.props.stylistNote }
						onClose={ this.onModalRequestClose }
						onSubmit={ stylistNote => {
							return Promise.resolve().then(() => {

								this.setState({
									isChanging: true,
								})

								return StylesheetService.publish(this.props.id, 'PUBLISHED', stylistNote, {}, this.props.token)

							}).then(() => {
								this.props.utilities.notification.show({
									title: 'Congratulations 🎉🎉🎉',
									message: 'One stylesheet down. Let\'s keep the spirit up!',
									type: this.props.utilities.notification.TYPES.SUCCESS,
								})

								this.onModalRequestClose()

								this.setState({
									isChanging: false,
								}, () => {
									this.props.onChanged &&
									this.props.onChanged('PUBLISHED', undefined, stylistNote)
								})
							}).catch(this.onError)
						} } />
				),
			})
		}

		onEditPurchaseRequest = (id, purchaseRequestId, refresh) => {
			this.props.utilities.alert.modal({
				component: (
					<PurchaseRequestUpdatePagelet editable id={ purchaseRequestId } onUpdate={ data => {
						PurchaseService.updateRequest(this.props.id, {
							...data,
							purchase_order_id: null,
							status: 'PENDING',
						}, this.props.token).then(() => {
							this.props.utilities.notification.show({
								title: 'Success',
								message: 'Purchase request updated',
								type: 'SUCCESS',
							})

							refresh &&
							refresh()
						}).catch(this.onError)
					} } onRemove={ () => {
						this.onRemove(id)
					} } />
				),
			})
		}

		onEditStylesheetInventory = (id, refresh) => {
			this.props.utilities.alert.modal({
				component: (
					<QuickEditStylesheetInventoryPagelet
						id={ id }
						onUpdated={ () => {
							refresh &&
							refresh()
						} }
					/>
				),
			})
		}

		onDuplicatePurchaseRequest = (stylesheetInventoryId, purchaseRequestId) => {
			this.props.utilities.alert.modal({
				component: (
					<LoaderLego />
				),
			})

			this.onRemove(stylesheetInventoryId).then(() => {
				return PurchaseService.getDetail(purchaseRequestId, {
					basic: true,
				}, this.props.token).then(pr => {
					this.props.utilities.alert.hide()

					this.props.utilities.alert.modal({
						component: (
							<PurchaseRequestUpdatePagelet editable noquantity unremoveable data={CommonHelper.stripUndefined({
								...pr,
								note: undefined,
								id: undefined,
								created_at: undefined,
								updated_at: undefined,
								status: undefined,
								purchase_order_id: undefined,
							})} onUpdate={data => {
								StylesheetService.onPutInventoryToStylesheet(this.props.id, {
									type: 'PURCHASE_REQUEST',
									data,
								}, this.props.token).then(sI => {
									this.props.utilities.notification.show({
										title: 'Success',
										message: 'Purchase request duplicated',
										type: 'SUCCESS',
									})

									this.onAddedIntoInventories(sI.id)

									this.props.utilities.alert.hide()
								}).catch(this.onError)
							}} />
						),
					})
				})
			}).catch(err => {
				this.warn(err)

				this.props.utilities.alert.hide()
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				timeout: 10000,
			})

			this.setState({
				isChanging: false,
			})
		}

		onDataLoaded = (id, inventory) => {
			if(!this.state.inventories[id]) {
				this.setState({
					inventories: {
						...this.state.inventories,
						[id]: inventory,
					},
				})
			}
		}

		onValueUpdate = ({ isValid }) => {
			this.setState({
				isValid: this.state.inventoryIds.length === Object.keys(this.state.inventories).length ? isValid : true,
			})
		}

		onRefresh = () => {
			this.props.refresh &&
			this.props.refresh(true)

			this.setState({
				isLoaded: false,
				inventories: {},
			})
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering style={ Styles.empty }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.text }>
						No Data
					</GeomanistBit>
				</BoxBit>
			)
		}

		titleRenderer() {
			return (
				<BoxBit row unflex>
					<GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 } style={Styles.title}>
						{ this.props.title }
					</GeomanistBit>
					<TouchableBit centering unflex onPress={ this.onRefresh } style={Styles.refresh}>
						<IconBit
							name={ 'refresh' }
							size={ 18 }
							// color={ }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		headerRenderer() {
			if(this.state.isLoaded) {
				if(this.props.data.status === 'PENDING') {
					return (
						<ButtonBit
							title={ 'Start Styling'}
							weight={ 'medium' }
							size={ ButtonBit.TYPES.SIZES.SMALL }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.state.isChanging ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onStartStyling }
							style={ Styles.button }
						/>
					)
				} else if (this.props.data.status === 'STYLING') {
					return this.props.data.count <= this.state.inventoryIds.length ? (
						<ButtonBit
							title={ 'Publish' }
							weight={ 'medium' }
							size={ ButtonBit.TYPES.SIZES.SMALL }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							// eslint-disable-next-line no-nested-ternary
							state={ this.state.isChanging ? ButtonBit.TYPES.STATES.LOADING : this.state.isValid ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onPublishStylesheet }
							style={ Styles.button }
						/>
					) : (
						<BoxBit unflex row style={Styles.header}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text} >
								{ this.state.inventoryIds.length } of { StringHelper.pluralize(this.props.data.count, 'item', '0 item', 's') }
							</GeomanistBit>
							<ButtonBit
								title={ 'Add Item' }
								weight={ 'medium' }
								size={ ButtonBit.TYPES.SIZES.SMALL }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								state={ this.props.data.status === 'STYLING' && (this.props.data.count <= this.state.inventoryIds.length || this.state.readonly) ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onAdd }
								style={ Styles.button }
							/>
						</BoxBit>
					)
				} else if (this.props.data.status === 'PUBLISHED' || this.props.data.status === 'APPROVED') {
					return (
						<ButtonBit
							title={ 'Reopen' }
							weight={ 'medium' }
							size={ ButtonBit.TYPES.SIZES.SMALL }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ ButtonBit.TYPES.STATES.DISABLED }
							style={ Styles.button }
						/>
					)
				} else {
					return false
				}
			} else {
				return false
			}
		}

		inventoryRenderer = (inventoryId, index) => {
			return (
				<SelectionStylesheetInventoryLego key={ inventoryId }
					editable={ !this.props.readonly && this.props.data.status === 'STYLING' }
					borderless={ index === this.state.inventoryIds.length - 1 }
					removable={ !this.state.readonly }
					disabled={ this.state.isRemovingIndex.indexOf(inventoryId) > -1 }
					id={ inventoryId }
					onEditPurchaseRequest={ this.onEditPurchaseRequest.bind(this, inventoryId) }
					onEditStylesheetInventory={ this.onEditStylesheetInventory }
					onDuplicatePurchaseRequest={ this.onDuplicatePurchaseRequest.bind(this, inventoryId) }
					onRemove={ this.onRemove.bind(this, inventoryId) }
					onDataLoaded={ this.onDataLoaded.bind(this, inventoryId) }
					style={ Styles.item }
				/>
			)
		}

		notificationRenderer = () => {
			if (this.props.data.lineup) {
				const lineupCategory = this.props.data.lineup.split(' + ')
				let matching = true

				this.state.inventoryIds.map(id => {
					if (this.state.inventories[id]) {
						return this.state.inventories[id]
					} else {
						return false
					}
				}).find(inventory => {
					if (inventory) {
						if (lineupCategory.indexOf(inventory.category) > -1) {
							// found
							lineupCategory.splice(lineupCategory.indexOf(inventory.category), 1)
							return false
						} else {
							matching = false
							return true
						}
					} else {
						matching = null
						return true
					}
				})

				if (matching === false) {
					return (
						<NotificationLego type={NotificationLego.TYPES.WARNING} message="Lineup mismatch. Please change some of the items to match stylesheet's lineup." />
					)
				} else if(!this.state.isValid && this.props.data.status === 'STYLING' && this.state.inventoryIds.length === this.props.data.count) {
					return (
						<NotificationLego type={NotificationLego.TYPES.WARNING} message="Retail value or purchase value doesn't fulfill the requirement." />
					)
				} else {
					return false
				}

			} else {
				return false
			}
		}

		viewOnLoading() {
			return (
				<BoxLego
					title={ this.props.title }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					<BoxBit unflex centering style={ Styles.empty }>
						<LoaderBit simple />
					</BoxBit>
				</BoxLego>
			)
		}

		view() {
			return (
				<BoxLego
					title={ this.titleRenderer() }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					<BoxBit unflex style={ Styles.padder }>
						{ this.notificationRenderer() }
						{ this.state.inventoryIds.length
							? this.state.inventoryIds.map(this.inventoryRenderer)
							: this.emptyRenderer()
						}
					</BoxBit>
					<ValuePart
						value={ this.props.data.value }
						minValue={ this.props.data.min_value }
						realValue={ this.props.data.real_value }
						currentValue={ this.state.totalRetailValue }
						currentRealValue={ this.state.totalRealValue }
						onUpdate={ this.onValueUpdate }
					/>
				</BoxLego>
			)
		}
	}
)
