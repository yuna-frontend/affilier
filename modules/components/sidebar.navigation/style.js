import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		position: 'fixed',
		left: 0,
		top: 0,
		width: 240,
		height: '100%',
		backgroundColor: Colors.black.palette(2),
		zIndex: 3,
		overflow: 'hidden',
		transform: 'translateX(-190px)',
		transition: 'transform cubic-bezier(0.21, 0.96, 0.7, 0.93) .2s',
	},

	innerContainer: {
		transition: 'transform cubic-bezier(0.21, 0.96, 0.7, 0.93) .2s',
	},

	closeIcon: {
		display: 'none',
	},

	nav: {
		alignItems: 'center',
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 24,
		paddingRight: 24,
	},

	backgroundGrey: {
		backgroundColor: Colors.grey.primary,
	},

	header: {
		paddingTop: 24,
		paddingBottom: 24,
		paddingLeft: 18,
		paddingRight: 18,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	icon: {
		marginRight: 20,
	},

	text: {
		color: Colors.white.primary,
	},

	textInactive: {
		color: Colors.white.palette(1, .4),
	},

	'@media screen and (min-width: 481px)': {
		container: {
			'&:hover, &:hover $innerContainer': {
				transform: 'translateX(0)',
			},
		},

		innerContainer: {
			transform: 'translateX(180px)',
		},
	},

	'@media screen and (max-width: 480px)': {
		closeIcon: {
			display: 'inherit',
		},

		containerMobile: {
			width: '100%',
			transform: 'translateX(-100%)',
		},
	
		containerMobileVisible: {
			transform: 'translateX(0)',
		},
	},
})
