import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'utils/constants/color'
import LineupHelper from 'utils/helpers/lineup';

import UtilitiesContext from 'coeur/contexts/utilities';

import AuthenticationHelper from 'utils/helpers/authentication';
import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import InputImageUrlBit from 'modules/bits/input.image.url';
import InputValidatedBit from 'modules/bits/input.validated';
import ScrollViewBit from 'modules/bits/scroll.view';
import SelectionMultipleBit from 'modules/bits/selection.multiple';
import InputFileBit from 'modules/bits/input.file'
import LoaderBit from 'modules/bits/loader'

import UserService from 'app/services/user.new'


import LoaderLego from 'modules/legos/loader';
import NotificationLego from 'modules/legos/notification';

// import FormPagelet from 'modules/pagelets/form';
import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';

import { capitalize, isEmpty } from 'lodash';

let cache = null
function titleToSlug(title) {
	return (title || '').toLowerCase()
		// replace all special characters | symbols with a space
		.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ')
		// trim spaces at start and end of string
		.replace(/^\s+|\s+$/gm, '')
		// replace space with dash/hyphen
		.replace(/\s+/g, '-')
}


export default ConnectHelper(
	class QuickCreateStylecardPagelet extends QueryStatefulModel {

		static TYPES = {
			DEFAULT: 'DEFAULT',
			PRESTYLED: 'PRESTYLED',
		}

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES),
				onCreated: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				id: state.me.id,
				token: state.me.token,
				creatable: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),
			}
		}

		static propsToQuery(state) {
			return cache ? null : [`{
				collections: collectionsList(filter: {
					deletedAt: {
						isNull: true
					}
				}) {
					id
					title
				}
				matchboxes: matchboxesList {
					id
					title
					brand {
						title
					}
				}
				stylist: userRolesList(filter: {
					role: { likeInsensitive: "stylist" }
				}) {
					data: user {
						id
						name: userProfile {
							firstName
							lastName
							stageName
						}
					}
				}
			}`, {}, state.me.token]
		}

		static getDerivedStateFromProps(nP, nS) {
			if (!nS.isLoaded && (!isEmpty(nP.data) || cache)) {
				if (cache === null) {
					cache = nP.data
				}

				// const matchboxes = cache.matchboxes.map(m => {
				// 	return {
				// 		key: m.id,
				// 		title: `${ m.brand.title } – ${ m.title }`,
				// 		selected: m.id === 12,
				// 	}
				// })

				const stylist = cache.stylist.map(s => {
					return {
						key: s.data.id,
						title: s.data.name.stageName || `${ s.data.name.firstName } ${ s.data.name.lastName }`,
					}
				})

				return {
					isLoaded: true,
					isCreating: false,
					updater: nS.updater + 1,
					config: nP.type === 'DEFAULT' ? [{
						id: 'stylist_id',
						title: 'Stylist',
						disabled: true,
						placeholder: 'Assign stylist',
						type: InputValidatedBit.TYPES.SELECTION,
						options: stylist.map(s => {
							return {
								...s,
								selected: s.key === nP.id,
							}
						}),
						style: Styles.input,
					}, {
						id: 'status',
						title: 'Status',
						required: true,
						type: InputValidatedBit.TYPES.SELECTION,
						options: ['PENDING', 'STYLING', 'PUBLISHED', 'EXCEPTION', 'RESOLVED'].map(s => {
							return {
								key: s,
								title: capitalize(s),
								selected: s === 'PENDING',
							}
						}),
						style: Styles.input,
					}, {
						id: 'title',
						title: 'Title',
						placeholder: 'Input here…',
						type: InputValidatedBit.TYPES.INPUT,
						style: Styles.input,
					}, {
						id: 'lineup',
						title: 'Lineup',
						type: InputValidatedBit.TYPES.SELECTION,
						options: LineupHelper.lineups,
						style: Styles.input,
					}, {
						id: 'count',
						title: 'Variant Count',
						required: true,
						type: InputValidatedBit.TYPES.NUMBER,
						placeholder: 'Input here…',
						description: 'Number of variant needs to be added',
						style: Styles.input,
					}, {
						id: 'is_master',
						label: 'Master Style Card',
						type: 'CHECKBOX',
						description: 'Create as a master style card that is duplicatable',
						style: Styles.input,
					}] : [{
						id: 'url',
					}, {
						id: 'collection_ids',
						options: cache.collections.map(c => {
							return {
								key: c.id,
								title: c.title,
							}
						}),
					}, {
						id: 'stylist_id',
						title: 'Stylist',
						required: true,
						placeholder: 'Assign stylist',
						type: InputValidatedBit.TYPES.SELECTION,
						options: stylist,
						style: Styles.input,
					}, {
						id: 'status',
						title: 'Status',
						required: true,
						type: InputValidatedBit.TYPES.SELECTION,
						options: ['PENDING', 'STYLING', 'PUBLISHED', 'EXCEPTION', 'RESOLVED'].map(s => {
							return {
								key: s,
								title: capitalize(s),
								selected: s === 'PENDING',
							}
						}),
						style: Styles.input,
					}, {
						id: 'title',
						title: 'Title',
						placeholder: 'Input here…',
						type: InputValidatedBit.TYPES.INPUT,
						style: Styles.input,
					}, {
						id: 'slug',
					}, {
						id: 'lineup',
						title: 'Lineup',
						type: InputValidatedBit.TYPES.SELECTION,
						options: LineupHelper.lineups,
						style: Styles.input,
					}, {
						id: 'count',
						title: 'Variant Count',
						required: true,
						type: InputValidatedBit.TYPES.NUMBER,
						placeholder: 'Input here…',
						description: 'Number of variant needs to be added',
						style: Styles.input,
					}, {
						id: 'note',
						title: 'Note',
						placeholder: 'Give notes to assigned stylist…',
						type: InputValidatedBit.TYPES.INPUT,
						style: Styles.input,
					}, {
						id: 'is_bundle',
						label: 'Bundled Purchase',
						type: 'CHECKBOX',
						description: 'Style card must be purchased as a bundle',
						style: Styles.input,
					}, {
						id: 'inventory_only',
						label: 'Use Inventory Only',
						type: 'CHECKBOX',
						description: 'Variants can only be purchased if inventory available in-house',
						style: Styles.input,
					}],
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			type: 'DEFAULT',
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				isCreating: false,
				config: [],
				data: {
					matchbox_id: 12,
					status: 'PENDING',
					stylist_id: p.type === 'DEFAULT' ? p.id : undefined,
					count: 5,
				},
				validity: {
					matchbox_id: true,
					stylist_id: p.type === 'DEFAULT' ? true : false,
					count: true,
				},
				focus: undefined,
				manualSlug: false,
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onChangeCollections = selections => {
			this.state.data['collection_ids'] = selections.map(s => s.key)
		}

		onUpdate = (id, data, isValid) => {
			let needChange = false

			this.state.focus = id

			if (id === 'title' && this.props.type !== 'DEFAULT' && !this.state.manualSlug && this.state.data[id] !== data) {
				needChange = true
				this.state.data.slug = titleToSlug(data)
				this.state.validity.slug = !!data
				this.state.data[id] = data
				this.state.validity[id] = isValid
			} else if (id === 'slug') {
				needChange = true
				this.state.manualSlug = true
				this.state.data[id] = titleToSlug(data)
				this.state.validity[id] = isValid
			} else {
				this.state.data[id] = data
				this.state.validity[id] = isValid
			}


			if (needChange) {
				this.forceUpdate()
			}

		}

		onSubmit = () => {
			if (
				this.state.validity.matchbox_id
				&& this.state.validity.stylist_id
				&& this.state.validity.count
			) {
				this.setState({
					isCreating: true,
				})

				StylecardService.create({
					matchbox_id: this.state.data.matchbox_id,
					stylist_id: this.state.data.stylist_id,
					status: this.state.data.status,
					count: this.state.data.count,
					title: this.state.data.title,
					slug: this.state.data.slug,
					note: this.state.data.note,
					image: this.state.data.url,
					lineup: this.state.data.lineup,
					is_master: this.state.data.is_master,
					is_bundle: this.state.data.is_bundle,
					inventory_only: this.state.data.inventory_only,
					stylist_note: this.state.data.stylist_note,
					collection_ids: this.state.data.collection_ids,
				}, this.props.token).then(res => {
					this.onClose()

					this.props.onCreated &&
					this.props.onCreated(res)

					this.props.utilities.notification.show({
						title: 'Yeay',
						message: 'Style card created.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: err ? err.detail || err.message || err : 'Something went wrong. Please try again.',
					})

					this.setState({
						isCreating: false,
					})
				})
			} else {
				this.props.utilities.notification.show({
					title: 'Data invalid or incomplete',
					message: 'Please fill in all required fields.',
				})
			}
		}

		onDataLoaded = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({image: undefined}, () => {
					// this.props.onImageRemoved()
				})
			} else if(id === 'adder') {
				this.setState({isUploading: true}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
						// this.props.onImageLoaded
						// && this.props.onImageLoaded(res)
	
						// this.onChange('image', res.url)
						this.setState({data: {
							...this.state.data,
							url: res.url,
						}, isUploading: false})
					})
				})
			}
		}

		viewOnError() {
			return (
				<ModalQuickViewPagelet
					title="Create Style Card"
					layout="1-way"
					onClose={ this.onClose }
				>
					<BoxBit row centering style={ Styles.empty }>
						<IconBit name="circle-info" color={ Colors.primary } />
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.error }>
							{ !this.props.creatable ? 'You are not authorized to create a style card.' : 'Something went wrong when loading the data' }
						</GeomanistBit>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return !this.props.creatable ? this.viewOnError() : (
				<ModalQuickViewPagelet
					title={ 'Create Style Card' }
					layout="1-way"
					onClose={ this.onClose }
				>
					<ScrollViewBit style={ Styles.content }>
						{ this.props.type === this.TYPES.PRESTYLED ? (
							<NotificationLego type={ NotificationLego.TYPES.WARNING } message="You need to manually set style card as public in order to publish it" />
						) : false }

						{ this.state.config.map(({ id, ...c }) => {
							switch(id) {
							case 'slug':
								return (
									<InputValidatedBit
										key={ `${ id }|${ this.state.data.slug }` }
										title="Slug"
										autofocus={ this.state.focus === id }
										placeholder={ 'Input here…' }
										type={ InputValidatedBit.TYPES.INPUT }
										onChange={ this.onUpdate.bind(this, id) }
										value={ this.state.data.slug }
										style={ Styles.input }
									/>
								)
							case 'collection_ids':
								return (
									<SelectionMultipleBit
										key={ id }
										title="Collections"
										placeholder="Select collection"
										options={ c.options }
										onChange={ this.onChangeCollections }
										style={ Styles.input }
									/>
								)
							case 'url':
								return (
									<BoxBit style={Styles.imageContainer}>
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold">
											Input Image
										</GeomanistBit>
										{this.state.isUploading
											? <LoaderBit simple />
											: <InputFileBit
												id={ this.state.data.url ? 'loader' : 'adder'}
												maxSize={3}
												value={ this.state.data.url && '//' + this.state.data.url }
												onDataLoaded={this.onDataLoaded}
												
											/>}

									</BoxBit>
									// <InputImageUrlBit
									// 	title="Image"
									// 	placeholder="Insert URL here…"
									// 	value={ this.state.data[id] }
									// 	onChange={ this.onUpdate.bind(this, id) }
									// 	style={ Styles.input }
									// />
								)
							default:
								if (c.type === 'CHECKBOX') {
									return (
										<InputCheckboxBit
											{ ...c }
											key={ id }
											onChange={ this.onUpdate.bind(this, id) }
											value={ this.state.data[id] }
										/>
									)
								} else {
									return (
										<InputValidatedBit
											{ ...c }
											key={ id }
											autofocus={ this.state.focus === id }
											onChange={ this.onUpdate.bind(this, id) }
											value={ this.state.data[id] }
										/>
									)
								}
							}
						}) }

					</ScrollViewBit>
					<BoxBit unflex style={ Styles.button }>
						<ButtonBit
							title={ 'Create Style Card' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.state.isCreating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onSubmit }
						/>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
