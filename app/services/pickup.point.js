import ServiceModel from 'coeur/models/service';


class PickupPointService extends ServiceModel {

	static displayName = 'pickuppoint'

	constructor() {
		super(process.env.API_URL + 'admin/pickuppoint/', 0)
	}

	getPickupPoints(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getPickupPoint(id, token) {
		return this.get(id, {
			token,
		})
	}

	addPickupPoints(pickupPoints, token) {
		return this.post('', {
			data: {
				creates: pickupPoints,
			},
			token,
		})
	}

	updatePickupPoints(pickupPoints, token) {
		return this.post('', {
			data: {
				updates: pickupPoints,
			},
			token,
		})
	}
}

export default new PickupPointService()
