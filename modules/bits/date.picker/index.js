import ConnectHelper from 'coeur/helpers/connect'
import CoreDatePickerBit from 'coeur/modules/bits/date.picker'

import BoxBit from '../box'
import IconBit from '../icon'

export default ConnectHelper(
	class DatePickerBit extends CoreDatePickerBit({
		BoxBit,
		IconBit,
	}) {}
)
