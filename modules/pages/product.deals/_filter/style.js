import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		width: Sizes.app.width,
		paddingTop: 8,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 8,
		backgroundColor: Colors.white.primary,
		justifyContent: 'space-between',
	},

	content: {
		paddingTop: 8,
		paddingBottom: 8,
	},
	
	contentMobile: {
		padding: 0,
		justifyContent: 'space-between',
	},

	div: {
		width: 8,
	},

	greyBase: {
		color: Colors.grey.palette(12),
	},

	...(Defaults.PLATFORM === 'web' ? {

		headerFilter: {
			paddingTop: 8,
			paddingBottom: 8,
			borderBottomWidth: 1,
			borderColor: Colors.black.palette(2, .1),
			justifyContent: 'space-between',
		},


		contentColl: {
			paddingBottom: Sizes.margin.default,
		},

		containerHeader: {
			paddingTop: 4,
			paddingBottom: 8,
		},

		contentContainerColl: {
			marginLeft: -4,
			marginRight: -4,
			paddingTop: 4,
			paddingBottom: 4,
			flexWrap: 'wrap',
		},

		containerSelect: {
			paddingTop: 4,
			paddingLeft: 4,
			paddingRight: 4,
			paddingBottom: 4,
		},

		select: {
			minHeight: 48,
			borderWidth: 1,
			borderColor: Colors.grey.palette(16),
			paddingLeft: 12,
			paddingRight: 12,
		},

		greyBase: {
			color: Colors.grey.palette(12),
		},

		'@media screen and (max-width: 480px)': {
			container: {
				width: Sizes.app.width,
				paddingTop: 8,
				paddingLeft: Sizes.margin.thick,
				paddingRight: Sizes.margin.thick,
				paddingBottom: 8,
				backgroundColor: Colors.white.primary,
				justifyContent: 'space-between',
			},
		},
	} : {} ),
})
