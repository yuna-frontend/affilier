import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

const marginThin = Sizes.margin.default / 2

export default StyleSheet.create({
	container: {
		position: 'absolute',
		top: '100%',
		right: -5,
		zIndex: 1,
		backgroundColor: Colors.white.primary,
		maxWidth: '180',
		paddingTop: marginThin,
		paddingBottom: marginThin,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	listItem: {
		width: '100%',
		paddingTop: marginThin,
		paddingBottom: marginThin,
		alignItems: 'center',
	},

	listItemInFilterModal: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick - 8,
		paddingRight: Sizes.margin.thick - 8,
		paddingBottom: Sizes.margin.default,
		justifyContent: 'space-between',
	},

	listItemTitle: {
		marginLeft: Sizes.margin.default / 2,
	},
})
