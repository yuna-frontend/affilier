import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import SwitchBit from 'modules/bits/switch'
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class BoxLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				borderless: PropTypes.bool,
				unflex: PropTypes.bool,
				small: PropTypes.bool,
				title: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.node,
				]).isRequired,
				header: PropTypes.node,
				children: PropTypes.node.isRequired,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
				useSwitch: PropTypes.bool,
				switchValue: PropTypes.bool,
				switchOnChange: PropTypes.func,
			}
		}

		static defaultProps = {
			title: 'Title',
			unflex: true,
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={[Styles.container, this.props.style]}>
					<BoxBit row style={[Styles.header, this.props.borderless && Styles.borderless, this.props.small && Styles.small]}>
						{ typeof this.props.title !== 'string' ? this.props.title : (
							<GeomanistBit type={this.props.small ? GeomanistBit.TYPES.SUBHEADER_1 : GeomanistBit.TYPES.HEADER_5} style={Styles.title}>
								{ this.props.title }
							</GeomanistBit>
						) }
						<BoxBit />
						{ this.props.useSwitch &&
							<SwitchBit
								value={ this.props.switchValue }
								onChange={ this.props.switchOnChange }
							/>
						}
						{ this.props.header }
					</BoxBit>
					<BoxBit unflex style={[Styles.content, this.props.contentContainerStyle]}>
						{ this.props.children }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
