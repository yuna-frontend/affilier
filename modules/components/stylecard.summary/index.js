import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import LinkBit from 'modules/bits/link';
import ShadowBit from 'modules/bits/shadow';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';
import ClipboardBit from 'modules/bits/clipboard';


import TimeHelper from 'coeur/helpers/time';


import BadgeStatusLego from 'modules/legos/badge.status';

import QuickViewStylecardPagelet from 'modules/pagelets/quick.view.stylecard';
import QuickViewClusterStylecardPagelet from 'modules/pagelets/quick.view.cluster.stylecard'
import PopupDuplicateStylecardPagelet from 'modules/pagelets/popup.duplicate.stylecard';

import Styles from './style';


export default ConnectHelper(
	class StylecardSummaryComponent extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				index: PropTypes.number,
				styleboardId: PropTypes.id,
				id: PropTypes.id,
				title: PropTypes.string,
				variants: PropTypes.arrayOf(PropTypes.shape({
					image: PropTypes.image,
				})),
				featuredImage: PropTypes.image,
				stylistNote: PropTypes.string,
				count: PropTypes.number,
				value: PropTypes.number,
				minValue: PropTypes.number,
				realValue: PropTypes.number,
				status: PropTypes.string,
				publishedAt: PropTypes.string,
				showLink: PropTypes.bool,
				isRecommendation: PropTypes.bool,
			}
		}

		static defaultProps = {
			isRecommendation: false,
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToStylesheet = () => {
			this.onModalRequestClose()
			this.props.page.navigator.navigate('stylecard/' + this.props.id)
		}

		onNavigate = (href, param) => {
			this.props.page.navigator.navigate(href, param)
		}

		onSelectStylesheet = () => {
			this.props.utilities.alert.modal({
				component: (
					<PopupDuplicateStylecardPagelet
						id={ this.props.id }
						styleboardId={ this.props.styleboardId }
						count={ this.props.count }
					/>
				),
			})
		}

		onPreviewAdmin = () => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewClusterStylecardPagelet
						title={this.props.title}
						featuredImage={ this.props.featuredImage }
						data={ this.props.variants }
						width={ 1440 }
						percentage={this.props.variants.map(variant => 100 - Math.round(100 * variant.price / variant.retail_price))}
					/>

				),
			})
		}

		onQuickView = () => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewStylecardPagelet
						id={ this.props.id }
						onNavigate={ this.onNavigate }
					/>
				),
			})
		}

		inventoryRenderer = (sI, i) => {
			return (
				<ShadowBit key={ i } x={ 0 } y={ 0 } blur={ 4 } style={ Styles.inventory }>
					<ImageBit
						broken={ !sI.image }
						source={ sI.image }
						style={ Styles.image }
						resizeMode={ ImageBit.TYPES.COVER }
					/>
				</ShadowBit>
			)
		}

		linkRenderer = () => {
			return (
				<ClipboardBit
					autogrow={ 44 }
					value={`helloyuna.io/stylecard/${this.props.id}${this.props.isRecommendation ? '?tr_source=wareco2207&utm_source=wareco2207&utm_medium=wa&utm_campaign=wa_blast' : ''}`}
					style={ Styles.clipboard }
				/>
			)
		}

		view() {
			return(
				<TouchableBit style={ Styles.container } onPress={ this.onNavigateToStylesheet }>
					<BoxBit row style={ Styles.header }>
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }><TextBit style={ Styles.id }>[SC-#{ this.props.id }]</TextBit> { !!this.props.title ? this.props.title : `Nº ${ this.props.index + 1 }` }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.completion }>Completion: <TextBit style={ this.props.variants.length === this.props.count ? Styles.ok : undefined }>{ this.props.variants.length }/{ this.props.count }</TextBit>, Stylist Note: <TextBit style={ this.props.stylistNote ? Styles.ok : Styles.no }>{ this.props.stylistNote ? 'OK' : 'Unfilled' }</TextBit></GeomanistBit>
						</BoxBit>
						<BoxBit unflex row>
							<TouchableBit unflex centering style={ Styles.icon } onPress={ this.onPreviewAdmin }>
								<IconBit
									name="images"
									color={ Colors.primary }
									size={ 14 }
								/>
							</TouchableBit>
							<TouchableBit unflex centering onPress={ this.onSelectStylesheet } style={ Styles.icon }>
								<IconBit
									name="copy"
									color={ Colors.primary }
									size={ 14 }
								/>
							</TouchableBit>
							<TouchableBit unflex centering onPress={ this.onQuickView } style={ Styles.icon }>
								<IconBit
									name="quick-view"
									color={ Colors.primary }
									size={ 16 }
								/>
							</TouchableBit>
							<BadgeStatusLego status={ this.props.status } />
						</BoxBit>
					</BoxBit>
					<BoxBit unflex row style={ Styles.images }>
						{ !this.props.variants || this.props.variants.length === 0 ? (
							<BoxBit centering>
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>No items yet, <LinkBit onPress={ this.onSelectStylesheet } underline>click here</LinkBit> to add from your previous stylesheets.</GeomanistBit>
							</BoxBit>
						) : this.props.variants.map(this.inventoryRenderer) }
					</BoxBit>
					{ !!this.props.showLink &&
						<BoxBit>
							{this.linkRenderer()}
						</BoxBit>
					}
					{ !!this.props.publishedAt &&
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>Published At {TimeHelper.moment(this.props.publishedAt).format('DD MMM YYYY [at] HH:mm:ss')} </GeomanistBit>
						</BoxBit>
					}
				</TouchableBit>
			)

		}
	}
)
