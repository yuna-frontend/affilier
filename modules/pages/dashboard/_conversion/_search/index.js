import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
// import InputDateBit from 'modules/bits/input.date'
import TextInputBit from 'modules/bits/text.input'
import DatePickerBit from 'modules/bits/date.picker'

import BoxRowLego from 'modules/legos/box.row'

import Styles from './style'

export default ConnectHelper(
	class SearchPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				product: PropTypes.string,
				startDate: PropTypes.number,
				endDate: PropTypes.number,
				onSearch: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				startDate: (p.startDate && new Date(p.startDate)) || null,
				endDate: (p.endDate && new Date(p.endDate)) || null,

				productUpdater: 0,
			})

			this.product = null
		}

		onChangeProduct = (e, val) => {
			if(!this.state.productUpdater) {
				this.setState({
					productUpdater: 1,
				})
			}

			this.product = val
		}

		onChangeStartDate = val => {
			this.setState({
				startDate : val,
			})
		}

		onChangeEndDate = val => {
			this.setState({
				endDate: val,
			})
		}

		onSearch = () => {
			this.props.onSearch &&
			this.props.onSearch(this.product, this.state.startDate, this.state.endDate)
		}

		view() {
			return (
				<BoxRowLego
					title="Conversion"
					data={[{
						data: [{
							title: 'Product Name',
							children: (
								<TextInputBit
									type={ TextInputBit.TYPES.TEXT }
									placeholder="Search based on product name"
									defaultValue={ this.props.product }
									onChange={ this.onChangeProduct }
								/>
							),
							style: Styles.col,
						}, {
							title: ' ',
							children: (
								<ButtonBit
									weight="medium"
									title="Search"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.isLoading ? ButtonBit.TYPES.STATES.LOADING : ((!this.state.productUpdater && !this.state.startDate && !this.state.endDate) && ButtonBit.TYPES.STATES.DISABLED) || ButtonBit.TYPES.STATES.NORMAL }
									onPress={this.onSearch}
								/>
							),
							// headerStyle: Styles.colHeaderHide,
							style: `${Styles.col} ${Styles.buttonDesktop}`,
						}],
						style: Styles.row,
					}, {
						data: [{
							title: 'Periode Pencarian',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Dari
									</GeomanistBit>
									<DatePickerBit showTimeSelect
										selected={ this.state.startDate }
										onChange={ this.onChangeStartDate }
									/>
								</BoxBit>
							),
							style: `${Styles.col} ${Styles.col_mb16}`,
						}, {
							title: ' ',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Sampai
									</GeomanistBit>
									<DatePickerBit showTimeSelect
										selected={ this.state.endDate }
										minDate={ this.state.startDate }
										onChange={ this.onChangeEndDate }
									/>
								</BoxBit>
							),
							headerStyle: Styles.colHeaderHide,
							style: Styles.col,
						}],
						style: Styles.row,
					}, {
						data: [{
							children: (
								<ButtonBit
									weight="medium"
									title="Search"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.isLoading ? ButtonBit.TYPES.STATES.LOADING : ((!this.state.product && !this.state.startDate && !this.state.endDate) && ButtonBit.TYPES.STATES.DISABLED) || ButtonBit.TYPES.STATES.NORMAL }
									onPress={this.onSearch}
								/>
							),
							style: Styles.buttonMobile,
						}],
					}]}
				/>
			)
		}
	}
)
