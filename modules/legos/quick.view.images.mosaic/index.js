import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import Styles from './style';

import { chunk } from 'lodash';

// const colors = [
// 	Colors.purple.palette(1),
// 	Colors.pink.palette(2),
// 	Colors.blue.palette(2),
// 	Colors.green.palette(1),
// 	Colors.yellow.palette(1),
// 	Colors.yellow.palette(2),
// 	Colors.red.palette(2),
// 	Colors.red.palette(4),
// ]


export default ConnectHelper(
	class QuickViewImagesMosaicLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				images: PropTypes.arrayOf(PropTypes.image),
				unflex: PropTypes.bool,
				row: PropTypes.bool,
				split: PropTypes.number,
				max: PropTypes.number,
				empty: PropTypes.string,
				small: PropTypes.bool,
				childRenderer: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			max: 8,
			images: [],
			empty: 'No images found',
		}

		// colorIndex = Math.floor(Math.random() * 8)

		// color() {
		// 	if(this.colorIndex > 7) {
		// 		this.colorIndex = 0
		// 	} else {
		// 		this.colorIndex = this.colorIndex + 1
		// 	}

		// 	return { backgroundColor: colors[this.colorIndex] }
		// }

		_key = 0

		key() {
			return this._key++
		}

		shouldComponentUpdate(nP) {
			return nP.images.length !== this.props.images.length
		}

		boxRenderer = image => {
			const key = this.key()

			return (
				<BoxBit key={ key } style={ Styles.box }>
					<ImageBit overlay unflex={ !image } resizeMode={ ImageBit.TYPES.COVER } broken={ !image } source={ image } style={ Styles.image } />
					{ key + 1 === this.props.max && this.props.images.length > this.props.max ? (
						<BoxBit unflex style={ Styles.overlay } centering>
							<GeomanistBit type={ GeomanistBit.TYPES.HEADER_4 } style={ Styles.count }>
								+{ this.props.images.length + 1 - this.props.max }
							</GeomanistBit>
						</BoxBit>
					) : false }
					{ this.props.childRenderer ? (
						<BoxBit unflex style={ Styles.image }>
							{ this.props.childRenderer(image) }
						</BoxBit>
					)  : false }
				</BoxBit>
			)
		}

		equalRenderer = (images, row) => {
			if (images.length > 1) {
				return (
					<BoxBit key={ this._key } row={ !row }>
						{ this.equalRenderer(images.slice(0, Math.ceil(images.length / 2)), !row) }
						{ this.equalRenderer(images.slice(Math.ceil(images.length / 2)), !row) }
					</BoxBit>
				)
			} else if(images.length === 1) {
				return (
					<BoxBit key={ this._key }>
						{ this.boxRenderer(images[0]) }
					</BoxBit>
				)
			} else {
				return false
			}
		}

		recursiveRenderer = (images, row) => {
			if (images.length > 1) {
				return (
					<BoxBit key={ `${ images.length }|${ this._key }` } row={ !row }>
						{ this.boxRenderer(images[0]) }
						{ this.recursiveRenderer(images.slice(1), !row) }
					</BoxBit>
				)
			} else if(images.length === 1) {
				return (
					<BoxBit key={ `${ images.length }|${ this._key }` }>
						{ this.boxRenderer(images[0]) }
					</BoxBit>
				)
			} else {
				return false
			}
		}

		view() {
			// Reset key on rerender
			this._key = 0

			return (
				<BoxBit unflex={ this.props.unflex } style={[ Styles.container, this.props.style ]}>
					{ this.props.images.length ? (
						<BoxBit row={ this.props.row } style={ Styles.wrapper }>
							{/* eslint-disable-next-line no-nested-ternary */}
							{ this.props.split
								? chunk(this.props.images.slice(0, this.props.max), Math.ceil(Math.min(this.props.images.length, this.props.max) / this.props.split)).map(images => this.recursiveRenderer(images, this.props.row))
								: this.props.images.length <= 5
									? this.recursiveRenderer(this.props.images.slice(0, this.props.max), this.props.row)
									: this.equalRenderer(this.props.images.slice(0, this.props.max), this.props.row)
							}
							<BoxBit unflex style={ Styles.shadowLeft } />
							<BoxBit unflex style={ Styles.shadowRight } />
						</BoxBit>
					) : (
						<BoxBit centering>
							<GeomanistBit type={ this.props.small ? GeomanistBit.TYPES.NOTE_3 : GeomanistBit.TYPES.PARAGRAPH_3 } align="center" style={ Styles.note }>
								{ this.props.empty }
							</GeomanistBit>
							<BoxBit unflex style={ Styles.shadowLeft } />
							<BoxBit unflex style={ Styles.shadowRight } />
						</BoxBit>
					) }
				</BoxBit>
			)
		}
	}
)
