import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import CommonHelper from 'coeur/helpers/common'

import LinkService from 'app/services/link'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import LoaderBit from 'modules/bits/loader'
import TextInputBit from 'modules/bits/text.input'

import HeaderFilterComponent from 'modules/components/header.filter'
import HeaderPageComponent from 'modules/components/header.page'
import PageAuthorizedComponent from 'modules/components/page.authorized'

// import TabLego from 'modules/legos/tab'
import TableLego from 'modules/legos/table'

// import ClicksPart from './_clicks'
// import ConversionPart from './_conversion'

import Styles from './style'

export default ConnectHelper(
	class ReportPage extends PageModel {

		static routeName = 'clicks'
		
		constructor(p) {
			const query = CommonHelper.getQueryString()

			const page = query.page
				? (query.page > 0 && query.page) || 1
				: 1

			const limit = query.limit
				? (query.limit <= 100 && query.limit) || 100
				: 40

			super(p, {
				isLoading: true,
				data: [],

				page,
				offset: page * limit - limit,
				limit,
				total: 0,

				searchTrigger: 0,
			})

			this.mounted = null

			this.tag = query.tag || null
			this.token = null
			this.userId = null
		}

		roles = ['affiliatepartner']

		// tabs = [{
		// 	title: 'Conversions Report',
		// 	icon: 'track',
		// 	onPress: this.onChangeTab.bind(this, 0),
		// }, {
		// 	title: 'Clicks Report',
		// 	icon: 'quick-view',
		// 	onPress: this.onChangeTab.bind(this, 1),
		// }]

		header = [{
			title: 'No.',
			width: .5,
		}, {
			title: 'URL',
			width: 2.5,
		}, {
			title: 'Jumlah Klik',
			width: 1,
		}]

		// onChangeTab(tabIndex) {
		// 	window.history.pushState({}, 'Report', `${this.props.location.pathname}?tab=${tabIndex + 1}`)
		// 	this.setState({
		// 		tabIndex,
		// 	})
		// }

		componentDidMount() {
			this.mounted = true
			this.getData()
		}

		componentDidUpdate(prevProps, prevState) {
			if(prevState.page !== this.state.page || prevState.searchTrigger !== this.state.searchTrigger) {
				const query = {
					page: this.state.page,
					limit: this.state.limit,
					...(!!this.tag && { tag: this.tag }),
				}

				const queryKeys = Object.keys(query)

				window.history.pushState({}, 'Clicks', !queryKeys.length ? this.props.location.pathname : `${this.props.location.pathname}?${queryKeys.map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		componentWillUnmount() {
			this.mounted = false
		}

		getData = () => {
			if(this.mounted) {
				this.setState({ isLoading: true }, () => {
					LinkService.getClicks(this.userId, {
						offset: this.state.offset,
						limit: this.state.limit,
					}, this.token).then(res => {
						if(res && res.data && typeof res.count === 'number') {
							this.setState({
								isLoading: false,
								data: res.data,
								total: res.count,
							})
						} else {
							this.onError()
						}
					}).catch(this.onError)
				})
			}
		}

		onError = err => {
			this.warn(err)

			this.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})
		}

		onAuthorized = (token, me) => {
			this.token = token
			this.userId = me.id
		}

		// contentRenderer = index => {
		// 	switch(index) {
		// 	case 0:
		// 	default:
		// 		return (
		// 			<ConversionPart/>
		// 		)
		// 	case 1:
		// 		return (
		// 			<ClicksPart onError={ this.onError }/>
		// 		)
		// 	}
		// }

		onChangeTags = (e, val) => {
			this.tag = val
		}

		onSearch = () => {
			this.setState(state => ({
				searchTrigger: state.searchTrigger + 1,
			}))
		}

		rowRenderer = (data, i) => {
			return {
				id: i + 1,
				data: [{
					title: i + 1,
				}, {
					title: data.affiliate_link,
				}, {
					title: data.click_count,
				}],
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					paths={[{ title: 'Clicks Report' }]}
					onAuthorized={ this.onAuthorized }
				>

					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Clicks Report"
							/>
							{/* <TabLego activeIndex={this.state.tabIndex} tabs={this.tabs} style={Styles.tab}/>
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple/>
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) } */}

							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.limit }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
								wrapperFilterStyle={ Styles.wrapperFilter }
								filterStyle={ Styles.filter }
								paginationStyle={ Styles.pagination }
								style={ Styles.headerFilter }
							>
								<BoxBit row style={ Styles.search }>
									{ this.state.isLoading ? (
										<BoxBit centering>
											<LoaderBit simple/>
										</BoxBit>
									) : (
										<TextInputBit unflex={ false }
											type={ TextInputBit.TYPES.TEXT }
											placeholder="Pencarian berdasarkan tag"
											defaultValue={ this.tag }
											onChange={ this.onChangeTags }
											onSubmitEditing={ this.onSearch }
										/>
									) }
									<ButtonBit type={ ButtonBit.TYPES.SHAPES.PROGRESS } state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL } onPress={ this.onSearch } title="Search" style={ Styles.ml8 }/>
								</BoxBit>
							</HeaderFilterComponent>

							<BoxBit unflex style={ Styles.outerTableWrapper }>
								<BoxBit style={ Styles.innerTableWrapper }>
									<TableLego
										isLoading={ this.state.isLoading }
										headers={ this.header }
										rows={ !!this.state.data.length && this.state.data.map(this.rowRenderer) }
									/>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}

	}
)
