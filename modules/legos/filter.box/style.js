import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	contentHeader: {
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	collapsible: {
		borderTopWidth: 0,
		borderColor: Colors.black.palette(2, .1),
	},

	header: {
		paddingTop: Sizes.margin.default,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: Sizes.margin.default,
	},

})
