import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import PurchaseService from 'app/services/purchase';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import TextBit from 'modules/bits/text';

import RowsLego from 'modules/legos/rows';
import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';
import TableLego from 'modules/legos/table';
import RowImagePurchaseOrderLego from 'modules/legos/row.image.po'

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';
import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';


export default ConnectHelper(
	class QuickViewPurchaseOrderPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onClose: PropTypes.func,
				onNavigateToPurchaseOrder: PropTypes.func,
			}
		}

		static propsToPromise(state, p) {
			return PurchaseService.getOrder(p.id, {}, state.me.token)
		}

		headers = [{
			title: 'ID',
			width: .8,
		}, {
			title: 'Product',
			width: 3.5,
		}, {
			title: 'Price',
			width: 1.5,
		}, {
			title: 'Qty',
			width: .5,
		}]

		rowRenderer = request => {
			return {
				data: [{
					title: `#${ request.id }`,
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={ Styles.title }>
								<TextBit style={ Styles.seller }>[{ request.seller }]</TextBit> { request.title }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={ Styles.note }>
								{ request.description }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: `IDR ${FormatHelper.currency(request.retail_price)}`,
					children: request.price !== request.retail_price ? (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={ Styles.retail }>
								IDR { FormatHelper.currency(request.retail_price) }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={ Styles.discounted }>
								IDR { FormatHelper.currency(request.price) }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					title: request.quantity,
					// children: (
					// 	<BadgeStatusLego status={ request.status } />
					// ),
				}],
			}
		}

		viewOnError() {
			return (
				<ModalClosableConfirmationPagelet
					title={ 'Purchase Order Preview' }
					confirm="Done"
					onClose={ this.props.onClose }
					onConfirm={ this.props.onClose }
					contentContainerStyle={Styles.container}
				>
					<NotFoundPagelet description="Something went wrong when loading the data." />
				</ModalClosableConfirmationPagelet>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ this.props.data.title || 'Purchase Order Preview' }
					header={( <BadgeStatusLego status={this.props.data.status} /> )}
					confirm={ this.props.onNavigateToPurchaseOrder ? 'View Purchase Order' : 'Done' }
					onClose={ this.props.onClose }
					onConfirm={ this.props.onNavigateToPurchaseOrder || this.props.onClose }
					contentContainerStyle={ Styles.container }
				>
					<BoxBit unflex type={BoxBit.TYPES.THIN}>
						<RowsLego
							data={[{
								data: [{
									title: 'Total Retail Value',
									content: `IDR ${ FormatHelper.currency(this.props.data.retail_price) }`,
								}, {
									title: 'Total Value',
									content: `IDR ${ FormatHelper.currency(this.props.data.price) }`,
								}],
							}, {
								data: [{
									title: 'Total Quantity',
									content: this.props.data.quantity,
								}, {
									title: 'Last Update',
									content: TimeHelper.format(this.props.data.updated_at, 'DD MMMM YYYY — HH:mm'),
								}],
							}, {
								data: [{
									title: 'Note',
									content: this.props.data.note || '-',
								}],
							}, {
								children: (
									<RowImagePurchaseOrderLego title="Proof of Transaction" purchaseOrderId={ this.props.id } addable={ false } />
								),
							}]}
						/>
					</BoxBit>
					<TableLego compact
						headers={ this.headers }
						rows={ this.props.data.requests.map(this.rowRenderer) }
						style={ Styles.table }
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
