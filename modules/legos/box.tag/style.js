import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	content: {
		marginTop: 8,
		marginBottom: 0,
	},

	group: {
		paddingBottom: 16,
	},

	wrap: {
		flexWrap: 'wrap',
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 8,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		backgroundColor: Colors.solid.grey.palette(4),
	},

	title: {
		color: Colors.black.palette(2),
	},

	header: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 16,
		paddingRight: 16,
	},

	count: {
		color: Colors.white.palette(1, .8),
		backgroundColor: Colors.primary,
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 2,
		paddingBottom: 2,
		alignSelf: 'center',
		borderRadius: 12,
		marginRight: 8,
	},

	collapsible: {
		borderBottomWidth: 0,
	},

	buttons: {
		alignItems: 'center',
	},

	button: {
		marginRight: 8,
	},
})
