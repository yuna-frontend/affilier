import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	container: {
		color: Colors.black.palette(2),
		whiteSpace: 'pre-wrap',
	},
})
