import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import CategoryService from 'app/services/category';
import StylesheetService from 'app/services/style.sheets';
import UserService from 'app/services/user.new'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import HintLego from 'modules/legos/hint';
import SelectionStylesheetLego from 'modules/legos/selection.stylesheet';
import SelectionVariantLego from 'modules/legos/selection.variant';

import ModalClosableConfirmationPagelet from '../modal.closable.confirmation';
import NotFoundPagelet from '../not.found'

import Styles from './style';

import { capitalize, without } from 'lodash'


export default ConnectHelper(

	class StylesheetPickerPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				categoryId: PropTypes.id,
				title: PropTypes.string,
				brand: PropTypes.string,
				images: PropTypes.arrayOf(PropTypes.object),
				colors: PropTypes.arrayOf(PropTypes.object),
				size: PropTypes.string,
				price: PropTypes.number,
				retailPrice: PropTypes.number,
				onSelected: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return Promise.all([
				UserService.getStylistStylesheet(state.me.id, state.me.token).catch(() => []),
				CategoryService.getRootCategory(oP.categoryId, state.me.token),
			]).then(results => {
				return {
					stylesheets: results[0],
					category: capitalize(results[1].title),
				}
			}).catch(() => {
				return {
					stylesheets: [],
					category: 'Category not found',
				}
			})
		}

		static defaultProps = {
			data: {
				stylesheets: [],
			},
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isSaving: false,
				selectedIds: [],
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSelect = () => {
			// TODO
			this.setState({
				isSaving: true,
			}, () => {
				Promise.all(this.state.selectedIds.map(sId => {
					return StylesheetService.onPutInventoryToStylesheet(sId, {
						type: 'VARIANT',
						id: this.props.id,
					}, this.props.token).then(() => {
						return {
							status: true,
							id: sId,
						}
					}).catch(err => {
						return {
							status: false,
							id: sId,
							err,
						}
					})
				})).then(results => {
					if(results.findIndex(r => r.status === false) > -1) {
						if(results.filter(r => r.status === true).length === 0) {
							this.props.utilities.notification.show({
								title: 'Adding Failed',
								message: 'Variant is not added into stylesheet(s)',
							})
						} else {
							this.props.utilities.notification.show({
								title: 'Partialy Failed',
								message: `Some of the stylesheets are not updated.${
									results.filter(r => r.status === false).map(r => {
										return `\nStylesheet #${ r.id }: ${ r.err ? r.err.detail || r.err.message : 'Something went wrong.' }`
									}).join('')
								}`,
							})
						}
					} else {
						this.props.utilities.notification.show({
							title: 'Adding Success',
							message: 'Variant is added into stylesheet(s)',
							type: 'SUCCESS',
						})
					}

					this.onClose()

					this.props.onSelected &&
					this.props.onSelected()
				})
			})
		}

		onToggle = id => {
			if(this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					// selectedIds: [...this.state.selectedIds, id],
					selectedIds: [id],
				})
			}
		}

		variantRenderer = () => {
			return (
				<SelectionVariantLego
					id={ this.props.id }
					image={ this.props.images[0] }
					brand={ this.props.brand }
					category={ this.props.data.category }
					title={ this.props.title }
					size={ this.props.size.title }
					color={ this.props.colors.map(c => capitalize(c.title)).join('/') }
					price={ this.props.price }
					retailPrice={ this.props.retailPrice }
				/>
			)
		}

		stylesheetRenderer = ({
			inventories,
			...stylesheet
		}) => {
			const isSelected = this.state.selectedIds.indexOf(stylesheet.id) > -1
			return (
				<SelectionStylesheetLego
					key={ stylesheet.id }
					{ ...stylesheet }
					items={ inventories }
					minValue={ stylesheet.min_value }
					realValue={ stylesheet.real_value }
					shipmentAt={ stylesheet.shipment_at }
					updatedAt={ stylesheet.updated_at }
					selected={ isSelected }
					accessible={ inventories.length < stylesheet.count }
					addedRetail={ isSelected ? this.props.retailPrice : 0 }
					addedPurchase={ isSelected ? this.props.price : 0 }
					addedCount={ isSelected ? 1 : 0 }
					onPress={ this.onToggle }
					style={ Styles.item }
				/>
			)
		}

		viewOnLoading() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add to Style Sheet"
					header={ (
						<HintLego title="Select style sheet you want to add this variant into." />
					) }
					subheader={ this.variantRenderer() }
					confirm="Add"
					disabled
					onCancel={ this.onClose }
					onConfirm={ this.onSelect }
					contentContainerStyle={ Styles.content }
					style={ Styles.container }
				>
					<NotFoundPagelet loading />
				</ModalClosableConfirmationPagelet>
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add to Style Sheet"
					header={ (
						<HintLego title="If inventory already booked, please try to reload the page." />
					) }
					subheader={ this.variantRenderer() }
					confirm="Add"
					disabled={ this.state.selectedIds.length === 0 }
					loading={ this.state.isSaving }
					onCancel={ this.onClose }
					onConfirm={ this.onSelect }
					contentContainerStyle={ Styles.content }
					style={ Styles.container }
				>
					{ this.props.data.stylesheets && this.props.data.stylesheets.length ? this.props.data.stylesheets.map(this.stylesheetRenderer) : (
						<BoxBit type={ BoxBit.TYPES.ALL_THICK } centering>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>You have no style sheet assigned!</GeomanistBit>
						</BoxBit>
					) }
				</ModalClosableConfirmationPagelet>
			)
		}
	}
);
