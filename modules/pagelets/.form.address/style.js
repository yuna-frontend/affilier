import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: '45.7%',
		borderRadius: 4,
	},

	content: {
		margin: 0,
	},
	padder: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.thick,
		maxHeight: 572,
		width: '100%',
		height: '100%',
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	empty: {
		paddingBottom: 16,
	},
	emptyHeader: {
		paddingBottom: 4,
	},

	padder16: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		marginTop: -16,
	},
	textAreaLayout: {
		width: '100%',
		alignSelf: 'stretch',
		marginTop: 16,
	},
	address: {
		paddingRight: 8,
	},

	primary: {
		color: Colors.primary,
	},

	addAddress: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	select: {
		padding: Sizes.margin.default,
		alignItems: 'center',
		justifyContent: 'space-between',
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
		backgroundColor: Colors.solid.grey.palette(4),
	},

	backgroundWhite: {
		backgroundColor: Colors.white.primary,
	},

	footer: {
		justifyContent: 'space-between',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	headerCollapsible: {
		padding: 0,
	},
	collapsible: {
		borderTopWidth: 0,
		// backgroundColor: 'lightblue',
		borderColor: Colors.black.palette(2, .16),
	},

	validated: {
		paddingLeft: 0,
		paddingRight: 0,
	},

	textarea: {
		height: 20,
	},
	capital: {
		textTransform: 'capitalize',
	},

	border: {
		borderTopWidth: StyleSheet.hairlineWidth,
		color: Colors.black.palette(2, .16),
	},

	form: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		paddingBottom: 16,
		paddingLeft: 16,
		paddingRight: 16,
	},
	column: {
		flex: 1,
		width: '50%',
		marginTop: 16,
	},
	columnTop: {
		flex: 1,
		width: '50%',
		marginTop: 0,
	},
	padderLeft: {
		marginLeft: 8,
	},
	padderRight: {
		marginRight: 8,
	},

	phoneNumber: {
		paddingLeft: 0,
	},
})
