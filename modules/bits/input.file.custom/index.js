import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import ClassName from 'coeur/libs/class.name';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import ButtonBit from '../button'
// import CalendarBit from '../calendar'
import GeomanistBit from '../geomanist'
import IconBit from '../icon';
// import InputValidatedBit from '../input.validated'
import TouchableBit from '../touchable';

import Styles from './style'


export default ConnectHelper(
	class InputUploaderBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				unflex: PropTypes.bool,
				accept: PropTypes.string,
				maxSize: PropTypes.number,
				disabled: PropTypes.bool,
				loading: PropTypes.bool,
				value: PropTypes.string,
				onChange: PropTypes.func,
				onDataLoaded: PropTypes.func,
				onDataError: PropTypes.func,
				postfix: PropTypes.node,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			accept: '.csv',
			unflex: true,
			title: 'Select File',
			maxSize: 10,
		}

		static getDerivedStateFromProps(nP, nS) {
			return super.getDerivedStateFromProps(nP, nS, () => {
				if (nP.value !== nS.lastValue) {
					return {
						lastValue: nP.value,
						file: nP.value,
					}
				}

				return null
			})
		}

		constructor(p) {
			super(p, {
				lastValue: p.value,
				file: p.value,
				title: p.title,
				fileAsString: undefined,
			})

			this._unmounted = false
		}

		componentDidMount() {
			if (this.state.file) {
				this.loadData()
			}
		}

		componentDidUpdate(pP, pS) {
			if (pS.file !== this.state.file) {
				this.props.onChange &&
				this.props.onChange(this.props.id, this.state.file)

				this.loadData()
			}

			if (this.props.onDataLoaded && pS.fileAsString !== this.state.fileAsString) {
				this.props.onDataLoaded(this.props.id, this.state.fileAsString)
			}
		}

		componentWillUnmount() {
			super.componentWillUnmount()
			this._unmounted = true
		}

		onChange = e => {
			const file = e.target.files[0]

			if (((file.size / 1024) / 1024).toFixed(4) > this.props.maxSize) {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: `Maximum file size permitted is ${this.props.maxSize} MB. Please upload smaller image.`,
				})
			} else {
				this.setState({
					file,
					title: file.name || this.props.title,
				})
			}
		}

		loadData = () => {
			if (!this._unmounted) {
				if (this.state.file instanceof File) {
					const reader = new FileReader();
					reader.onload = this.onDataLoaded;
					reader.onerror = this.onDataError;
					reader.readAsText(this.state.file, 'UTF8');
				} else if (this.state.file === undefined) {
					this.setState({
						title: this.props.title,
						fileAsString: undefined,
					})
				} else if (this.state.file instanceof Promise) {
					this.state.file.then(({
						title,
						fileAsString,
					}) => {
						if (!this._unmounted) {
							this.setState({
								title,
								fileAsString,
							})
						}
					}).catch(this.onDataError)
				} else {
					this.warn('File unknown')
				}
			}
		}

		onDataLoaded = data => {
			if (!this._unmounted) {
				this.setState({
					fileAsString: data.target.result,
				})
			}
		}

		onDataError = err => {
			if (this.props.onDataError) {
				this.props.onDataError(this.props.id, err)
			} else {
				this.onRemoveFile()

				this.props.utilities.notification.show({
					title: 'Oops',
					message: 'There is an unknown error when trying to load your file. Please try again',
				})
			}
		}

		onRemoveFile = () => {
			if (!this._unmounted) {
				this.setState({
					file: undefined,
				})
			}
		}

		postfixRenderer() {
			return this.props.postfix ? this.props.postfix : (
				<IconBit
					name={ this.state.file ? 'close-fat' : 'expand' }
					size={ 20 }
					color={ this.state.file ? Colors.red.primary : Colors.black.palette(2, .9) }
				/>
			)
		}

		view() {
			return (
				<BoxBit row unflex={ this.props.unflex } style={ this.props.style }>
					<BoxBit accessible={ false }>
						<ButtonBit
							title={ ' ' }
							align={ ButtonBit.TYPES.ALIGNS.LEFT }
							weight="normal"
							align="between"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							theme={ ButtonBit.TYPES.THEMES.SECONDARY }
							state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : this.props.loading && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
						/>

					</BoxBit>
					{ !this.props.disabled && !this.props.loading ? (
						<label {...ClassName.parse([
							Styles.label,
						])}>
							<input type="file"
								accept={ this.props.accept }
								onChange={ this.onChange }
								{...ClassName.parse([
									Styles.input,
								])}
							/>
						</label>
					) : false }
					{ !this.props.loading ? (
						<React.Fragment>
							<BoxBit accessible={ false } unflex style={[Styles.floater, Styles.title, this.props.disabled ? Styles.disabled : undefined]}>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.text }>
									{ this.state.title }
								</GeomanistBit>
							</BoxBit>
							<TouchableBit accessible={ !!this.state.file } activeOpacity={ this.props.disabled ? 1 : undefined } centering unflex style={[Styles.floater, this.props.disabled ? Styles.disabled : undefined ]} onPress={ this.onRemoveFile }>
								{ this.postfixRenderer() }
							</TouchableBit>
						</React.Fragment>
					) : false }
				</BoxBit>
			)
		}
	}
)
