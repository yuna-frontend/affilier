import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'

import { capitalize } from 'lodash'


export default ConnectHelper(
	class ShipmentListLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				isReturn: PropTypes.bool,
				courier: PropTypes.string,
				service: PropTypes.string,
				awb: PropTypes.string,
				status: PropTypes.string,
				editable: PropTypes.bool,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			selectable: true,
			shipments: [],
		}

		view() {
			return (
				<TouchableBit unflex row onPress={ this.props.onPress } style={[ Styles.shipment, this.props.style ]}>
					<BoxBit unflex row>
						<IconBit name="shipment" size={ 20 } color={ this.props.isReturn ? Colors.green.palette(1) : undefined } style={ Styles.ship } />
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ this.props.isReturn ? Styles.return : Styles.id }>{ this.props.isReturn ? 'Return' : `#SH-${ this.props.id }` }</GeomanistBit>
					</BoxBit>
					<BoxBit unflex style={ Styles.divider } />
					<BoxBit style={ Styles.description }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ this.props.courier } - { this.props.service } – AWB: { this.props.awb || '-' }</GeomanistBit>
					</BoxBit>
					<BoxBit unflex row style={ Styles.status }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.stats }>{ this.props.status.split('_').map(s => capitalize(s)).join(' ') }</GeomanistBit>
					</BoxBit>
					<BoxBit />
					{ this.props.onPress ? (
						<BoxBit unflex style={ Styles.icon }>
							<IconBit
								name={ this.props.editable && !this.props.isReturn ? 'edit' : 'quick-view' }
								size={ 24 }
								color={ Colors.primary }
							/>
						</BoxBit>
					) : false }
				</TouchableBit>
			)
		}
	}
)
