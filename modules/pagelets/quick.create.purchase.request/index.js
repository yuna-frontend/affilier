import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'utils/constants/color'

import UtilitiesContext from 'coeur/contexts/utilities';

import PurchaseService from 'app/services/purchase';
import VariantService from 'app/services/variant'


import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ScrollViewBit from 'modules/bits/scroll.view';

import LoaderLego from 'modules/legos/loader';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';
import PurchaseRequestUpdatePagelet from 'modules/pagelets/purchase.request.update';

import Styles from './style';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class QuickCreatePurchaseRequestPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				override: PropTypes.bool,
				id: PropTypes.id,
				approved: PropTypes.bool,
				onCreated: PropTypes.func,
				style: PropTypes.style,
				title: PropTypes.string,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.override) {
				return VariantService.getVariantById(oP.id, {}, state.me.token).then(res => {
					return res
				})
			} else {
				return {}
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.override && !nS.isLoaded && !isEmpty(nP.data)) {
				return {
					...nS,
					isLoaded: true,
					purchase: {
						...nS.purchase,
						title: nP.title,
						brand_id: nP.data.brand_id,
						url: nP.data.url,
						category_id: nP.data.category_id,
						price: nP.data.price,
						retail_price: nP.data.retail_price,
						color_id: nP.data.color_id,
						size_id: nP.data.size_id,
					},
				}
			} else {
				return nS
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoaded: false,
				isCreating: false,
				purchase: {},
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onUpdatePurchaseData = (key, value) => {
			this.state.purchase[key] = value

			this.setState({
				purchase: { ...this.state.purchase },
			})
		}

		onSubmit = () => {
			if (
				this.state.purchase.url &&
				this.state.purchase.seller_id &&
				this.state.purchase.brand_id &&
				this.state.purchase.title &&
				this.state.purchase.category_id &&
				this.state.purchase.color_id &&
				this.state.purchase.size_id &&
				this.state.purchase.price &&
				this.state.purchase.retail_price
			) {
				this.setState({
					isCreating: true,
				})

				PurchaseService.createRequest({
					...this.state.purchase,
					...(this.props.approved ? {
						status: 'APPROVED',
					} : {}),
				}, this.props.token).then(data => {
					this.props.utilities.notification.show({
						title: 'Success',
						message: 'Purchase request created',
						type: 'SUCCESS',
					})

					this.props.onCreated &&
					this.props.onCreated(data)

					this.onClose()
				}).catch(this.onError).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: err ? err.detail || err.message || err : 'Something went wrong. Please try again.',
					})

					this.setState({
						isCreating: false,
					})
				})
			} else {
				this.props.utilities.notification.show({
					title: 'Data invalid or incomplete',
					message: 'Please fill in all required fields.',
				})
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={ Styles.empty }>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.error }>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalQuickViewPagelet
					title={ 'Create Purchase Request' }
					layout="2-way"
					onClose={ this.onClose }
				>
					<ScrollViewBit style={ Styles.content }>
						<PurchaseRequestUpdatePagelet
							override={ this.props.override }
							purchaseData={ this.state.purchase }
							bodyless editable onUpdate={ this.onUpdatePurchaseData } />
					</ScrollViewBit>
					<BoxBit unflex style={ Styles.button }>
						<ButtonBit
							title={ 'Create Purchase Request' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.state.isCreating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onSubmit }
						/>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
