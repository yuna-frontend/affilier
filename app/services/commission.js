import ServiceModel from 'coeur/models/service';


class CommissionService extends ServiceModel {

	static displayName = 'commission'

	constructor() {
		super(process.env.CONNECTER_URL + 'affiliate/', 0)
	}

	getProducts(query, token) {
		return this.get('product', {
			query,
			token,
		})
	}

	getBalance(userId, token) {
		return this.get('detail/' + userId, {
			token,
		})
	}

}

export default new CommissionService()
