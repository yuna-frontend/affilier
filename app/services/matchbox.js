import ServiceModel from 'coeur/models/service';


class MatchboxService extends ServiceModel {

	static displayName = 'matchbox'

	constructor() {
		super(process.env.API_URL + process.env.CONNECTER_VERSION + 'matchbox/', 60 * 1000)
	}

	getMatchboxes(ids = []) {
		if(ids.length) {
			return this.get('', {
				query: {
					ids: ids.join(','),
				},
			})
		}

		return this.get('')
	}

	getMatchbox(id) {
		return this.get(id)
	}
}

export default new MatchboxService()
