import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import UserService from 'app/services/user.new';
import BrandService from 'app/services/brand';
import OrderService from 'app/services/new.order';

import SelectionAreaLego from 'modules/legos/selection.area';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class AddressAreaPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				addressType: PropTypes.oneOf([
					'user',
					'order',
					'brand',
				]),
				addressTypeId: PropTypes.id,
				id: PropTypes.id,
				type: PropTypes.oneOf([
					'TIKI',
					'MANUAL',
					'NINJA',
				]),
				keyword: PropTypes.string,
				areas: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					district: PropTypes.string,
					city: PropTypes.string,
					province: PropTypes.string,
					zip_code: PropTypes.string,
				})),
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			selectedId: -1,
			areas: [],
		}

		constructor(p) {
			super(p, {
				selectedId: p.selectedId,
				isSaving: false,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onConfirm = () => {
			this.setState({
				isSaving: true,
			}, () => {
				switch (this.props.addressType) {
				case 'brand':
					return BrandService.updateAddress(this.props.addressTypeId, this.props.id, { metadata: { [this.props.keyword]: this.state.selectedId } }, this.props.token).then(this.onUpdated).catch(this.onError)
				case 'user':
				default:
					return UserService.updateAddress(this.props.addressTypeId, this.props.id, { metadata: { [this.props.keyword]: this.state.selectedId } }, this.props.token).then(this.onUpdated).catch(this.onError)
				case 'order':
					return OrderService.updateAddress(this.props.addressTypeId, this.props.id, { metadata: { [this.props.keyword]: this.state.selectedId } }, this.props.token).then(this.onUpdated).catch(this.onError)
				}
			})
		}

		onUpdated = () => {
			this.props.utilities.notification.show({
				title: 'Yeay!',
				message: 'Address saved successfully.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.onModalRequestClose()

			this.props.onUpdate &&
			this.props.onUpdate(this.state.selectedId)
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isSaving: false,
			})
		}

		onChangeSelected = id => {
			this.setState({
				selectedId: id,
			})
		}

		rowRenderer = (area, i) => {
			return (
				<SelectionAreaLego
					key={ area.id }
					odd={ !(i & 1) }
					isSelected={ area.id === this.state.selectedId }
					{ ...area }
					onPress={ this.onChangeSelected }
				/>
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ 'Select Delivery Area' }
					confirm={ 'Select' }
					loading={ this.state.isSaving }
					disabled={ this.state.selectedId === -1 }
					onConfirm={ this.onConfirm }
					contentContainerStyle={Styles.content}
				>
					{ this.props.areas.map(this.rowRenderer) }
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
