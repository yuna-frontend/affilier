import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	DividerPart as CoreDividerPart,
} from 'coeur/modules/pagelets/menu';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'


export default ConnectHelper(
	class DividerPart extends CoreDividerPart({
		BoxBit,
		TextBit: GeomanistBit,
	}) {
		titleRenderer(title, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={style}>{ title }</GeomanistBit>
			)
		}
	}
)
