import ServiceModel from 'coeur/models/service';

class ReportService extends ServiceModel {

	static displayName = 'report'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/report/')
	}

	getBCA(query, token) {
		return this.get('bca', {
			query,
			token,
		})
	}

	getInventory(query, token) {
		return this.get('inventory', {
			query,
			token,
		})
	}
}

export default new ReportService()
