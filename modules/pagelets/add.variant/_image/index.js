import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';

import RowImageVariant from 'modules/legos/row.image.variant'

import Styles from './style';

import HeaderPart from '../__header'


export default ConnectHelper(
	class ImagePart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
				selectedVariantId: PropTypes.number,
			}
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					<HeaderPart title="Variation Images" style={Styles.header} />
					<BoxBit style={Styles.contentContainer}>
						<RowImageVariant
							title={null}
							variantId={this.props.selectedVariantId}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
