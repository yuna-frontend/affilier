import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import BatchManager from 'app/managers/batch';

import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';
import Defaults from 'coeur/constants/default';

import BoxBit from 'modules/bits/box';
import BoxLoadingBit from 'modules/bits/box.loading';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import ColumnPart from './_column';

import Styles from './style';

export {
	ColumnPart,
};


export default ConnectHelper(
	class CardOrderComponent extends CardComponentModel(BatchManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				unPressable: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		getStatusColor() {
			switch(this.props.data.status.code) {
			case 'UNPAID':
			case 'CANCELLED':
			case 'RETURNED':
				return Colors.red.primary;
			case 'PAID':
			case 'ON_COURIER':
			case 'PROCESSING':
				return Colors.green.primary;
			case 'DELIVERED':
				return Colors.purple.palette(2);
			case 'EXPIRED':
			default:
				return Colors.black.palette(2, .5);
			}
		}

		getStatusTitle() {
			return Defaults.ORDER_STATUSES[this.props.data.status.code] || this.props.data.status.code || ''
		}

		view() {
			return this.props.data._isGetting ? (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<BoxBit unflex row style={[Styles.row, Styles.header]}>
						<BoxLoadingBit unflex width={45} height={26} />
						<BoxLoadingBit unflex width={44} height={20} />
					</BoxBit>

					<BoxLoadingBit unflex style={Styles.bottom4} height={13} width={146} />
					<BoxLoadingBit unflex style={Styles.row} height={18} width={194} />

					<ColumnPart
						isLoading
						data={Array(2).fill()}
					/>
				</BoxBit>
			) : (
				<TouchableBit unflex style={[Styles.container, this.props.style]} onPress={this.onPress} activeOpacity={ this.props.unPressable ? 1 : undefined }>
					<BoxBit unflex row style={[Styles.row, Styles.header]}>
						<GeomanistBit type={GeomanistBit.TYPES.HEADER_5} weight="medium" style={Styles.colorBlack}>{ this.props.data.orderNumber }</GeomanistBit>
						{ !this.props.unPressable && (
							<TouchableBit unflex enlargeHitSlop onPress={this.onPress}>
								<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_3} weight="medium" style={Styles.colorGrey}>Details</GeomanistBit>
							</TouchableBit>
						) }
					</BoxBit>

					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.colorPale, Styles.bottom4]}>STATUS</GeomanistBit>
					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="medium" style={[Styles.status, Styles.row, {
						color: this.getStatusColor(),
					}]}>{ this.getStatusTitle().toUpperCase() }</GeomanistBit>
					{/* <GeomanistBit type={GeomanistBit.TYPES.HEADER_5}>{ this.props.data.productTitle }</GeomanistBit> */}

					<ColumnPart
						data={[{
							title: 'ESTIMATE SHIPMENT',
							content: this.props.data.estimateShipment,
						}, {
							title: 'ORDER DATE',
							content: TimeHelper.format(this.props.data.created),
						}]}
					/>
					{/* <PricesPart
						price={ this.props.data.price }
						discount={ this.props.data.discount }
						shipping={ this.props.data.shipping }
						total={ this.props.data.total }
					/> */}
				</TouchableBit>
			)
		}
	}
)
