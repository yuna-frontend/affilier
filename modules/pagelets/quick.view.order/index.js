import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import Colors from 'utils/constants/color'

import OrderService from 'app/services/new.order';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';

import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';
import TableProductLego from 'modules/legos/table.product';

import AddressesComponent from 'modules/components/addresses';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class QuickViewOrderPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onClose: PropTypes.func,
				onNavigateToOrder: PropTypes.func,
				onNavigateToOrderDetail: PropTypes.func,
			}
		}

		static propsToPromise(state, p) {
			return OrderService.getOrderDetail(p.id, state.me.token)
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		onNavigateToOrderDetail = (id, type, refId) => {
			this.props.onClose &&
			this.props.onClose()

			this.props.onNavigateToOrderDetail &&
			this.props.onNavigateToOrderDetail(id, type, refId)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ this.props.data.number || 'Order Preview' }
					header={( <BadgeStatusLego status={this.props.data.status} /> )}
					confirm="View Order"
					onClose={ this.props.onClose }
					onConfirm={ this.props.onNavigateToOrder }
					contentContainerStyle={ Styles.container }
				>
					<BoxBit unflex type={BoxBit.TYPES.THIN}>
						<RowsLego
							data={[{
								data: [{
									title: 'Client',
									content: this.props.data.user_name || '-',
								}, {
									title: 'Email Address',
									content: this.props.data.user_email || '-',
								}],
							}, {
								data: [{
									title: 'Order Date',
									content: TimeHelper.format(this.props.data.created_at, 'DD/MM/YYYY hh:mm A'),
								}, {
									title: 'Shipment Schedule',
									content: this.props.data.shipment_at ? TimeHelper.getRange( ...this.props.data.shipment_at ) : '-',
								}],
							}]}
						/>
						<AddressesComponent data={this.props.data.addresses } />
					</BoxBit>
					<TableProductLego
						orderDetails={ this.props.data.details }
						onNavigateToOrderDetail={ this.onNavigateToOrderDetail }
						style={ Styles.table }
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
