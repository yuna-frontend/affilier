import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import InputValidatedBit from 'modules/bits/input.validated'

import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class RowProductLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				value: PropTypes.string,
			}
		}

		static defaultProps = {
			title: 'Slug',
		}

		constructor(p) {
			super(p, {
				len: p.value ? p.value.length : 0,
			})
		}

		onChange = (val) => {
			this.setState({
				len: val.length,
			})

			this.props.onChange &&
			this.props.onChange(val)
		}

		view() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						children: (
							<BoxBit>
								<InputValidatedBit required
									placeholder="-"
									value={ this.props.value }
									onChange={ this.onChange }
								/>
							</BoxBit>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
