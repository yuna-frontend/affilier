import ServiceModel from 'coeur/models/service';


class OrderService extends ServiceModel {

	static displayName = 'order'

	constructor() {
		super(process.env.API_URL + 'admin/order/')
	}

	newOrderList(data, token) {
		return this.get('', {
			data,
			token,
		})
	}

	updateStatus({
		id,
		code,
	}, token) {
		return this.post(id + '/status', {
			data: {
				code,
			},
			token,
		})
	}

	updateShipment({
		id,
		awb,
		trackLink,
	}, token) {
		return this.post(id + '/shipment', {
			data: {
				awb,
				trackLink,
			},
			token,
		})
	}

	updateShipmentViaShipper({
		id,
	}, token) {
		return this.post(id + '/shipper', {
			token,
		})
	}

	getTrackLink({
		id,
	}, token) {
		return this.post(id + '/shipper', {
			data: {
				trackLink: true,
			},
			token,
		})
	}
}

export default new OrderService()
