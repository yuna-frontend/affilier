import React from 'react';
import Masonry from 'react-masonry-component';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UserService from 'app/services/user.new';
import QuestionService from 'app/services/question';

import UtilitiesContext from 'coeur/contexts/utilities';

import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import LinkBit from 'modules/bits/link';
import TouchableBit from 'modules/bits/touchable';

import ImagePagelet from 'modules/pagelets/image';
import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';

import { capitalize, isEmpty } from 'lodash';


const options = {
	gutter: Sizes.margin.default,
};

export default ConnectHelper(
	class GroupPart extends PromiseStatefulModel {

		static TYPES = {
			ESSENTIAL: 'ESSENTIAL',
			ALL: 'ALL',
		}

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES),
				userId: PropTypes.id,
				date: PropTypes.date,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.type === 'ESSENTIAL') {
				return QuestionService.getQuestionsById([2, 83, 15, 44, 48, 49, 24, 45, 30, 75], state.me.token).then(questions => {
					const questionIds = []

					questions.forEach(question => {
						questionIds.push(question.id)
						if (question.questions && question.questions.length) {
							question.questions.forEach(q => questionIds.push(q.id))
						}
					})

					return Promise.all([questions, UserService.getAnswerByQuestionIds(oP.userId, questionIds, oP.date, state.me.token)]).catch(() => [])
				}).then(([questions, answers]) => {
					return {
						questions,
						answers: answers && answers.filter(a => a !== null),
					}
				})
			} else {
				return QuestionService.getGroups(state.me.token).then(groups => {
					return Promise.all(groups.map(group => {
						return Promise.all([
							QuestionService.getQuestionsByGroupId(group.id, state.me.token).catch(() => []),
							UserService.getAnswersByGroupId(oP.userId, group.id, oP.date, state.me.token).catch(() => []),
						]).then(([questions, answers]) => {
							return {
								questions,
								answers,
							}
						})
					})).then(qas => {
						return qas.reduce((sum, qa) => {
							return {
								questions: sum.questions.concat(qa.questions),
								answers: sum.answers.concat(qa.answers),
							}
						}, {
							questions: [],
							answers: [],
						})
					})
				})
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!nS.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					questions: nP.data.questions || [],
					answers: nP.data.answers || [],
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoaded: false,
				questions: [],
				answers: [],
			})
		}

		isPeintureURL = file => {
			return (file + '').slice(0, 2) === '//'
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onImagePopup = source => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet source={ source } onPress={ this.onModalRequestClose } transform={{
						original: true,
					}} />
				),
			})
		}

		getAnswer = (question, answer) => {
			switch(question.type) {
			case 'CHOICE':
				return (
					<React.Fragment>
						{!!answer.CHOICE && (
							<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ typeof answer.CHOICE === 'object' ? answer.CHOICE.join(', ') : answer.CHOICE }
							</GeomanistBit>
						)}
						{ answer.OTHER && (
							<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ answer.OTHER }
							</GeomanistBit>
						) }
					</React.Fragment>
				)
			case 'TAB':
				return (
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ answer.TAB } { answer.CHOICES && answer.CHOICES.join(', ') }
					</GeomanistBit>
				)
			case 'FILE':
				return (
					<BoxBit row style={Styles.images}>
						{ answer.FILE.map(url => {
							if(!!url) {
								if(this.isPeintureURL(url)) {
									return (
										<TouchableBit unflex key={ url } onPress={ this.onImagePopup.bind(this, url) }>
											<ImageBit overlay unflex source={ url } style={Styles.media} />
										</TouchableBit>
									)
								} else {
									return (
										<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.url }>
											<LinkBit target={ LinkBit.TYPES.BLANK } underline href={ url } style={ Styles.link }>
												{ url }
											</LinkBit>
										</GeomanistBit>
									)
								}
							} else {
								return null
							}
						}) }
					</BoxBit>
				)
			case 'URL':
				return (
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						<LinkBit target={ LinkBit.TYPES.BLANK } underline href={ answer.URL } style={ Styles.link }>
							{ answer.URL }
						</LinkBit>
					</GeomanistBit>
				)
			case 'BOOLEAN':
				return (
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ answer.BOOLEAN ? 'Yes' : 'No' }
					</GeomanistBit>
				)
			case 'SCALE':
				return (
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ question.metadata.choices[answer.SCALE] ? question.metadata.choices[answer.SCALE].title : answer.SCALE }
					</GeomanistBit>
				)
			case 'NUMBER':
				return(
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ answer.NUMBER } { question.description }
					</GeomanistBit>
				)
			case 'DROPDOWN':
				return (
					<GeomanistBit selectable type={GeomanistBit.TYPES.PARAGRAPH_3}>
						{/* eslint-disable-next-line no-nested-ternary */}
						{ answer.DROPDOWN }{ answer.CHOICES ? Array.isArray(answer.CHOICES) ? ` – ${ answer.CHOICES.join('/') }` : ` – ${ answer.CHOICES }` : '' }
					</GeomanistBit>
				)
			case 'DEFINITION':
				return false
			default:
				if(question.id === 60) {
					return (
						<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							<LinkBit target={ LinkBit.TYPES.BLANK } underline href={ `https://www.instagram.com/${ answer.TEXT_SHORT.replace('@', '') }/` }>
								{ answer.TEXT_SHORT }
							</LinkBit>
						</GeomanistBit>
					)
				}

				return answer && question ? (
					<GeomanistBit selectable type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ !isEmpty(answer) ? answer[question.type] || '—' : answer[question.type] || '—' }
					</GeomanistBit>
				) : false
			}
		}

		questionRenderer = question => {
			const answer = this.state.answers.find(a => a.question_id === question.id)

			return (
				<BoxBit key={ question.id } style={ Styles.box }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" style={ Styles.header }>
						{ question.title }
					</GeomanistBit>
					{/* eslint-disable-next-line no-nested-ternary */}
					{ !isEmpty(question.questions)
						? (
							<BoxBit unflex style={Styles.questions}>
								{ question.questions.map(this.childrenRenderer) }
							</BoxBit>
						) : !isEmpty(answer)
							? this.getAnswer(question, answer.answer)
							: '—'
					}
				</BoxBit>
			)
		}

		childrenRenderer = question => {
			const answer = this.state.answers.find(a => a.question_id === question.id)

			return (
				<BoxBit row key={question.id} style={Styles.question}>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
						{ capitalize(question.title) }
					</GeomanistBit>
					{ !isEmpty(answer)
						? this.getAnswer(question, answer.answer)
						: '—'
					}
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return !this.state.questions.length ? (
				<NotFoundPagelet description="To sum it up, where is the questions?" />
			) : (
				<Masonry options={ options }>
					{ this.state.questions.map(this.questionRenderer) }
				</Masonry>
			)
		}
	}
)
