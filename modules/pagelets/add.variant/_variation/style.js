import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		padding: Sizes.margin.default,
	},

	containerList: {
		padding: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		justifyContent: 'center',
	},

	containerAddNew: {
		padding: Sizes.margin.default + 2,
		borderBottomWidth: 0,
	},

	divider: {
		height: Sizes.margin.default,
	},

	radio: {
		alignSelf: 'center',
	},

	primary: {
		color: Colors.primary,
	},

	blueGrey: {
		color: Colors.grey.primary,
	},
})
