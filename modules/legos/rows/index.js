import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box'

import RowLego from '../row'


export default ConnectHelper(
	class RowsLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				data: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
			data: [],
		}

		rowRenderer = (data, i) => {
			return data === null ? false : (
				<RowLego key={ i } { ...data } />
			)
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex} style={ this.props.style }>
					{ this.props.data.map(this.rowRenderer) }
				</BoxBit>
			)
		}
	}
)
