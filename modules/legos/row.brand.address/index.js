import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import RowLego from 'modules/legos/row';
import SelectBrandLego from '../select.brand';
import SelectBrandAddressLego from '../select.brand.address';

import Styles from './style';


export default ConnectHelper(
	class RowBrandLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				brandId: PropTypes.number,
				isLoading: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				brandId: p.brandId,
				brandAddressId: p.brandAddressId,
			})
		}

		onChange = (type, id) => {
			switch(type) {
			case 'brand':
				this.setState({
					brandId: id,
				}, () => {
					this.props.onChange &&
					this.props.onChange(this.state)
				})
				break
			case 'address':
				this.setState({
					brandAddressId: id,
				}, () => {
					this.props.onChange &&
					this.props.onChange(this.state)
				})
			}

		}

		view() {
			return (
				<RowLego
					key={ `${this.state.isAdding}${this.state.brandId}` }
					data={[{
						title: 'Seller*',
						children: (
							<SelectBrandLego isRequired
								key={ this.state.brandId }
								brandId={ this.state.brandId }
								placeholder="Select brand"
								isLoading={ this.props.isLoading }
								onChange={ this.onChange.bind(this, 'brand') }
							/>
						),
					}, {
						title: 'Pickup Point*',
						children: (
							<SelectBrandAddressLego isRequired
								key={ this.state.brandAddressId }
								brandId={ this.state.brandId }
								brandAddressId={ this.state.brandAddressId }
								isLoading={ this.props.isLoading }
								onChange={ this.onChange.bind(this, 'address') }
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
