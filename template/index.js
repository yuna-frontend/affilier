import React from 'react';
import ReactDOM from 'react-dom';
import './style.scss';
// import Defaults from 'utils/constants/default';

let $loader, $root, _isLoaderAnimating = false;

export default {
	init(rootComponent) {
		$loader = document.querySelector('#loader')
		$root = document.querySelector('#root')

		window.onload = () => {
			ReactDOM.render(React.createElement(rootComponent), $root)
			this.hideLoader()
		}

		if(process.env.ENV === 'local') {
			module.hot.accept();
		}
	},

	showLoader(cb) {
		if(!_isLoaderAnimating) {
			_isLoaderAnimating = true

			$loader.removeAttribute('style');
			$loader.classList.toggle('is-active');

			$root.classList.toggle('is-ready')

			setTimeout(function() {
				_isLoaderAnimating = false

				$root.setAttribute('style', 'visibility: hidden; pointer-events: none;')

				cb &&
				cb()
			}, 300)
		}
	},

	hideLoader(cb) {
		if(!_isLoaderAnimating) {
			_isLoaderAnimating = true

			$loader.classList.toggle('is-active');

			$root.removeAttribute('style')
			$root.classList.toggle('is-ready')

			setTimeout(function() {
				_isLoaderAnimating = false

				$loader.setAttribute('style', 'display: none;')

				cb &&
				cb()
			}, 300)
		}
	},
}
