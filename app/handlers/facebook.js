import HandlerModel from 'coeur/models/handler'

import ErrorHelper from 'coeur/helpers/error'


class FacebookHandler extends HandlerModel {

	static displayName = 'facebook'

	constructor() {
		super({
			init: false,
			loginStatus: false,
			response: undefined,
			userData: undefined,
		}, [
			'setLoginStatus',
		])
	}

	init() {
		if(super.init()) {
			this.initPromise = new Promise((res) => {
				window.fbAsyncInit = () => {
					window.FB.init({
						appId: process.env.FB_APP_ID,
						autoLogAppEvents: true,
						xfbml: true,
						version : 'v3.0',
					})

					window.FB.getLoginStatus(this.setLoginStatus)

					this.set('init', 'ready')

					res(true)
				}

				// LOAD THE SDK
				(function(d, s, id) {
					const fjs = d.getElementsByTagName(s)[0];
					const js = d.createElement(s); js.id = id;
					if (d.getElementById(id)) return;
					js.src = '//connect.facebook.net/en_US/sdk.js';
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			})
		}

		return this.initPromise
	}

	login(relogin = false) {

		const option = {
			scope: 'public_profile,email',
		}

		if(relogin) {
			option.auth_type = 'rerequest'
		}


		return new Promise((res, rej) => {
			window.FB.login((response)=>{
				if(response.authResponse) {
					// LOGGED
					this.set('loginStatus', 'connected')
						.set('response', response)

					res(response.authResponse);

					// res(response);
				} else {
					this.set('loginStatus', 'unknown')

					rej(ErrorHelper.create('003', response))
				}

			}, option)
		})
	}

	setLoginStatus(response) {
		this.set('loginStatus', response.status)
			.set('response', response)
	}
}

export default new FacebookHandler()
