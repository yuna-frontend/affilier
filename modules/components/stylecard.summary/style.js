import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	header: {
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingBottom: Sizes.margin.default,
	},

	images: {
		minHeight: 88,
		backgroundColor: Colors.grey.palette(5),
		paddingLeft: Sizes.margin.default / 2,
		paddingRight: Sizes.margin.default / 2,
		paddingTop: Sizes.margin.default / 2,
		paddingBottom: Sizes.margin.default / 2,
		marginBottom: Sizes.margin.default,
	},

	inventory: {
		marginRight: 4,
	},

	image: {
		height: 96,
		width: 72,
		borderWidth: 2,
		borderColor: Colors.white.primary,
	},

	id: {
		color: Colors.primary,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	completion: {
		marginTop: 4,
		color: Colors.black.palette(2, .6),
	},

	ok: {
		color: Colors.green.primary,
	},

	no: {
		color: Colors.red.primary,
	},

	icon: {
		width: 24,
		marginRight: 8,
	},
})
