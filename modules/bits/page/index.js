import ConnectHelper from 'coeur/helpers/connect';
import CorePageBit from 'coeur/modules/bits/page';

import BoxBit from '../box';
import ScrollViewBit from '../scroll.view';

import Styles from './style'


export default ConnectHelper(
	class PageBit extends CorePageBit({
		Styles,
		BoxBit,
		ScrollViewBit,
	}) {
		static defaultProps = {
			unflex: true,
			scroll: true,
			footerHeight: 50,
			headerHeight: 10,
		}
	}
)
