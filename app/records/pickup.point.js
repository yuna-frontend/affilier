import RecordModel from 'coeur/models/record'


function PickupPointRecordFactory(additionalConfig = {}) {
	return class PickupPointRecord extends RecordModel('pickupPoint', {
		id				: RecordModel.TYPES.ID,
		addressId		: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,

		...additionalConfig,
	}) {
	}
}

export default PickupPointRecordFactory
