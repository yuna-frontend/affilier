import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	alignItemsCenter: {
		alignItems: 'center',
	},

	mr8: {
		marginRight: 8,
	},
})
