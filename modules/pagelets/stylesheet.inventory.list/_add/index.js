import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'

import StylesheetService from 'app/services/style.sheets';

import Linking from 'coeur/libs/linking';

// import TextInputBit from 'modules/bits/text.input';

// import RowLego from 'modules/legos/row';
import TabLego from 'modules/legos/tab';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'
import PurchaseRequestUpdatePagelet from 'modules/pagelets/purchase.request.update'

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class AddPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				index: 0,
				isLoading: false,
				selectedId: null,
				purchase: {},
				search: undefined,
			})
		}

		tabs = [{
			title: 'Request Purchase',
		}, {
			title: 'Inventory',
			onPress: () => {
				Linking.open(window.location.href.split('/')[0] + '/stylist-inventory')
				this.onClose()
			},
		}]

		isPurchaseValid = () => {
			return !!this.state.purchase.variant_id || (
				this.state.purchase.url &&
				this.state.purchase.seller_id &&
				this.state.purchase.brand_id &&
				this.state.purchase.title &&
				this.state.purchase.category_id &&
				this.state.purchase.color_id &&
				this.state.purchase.size_id &&
				this.state.purchase.price &&
				this.state.purchase.retail_price
			)
		}

		onSearch = debounce((e, val) => {
			this.setState({
				search: val,
			})
		}, 300)

		onAddItems = () => {
			this.setState({
				isLoading: true,
			}, () => {
				if(this.state.index === 0) {
					StylesheetService.onPutInventoryToStylesheet(this.props.id, {
						type: 'PURCHASE_REQUEST',
						data: this.state.purchase,
					}, this.props.token).then(this.onSuccess).catch(this.onError)
				} else {
					StylesheetService.onPutInventoryToStylesheet(this.props.id, {
						type: 'VARIANT',
						id: this.state.selectedId,
					}, this.props.token).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSuccess = res => {
			this.props.utilities.notification.show({
				title: 'Success',
				message: `Inventory added to stylesheet #${this.props.id}`,
				type: 'SUCCESS',
			})

			this.props.onUpdate &&
			this.props.onUpdate(res.id)

			this.onClose()
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSelect = id => {
			this.setState({
				selectedId: id,
			})
		}

		onUpdatePurchaseData = (key, value) => {
			this.state.purchase[key] = value

			this.setState({
				purchase: { ...this.state.purchase },
			})
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add New Item"
					subheader={(
						<React.Fragment>
							<TabLego activeIndex={ this.state.index } tabs={ this.tabs } />
						</React.Fragment>
					)}
					loading={ this.state.isLoading }
					disabled={ this.state.index === 0
						? !this.isPurchaseValid()
						: this.state.selectedId === null }
					confirm={ this.state.index === 0 ? 'Submit' : 'Select Item' }
					onCancel={ this.onClose }
					onConfirm={ this.onAddItems }
					contentContainerStyle={ this.state.index === 0 ? Styles.content2 : Styles.content }
				>
					<PurchaseRequestUpdatePagelet editable bodyless noquantity data={ this.state.purchase } onUpdate={ this.onUpdatePurchaseData } />
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
