import ServiceModel from 'coeur/models/service';
// import { isEmpty } from 'lodash'


class PurchaseService extends ServiceModel {

	static displayName = 'purchase'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/purchase/', 0)
	}

	filterRequest(query, token) {
		return this.get('request', {
			query,
			token,
		})
	}

	getDetail(id, query, token) {
		return this.get('request/' + id, {
			query,
			token,
		})
	}

	createRequest(data, token) {
		return this.post('request/', {
			data,
			token,
		})
	}

	updateRequest(id, data, token) {
		return this.post('request/' + id, {
			data,
			token,
		})
	}

	approveRequest(id, note, token) {
		return this.post('request/' + id + '/approve', {
			data: {
				note,
			},
			token,
		})
	}

	rejectRequest(id, note, token) {
		return this.post('request/' + id + '/reject', {
			data: {
				note,
			},
			token,
		})
	}

	resolveRequest(id, data, token) {
		return this.post('request/' + id + '/convert', {
			data,
			token,
		})
	}

	despiseRequest(id, data, token) {
		return this.post('request/' + id + '/despise', {
			data,
			token,
		})
	}

	revokeRequest(id, purchaseOrderId, note, replacement, token) {
		return this.post('order/' + purchaseOrderId + '/request/' + id + '/revoke', {
			data: {
				note,
				replacement,
			},
			token,
		})
	}

	filterOrder(query, token) {
		return this.get('order', {
			query,
			token,
		})
	}

	getOrder(id, query, token) {
		return this.get('order/' + id, {
			query,
			token,
		})
	}

	createOrder(title, purchaseRequestIds, token) {
		return this.post('order/', {
			data: {
				request_ids: purchaseRequestIds,
				title,
			},
			token,
		})
	}

	removeRequest(orderId, id, token) {
		return this.delete('order/' + orderId + '/request/' + id, {
			token,
		})
	}

	addRequestToOrder(id, purchaseRequestIds, token) {
		return this.post('order/' + id + '/request', {
			data: {
				request_ids: purchaseRequestIds,
			},
			token,
		})
	}

	updateOrder(id, data, token) {
		return this.post('order/' + id, {
			data,
			token,
		})
	}

	// '/admin/purchase/order/:id/asset/:asset_id?': {
	// 	get: this.getPurchaseOrderAsset,
	// 	post: this.uploadAssets,
	// 	put: this.reorderAssets,
	// 	delete: this.removeAssets,
	// },

	getAssets(id, token) {
		return this.get('order/' + id + '/asset', {
			token,
		})
	}

	createAsset(id, image, order, token) {
		return this.post('order/' + id + '/asset', {
			token,
			data: {
				image,
				order,
			},
		})
	}

	reorderAsset(id,  ids, token) {
		return this.put('order/' + id + '/asset', {
			data: {
				asset_ids: ids,
			},
			token,
		})
	}

	deleteAsset(id, assetId, token) {
		return this.delete('order/' + id + '/asset/' + assetId, {
			token,
		})
	}

}

export default new PurchaseService()
