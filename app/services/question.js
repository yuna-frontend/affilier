import ServiceModel from 'coeur/models/service';


class QuestionService extends ServiceModel {

	static displayName = 'user'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'question/', 0)
	}

	getGroups(token) {
		return this.get('group', {
			token,
		})
	}

	getQuestionsByGroupId(groupId, token) {
		return this.get('group/' + groupId, {
			token,
		})
	}

	getQuestionsById(ids, token) {
		return this.get(ids.join(','), {
			token,
		})
	}
}

export default new QuestionService()
