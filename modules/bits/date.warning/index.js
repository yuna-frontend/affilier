import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import BoxBit from '../box';
import GeomanistBit from '../geomanist'

import Styles from './style'


export default ConnectHelper(
	class DateWarningBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				date: PropTypes.date,
				format: PropTypes.string,
				mark: PropTypes.oneOfType([
					PropTypes.number,
					PropTypes.arrayOf(PropTypes.number),
				]),
				style: PropTypes.style,
				textStyle: PropTypes.style,
			}
		}

		static getDerivedStateFromProps(nP) {
			const date = TimeHelper.moment(nP.date)
			return {
				diff: date.diff(new Date(), 'd', true),
				date,
			}
		}

		static defaultProps = {
			mark: 0,
			format: 'DD MMMM YYYY',
		}

		constructor(p) {
			super(p, {
				diff: 0,
				date: TimeHelper.moment(),
			})
		}

		getStyle() {
			if(Array.isArray(this.props.mark)) {
				const mark = this.props.mark.slice()
				const alertMark = mark.shift()
				const warningMark = mark.shift() || alertMark

				if (this.state.diff > warningMark) {
					return Styles.note
				} else if (this.state.diff > alertMark) {
					return Styles.warning
				} else {
					return Styles.alert
				}
			} else {
				if (this.state.diff > this.props.mark) {
					return Styles.note
				} else if (this.state.diff === this.props.mark) {
					return Styles.warning
				} else {
					return Styles.alert
				}
			}
		}

		view() {
			return (
				<BoxBit unflex style={ this.props.style }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ this.props.textStyle }>
						{ this.state.date.format(this.props.format) }
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ this.getStyle() }>
						— { this.state.date.fromNow() }
					</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
