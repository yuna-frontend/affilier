import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default * .75,
		paddingBottom: Sizes.margin.default * .75,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderRadius: 2,
	},
	titleContainer: {
		marginLeft: Sizes.margin.default,
	},
	title: {
		color: Colors.white.primary,
	},
})
