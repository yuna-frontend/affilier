import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
	},

	content: {
		marginLeft: Sizes.margin.default,
		marginRight: Sizes.margin.default,
		marginBottom: Sizes.margin.default,
	},

	header: {
		paddingBottom: 14,
		paddingTop: 14,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		alignItems: 'center',
	},

	borderless: {
		borderBottomWidth: 0,
	},

	small: {
		paddingTop: 7,
		paddingBottom: 7,
	},

	title: {
		color: Colors.black.palette(2),
	},
})
