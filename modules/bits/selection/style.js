import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		height: 44,
		minHeight: 44,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
		borderRadius: 2,
		...(Defaults.PLATFORM === 'web' ? {
			transition: '.3s color, .3s border, .3s opacity',
		} : {}),
		backgroundColor: Colors.white.primary,
		paddingLeft: Sizes.margin.default,
		paddingRight: 8,
	},
	placeholder: {
		color:Colors.black.palette(2, .6),
	},
	disabled: {
		opacity: .5,
	},
	focused: {
		borderBottomColor: 0,
	},
	input: {
		color: Colors.black.palette(2),
		// minWidth: 112,
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		paddingRight: 16,
	},
	hidden: {
		opacity: 0,
		width: 0,
		height: 0,
	},
})
