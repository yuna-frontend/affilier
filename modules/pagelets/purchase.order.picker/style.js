import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'


export default StyleSheet.create({
	content: {
		marginTop: Sizes.margin.default,
	},

	item: {
		paddingLeft: Sizes.margin.default - 4,
		paddingRight: Sizes.margin.default - 4,
		paddingTop: Sizes.margin.default / 2,
		paddingBottom: Sizes.margin.default / 2,
		backgroundColor: Colors.grey.palette(2),
	},

	list: {
		paddingTop: Sizes.margin.default - 4,
		paddingBottom: Sizes.margin.default - 4,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomColor: Colors.black.palette(2, .16),
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	odd: {
		backgroundColor: Colors.grey.palette(2),
	},

	add: {
		color: Colors.primary,
	},

	retail: {
		textDecoration: 'line-through',
		marginRight: 4,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	discounted: {
		color: Colors.red.palette(7),
	},
})
