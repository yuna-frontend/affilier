import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderTopRightRadius: 2,
		borderTopLeftRadius: 2,
		borderStyle: 'solid',
		borderWidth: 0,
		borderTopWidth: 1,
		borderLeftWidth: 1,
		borderRightWidth: 1,
	},

	normal: {
		height: 44,
		paddingLeft: 16,
		paddingRight: 16,
	},
})
