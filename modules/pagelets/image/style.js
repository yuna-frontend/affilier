import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		background: Colors.white.primary,
		width: Styles.size.fluidWidth,
		height: Styles.size.fluidHeight,
	},
	image: {
		width: Styles.size.fluidWidth,
		height: Styles.size.fluidHeight,
	},
	icon: {
		position: 'absolute',
		right: 24,
		top: 24,
	},
	edit: {
		position: 'absolute',
		right: 72,
		top: 24,
	},
})
