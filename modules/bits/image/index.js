import ConnectHelper from 'coeur/helpers/connect';
import CoreImageBit from 'coeur/modules/bits/image';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import IconBit from '../icon';
import LoaderBit from '../loader';

export default ConnectHelper(
	class ImageBit extends CoreImageBit({
		BoxBit,
		IconBit,
		LoaderBit,
		imageResolver: function(path) {
			return import(
				/* webpackMode: "eager" */
				`@affilier/assets/img/${path}`
			).catch(() => {
				return Promise.resolve(false)
			})
		},
		backgroundColor: Colors.grey.palette(1, .04),
		indicatorColor: Colors.grey.palette(1, .5),
	}) {}
)
