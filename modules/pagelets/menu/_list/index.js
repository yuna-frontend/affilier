import ConnectHelper from 'coeur/helpers/connect';
import {
	ListPart as CoreListPart,
} from 'coeur/modules/pagelets/menu';

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon';
import FlatlistBit from 'modules/bits/flatlist'
import LoaderBit from 'modules/bits/loader'
import TextInputBit from 'modules/bits/text.input'

import ButtonPart from '../__button';
import SwitchPart from '../__switch';

import Styles from './style'


export default ConnectHelper(
	class ListPart extends CoreListPart({
		Styles,
		BoxBit,
		FlatlistBit,
		IconBit,
		LoaderBit,
		TextInputBit,
		ButtonPart,
		SwitchPart,
	}) {}
)
