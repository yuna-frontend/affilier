import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	RadioPart as CoreRadioPart,
} from 'coeur/modules/pagelets/alert'

import RadioBit from 'modules/bits/radio';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class RadioPart extends CoreRadioPart({
		Styles,
		RadioBit,
		TextBit: GeomanistBit,
		TouchableBit,
	}) {
		titleRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_1} style={style}>{ this.props.title }</GeomanistBit>
			)
		}
	}
)
