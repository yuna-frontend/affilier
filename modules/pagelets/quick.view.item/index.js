import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities';

import InventoryService from 'app/services/new.inventory';
import VariantService from 'app/services/variant';
import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import BadgeStatusLego from 'modules/legos/badge.status';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'
import NotFoundPagelet from 'modules/pagelets/not.found'

import Styles from './style';


export default ConnectHelper(
	class QuickViewItemPagelet extends PromiseStatefulModel {

		static TYPES = {
			'INVENTORY': 'INVENTORY',
			'VARIANT': 'VARIANT',
			'PURCHASE_REQUEST': 'PURCHASE_REQUEST',
			'STYLESHEET_INVENTORY': 'STYLESHEET_INVENTORY',
		}

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES),
				id: PropTypes.id,
				data: PropTypes.shape({
					brand: PropTypes.string,
					title: PropTypes.string,
					status: PropTypes.string,
					description: PropTypes.string,
					image: PropTypes.oneOfType([
						PropTypes.string,
						PropTypes.object,
					]),
					price: PropTypes.number,
					retailPrice: PropTypes.number,
				}),
				title: PropTypes.string,
				confirm: PropTypes.string,
				children: PropTypes.node,
				onClose: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.id) {
				switch(oP.type) {
				case 'INVENTORY':
					return InventoryService.getDetail(oP.id, {
						query: { item: true },
						token: state.me.token,
					})
				case 'VARIANT':
					return VariantService.getVariantById(oP.id, { item: true }, state.me.token)
				case 'PURCHASE_REQUEST':
					return InventoryService.getPurchaseRequest(oP.id, state.me.token).then(data => data[0])
				case 'STYLESHEET_INVENTORY':
					return StylesheetService.getInventoryDetail(oP.id, {
						token: state.me.token,
					}).then(sI => {
						return {
							brand: sI.brand,
							title: sI.title,
							status: sI.status,
							description: `${ sI.category } — ${ sI.color } – ${ sI.size }`,
							image: sI.image,
							price: sI.price,
							retailPrice: sI.retail_price,
						}
					})
				default:
					return null
				}
			} else if(oP.data) {
				return Promise.resolve(oP.data)
			} else {
				return null
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Quick View',
			confirm: 'Done',
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		viewOnError(err) {
			return (
				<ModalClosableConfirmationPagelet
					title={this.props.title}
					confirm={this.props.confirm}
					onClose={this.onClose}
					onConfirm={this.onClose}
				>
					<NotFoundPagelet description={err ? err.detail || err.message || err : 'Something went wrong'} />
				</ModalClosableConfirmationPagelet>
			)
		}

		viewOnLoading() {
			return (
				<ModalClosableConfirmationPagelet
					title={this.props.title}
					confirm={this.props.confirm}
					onClose={this.onClose}
					onConfirm={this.onClose}
				>
					<NotFoundPagelet loading />
				</ModalClosableConfirmationPagelet>
			)
		}

		view() {
			return  (
				<ModalClosableConfirmationPagelet
					title={ this.props.title }
					confirm={ this.props.confirm }
					onClose={ this.onClose }
					onConfirm={ this.onClose }
				>
					<BoxBit row style={Styles.content}>
						<ImageBit overlay resizeMode={ ImageBit.TYPES.COVER }
							broken={ !this.props.data.image }
							source={ this.props.data.image }
							style={ Styles.image }
						/>
						<BoxBit>
							{ this.props.data.brand ? (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} weight="medium">
									{ this.props.data.brand }
								</GeomanistBit>
							) : false }
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} weight="medium">
								{ this.props.data.title }
							</GeomanistBit>
							{ this.props.data.retail_price !== this.props.data.price ? (
								<BoxBit row unflex>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.discount}>
										IDR { FormatHelper.currency(this.props.data.price) }
									</GeomanistBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.retail}>
										IDR { FormatHelper.currency(this.props.data.retail_price) }
									</GeomanistBit>
								</BoxBit>
							) : (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									IDR { FormatHelper.currency(this.props.data.price) }
								</GeomanistBit>
							) }
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.description}>
								{ this.props.data.description }
							</GeomanistBit>
							{ this.props.data.status ? (
								<BoxBit row unflex>
									<BadgeStatusLego status={ this.props.data.status } style={Styles.status} />
								</BoxBit>
							) : false }
							{ this.props.data.note ? (
								<GeomanistBit italic type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
									{ this.props.data.note }
								</GeomanistBit>
							) : false }
							{ this.props.children }
						</BoxBit>
					</BoxBit>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
