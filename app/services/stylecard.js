import ServiceModel from 'coeur/models/service';


class StyleCardService extends ServiceModel {

	static displayName = 'stylecard'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/stylecard/', 0)
	}

	// '/stylecard/variant/:ids': {
	// 	get: this.getStylesheetVariants,
	// 	delete: this.removeStylesheetVariant,
	// },

	// // Stylecards
	// '/stylecard': {
	// 	get: this.getStylecard,
	// 	post: this.createStylecard,
	// },
	// '/stylecard/:id': {
	// 	get: this.getStylecardById,
	// 	post: this.updateStylecard,
	// },
	// '/stylecard/:id/assign': {
	// 	post: this.assign,
	// },
	// '/stylecard/:id/duplicate': {
	// 	post: this.duplicate,
	// },
	// '/stylecard/:id/variant': {
	// 	post: this.createStylecardVariant,
	// },
	// '/stylecard/:id/status': {
	// 	post: this.createStylecardStatus,
	// },
	// '/stylecard/:id/note': {
	// 	post: this.setStylistNote,
	// },

	getViewer(stylecardId, token) {
		return this.get(`${stylecardId}/viewer`, {
			token,
		})
	}

	getVariantDetail = this._createSmartGetters((ids, params) => {
		return this.get('variant/' + ids.join(','), {
			token: params.token,
		})
	}, (id, params) => {
		return this.get('variant/' + id, {
			token: params.token,
		})
	}, function(id, [ids, datas]) {
		if (ids.length) {
			return datas.find(d => d.id === id)
		} else {
			return datas[0]
		}
	})

	getStylecards(data, token) {
		return this.get('', {
			token,
			query: data,
		})
	}

	getStylecardDetailed(id, token) {
		return this.get(id, {
			token,
			query: {
				detailed: true,
			},
		})
	}

	getStylecardShallow(id, token) {
		return this.get(id, {
			token,
			query: {
				shallow: true,
			},
		})
	}

	putVariantIntoStylecard(id, data, token) {
		return this.post(id + '/variant', {
			data,
			token,
		})
	}

	removeVariantInStylecard(id, token) {
		return this.delete('variant/' + id, {
			token,
		})
	}

	saveNote(data, token) {
		return this.post(data.id + '/note', {
			data: {
				data,
			},
			token,
		})
	}

	assignStylist(id, stylistId, token) {
		return this.post(id + '/assign', {
			data: {
				stylist_id: stylistId,
			},
			token,
		})
	}

	duplicate(id, sourceId, consent, token) {
		return this.post(id + '/duplicate', {
			data: {
				source_stylecard_id: sourceId,
				consent,
			},
			token,
		})
	}

	setStatus(id, status, note, token) {
		return this.post(id + '/status', {
			data: {
				code: status,
				note,
			},
			token,
		})
	}

	publish(id, status, stylistNote, data, token) {
		return this.post(id + '/status', {
			data: {
				...data,
				code: status,
				stylist_note: stylistNote,
			},
			token,
		})
	}

	updateDiscount(stylecardVariantId, discount, token) {
		return this.put('variant/' + stylecardVariantId, {
			data: {
				bundle_discount: discount,
			},
			token,
		})
	}

	update(id, data, note, token) {
		return this.post(id, {
			data: {
				...data,
				changelog: note,
			},
			token,
		})
	}

	create(data, token) {
		return this.post('', {
			data,
			token,
		})
	}
}

export default new StyleCardService()
