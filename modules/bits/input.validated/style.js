import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	title: {
		color: Colors.black.palette(2),
	},
	titleContainer: {
		marginBottom: 4,
		marginTop: 4,
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	inputInvalid: {
		color: Colors.red.palette(4),
	},
	inputValid: {
		color: Colors.black.palette(2),
	},
	prefix: {
		marginRight: 8,
		marginLeft: 16,
		color: Colors.black.palette(2),
	},
	postfix: {
		marginRight: 12,
	},
	darkGrey: {
		color: Colors.black.palette(2),
	},
	counter: {
		color: Colors.black.palette(2, .6),
	},
})
