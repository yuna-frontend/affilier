import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import FormatHelper from 'coeur/helpers/format';

import StylecardService from 'app/services/stylecard';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import LinkBit from 'modules/bits/link';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';
import InputCurrencyBit from 'modules/bits/input.currency'
import ButtonBit from 'modules/bits/button'

import Styles from './style';

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class SelectionStylecardVariantLego extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				editable: PropTypes.bool,
				removable: PropTypes.bool,
				borderless: PropTypes.bool,
				data: PropTypes.shape({
					id: PropTypes.id,
					variant_id: PropTypes.id,
					image: PropTypes.object,
					brand: PropTypes.string,
					title: PropTypes.string,
					category: PropTypes.string,
					size: PropTypes.string,
					color: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
				}),
				onRemove: PropTypes.func,
				onChange: PropTypes.func,
				onDataLoaded: PropTypes.func,
				isBundle: PropTypes.bool,
				style: PropTypes.style,
				token: PropTypes.string,
				refresh: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? StylecardService.getVariantDetail(oP.id, {
				token: state.me.token,
			}).then(data => {
				oP.onDataLoaded &&
				oP.onDataLoaded(data)

				return data
			}) : null
		}

		constructor(p) {
			super(p, {
				isUpdating: false,
				bundleDiscount: 0,
				mode: 'IDR',
			})
		}

		onUpdateBundleDiscount = () => {
			this.setState({
				isUpdating: true,
			})
			StylecardService.updateDiscount(this.props.id, this.state.bundleDiscount, this.props.token).then( () => {
				this.setState({
					isUpdating: false,
				}, () => {
					this.props.refresh &&
					this.props.refresh()
				})
			})
		}

		onChangeBundleDiscount = (val, isValid) => {
			if(isValid) {
				this.setState({
					bundleDiscount: this.state.mode === 'IDR' ? val : parseInt(val / 100 * this.props.data.price, 10),
				})
			}
		}

		onChangeMode = () => {
			if(this.state.mode === 'IDR') {
				this.setState({
					mode: '%',
				})
			} else {
				this.setState({
					mode: 'IDR',
				})
			}
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={[ Styles.container, Styles.empty, this.props.style ]}>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return isEmpty(this.props.data) ? this.viewOnLoading() : (
				<BoxBit unflex row style={[ Styles.container, this.props.borderless && Styles.borderless, this.props.style ]}>
					<ImageBit overlay
						resizeMode={ ImageBit.TYPES.COVER }
						broken={ !this.props.data.image }
						source={ this.props.data.image || undefined }
						style={ Styles.image }
					/>
					<BoxBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.text }>
							{ `#VA-${ this.props.data.variant_id }` }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.text}>
							{ this.props.data.brand || '-' } – { this.props.data.title || '-' }
						</GeomanistBit>
						{ this.props.data.price === this.props.data.retail_price ? (
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="normal" style={ Styles.price }>
								IDR { FormatHelper.currencyFormat(this.props.data.price) || '-' }
							</GeomanistBit>
						) : (
							<BoxBit unflex row>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="normal" style={ Styles.discounted }>
									IDR { FormatHelper.currencyFormat(this.props.data.price) || '-' }
								</GeomanistBit>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="normal" style={ Styles.retail }>
									IDR { FormatHelper.currencyFormat(this.props.data.retail_price) || '-' }
								</GeomanistBit>
							</BoxBit>
						) }
						<BoxBit />
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
							{ this.props.data.category || '-' } – Color: { this.props.data.color } – Size: { this.props.data.size }
						</GeomanistBit>
						{ this.props.data.variant_id ? false : (
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								<LinkBit underline href={ this.props.data.url } target={ LinkBit.TYPES.BLANK }>{ this.props.data.url ? `${ this.props.data.url.substr(0, 20) }…` : false }</LinkBit>
							</GeomanistBit>
						) }
					</BoxBit>
					{
						this.props.isBundle
							? <BoxBit unflex>
								<BoxBit unflex>
									<GeomanistBit>Bundle Discount</GeomanistBit>
								</BoxBit>
								<BoxBit row unflex>
									<TouchableBit
										onPress={this.onChangeMode}
									>
										<InputCurrencyBit
											title={ this.state.mode }
											value={ this.state.mode === 'IDR' ? this.props.data.bundle_discount : parseInt(this.props.data.bundle_discount / this.props.data.price * 100, 10)  }
											onChange={ this.onChangeBundleDiscount }
										/>
									</TouchableBit>
									<ButtonBit
										title={'update discount'}
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										onPress={ this.onUpdateBundleDiscount }
										state={ this.state.isUpdating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
									/>
								</BoxBit>
							</BoxBit>
							: false
					}
					
					<BoxBit unflex>
						<BoxBit row>
							<BoxBit />
							<BoxBit unflex row>
								{ this.props.editable ? (
									<TouchableBit unflex onPress={ this.props.onChange } style={ Styles.edit }>
										<IconBit
											name="edit"
											size={ 20 }
											color={ Colors.primary }
										/>
									</TouchableBit>
								) : false}

								{ this.props.removable ? (
									<TouchableBit unflex onPress={ this.props.onRemove } style={ Styles.trash }>
										<IconBit
											name="trash"
											size={ 20 }
											color={ Colors.primary }
										/>
									</TouchableBit>
								) : false }
							</BoxBit>
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
