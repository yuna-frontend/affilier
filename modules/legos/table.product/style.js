import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	table: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.black.palette(2, .16),
	},

	image: {
		width: 60,
		height: 60,
		marginRight: 16,
	},

	product: {
		justifyContent: 'flex-start',
		alignItems: 'center',
	},

	status: {
		alignItems: 'center',
	},

	price: {
		alignItems: 'flex-end',
	},

	title: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	box: {
		overflow: 'hidden',
	},

	exchange: {
		color: Colors.yellow.palette(2),
	},

})
