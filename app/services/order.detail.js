import ServiceModel from 'coeur/models/service';


class OrderDetailService extends ServiceModel {

	static displayName = 'order.detail'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	getOrderDetails(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getOrderDetail(id, token) {
		return this.get(id, {
			token,
		})
	}

	getDetailOrderDetail(id, query, token) {
		return this.get('order/detail/' + id, {
			query,
			token,
		}).then(datas => datas.pop())
	}

	getPacked(data, token) {
		return this.get('order/detail', {
			query: data,
			token,
		})
	}

	updateOrderDetailStatus(data, token) {
		return this.post('order/detail/' + data.id + '/status', {
			data,
			token,
		})
	}

	getFiltered(filter, token) {
		return this.post('getOrder', {
			query: filter,
			token,
		})
	}

	bookInventory(id, inventoryId, note, token) {
		return this.post('order/detail/' + id + '/book', {
			data: {
				id: inventoryId,
				note,
			},
			token,
		})
	}

	unbookInventory(id, note, token) {
		return this.delete('order/detail/' + id + '/book', {
			data: {
				note,
			},
			token,
		})
	}

	createRefund(id, note, token) {
		return this.post(`order/detail/${id}/refund`, {
			data: {
				note,
			},
			token,
		})
	}
}

export default new OrderDetailService()
