import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CoreHeaderBit from 'coeur/modules/bits/header';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import GeomanistBit from '../geomanist';

import ButtonPart from './_button'

export {
	ButtonPart,
}


export default ConnectHelper(
	class HeaderBit extends CoreHeaderBit({
		BoxBit,
		TextBit: function(props) {
			return (
				<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_1 } { ...props } />
			)
		},
		ButtonPart,
		backgroundColor: Colors.white.primary,
	}) {}
)
