import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';


export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<GeomanistBit type={GeomanistBit.TYPES.HEADER_5}>
						{ this.props.title }
					</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
