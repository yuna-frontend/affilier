import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import OrderDetailService from 'app/services/order.detail'

import RowLego from 'modules/legos/row';
import SelectStatusLego from 'modules/legos/select.status';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'


export default ConnectHelper(
	class OrderDetailStatusPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				status: PropTypes.string,
				note: PropTypes.string,
				onCancel: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isChanged: false,
			})

			this.data = {
				status: p.status,
				note: p.note,
			}
		}

		onChange = (val) => {
			this.data.status = val
			// this.data.note = note

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onSave = () => {
			this.setState({
				isLoading: true,
			}, () => {
				OrderDetailService.updateOrderDetailStatus({
					id: this.props.id,
					code: this.data.status,
				}, this.props.token).then(this.props.onUpdate).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					})
				})
			})
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Change Order Status"
					loading={ this.state.isLoading }
					disabled={ !this.state.isChanged }
					onCancel={ this.props.onCancel }
					onConfirm={ this.onSave }
				>
					{/* <RowStatusInventoryLego status={this.props.status} note={this.props.note} onChange={this.onChange} /> */}
					<RowLego
						data={[{
							children: (
								<SelectStatusLego
									type={ SelectStatusLego.TYPES.ORDER_DETAILS }
									status={this.data.status}
									onChange={this.onChange}
								/>
							),
						}, {
							title: 'Note',
							content: this.data.note,
						}]}
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)

