import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'


import VariantService from 'app/services/variant'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
// import ScrollViewBit from 'modules/bits/scroll.view';

import SelectionInventoryLego from 'modules/legos/selection.inventory'

import Styles from './style';

let __id = 1

export default ConnectHelper(
	class VariantsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				keyword: PropTypes.string,
				onSelect: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
				inventories: [],
				selectedId: -1,
			})
		}

		componentDidMount() {
			if(this.props.keyword) {
				this.getData()
			}
		}

		componentDidUpdate(pP) {
			if(pP.keyword !== this.props.keyword) {
				this.getData()
			}
		}

		getData = () => {
			__id++

			const _id = __id

			// eslint-disable-next-line react/no-did-update-set-state
			this.setState({
				isUpdating: true,
			}, () => {
				VariantService.search(this.props.keyword, 0, 12, false, this.props.token).then(res => {
					if (_id === __id) {
						this.setState({
							isUpdating: false,
							inventories: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong, please try again later',
					})

					this.setState({
						isUpdating: false,
						inventories: [],
					})
				})
			})
		}

		onSelect = id => {
			this.setState({
				selectedId: id,
			})

			this.props.onSelect &&
			this.props.onSelect(id)
		}

		inventoryRenderer = (variant, i) => {
			return (
				<SelectionInventoryLego dumb selectable key={`${i}|${variant.id}`}
					id={ variant.id }
					borderless={ i === this.state.inventories.length - 1 }
					data={ variant }
					isSelected={ variant.id === this.state.selectedId }
					onSelect={ this.onSelect }
				/>
			);
		}

		loadingRenderer() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.loader}>
						Fetching your search detail…
					</GeomanistBit>
				</BoxBit>
			);
		}

		view() {
			return this.state.isUpdating ? this.loadingRenderer() : (
				<BoxBit>
					{ /* eslint-disable-next-line no-nested-ternary */ }
					{ this.state.inventories.length ? this.state.inventories.map(this.inventoryRenderer) : this.props.keyword ? (
						<BoxBit centering>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.loader}>
								Oops… No result found.
							</GeomanistBit>
						</BoxBit>
					) : false }
				</BoxBit>
			)
		}
	}
)
