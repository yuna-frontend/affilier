import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';


export default ConnectHelper(
	class PromoLego
		extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				coupons: PropTypes.oneOfType([
					PropTypes.array,
					PropTypes.node,
				]),
				campaigns: PropTypes.array,
				style: PropTypes.style,

				// shipments: PropTypes.array,
			}
		}

		static defaultProps = {
			campaigns: [],
			coupons: [],
		}

		promoRenderer = couponOrCampaign => {
			return (
				<BoxBit key={ couponOrCampaign.code } row unflex>
					<BoxBit unflex style={ Styles.promo }>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium">
							{ couponOrCampaign.code || '-' }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.amount }>
							IDR { FormatHelper.currency(couponOrCampaign.discount) }
						</GeomanistBit>
					</BoxBit>
				</BoxBit>
			);
		}

		view() {
			return (
				<BoxBit unflex row style={ Styles.wrapper }>
					{ Array.isArray(this.props.coupons)
						? (this.props.coupons.length || this.props.campaigns.length)
							&& this.props.coupons.concat(this.props.campaigns).map(this.promoRenderer)
							|| (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium">
									-
								</GeomanistBit>
							)
						: this.props.coupons
					}
				</BoxBit>
			)
		}
	}
)
