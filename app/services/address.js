import ServiceModel from 'coeur/models/service';
import MeManager from 'app/managers/me';

class AddressService extends ServiceModel {

	static displayName = 'address'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/user/')
	}

	getAddresses(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getAddress(id, token) {
		return this.get(id + '/address', {
			token,
		})
	}

	addAddress(id, data, token) {
		return this.post(id + '/address', {
			data,
			token,
		})
	}

	addAddresses(addresses, token) {
		return this.post('', {
			data: {
				creates: addresses,
			},
			token,
		})
	}

	updateAddresses(addresses, token) {
		return this.post('', {
			data: {
				updates: addresses,
			},
			token,
		})
	}
}

export default new AddressService()
