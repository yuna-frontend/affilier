import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import AddressManager from 'app/managers/address';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class CardAddressComponent extends CardComponentModel(AddressManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				divider: PropTypes.bool,
				selectionMode: PropTypes.bool,
				isActive: PropTypes.bool,
				isDefaultAddress: PropTypes.bool,
			}
		}

		view() {
			return (
				<TouchableBit unflex onPress={ this.onPress } style={[Styles.container, this.props.divider && Styles.divider]}>
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">{ this.props.data.title || this.props.data.pic }</GeomanistBit>
							{ this.props.data.phone && (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">{ this.props.data.phone }</GeomanistBit>
							) }
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>{ this.props.data.address }</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>{ this.props.data.district }</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>{ this.props.data.postal }</GeomanistBit>
						</BoxBit>
						{ this.props.selectionMode ? (
							<RadioBit isActive={this.props.isActive} onPress={ this.onPress } />
						) : (
							<IconBit
								name="edit"
								color={Colors.black.palette(2, .5)}
							/>
						) }
					</BoxBit>
					{ this.props.isDefaultAddress && (
						<BoxBit unflex row style={Styles.mark}>
							<IconBit
								name="approved"
								size={20}
								color={Colors.green.primary}
							/>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.footnote}>My preffered shipping address</GeomanistBit>
						</BoxBit>
					) }
				</TouchableBit>
			)
		}
	}
)
