import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	mainText: {
		color: Colors.black.palette(1, .7),
	},

	darkGrey80: {
		color: Colors.black.palette(2, .8),
		paddingRight: 6,
	},

	padder: {
		paddingRight: 4,
	},

	centering: {
		alignItems: 'center',
	},
})
