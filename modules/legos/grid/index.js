import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box'
import BoxImageBit from 'modules/bits/box.image'
import GeomanistBit from 'modules/bits/geomanist'

import Styles from './style'

export default ConnectHelper(
	class GridLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				width: PropTypes.number,
				data: PropTypes.array,
				length: PropTypes.number,
				percentage: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			width: Sizes.app.width,
			percentage: [],
		}

		height = (this.props.width / 2) + 55.5
		widthGrid = (66.5 / 100) * this.props.width

		widthSquare = (33 / 100) * this.props.width
		heightSquare = (49.5 / 100) * this.height
		heightLongSquare = (99.333 / 100) * this.height

		halfWidth = (49.6 / 100) * this.props.width
		bigWidthSquare = (66.4 / 100) * this.props.width

		contentRenderer = () => {
			const length = this.props.length
			const container = {
				height: this.height,
				width: this.props.width,
			}

			switch(true) {
			case length === 2:
				return (
					this.props.data.map(this.verticalRenderer)
				)
			case length === 3:
				return (
					<BoxBit unflex row style={[ Styles.container, container ]}>
						<BoxImageBit unflex source={ this.props.data[0] } resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{ width: this.bigWidthSquare, height: this.height }}>
							{ this.props.percentage[0] > 0 && (
								<BoxBit unflex centering style={Styles.boxDiscount}>
									<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
										{ this.props.percentage[0] }% OFF
									</GeomanistBit>
								</BoxBit>
							) }
						</BoxImageBit>
						<BoxBit unflex style={[ Styles.grid, {width: this.widthSquare, height: this.height} ]}>
							{ this.props.data.slice(1, 3).map(this.gridRenderer) }
						</BoxBit>
					</BoxBit>
				)
			case length === 4:
				return (
					<BoxBit unflex row style={[ Styles.container, container ]}>
						<BoxImageBit unflex source={ this.props.data[0] } resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightLongSquare}}>
							{ this.props.percentage[0] > 0 && (
								<BoxBit unflex centering style={Styles.boxDiscount}>
									<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
										{ this.props.percentage[0] }% OFF
									</GeomanistBit>
								</BoxBit>
							) }
						</BoxImageBit>
						<BoxBit unflex style={[ Styles.grid, {width: this.widthSquare, height: this.heightLongSquare} ]}>
							{ this.props.data.slice(1, 3).map(this.gridRenderer) }
						</BoxBit>
						<BoxImageBit unflex source={ this.props.data[3] } resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightLongSquare}}>
							{ this.props.percentage[3] > 0 && (
								<BoxBit unflex centering style={Styles.boxDiscount}>
									<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
										{ this.props.percentage[3] }% OFF
									</GeomanistBit>
								</BoxBit>
							) }
						</BoxImageBit>
					</BoxBit>
				)
			case length === 5:
				return (
					<BoxBit unflex row style={[ Styles.container, container ]}>
						<BoxBit row unflex style={[Styles.grid, {width: this.widthGrid, height: this.height}]}>
							{ this.props.data.slice(0, 4).map(this.gridRenderer) }
						</BoxBit>
						<BoxImageBit unflex source={ `//${this.props.data[4].url}` } resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightLongSquare}}>
							{ this.props.percentage[4] > 0 && (
								<BoxBit unflex centering style={Styles.boxDiscount}>
									<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
										{ this.props.percentage[4] }% OFF
									</GeomanistBit>
								</BoxBit>
							) }
						</BoxImageBit>
					</BoxBit>
				)
			default:
				return (
					<BoxBit row style={[ Styles.grid, {width: this.width, height: this.height} ]}>
						{ this.props.data.slice(0, 6).map(this.gridRenderer) }
					</BoxBit>
				)
			}
		}

		gridRenderer = (source, i) => {
			const length = this.props.length

			if (length > 6) {
				return i === 5 ? (
					<BoxBit key={ i } unflex style={{width: this.widthSquare, height: this.heightSquare}}>
						<BoxImageBit unflex key={ i } source={`//${source.url}`} resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightSquare}}>
							{ this.props.percentage[i] > 0 && (
								<BoxBit unflex centering style={Styles.boxDiscount}>
									<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
										{ this.props.percentage[i] }% OFF
									</GeomanistBit>
								</BoxBit>
							) }
						</BoxImageBit>
						<BoxBit unflex centering style={[ Styles.overlay, {width: this.widthSquare, height: this.heightSquare} ]} >
							<GeomanistBit weight="medium" style={ Styles.count }>
								+{ length - 6 }
							</GeomanistBit>
						</BoxBit>
					</BoxBit>
				) : (
					<BoxImageBit unflex key={ i } source={`//${source.url}`} resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightSquare}}>
						{ this.props.percentage[i] > 0 && (
							<BoxBit unflex centering style={Styles.boxDiscount}>
								<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
									{ this.props.percentage[i] }% OFF
								</GeomanistBit>
							</BoxBit>
						) }
					</BoxImageBit>
				)
			} else {
				return (
					<BoxImageBit unflex key={ i } source={`//${source.url}`} resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.widthSquare, height: this.heightSquare}}>
						{ this.percentage(i) > 0 && (
							<BoxBit unflex centering style={Styles.boxDiscount}>
								<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
									{ this.percentage(i) }% OFF
								</GeomanistBit>
							</BoxBit>
						) }
					</BoxImageBit>
				)
			}
		}

		percentage = (i) => {
			switch(true) {
			case this.props.length === 5:
				return this.props.percentage.slice(0, 4)[i]
			case this.props.length > 6:
				return this.props.percentage.slice(0, 6)[i]
			default:
				return this.props.percentage.slice(1, 3)[i]
			}
		}

		verticalRenderer = (source, i) => {
			return  (
				<BoxImageBit unflex key={i} source={`//${source.url}`} resizeMode={ BoxImageBit.TYPES.COVER } transform={{crop: 'fit'}} style={{width: this.halfWidth, height: this.height}}>
					{ this.props.percentage[i] > 0 && (
						<BoxBit unflex centering style={Styles.boxDiscount}>
							<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={Styles.white}>
								{ this.props.percentage[i] }% OFF
							</GeomanistBit>
						</BoxBit>
					) }
				</BoxImageBit>
			)
		}

		view() {
			return (
				<BoxBit unflex row
					style={[
						Styles.container,
						{height: this.height},
						{width: this.props.width},
						this.props.style,
					]}
				>
					{ this.contentRenderer() }
				</BoxBit>
			)
		}
	}
)
