import ConnectHelper from 'coeur/helpers/connect';
import CoreBoxImageBit from 'coeur/modules/bits/box.image';

export default ConnectHelper(
	class BoxImageBit extends CoreBoxImageBit() {}
)
