import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		top: 123,
		left: 0,
		width: 230,
		bottom: 0,
		position: 'fixed',
		zIndex: 1,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		direction: 'rtl',
	},
	content: {
		direction: 'ltr',
	},
	padder: {
		marginTop: Sizes.margin.default,
	},
})
