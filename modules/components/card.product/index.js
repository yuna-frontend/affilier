import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/connected.stateful'

import FormatHelper from 'coeur/helpers/format'
import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import BoxLoadingBit from 'modules/bits/box.loading'
// import ButtonBit from 'modules/bits/button'
// import IconLego from 'modules/bits/icon'
import BoxImageBit from 'modules/bits/box.image'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'

import ColorLego from 'modules/legos/color'

import Styles from './style'


export default ConnectHelper(
	class CardProductComponent extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				source: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.object,
				]),
				brand: PropTypes.string,
				title: PropTypes.string,
				price: PropTypes.number,
				purchasePrice: PropTypes.number,
				commission: PropTypes.shape({
					type: PropTypes.oneOf(['TOTAL', 'AMOUNT']),
					amount: PropTypes.number,
				}),

				onGetUrl: PropTypes.func,
				onPress: PropTypes.func,
				style: PropTypes.style,
				imageStyle: PropTypes.style,

				variantId: PropTypes.id,
				width: PropTypes.number,
				image: PropTypes.string,
				colors: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({
					hex: PropTypes.string,
					image: PropTypes.string,
				}))),
				isLoading: PropTypes.bool,
			}
		}

		// onPress = () => {
		// 	this.props.onPress &&
		// 	this.props.onPress(this.props.variantId)
		// }

		colorRenderer = (colors, index) => {
			return (
				<ColorLego border={ this.props.noBorder ? 0 : undefined } colors={colors} key={index} style={!!index ? Styles.color : {}} />
			)
		}

		viewOnLoading = () => {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<BoxBit unflex style={[Styles.image, Styles.loading, this.props.imageStyle]}/>
					<BoxLoadingBit unflex height={20} width={120} style={Styles.mb4}/>
					<BoxLoadingBit unflex height={20} width={80} />
				</BoxBit>
			)
		}

		view() {
			return this.props.isLoading ? this.viewOnLoading() : (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<TouchableBit unflex onPress={this.props.onPress}>
						<BoxBit unflex>
							<BoxImageBit unflex overlay
								source={this.props.image && `//${this.props.image}`}
								broken={!this.props.image}
								resizeMode={BoxImageBit.TYPES.COVER}
								transform={{ crop: 'fit' }}
								style={[Styles.image, this.props.imageStyle]}
							/>
							{ this.props.colors &&
							<BoxBit unflex row style={Styles.colors}>
								{ this.props.colors.map(this.colorRenderer) }
							</BoxBit>
							}
						</BoxBit>
						<BoxBit unflex style={Styles.description}>
							<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_3} style={[Styles.aline, Styles.brand]}>
								{ !!this.props.brand && this.props.brand }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.aline, Styles.title]}>
								{ this.props.title }
							</GeomanistBit>
							{ this.props.price !== this.props.purchasePrice ? (
								<BoxBit unflex row>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.retail}>IDR { FormatHelper.currencyFormat(this.props.price) }</GeomanistBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.discount}>IDR { FormatHelper.currencyFormat(this.props.purchasePrice) }</GeomanistBit>
								</BoxBit>
							) : (
								<BoxBit unflex row>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.price}>IDR { FormatHelper.currencyFormat(this.props.price) }</GeomanistBit>
									<BoxBit unflex style={Styles.empty} />
								</BoxBit>
							) }
							{ this.props.commission && (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.mt8 }>
									Komisi: { this.props.commission.type === 'TOTAL' ? `IDR ${FormatHelper.currencyFormat(this.props.commission.amount)}` : this.props.commission.amount + '%' }
								</GeomanistBit>
							) }
						</BoxBit>
					</TouchableBit>
					<TouchableBit onPress={this.props.onGetUrl}>
						<BoxBit style={Styles.mT6}>
							<BoxBit unflex style={Styles.getUrl}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} align="center">
									Dapatkan Tautan
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
