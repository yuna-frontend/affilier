/* eslint-disable no-nested-ternary */
import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import TouchableBit from 'modules/bits/touchable'

import ModalPagelet from 'modules/pagelets/modal'

import ColorPart from '../_filter/_color'
import SizePart from '../_filter/_size'
import PricePart from '../_filter/_price'
import SortPart from '../_sort'

import { isEqual, isNil } from  'lodash'

import Styles from './style'

export default ConnectHelper(
	class FilterModalPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				onChangeFilter: PropTypes.func,
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				filter: PropTypes.object,
			}
		}
		
		constructor(p) {
			super(p, {
				filterModal: p.filter,
			})
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onChangeSortFilterModal = (key, val) => {
			this.setState({
				filterModal: {
					...this.state.filterModal,
					sort: isNil(this.state.filterModal.sort[key])
						? { [key]: val }
						: this.state.filterModal.sort[key] !== val
							? { [key]: val }
							: {},
				},
			})
		}

		onChangeFilterModal = (key, val, multiple) => {
			this.setState({
				filterModal: {
					...this.state.filterModal,
					...(!!key ? {
						[key]: multiple
							? ( this.state.filterModal[key].indexOf(val) > -1
								? [...(this.state.filterModal[key].filter(d => d !== val))]
								: [...this.state.filterModal[key], val] )
							: isEqual(this.state.filterModal[key], val) ? '' : val,
					} : {
						...val,
					}),
				},
			})
		}

		onApplyFilterModal = () => {
			this.props.onChangeFilter(null, this.state.filterModal)
			this.props.utilities.alert.close()
		}

		onResetFilterModal = () => {
			this.setState({
				// same as index.js of shop's filter state
				filterModal: {
					price: '',
					colorIds: [],
					brandIds: [],
					sizeIds: [],
					sort: {},
				},
			}, () => {
				this.props.onChangeFilter(null, this.state.filterModal)
				this.props.utilities.alert.close()
			})
		}

		headerRenderer = () => {
			return (
				<BoxBit centering unflex row style={[Styles.menuListHeader, {height: 55}]}>
					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1}>
						TERAPKAN FILTER
					</GeomanistBit>
					<TouchableBit unflex centering enlargeHitSlop onPress={ this.onClose }>
						<IconBit
							name="close"
							size={ 20 }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		footerRenderer = () => {
			return (
				<BoxBit unflex style={ Styles.footer }>
					<BoxBit row>
						<BoxBit style={ Styles.btn }>
							<ButtonBit
								title="Reset"
								theme={ ButtonBit.TYPES.THEMES.OUTLINE }
								size={ ButtonBit.TYPES.SIZES.MEDIUM }
								onPress={ this.onResetFilterModal }
							/>
						</BoxBit>
						<BoxBit style={ Styles.btn }>
							<ButtonBit
								title="Terapkan"
								size={ ButtonBit.TYPES.SIZES.MEDIUM }
								onPress={ this.onApplyFilterModal }
							/>
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<ModalPagelet unclosable
					header={ this.headerRenderer() }
					footer={ this.footerRenderer() }
					style={ Styles.container }
					pageStyle={ Styles.modalMenuPage }
					containerStyle={ Styles.menuListContainer }
				>
					<BoxBit unflex>
						<BoxBit unflex style={ Styles.headerFilter }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
								Urutkan
							</GeomanistBit>
						</BoxBit>
						<SortPart inFilterModal sort={ this.state.filterModal.sort } onChangeSort={ this.onChangeSortFilterModal }/>
						<BoxBit unflex style={ Styles.headerFilter }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
								Filter
							</GeomanistBit>
						</BoxBit>
						{/* { this.list.map(this.collapsible) } */}
						<ColorPart inFilterModal colorIds={ this.state.filterModal.colorIds } onChangeColorIds={ val => this.onChangeFilterModal(null, { colorIds: val }, true) }/>
						<SizePart inFilterModal key={ this.props.categoryIds } categoryIds={ this.props.categoryIds } sizeIds={ this.state.filterModal.sizeIds } onChangeSizeIds={ val => this.onChangeFilterModal('sizeIds', val, true) }/>
						<PricePart inFilterModal price={ this.state.filterModal.price } onChangePrice={ val => this.onChangeFilterModal('price', val) }/>
					</BoxBit>
				</ModalPagelet>
			)
		}
	}
)
