import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
		marginBottom: 25,
		width: '23.5%',
	},

	image: {
		height: 336,
	},

	content: {
		paddingTop: 8,
		paddingBottom: 8,
	},

	aline: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},

	brand: {
		color: Colors.black.palette(2, .6),
	},

	title: {
		color: Colors.black.palette(2),
	},

	discount: {
		color: Colors.red.palette(7),
	},

	retail: {
		textDecoration: 'line-through',
		color: Colors.black.palette(2, .6),
	},

	price: {
		color: Colors.black.palette(2, .6),
	},

	empty: {
		height: 24,
	},

	colors: {
		marginTop: 8,
		alignItems: 'center',
		flexWrap: 'wrap',
	},

	color: {
		marginRight: 4,
	},

	'@media screen and (min-width:1600px)': {
		container: {
			width: '18.5%',
		},
	},
})
