import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';

// import { without } from 'lodash';


export default ConnectHelper(
	class PriceLego extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				align: PropTypes.oneOf([
					'right',
					'left',
					'center',
				]),
				price: PropTypes.number,
				retailPrice: PropTypes.number,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			align: 'right',
		}

		view() {
			return (
				<BoxBit style={ this.props.style }>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } align={ this.props.align } style={ Styles.retail }>
						{ `IDR ${FormatHelper.currency(this.props.retailPrice)}` }
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ this.props.align } style={ Styles.discounted }>
						{ `IDR ${FormatHelper.currency(this.props.price)}` }
					</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
