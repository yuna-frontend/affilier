import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ProductService from 'app/services/product'
import VariantService from 'app/services/variant'

import PageContext from 'coeur/contexts/page';

import BoxTagLego from 'modules/legos/box.tag';

import GetterCategoryComponent from 'modules/components/getter.category'


export default ConnectHelper(
	class TagPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				productId: PropTypes.id,
				variantId: PropTypes.id,
				categoryId: PropTypes.id,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			PageContext,
		]

		constructor(p) {
			super(p, {
				isLoadingProductTags: true,
				isLoadingVariantTags: true,
				productTagIds: [],
				variantTagIds: [],
			})
		}

		componentDidMount() {
			if (this.props.productId) {
				this.getProductTags()
			}

			if (this.props.variantId) {
				this.getVariantTags()
			}
		}

		componentDidUpdate(nP) {
			if (nP.productId && nP.productId !== this.props.productId) {
				this.getProductTags()
			}

			if (nP.variantId && nP.variantId !== this.props.variantId) {
				this.getVariantTags()
			}
		}

		getProductTags() {
			this.setState({
				isLoadingProductTags: true,
			}, () => {
				ProductService.getProductTags(this.props.productId, this.props.token).catch(err => {

					this.warn(err)

					if (err.code === 'ERR_101') {
						// tag is empty
						return []
					}

					throw err
				}).then(tags => {
					this.setState({
						isLoadingProductTags: false,
						productTagIds: tags.map(t => t.id),
					})
				})
			})
		}

		getVariantTags() {
			this.setState({
				isLoadingVariantTags: true,
			}, () => {
				VariantService.getVariantTags(this.props.variantId, this.props.token).then(tags => {
					this.setState({
						isLoadingVariantTags: false,
						variantTagIds: tags.map(t => t.id),
					})
				})
			})
		}

		viewRenderer = ({ categoryId }) => {
			return (
				<BoxTagLego
					categoryId={categoryId}
					tagIds={this.props.variantId ? this.state.variantTagIds : this.state.productTagIds}
					parentTagIds={this.props.variantId ? this.state.productTagIds : []}
					onChange={this.props.onChange}
				/>
			)
		}

		view() {
			return (
				<GetterCategoryComponent
					key={`${this.props.categoryId}${this.state.isLoadingProductTags}${this.state.isLoadingVariantTags}`}
					categoryId={ this.props.categoryId }
					children={ this.viewRenderer }
				/>
			)
		}
	}
)
