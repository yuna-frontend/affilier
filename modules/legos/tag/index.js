import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import BoxLego from 'modules/legos/box';

import ItemToggler from './_item.toggler';

import Styles from './style';

import { without } from 'lodash';


export default ConnectHelper(
	class TagsLego extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				choice: PropTypes.array,
				style: PropTypes.style,
			};
		}

		constructor(p) {
			super(p, {
				tags: p.value || [],
			}, [
				'choiceRenderer',
				'onToggle',
			]);
		}

		componentDidUpdate(pP, pS) {
			if(pS.tags !== this.state.tags) {
				this.props.onUpdate &&
				this.props.onUpdate( this.state.tags )
			}
		}

		onToggle(id) {
			if (!this.state.tags.length) {
				this.setState({
					tags: [id],
				});
			} else {
				const index = this.state.tags.findIndex(el => el === id);
				if (index === -1) {
					this.setState(state => {
						return {
							tags: state.tags.concat(id),
						}
					})
				} else {
					this.setState( state => {
						return {
							tags: without(state.tags, id),
						}
					})
				}
			}
		}

		choiceRenderer({header, options}, i) {
			return (
				<BoxBit key={i} unflex style={Styles.choice}>
					<GeomanistBit
						type={GeomanistBit.TYPES.SUBHEADER_2}
						weight="medium"
						style={Styles.title}
					>
						{header.title}
					</GeomanistBit>
					<BoxBit row unflex style={Styles.wrap}>
						<ItemToggler
							key={i}
							options={options}
							headerId={header.id}
							tags={ this.state.tags }
							productTags={this.props.productTags}
							onToggle={ this.onToggle }
						/>
					</BoxBit>
				</BoxBit>
			);
		}

		view() {
			return (
				<BoxLego title={this.props.title} contentContainerStyle={this.props.style}>
					{ this.props.choice.map(this.choiceRenderer) }
				</BoxLego>
			);
		}
	}
);
