import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
		alignSelf: 'stretch',
		backgroundColor: Colors.solid.grey.palette(1),
	},

	body: {
		backgroundColor: Colors.solid.grey.palette(1),
		width: '100vw',
	},

	header: {
		width: '100vw',
		alignSelf: 'center',
	},

	footer: {
		width: '100vw',
		alignSelf: 'center',
	},

	title: {
		padding: 0,
	},

	closer: {
		width: 44,
		height: 44,
		marginRight: -10,
	},

	'@media (max-width: 1366px)': {
		body: {
			width: '100vw',
		},

		header: {
			width: '100vw',
		},

		footer: {
			width: '100vw',
		},
	},
})
