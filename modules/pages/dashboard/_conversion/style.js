import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	pt6: {
		paddingTop: 6,
		// borderTopWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.black.primary,
	},

	footer: {
		backgroundColor: Colors.white.primary,
		padding: Sizes.margin.default,
		alignItems: 'flex-end',
	},

	outerTableWrapper: {
		overflow: 'auto',
	},

	innerTableWrapper: {
		minWidth: 960,
		minHeight: 280,
	},

})
