import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';

import PurchaseService from 'app/services/purchase';
import UserService from 'app/services/user.new'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated';
import RadioBit from 'modules/bits/radio';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import HintLego from 'modules/legos/hint';
import LoaderLego from 'modules/legos/loader';
import RowLego from 'modules/legos/row';
import TableLego from 'modules/legos/table';

import ModalClosableConfirmationPagelet from '../modal.closable.confirmation';

import Styles from './style';

// import { capitalize } from 'lodash'


export default ConnectHelper(

	class PurchaseOrderPickerPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				ids: PropTypes.arrayOf(PropTypes.id),
				onUpdate: PropTypes.func,
				onCancel: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return Promise.all([
				UserService.getPurchaseOrder(state.me.id, {}, state.me.token).catch(() => []),
				Promise.all(oP.ids.map(id => {
					return PurchaseService.getDetail(id, {
						shallow: true,
					}, state.me.token)
				})),
			]).then(results => {
				return {
					purchaseOrders: results[0],
					requests: results[1],
				}
			}).catch(() => {
				return {
					purchaseOrders: [],
					requests: [],
				}
			})
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				removedIds: [],
				title: undefined,
				selectedId: -1,
			})
		}

		headers = [{
			title: 'Product',
			width: 2,
		}, {
			title: 'Detail',
			width: 1.5,
		}, {
			title: 'Price',
			width: 1.5,
		}, {
			title: '',
			width: .3,
		}]

		onClose = () => {
			this.props.utilities.alert.hide()

			this.props.onCancel &&
			this.props.onCancel()
		}

		onSelect = () => {
			// TODO
			if (this.state.selectedId === null) {
				this.setState({
					isSaving: true,
				}, () => {
					PurchaseService
						.createOrder(this.state.title, this.props.data.requests.map(r => r.id).filter(id => this.state.removedIds.indexOf(id) === -1), this.props.token)
						.then(() => {
							this.props.utilities.notification.show({
								title: 'Yeay!',
								message: 'Success adding purchase request(s) into purchase order.',
								type: 'SUCCESS',
							})

							this.props.utilities.alert.hide()

							this.props.onUpdate &&
							this.props.onUpdate()
						}).catch(this.onError)
				})
			} else if (this.state.selectedId === -1) {
				// INVALID
			} else {
				this.setState({
					isSaving: true,
				}, () => {
					// Add into existing
					PurchaseService
						.addRequestToOrder(this.state.selectedId, this.props.data.requests.map(r => r.id).filter(id => this.state.removedIds.indexOf(id) === -1), this.props.token)
						.then(() => {
							this.props.utilities.notification.show({
								title: 'Yeay!',
								message: 'Success adding purchase request(s) into purchase order.',
								type: 'SUCCESS',
							})

							this.onClose()

							this.props.onUpdate &&
							this.props.onUpdate()
						}).catch(this.onError)
				})
			}

		}

		onToggle = id => {
			this.setState({
				selectedId: id,
			})
		}

		onRemoveItem = id => {
			this.setState({
				removedIds: [...this.state.removedIds, id],
			})
		}

		onChangeTitle = title => {
			this.state.title = title
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isSaving: false,
			})

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong. Please try again.',
			})
		}

		rowRenderer = request => {
			return {
				data: [{
					title: `${request.brand } – ${ request.title }`,
				}, {
					title: `${ request.category } – ${ request.color } – ${ request.size }`,
				}, {
					children: (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
							IDR <TextBit style={ request.retail_price !== request.price ? Styles.retail : undefined }>{ FormatHelper.currency( request.retail_price ) }</TextBit>
							{ request.retail_price !== request.price ? (
								<TextBit style={ Styles.discounted }>{ FormatHelper.currency( request.price ) }</TextBit>
							) : false }
						</GeomanistBit>
					),
				}, {
					children: (
						<TouchableBit unflex centering onPress={ this.onRemoveItem.bind(this, request.id) }>
							<IconBit
								name="close-fat"
								color={ Colors.red.palette(7) }
							/>
						</TouchableBit>
					),
				}],
			}
		}

		purchaseRequestRenderer = () => {
			return (
				<TableLego compact
					headers={ this.headers }
					rows={ this.props.data.requests.filter(r => this.state.removedIds.indexOf(r.id) === -1).map( this.rowRenderer ) }
				/>
			)
		}

		purchaseOrderRenderer = (purchaseOrder, i) => {
			return (
				<TouchableBit key={ purchaseOrder.id } row style={[Styles.list, !(i & 1) ? Styles.odd : undefined]} onPress={ this.onToggle.bind(this, purchaseOrder.id) }>
					<BoxBit row>
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ purchaseOrder.title }
							</GeomanistBit>
						</BoxBit>
						<BoxBit centering>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ TimeHelper.format(purchaseOrder.created_at, 'DD/MM/YY HH:mm') }
							</GeomanistBit>
						</BoxBit>
					</BoxBit>
					<BoxBit unflex centering>
						<RadioBit dumb isActive={ this.state.selectedId === purchaseOrder.id } onPress={ this.onToggle.bind(this, purchaseOrder.id) } />
					</BoxBit>
				</TouchableBit>
			)
			// return (
			// 	// <SelectionStylesheetLego
			// 	// 	key={ stylesheet.id }
			// 	// 	{ ...stylesheet }
			// 	// 	minValue={ stylesheet.min_value }
			// 	// 	realValue={ stylesheet.real_value }
			// 	// 	shipmentAt={ stylesheet.shipment_at }
			// 	// 	updatedAt={ stylesheet.updated_at }
			// 	// 	selected={ isSelected }
			// 	// 	addedRetail={ isSelected ? this.props.retailPrice : 0 }
			// 	// 	addedReal={ isSelected ? this.props.price : 0 }
			// 	// 	onPress={ this.onToggle }
			// 	// />
			// )
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add to Purchase Order"
					header={ (
						<HintLego title="Select purchase order you want to add this request into or create a new purchase order." />
					) }
					subheader={ this.purchaseRequestRenderer() }
					confirm="Select"
					disabled={ this.state.selectedId === -1 || this.props.data.requests.length === this.state.removedIds.length }
					loading={ this.state.isSaving }
					onCancel={ this.onClose }
					onConfirm={ this.onSelect }
					contentContainerStyle={ Styles.content }
				>
					{ this.props.data.purchaseOrders ? this.props.data.purchaseOrders.map(this.purchaseOrderRenderer) : false }
					<TouchableBit style={Styles.list} onPress={ this.onToggle.bind(this, null) }>
						<BoxBit unflex row>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.add }>
								Add New Purchase Order
							</GeomanistBit>
							<BoxBit />
							<RadioBit dumb isActive={ this.state.selectedId === null } onPress={ this.onToggle.bind(this, null) } />
						</BoxBit>
						{ this.state.selectedId === null ? (
							<RowLego
								data={[{
									title: 'Purchase Order Title',
									children: (
										<InputValidatedBit autofocus
											placeholder="Input Here…"
											type={ InputValidatedBit.INPUT }
											value={ this.state.title }
											onChange={ this.onChangeTitle }
										/>
									),
									description: 'Put a title on this purchase order for future reference.',
								}]}
							/>
						) : false }
					</TouchableBit>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
);
