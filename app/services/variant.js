import ServiceModel from 'coeur/models/service';


class VariantService extends ServiceModel {

	static displayName = 'variant'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/variant/', 0)
	}

	book(id, note, token) {
		return this.post(id + '/book', {
			data: {
				note,
			},
			token,
		})
	}

	search(search, offset, limit, available, token) {
		return this.get('', {
			query: {
				search,
				offset,
				limit,
				available,
			},
			token,
		})
	}

	reorderAssets(variantId, assetIds, token) {
		return this.put(variantId + '/asset/', {
			data: {
				asset_ids: assetIds,
			},
			token,
		})
	}

	getVariantTags(id, token) {
		return this.get(id + '/tag', {
			token,
		})
	}

	getVariantTagsGrouped(id, token) {
		return this.get(id + '/tag', {
			token,
			query: {
				grouped: true,
			},
		})
	}

	getVariantMeasurements(id, token) {
		return this.get(id + '/measurement', {
			token,
		})
	}

	getVariantAssets(id, token) {
		return this.get(id + '/asset', {
			token,
		})
	}

	getVariantInventories(id, offset = 0, limit = 10, token) {
		return this.get(id + '/inventory', {
			query: {
				offset,
				limit,
			},
			token,
		})
	}

	removeVariantAssets(id, assetId, token) {
		return this.delete(id + '/asset/' + assetId, {
			token,
		})
	}

	getVariantById(id, query, token) {
		return this.get(id, {
			query,
			token,
		})
	}

	getMediaById(id, token) {
		return this.get(id, {
			token,
		})
	}

	upload(data, token) {
		return this.post(data.variantId + '/asset', {
			data: {
				image: 	data.image,
				order: data.order,
			},
			token,
		})
	}

	// updateVariantImage(ids, data, query, token) {
	// 	console.log(ids)
	// 	return this.post(ids.join(',') + '/asset', {
	// 		data,
	// 	})
	// }

	updateVariantImage(ids, data, query, token) {
		return this.post(ids.join(',') + '/asset', {
			data,
			query,
			token,
		})
	}

	deleteVariantFromProduct(variant, token) {
		return this.delete(variant, {
			token,
		})
	}
	createOrUpdateVariant(data, token) {
		return this.post(data.id, {
			data,
			token,
		})
	}
	createVariant(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	updateVariant(id, data, token) {
		return this.post(id, {
			data,
			token,
		})
	}

	// getVariantsWithInventory({
	// 	offset,
	// 	count,
	// 	brandId,
	// 	color,
	// 	size,
	// 	productTitle,
	// }, token) {
	// 	return this.get('', {
	// 		query: {
	// 			inventory: true,
	// 			offset,
	// 			count,
	// 			brandId,
	// 			color,
	// 			size,
	// 			productTitle,
	// 		},
	// 		token,
	// 	})
	// }

	// searchByProductId({
	// 	productId,
	// 	offset,
	// 	count,
	// 	search,
	// }, token) {
	// 	return this.get('', {
	// 		query: {
	// 			search,
	// 			offset,
	// 			count,
	// 			productId,
	// 		},
	// 		token,
	// 	})
	// }

	// addVariants(productVariants, token) {
	// 	return this.post('', {
	// 		data: {
	// 			creates: productVariants,
	// 		},
	// 		token,
	// 	})
	// }

	// updateVariants(productVariants, token) {
	// 	return this.post('', {
	// 		data: {
	// 			updates: productVariants,
	// 		},
	// 		token,
	// 	})
	// }

	// updateMedias({
	// 	id,
	// 	mediaIds,
	// }, token) {
	// 	return this.post(id + '/media', {
	// 		data: {
	// 			mediaIds,
	// 		},
	// 		token,
	// 	})
	// }

	// addInventories({
	// 	id,
	// 	inventoryIds,
	// }, token) {
	// 	return this.post(id + '/inventory', {
	// 		data: {
	// 			inventoryIds,
	// 		},
	// 		token,
	// 	})
	// }
}

export default new VariantService()
