import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		// borderBottomWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.black.palette(2, .16),
		// borderBottomStyle: 'solid',
	},

	tabActive: {
		backgroundColor: Colors.primary,
	},

	tab: {
		borderColor: Colors.primary,
		borderWidth: 1,
		borderRadius: 2,
		paddingLeft: 16,
		paddingRight: 16,
		paddingTop: 10,
		paddingBottom: 10,
		marginRight: 8,
	},

	disabled: {
		opacity: .6,
	},

	isActive: {
		color: Colors.white.primary,
	},

	isInactive: {
		color: Colors.primary,
	},
})
