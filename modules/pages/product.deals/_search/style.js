import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		width: Math.min(480, Sizes.app.width),
		paddingTop: 0,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: 48,
		backgroundColor: Colors.white.primary,
	},

	noRadius: {
		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,
	},

	header: {
		width: Sizes.app.width,
		backgroundColor: Colors.white.primary,
		paddingTop: 8,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 12,
	},

	containerInput: {
		height: 36,
		backgroundColor: Colors.grey.palette(7),
	},

	input: {
		paddingLeft: 8,
		height: 24,
		backgroundColor: Colors.grey.palette(7),
	},

	cancel: {
		justifyContent: 'center',
		marginLeft: Sizes.margin.default,
	},

	subheader: {
		paddingTop: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 8,
	},

	delete: {
		marginLeft: 8,
		justifyContent: 'center',
	},

	list: {
		borderBottomWidth: 0,
	},

	icon: {
		marginRight: Sizes.margin.default,
	},

	greyBase: {
		color: Colors.grey.palette(12),
	},
})
