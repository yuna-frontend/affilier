import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(4),
		padding: 12,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	containerEven: {
		backgroundColor: Colors.white.primary,
	},

	header: {
		backgroundColor: Colors.white.primary,
	},

	disabled: {
		opacity: .6,
	},

	noBorder: {
		borderBottomWidth: 0,
	}
})
