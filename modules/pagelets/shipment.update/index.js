import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import ShipmentService from 'app/services/shipment';
import ExchangeService from 'app/services/exchange'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import TextBit from 'modules/bits/text';

import CellLego from 'modules/legos/cell';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import GetterPacketComponent from 'modules/components/getter.packet';
import AddressesComponent from 'modules/components/addresses';

import AddressAreaPagelet from '../address.area';
import ModalClosableConfirmationPagelet from '../modal.closable.confirmation';

import DetailPart from './_detail'

import Styles from './style';

import { isEmpty, uniq } from 'lodash'


export default ConnectHelper(
	class ShipmentUpdatePagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				editable: PropTypes.bool,
				method: PropTypes.oneOf([
					'MANUAL',
					'TIKI',
					'NINJA',
				]),
				orderId: PropTypes.id,
				shipment: PropTypes.shape({
					destination: PropTypes.object,
					orderDetails: PropTypes.arrayOf(PropTypes.shape({
						id: PropTypes.id,
						title: PropTypes.string,
						packet_id: PropTypes.id,
					})),
				}),
				isExchange: PropTypes.bool,
				exchangeId: PropTypes.number,
				exchangeDetailIds: PropTypes.array,
				// to create
				// add into shipment: destination, orderDetails,
				onShipmentCreated: PropTypes.func,
			}
		}

		static defaultProps = {
			isExchange: false,
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			if (oP.id) {
				if (!isEmpty(oP.shipment)) {
					return [oP.shipment.id, Promise.resolve(oP.shipment)]
				} else {
					return [oP.id, ShipmentService.getShipmentDetail(oP.id, state.me.token)]
				}
			} else if (!isEmpty(oP.shipment)) {
				// CREATING
				// Get rate please
				if(oP.method === 'MANUAL') {
					return Promise.resolve(oP.shipment)
				} else {
					// GET RATE
					return ShipmentService.getRates(oP.shipment.orderDetails.map(oD => oD.id), oP.shipment.destination, oP.method, state.me.token).then(res => {
						const couriers = uniq(res.rates.map(rate => rate.courier))
						const rate = res.rates.reduce((r1, r2) => {
							return r1.rate < r2.rate ? r1 : r2
						})
						return {
							...oP.shipment,
							packet: res.packet,
							couriers,
							rates: res.rates,
							status: 'PENDING',
							courier: rate.courier,
							service: rate.service,
							awb: undefined,
							url: undefined,
							amount: rate.rate,
							metadata: {
								min_delivery_day: rate.min_day,
								max_delivery_day: rate.max_day,
							},
							created_at: new Date(),
						}
					})
				}
			} else {
				return null
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nP.data) && !nS.isLoaded) {
				return {
					isLoaded: true,
					packet: nP.data.packet,
					address: nP.data.destination,
					details: nP.data.orderDetails || [],
					rates: nP.data.rates || [],
					couriers: nP.data.couriers || [],
					data: {
						packetId: nP.data.packet_id,
						// origin_id: nP.data.origin_id,
						// destination_id: nP.data.destination_id,
						// type: nP.data.type,
						// is_facade: nP.data.is_facade,
						// prices: nP.data.prices,
						// note: nP.data.note,
						status: nP.data.status,
						courier: nP.data.courier,
						service: nP.data.service,
						awb: nP.data.awb,
						url: nP.data.url,
						amount: nP.data.amount,
						metadata: nP.data.metadata || {},
						createdAt: nP.data.created_at,
					},
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				isEditable: p.id ? p.id && p.editable : p.method === 'MANUAL',
				isLoaded: false,
				isChanged: false,
				isSaving: false,
				data: {},
				packet: {},
				address: {},
				details: [],
				rates: [],
				shouldSendEmail: false,
			})
		}

		tableHeader = [{
			title: 'Items in Shipment',
			width: 5,
		}, {
			title: 'Dimension (L x W x H)',
			children: (
				<BoxBit row style={{justifyContent: 'space-between'}}>
					<GeomanistBit weight="bold" type={GeomanistBit.TYPES.PARAGRAPH_3}>
						Dimension <TextBit weight="normal">(W x H x L)</TextBit>
					</GeomanistBit>
					<GeomanistBit weight="bold" type={GeomanistBit.TYPES.PARAGRAPH_3}>
						Weight
					</GeomanistBit>
				</BoxBit>
			),
			width: 4,
		}]

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onCreateShipment = () => {
			if((this.props.id && this.props.editable) || !this.props.id) {
				this.setState({
					isSaving: true,
				}, () => {
					Promise.resolve().then(() => {
						if(this.props.isExchange) {
							return ExchangeService.createReplacementShipment(this.props.exchangeId, {
								awb: this.state.data.awb,
								courier: this.state.data.courier,
								user_address_id: this.props.shipment.destination.id,
								exchange_detail_ids: this.props.exchangeDetailIds,
								type: 'MANUAL',
							}, this.props.token)
						} else if(!this.props.id) {
							// creating
							return ShipmentService.createShipment({
								method: this.props.method,
								packet: this.state.packet,
								order_detail_ids: this.state.details.map(d => d.id),
								destination: this.state.address,
								amount: this.state.data.amount,
								awb: this.state.data.awb,
								url: this.state.data.url,
								metadata: this.state.data.metadata,
								status: this.state.data.status,
								courier: this.state.data.courier,
								service: this.state.data.service,
								created_at: this.state.data.createdAt,
								shouldSendEmail: this.state.shouldSendEmail,
							}, this.props.token)
						} else {
							// editing
							return ShipmentService.updateShipment(this.props.id, {
								// type: SHIPMENTS;
								// note?: string;
								// prices?: object;
								// is_facade?: boolean;
								packet: this.state.packet,
								status: this.state.data.status,
								courier: this.state.data.courier,
								service: this.state.data.service,
								awb: this.state.data.awb,
								url: this.state.data.url,
								amount: this.state.data.amount,
								metadata: this.state.data.metadata,
								created_at: this.state.data.createdAt,
								shouldSendEmail: this.state.shouldSendEmail,
							}, this.props.token)
						}
					}).then(shipmentOrBoolean => {
						this.props.utilities.notification.show({
							title: 'Yeay!',
							message: 'Shipment created / editted',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.onShipmentCreated &&
						this.props.onShipmentCreated(shipmentOrBoolean, this.state.details, this.props.id, this.state.data)

						this.onClose()
					}).catch(err => {
						this.warn(err)

						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Something went wrong. Please try again later.',
						})
					}).finally(() => {
						this.setState({
							isSaving: false,
						})
					})
				})
			} else {
				this.onClose()
			}
		}

		onChange = (key, value) => {
			if(key === 'packet') {
				this.setState({
					packet: { ...value },
				})

				if (!this.state.isChanged) {
					this.setState({
						isChanged: true,
					})
				}
			} else {
				this.state.data[key] = value

				if(!this.state.isChanged) {
					this.setState({
						isChanged: true,
					})
				} else {
					this.forceUpdate()
				}
			}
		}

		onToggleShouldSendEmail = value => {
			this.state.shouldSendEmail = value
		}

		title() {
			// eslint-disable-next-line no-nested-ternary
			return this.props.id
				? this.props.editable
					? 'Edit Shipment'
					: 'Shipment Detail'
				: 'Create Shipment'
		}

		confirm() {
			// eslint-disable-next-line no-nested-ternary
			return this.props.id
				? this.props.editable
					? 'Save Change'
					: 'Okay'
				: 'Ship'
		}

		productRenderer = detail => {
			return {
				data: [{
					title: detail.title,
				}, {
					children: (
						<GetterPacketComponent packetId={detail.packet_id} children={this.packetRenderer} />
					),
				}],
			}
		}

		packetRenderer = packet => {
			return (
				<BoxBit row style={{ justifyContent: 'space-between' }}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
						{ packet.length } x { packet.width } x { packet.height }cm
					</GeomanistBit>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
						{ packet.weight }kg
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnError() {
			if(this.props.isCrashing && this.props.isCrashing.code === 'ERR_107') {
				const type = this.props.isCrashing.detail.type
				const areas = this.props.isCrashing.detail.areas
				const keyword = this.props.isCrashing.detail.keyword

				this.props.utilities.alert.modal({
					component: (
						<AddressAreaPagelet
							addressType={ 'order' }
							addressTypeId={ this.props.orderId }
							id={ this.props.shipment.destination.id }
							type={ type }
							keyword={ keyword }
							areas={ areas }
							onUpdate={ id => {
								this.props.shipment.destination.metadata[keyword] = id
								this.props.refresh()
							} }
						/>
					),
				})
			}

			return (
				<LoaderLego />
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.state.isLoaded ? (
				<ModalClosableConfirmationPagelet
					// eslint-disable-next-line no-nested-ternary
					disabled={ this.state.isLoading || (this.state.isEditable && !this.state.isChanged) }
					loading={ this.state.isSaving }
					onClose={ this.onClose }
					onCancel={ this.onClose }
					onConfirm={ this.onCreateShipment }
					confirm={ this.confirm() }
					// eslint-disable-next-line no-nested-ternary
					title={ this.title() }
				>
					<AddressesComponent data={[this.state.address]} >
						<CellLego title="Shipment Status">
							<SelectStatusLego isRequired unflex disabled={ !this.state.isEditable || (!this.props.id && this.props.method !== 'MANUAL') }
								type={ SelectStatusLego.TYPES.SHIPMENT_STATUSES }
								status={ this.state.data.status }
								onChange={ this.onChange.bind(this, 'status') }
							/>
							<InputCheckboxBit disabled={ !this.state.isEditable } title="Send email to customer" value={ this.state.shouldSendEmail } onChange={ this.onToggleShouldSendEmail } style={Styles.checkbox} />
						</CellLego>
					</AddressesComponent>

					<RowsLego
						data={[{
							data: [{
								title: '',
							}],
						}]}
					/>

					<TableLego
						headers={ this.tableHeader }
						rows={ this.state.details.map(this.productRenderer) }
						style={ Styles.table }
					/>

					<DetailPart
						key={ this.state.isLoaded }
						editable={ this.state.isEditable }
						id={ this.props.id }
						data={ this.state.data }
						packet={ this.state.packet }
						couriers={ this.state.couriers }
						rates={ this.state.rates }
						onChange={ this.onChange }
					/>

				</ModalClosableConfirmationPagelet>
			) : this.viewOnLoading()
		}
	}
)
