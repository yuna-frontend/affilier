import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: Colors.grey.palette(1, .2),
	},

	title: {
		position: 'absolute',
		top: -14,
		backgroundColor: Colors.white.primary,
		paddingLeft: 8,
		paddingRight: 8,
	},
})
