import ConnectHelper from 'coeur/helpers/connect';
import CoreTextAreaBit from 'coeur/modules/bits/text.area';

import Colors from 'coeur/constants/color';

import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class TextAreaBit extends CoreTextAreaBit({
		Styles,
		TouchableBit,
		placeholderColor: Colors.black.palette(2, .6),
	}) {}
)
