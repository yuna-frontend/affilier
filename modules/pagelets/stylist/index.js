import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import SelectStylistLego from 'modules/legos/select.stylist';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import Styles from './style'

export default ConnectHelper(
	class StylistPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				stylesheetId: PropTypes.id,
				stylist: PropTypes.string,
				onCancel: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isChanged: false,
			}, [
				'onSave',
			])

			this.data = {
				stylesheetId: p.stylesheetId,
				stylistId: null,
				stylist: p.stylist,
			}
		}

		onChange = (id, val) => {
			this.data.stylistId = id
			this.data.stylist = val

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onSave() {
			this.setState({
				isLoading: true,
			}, () => {
				StylesheetService.assignStylist({
					stylistId: this.data.stylistId,
					stylesheetId: this.props.stylesheetId,
				}, this.props.token).then(res => {
					if(res) {
						this.setState({
							isLoading: false,
						}, () => {
							this.props.onUpdate &&
							this.props.onUpdate()

							this.props.onCancel &&
							this.props.onCancel()
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					})
				})
			})
		}


		footerRenderer() {
			return [(
				<ButtonBit
					key={0}
					title="Cancel"
					weight="medium"
					size={ButtonBit.TYPES.SIZES.SMALL}
					theme={ButtonBit.TYPES.THEMES.SECONDARY}
					onPress={ this.props.onCancel }
				/>
			), (
				<BoxBit key={1} />
			), (
				<ButtonBit
					key={2}
					title="Change"
					weight="medium"
					type={ButtonBit.TYPES.SHAPES.PROGRESS}
					size={ButtonBit.TYPES.SIZES.SMALL}
					state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isChanged && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
					onPress={this.onSave}
				/>
			)]
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Change Stylist"
					loading={ this.state.isLoading }
					disabled={ !this.state.isChanged }
					onCancel={ this.props.onCancel }
					onConfirm={ this.onSave }
				>
					<SelectStylistLego
						stylist={this.props.stylist}
						onChange={this.onChange}
						style={Styles.content}
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)

