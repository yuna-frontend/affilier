import ServiceModel from 'coeur/models/service';


class UserService extends ServiceModel {

	static displayName = 'user'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	getNote(id, noteId, token) {
		return this.get('user/' + id + '/note/' + noteId, {
			token,
		})
	}

	getNotes(id, token) {
		return this.get('user/' + id + '/note', {
			token,
		})
	}

	updateNote(id, noteId, note, token) {
		return this.post('user/' + id + '/note/' + noteId, {
			data: { note },
			token,
		})
	}

	createNote(id, note, token) {
		return this.post('user/' + id + '/note', {
			data: { note },
			token,
		})
	}

	deleteNote(userId, noteId, token) {
		return this.delete('user/' + userId + '/note/' + noteId, {
			token,
		})
	}

	getUsers(filter, token) {
		return this.get('user', {
			token,
			query: filter,
		})
	}

	getUser(id, token) {
		return this.get('user/' + id, {
			token,
		}).then(datas => datas[0])
	}

	setStylist(userId, stylistId, reason, token,) {
		return this.post('user/' + userId + '/stylist', {
			data: {
				stylist_id: stylistId,
				reason,
			},
			token,
		})
	}

	unsetStylist(userId, token) {
		return this.delete('user/' + userId + '/stylist', {
			token,
		})
	}

	getProfile(userId, token) {
		return this.get('users/' + userId + '/profile', {
			token,
		})
	}

	getAnswers(userId, token) {
		return this.get('user/' + userId  + '/answer', {
			token,
		})
	}

	getAnswersByGroupId(userId, groupId, date, token) {
		return this.get('user/' + userId + '/answer/group/' + groupId, {
			token,
			query: date ? {
				date,
			} : {},
		})
	}

	getAnswerByQuestionIds(userId, ids, date, token) {
		return this.get('user/' + userId + '/answer/' + ids.join(','), {
			token,
			query: date ? {
				date,
			} : {},
		})
	}

	addAddress(userId, data, token) {
		return this.post('user/' + userId + '/address/', {
			data,
			token,
		})
	}

	getUserOrderDetail(userId, orderId, token) {
		return this.get('user/' + userId + '/orders/' + orderId, {
			token,
			query: {
				detail: true,
			},
		}).then(datas => datas[0])
	}

	getUserAssets(userId, id, token) {
		return this.get('user/' + userId + '/asset/' + id, {
			token,
		})
	}

	upload(userId, data, token) {
		return this.post('user/' + userId + '/asset/', {
			data: {
				...(data.type ? {
					type: data.type,
				} : {}),
				// type: data.type || 'image',
				image: data.image,
				order: data.order,
			},
			token,
		})
	}

	removeAsset(userId, id, token) {
		return this.delete('user/' + userId + '/asset/' + id, {
			token,
		})
	}

	getUserAddresses(userId, token) {
		return this.get('user/' + userId + '/address', {
			token,
		})
	}

	updateAddress(userId, addressId, data, token) {
		return this.post('user/' + userId + '/address/' + addressId, {
			data,
			token,
		})
	}

	checkValidityCoupon(userId, data, token) {
		return this.get('user/' + userId + '/coupon/' + data, {
			token,
		})
	}

	getAddress(userId, addressId, token) {
		return this.get('user/' + userId + '/address/' + addressId, {
			token,
		})
	}

	getStyleHistory(id, token) {
		return this.get('user/' + id + '/stylesheet', {
			token,
		})
	}

	getPurchaseOrder(id, query, token) {
		return this.get('user/' + id + '/purchase-order', {
			query,
			token,
		})
	}

	getStylistStylesheet(id, token) {
		return this.get('user/' + id + '/stylist-stylesheet', {
			token,
		})
	}

	getStylistStylecard(id, token) {
		return this.get('user/' + id + '/stylist-stylecard', {
			token,
		})
	}

	addRole(id, role, token) {
		return this.post('user/' + id + '/role/' + role, {
			token,
		})
	}

	removeRole(id, role, token) {
		return this.delete('user/' + id + '/role/' + role, {
			token,
		})
	}

	update(id, data, token) {
		return this.post('user/' + id, {
			data,
			token,
		})
	}

	getFeedbackLike(id, query, token) {
		return this.get('user/' + id + '/likes', {
			query,
			token,
		})
	}

	getStyleProfileStatus = this._createSmartGetters((ids, token) => {
		return this.get('user/' + ids.join(',') + '/style-profile', {
			token,
		})
	}, (id, token) => {
		return this.get('user/' + id + '/style-profile', {
			token,
		})
	})
}

export default new UserService()
