import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: 32,
		paddingRight :32,
		paddingBottom: 16,
		paddingTop: 16,
	},

	headerFilter: {
		flexWrap: 'wrap',
	},

	wrapperFilter: {
		flexGrow: 1,
	},

	outerTableWrapper: {
		overflow: 'auto',
	},

	innerTableWrapper: {
		minWidth: 960,
		minHeight: 320,
	},

	ml8: {
		marginLeft: 8,
	},

	'@media screen and (max-width: 640px)': {
		filter: {
			marginRight: 0,
			marginBottom: Sizes.margin.default,
		},

		search: {
			flexBasis: '100%',
		},

		pagination: {
			flexBasis: '100%',
		},
	},
})
