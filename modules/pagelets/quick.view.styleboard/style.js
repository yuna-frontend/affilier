import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	id: {
		marginTop: 8,
		marginLeft: 8,
		paddingTop: 2,
		paddingLeft: 4,
		paddingBottom: 2,
		paddingRight: 4,
		backgroundColor: Colors.black.palette(2, .8),
		alignSelf: 'flex-start',
	},

	text: {
		color: Colors.white.primary,
	},

	color: {
		borderColor: Colors.white.primary,
		marginBottom: 8,
		marginLeft: 8,
	},

	stylist: {
		alignItems: 'flex-end',
	},

	initial: {
		marginRight: 8,
		width: 32,
		height: 32,
	},

	name: {
		color: Colors.black.palette(2, .6),
	},

	detail: {
		paddingTop: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	table: {
		// marginLeft: -24,
		// marginRight: -24,
		marginTop: 8,
		marginBottom: 8,
	},

	stylesheet: {
		marginTop: Sizes.margin.default,
	},

	button: {
		marginLeft: 8,
		marginRight: 8,
		marginBottom: 8,
	},

})
