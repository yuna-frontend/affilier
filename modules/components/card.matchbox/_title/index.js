import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import SpectralBit from 'modules/bits/spectral';

import Styles from './style';


export default ConnectHelper(
	class TitlePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				type: PropTypes.string,
				title: PropTypes.string,
				isDiscounted: PropTypes.bool,
				basePrice: PropTypes.number,
				price: PropTypes.number,
			}
		}

		static defaultProps = {
			type: '',
			title: '',
			isDiscounted: false,
			basePrice: 0,
			price: 0,
		}

		static getDerivedStateFromProps(nP) {
			return {
				useSmallTitle: nP.title.length > 22,
			}
		}

		constructor(p) {
			super(p, {
				useSmallTitle: false,
			})
		}

		view() {
			return (
				<BoxBit unflex>
					<BoxBit unflex style={Styles.type} />
					{/* <GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_2} style={[Styles.subheader, this.state.useSmallTitle && Styles.subheaderMargin]} weight="medium">{ this.props.type.toUpperCase() }</GeomanistBit> */}
					<SpectralBit type={this.state.useSmallTitle ? SpectralBit.TYPES.HEADER_3 : SpectralBit.TYPES.HEADER_1} weight="bold">{ this.props.title }</SpectralBit>
					{ this.props.isDiscounted && (
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.basePrice}>IDR { FormatHelper.currencyFormat(this.props.basePrice) }</GeomanistBit>
					) }
					<GeomanistBit type={GeomanistBit.TYPES.HEADER_4} style={[Styles.price, this.props.isDiscounted && Styles.noPriceMargin]}>IDR { FormatHelper.currencyFormat(this.props.price) }</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
