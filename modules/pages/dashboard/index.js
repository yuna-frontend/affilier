import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'

import PageAuthorizedComponent from 'modules/components/page.authorized'

import CommissionPart from './_commission'
import ConversionPart from './_conversion'
import ProfilePart from './_profile'

import Styles from './style'

// const data = [{
// 	id: 1,
// 	updated_at: '2021-09-16T06:50:11.628Z',
// 	number: 'YCO2109160015006',
// 	details: [{
// 		id: 13764,
// 		brand: 'Modemaya',
// 		title: 'Olive Shirt',
// 		price: [269910, 299900],
// 		commission: 20000,
// 		quantity: 1,
// 	}, {
// 		id: 12347,
// 		brand: 'Ramune',
// 		title: 'Chaerin Frill Top',
// 		price: [191400, 319000],
// 		commission: 15000,
// 		quantity: 2,
// 	}],
// 	prices: {
// 		total: 652710,
// 		commission: 50000,
// 	},
// }, {
// 	id: 2,
// 	updated_at: '2021-09-16T04:07:28.154Z',
// 	number: 'YCO2109160010704',
// 	details: [{
// 		id: 12347,
// 		brand: 'RAMUNE',
// 		title: 'Chaerin Frill Top',
// 		price: [191400, 319000],
// 		commission: 15000,
// 		quantity: 3,
// 	}],
// 	prices: {
// 		total: 574200,
// 		commission: 45000,
// 	},
// }, {
// 	id: 3,
// 	timestamp: '2021-09-10T16:52:26.962Z',
// 	number: 'YCO2109100015216',
// 	details: [{
// 		id: 14660,
// 		brand: 'RAMUNE',
// 		title: 'Haru Loose Top',
// 		price: [129000, 249000],
// 		commission: 18500,
// 		quantity: 7,
// 	}],
// 	prices: {
// 		total: 903000,
// 		commission: 129500,
// 	},
// }]

export default ConnectHelper(
	class DashboardPage extends PageModel {
		static routeName = 'dashboard'

		constructor(p) {
			super(p)

			this.isDesktopView = Sizes.screen.width > 800
		}

		view() {
			return (
				<PageAuthorizedComponent
					paths={[{ title: 'Dashboard' }]}
					roles="affiliatepartner"
					onAuthorized={ this.onAuthorized }
				>
					{() => (
						<BoxBit style={Styles.main}>
							<BoxBit unflex style={ Styles.notif }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.white }>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
								</GeomanistBit>
							</BoxBit>
							<BoxBit unflex row style={ Styles.headerContainer }>
								<ProfilePart style={ Styles.profilePart }/>
								<CommissionPart style={ Styles.commissionPart }/>
							</BoxBit>
							<BoxBit style={ !this.isDesktopView ? Styles.overflow : {} }>
								<ConversionPart
									onError={ this.onError }
								/>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
