import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';

import UtilitiesContext from 'coeur/contexts/utilities';

// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class EditBrandPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, brand = id && BrandManager.get(id) || new BrandManager.record()

			return {
				brand,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.brand._isGetting !== nS.isGettingBrand) {
				return {
					isGettingBrand: nP.brand._isGetting,
					data: {
						title: nP.brand.title,
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingBrand: p.brand._isGetting,
				data: {
					title: p.brand.title,
					titles: undefined,
				},
				isDone: false,
				isLoading: false,
			}, [
				'onUpdate',
				'onUpdateMultiple',
				'onSubmit',
				'onSubmitMultiple',
				'onSuccess',
				'onError',
				'onCancel',
			])

			this._titles = undefined
		}

		onUpdate(isDone, {
			title,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					title: title.value,
				},
			})
		}

		onUpdateMultiple(isDone, {
			titles,
		}) {
			this._titles = titles.value
		}

		onSubmitMultiple() {
			this.setState({
				isLoading: true,
			}, () => {
				const titles = this._titles

				// CREATE
				BrandManager.createBrands(titles.split(',')).then(this.onSuccess).catch(this.onError)
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
				} = this.state.data

				if(this.props.id) {
					// UPDATE
					BrandManager.updateBrand({
						id: this.props.id,
						title,
					}).then(this.onSuccess).catch(this.onError)
				} else {
					// CREATE
					BrandManager.createBrand({
						title,
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSuccess(brand) {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Brand updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onUpdate &&
			this.props.onUpdate(brand)

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxLego title="Detail" contentContainerStyle={Styles.box}>
						<FormPagelet
							config={[{
								title: 'Name',
								required: true,
								autofocus: true,
								id: 'title',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.title,
								testOnMount: true,
							}]}
							onUpdate={this.onUpdate}
							onSubmit={this.onSubmit}
						/>
					</BoxLego>
					<BoxLego title="Add Multiple">
						<FormPagelet
							config={[{
								title: 'Names',
								description: 'Separated by comma (,)',
								id: 'titles',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.titles,
							}]}
							onUpdate={ this.onUpdateMultiple }
							onSubmit={ this.onSubmitMultiple }
						/>
					</BoxLego>
				</DetailPagelet>
			)
		}
	}
)
