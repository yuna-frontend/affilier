import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import RadioBit from 'modules/bits/radio'

import DropdownSearchLego from 'modules/legos/dropdown.search'
import ListLego from 'modules/legos/list'

import Styles from './style';

import UtilitiesContext from 'coeur/contexts/utilities';


import ResultProductPart from '../__result.product'
import ProductDataPart from '../__product.data'
import ProductListPart from './_product.list'


export default ConnectHelper(
	class ProductPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
				products: PropTypes.array,

				title:PropTypes.string,
				brand: PropTypes.string,
				brandId: PropTypes.number,
				category: PropTypes.string,
				categoryId: PropTypes.number,
				

			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				showForm: false,
				select: false,
			}, [
			])


		}

		onSelect = (product) => {
			this.setState({
				select: true,
				showForm: false,
			})

			this.props.onSelectProduct &&
			this.props.onSelectProduct(product)
		}

		onAddNew = () => {
			this.setState({
				showForm: true,
				select: false,
			})
		}

		onChangeSelected = ( productId ) => {
			this.props.onChangeSelected &&
			this.props.onChangeSelected(productId)
		}

		listRenderer = (product, i) => {
			return(
				<ProductListPart
					key={ i }
					selectedProductId={ this.props.selectedProductId }
					id={ product.id }
					brand={ product.brand }
					title= { product.title }
					category= { product.category }
					onPress= { this.onChangeSelected }
				/>
			)
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					<DropdownSearchLego
						placeholder="Search product by brand or product name"
						onSelect={this.onSelect}
						search={ this.props.title }
						children={(
							<ResultProductPart
								search={this.state.search}
								{ ...this.props }
							/>
						)}
					/>
					<BoxBit unflex style={Styles.divider} />

					{ this.props.products.map(this.listRenderer) }
					<BoxBit/>
					<ListLego isHeader row
						onPress={this.onAddNew}
						style={Styles.containerAddNew}
					>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.primary}>
							Add New Product
						</GeomanistBit>
						<BoxBit />
					</ListLego>
					{ this.state.showForm && (
						<ProductDataPart
							onAddNewProduct={this.props.onSelectProduct}
							title={ this.props.title }
							brand={ this.props.brand }
							brandId={ this.props.brandId }
							category={ this.props.category }
							categoryId={ this.props.categoryId }
						/>
					) }
				</BoxBit>
			)
		}
	}
)
