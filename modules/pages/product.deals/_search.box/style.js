import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	containerInput: {
		backgroundColor: Colors.grey.palette(7),
		height: 36,
		paddingTop: Sizes.margin.default / 2,
		paddingBottom: Sizes.margin.default / 2,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
	},

	pr8: {
		paddingRight: 8,
	},

	inputText: {
		color: Colors.grey.primary,
	},
})
