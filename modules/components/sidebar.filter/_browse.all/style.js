import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	text: {
		textTransform: 'capitalize',
		color: Colors.black.palette(1, .7),
	},

	isActive: {
		color: Colors.primary,
	},

	isLoading: {
		opacity: .6,
	},

	item: {
		marginLeft: 8,
		marginTop: 4,
		marginBottom: 4,
	},
})
