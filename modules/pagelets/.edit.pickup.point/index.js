import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import AddressManager from 'app/managers/address';
import BrandManager from 'app/managers/brand';
import PickupPointManager from 'app/managers/pickup.point';
// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import GroupLego from 'modules/legos/group';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class EditBrandPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				brandId: PropTypes.id,
				onRequestClose: PropTypes.func.isRequired,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, pickupPoint = id && PickupPointManager.get(id) || new PickupPointManager.record()
				, address = pickupPoint.addressId && AddressManager.get(pickupPoint.addressId) || new AddressManager.record()

			return {
				pickupPoint,
				address,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.address._isGetting !== nS.isGettingAddress) {
				return {
					isGettingAddress: nP.address._isGetting,
					data: {
						title: nP.pickupPoint.title,
						description: nP.pickupPoint.description,
						pic: nP.address.pic,
						phone: nP.address.phone,
						address: nP.address.address,
						district: nP.address.district,
						postal: nP.address.postal,
						metadata: nP.address.metadata,
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingAddress: p.address._isGetting,
				data: {
					title: p.pickupPoint.title,
					description: p.pickupPoint.description,
					pic: p.address.pic,
					phone: p.address.phone,
					address: p.address.address,
					district: p.address.district,
					postal: p.address.postal,
					metadata: p.address.metadata,
				},
				isDone: {},
				isLoading: false,
				invalidPostal: [],
			}, [
				'invalidPostalLength',
				'checkInvalidPostal',
				'validateAndRetrieveArea',
				'saveDistrict',
				'onUpdate',
				'onUpdateAddress',
				'onSubmit',
				'onUpdatePickupPoint',
				'onSuccess',
				'onError',
				'onCancel',
			])

			this.validateAndRetrieveArea = _.debounce(this.validateAndRetrieveArea, 300)
		}

		componentDidMount() {
		}

		invalidPostalLength() {
			return this.state.invalidPostal.length
		}

		checkInvalidPostal(value) {
			return !this.state.invalidPostal.length || this.state.invalidPostal.indexOf(value) === -1
		}

		validateAndRetrieveArea(postal) {
			AddressManager.validatePostalAndRetrieveAreas(postal).then(areas => {
				if(areas.length > 1) {
					this.props.utilities.menu.show({
						title: 'Select area',
						actions: areas.map(area => {
							return {
								title: area.area_name,
								type: this.props.utilities.menu.TYPES.ACTION.RADIO,
								onPress: this.saveDistrict.bind(this, area),
							}
						}),
						closeOnPress: true,
					})
				} else if(areas.length === 1) {
					this.saveDistrict(areas[0])
				} else {
					throw new Error('invalid postal')
				}
			}).catch(() => {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Postal code invalid',
					timeout: 3000,
				})

				this.setState({
					invalidPostal: [
						...this.state.invalidPostal,
						postal,
					],
				})
			})
		}

		saveDistrict(area) {
			this.setState({
				data: {
					...this.state.data,
					district: `${area.suburb_name}/${area.area_name}`,
					metadata: _.mapKeys(area, (value, key) => {
						return _.camelCase(key)
					}),
				},
			})
		}

		onUpdate(isDone, {
			title,
			description,
		}) {
			this.setState({
				isDone: {
					...this.state.isDone,
					pickup: isDone,
				},
				data: {
					...this.state.data,
					title: title.value,
					description: description.value,
				},
			})
		}

		onUpdateAddress(isDone, {
			pic,
			phone,
			address,
			district,
			postal,
		}) {

			let _district = district.value,
				_isDone = isDone

			if(postal.value !== this.state.data.postal && postal.isValid) {
				_district = null
				_isDone = false
				this.validateAndRetrieveArea(postal.value)
			}

			this.setState({
				isDone: {
					...this.state.isDone,
					address: _isDone,
				},
				data: {
					...this.state.data,
					pic: pic.value,
					phone: phone.value,
					address: address.value,
					district: _district,
					postal: postal.value,
				},
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
					description,
					pic,
					phone,
					address,
					district,
					postal,
				} = this.state.data

				// if(this.props.pickupPoint.id) {
				// }

				if(this.props.id) {
					if(this.props.address.id) {
						AddressManager.updateAddress({
							id: this.props.address.id,
							pic,
							phone,
							address,
							district,
							postal,
						}).then(this.onUpdatePickupPoint).catch(this.onError)
					} else {
						AddressManager.createAddress({
							pic,
							phone,
							address,
							district,
							postal,
						}).then(this.onUpdatePickupPoint).catch(this.onError)
					}
				} else {
					BrandManager.createPickupPoint({
						id: this.props.brandId,
						title,
						description,
						pic,
						phone,
						address,
						district,
						postal,
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onUpdatePickupPoint(address) {
			if(!this.props.pickupPoint.id) {
				PickupPointManager.createPickupPoint({
					addressId: address.id,
					title: this.state.data.title,
					description: this.state.data.description,
				}).then(this.onSuccess).catch(this.onError)
			} else {
				PickupPointManager.updatePickupPoint({
					id: this.props.pickupPoint.id,
					addressId: address.id,
					title: this.state.data.title,
					description: this.state.data.description,
				}).then(this.onSuccess).catch(this.onError)
			}
		}

		onSuccess() {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Pickup point updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		view() {
			return (
				<PageBit header={ this.props.header } contentContainerStyle={Styles.container}>
					<FormPagelet
						config={[{
							title: 'Title',
							isRequired: true,
							id: 'title',
							type: FormPagelet.TYPES.TITLE,
							value: this.state.data.title,
							testOnMount: true,
						}, {
							title: 'Description',
							id: 'description',
							type: FormPagelet.TYPES.ADDRESS,
							value: this.state.data.description,
							testOnMount: true,
						}]}
						onUpdate={ this.onUpdate }
					/>
					<GroupLego title="Address" style={Styles.address}>
						{ this.state.isGettingAddress ? (
							<BoxBit type={BoxBit.TYPES.ALL_THICK} centering>
								<LoaderBit />
							</BoxBit>
						) : (
							<FormPagelet ignoreNull
								updater={ this.state.data.district || this.invalidPostalLength() }
								config={[{
									title: 'Receiver Name',
									id: 'pic',
									type: FormPagelet.TYPES.NAME,
									value: this.state.data.pic,
									placeholder: this.state.data.title,
									style: Styles.firstInput,
									testOnMount: true,
								}, {
									title: 'Phone',
									id: 'phone',
									type: FormPagelet.TYPES.PHONE,
									value: this.state.data.phone,
									inputContainerStyle: Styles.inputContainerPhone,
									inputStyle: Styles.inputPhone,
									testOnMount: true,
								}, {
									title: 'Address',
									isRequired: true,
									id: 'address',
									type: FormPagelet.TYPES.ADDRESS,
									value: this.state.data.address,
									testOnMount: true,
								}, {
									title: 'District (Kecamatan / Kelurahan)',
									id: 'district',
									isRequired: true,
									disabled: true,
									placeholder: 'Autofill',
									type: FormPagelet.TYPES.TITLE,
									value: this.state.data.district,
									testOnMount: true,
								}, {
									title: 'Postal Code',
									id: 'postal',
									isRequired: true,
									updater: this.invalidPostalLength,
									type: FormPagelet.TYPES.POSTAL,
									validator: this.checkInvalidPostal,
									testOnMount: true,
									value: this.state.data.postal,
								}]}
								onUpdate={ this.onUpdateAddress }
								onSubmit={ this.onSubmit }
							/>
						) }
					</GroupLego>
					<BoxBit unflex row>
						<ButtonBit
							title={ 'CANCEL' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onCancel }
							style={ Styles.button }
						/>
						<ButtonBit
							title={ 'SAVE' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							width={ ButtonBit.TYPES.WIDTHS.BLOCK }
							state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone.address && this.state.isDone.pickup && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onSubmit }
							style={ [Styles.button, Styles.rightButton] }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
