import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { capitalize, flattenDeep, orderBy, isEmpty } from 'lodash'

const cache = {}


export default ConnectHelper(
	class SelectCategoryLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				categoryId: PropTypes.number,
				parentCategoryId: PropTypes.number,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache[oP.parentCategoryId] ? [`
				query {
					categories: categoriesList(filter: {
						${ oP.parentCategoryId ? `id: {
							equalTo: ${ oP.parentCategoryId }
						}` : `categoryId: {
							isNull: true
						}` }
					}){
						categories {
							id
							title
						}
					}
				}
			`, {
				fetchPolicy: _cache ? 'cache-first' : 'no-cache',
			}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache[nP.parentCategoryId] = nP.data
			} else if (cache[nP.parentCategoryId]) {
				// check for cache
			} else {
				return null
			}

			return {
				categories: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(orderBy(flattenDeep(cache[nP.parentCategoryId].categories.map((res) => {
					return res.categories.map(({id, title}) => {
						return {
							selected: nP.categoryId === id,
							key: id,
							title: capitalize(title),
						}
					})
				})), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			cache: true,
			disabled: false,
			unflex: false,
			placeholder: 'Filter by category',
			emptyTitle: 'All Categories',
		}

		constructor(p) {
			super(p, {
				categories: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit
					unflex={ this.props.unflex } disabled
					placeholder={'Loading ...'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit
					disabled={this.props.disabled}
					key={ this.props.categoryId }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={this.state.categories || []}
					onChange={this.props.onChange}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}
	}
)
