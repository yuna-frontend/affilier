/* eslint-disable no-nested-ternary */
import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities'

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import InputFileBit from 'modules/bits/input.file';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import RowLego from 'modules/legos/row';

import ImagePagelet from 'modules/pagelets/image'
import ImageEditorPagelet from 'modules/pagelets/image.editor'


import Styles from './style';

const SYNTHETIC_PREFIX = 'asset_'
let id = 1

export default ConnectHelper(
	class RowImageLego extends StatefulModel {
		_isMounted = false;

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				headerless: PropTypes.bool,
				addable: PropTypes.bool,
				reorder: PropTypes.bool,
				images: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					url: PropTypes.string,
				})),
				variantId: PropTypes.number,
				uploader: PropTypes.func,
				remover: PropTypes.func,
				orderer: PropTypes.func,
				onUpdate: PropTypes.func,
				max_attachment: PropTypes.number,
				styleImage: PropTypes.style,
				containerStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Images',
			images: [],
			reorder: true,
			addable: true,
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				images: p.images,
			})
		}

		transform = { original: true }

		onEdit = (source) => {
			this.props.utilities.alert.modal({
				component: (
					<ImageEditorPagelet
						variantId={ this.props.variantId }
						source={source}
						onPress={this.onModalRequestClose}
						onDataLoaded={ this.onDataLoaded }
						newIndex={this.state.images.length}
						transform={this.transform}
					/>
				),
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onImagePopUp = source => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet source={source} onPress={this.onModalRequestClose} onEdit={this.onEdit.bind(this, source)} transform={this.transform}/>
				),
			})
		}

		onChangeOrder = (index, to) => {
			const images = this.state.images
			// swapping magic
			if(to === 'right') {
				images[index] = ([images[index + 1], images[index + 1] = images[index]][0])
			} else {
				images[index] = ([images[index - 1], images[index - 1] = images[index]][0])

			}

			this.setState({
				images: [...images],
			}, () => {
				this.props.orderer &&
				this.props.orderer(this.state.images)

				this.props.onUpdate &&
				this.props.onUpdate(images)
			})
		}

		onDataLoaded = (index, value, closePopup) => {
			if(value === undefined) {
				// removing
				// todo remove from cloudinary and database
				if (this.state.images[index].id && (
					typeof this.state.images[index].id !== 'string'
					|| !this.state.images[index].id.match(SYNTHETIC_PREFIX)
				)) {
					// remove
					this.props.remover &&
					this.props.remover(this.state.images[index].id)
				}

				const images = this.state.images.slice()
				images.splice(index, 1)

				this.setState({
					images,
				}, () => {
					this.props.onUpdate &&
					this.props.onUpdate(images)
				})
			} else if(index === 'adder') {
				// adding
				// upload

				const _id = `${SYNTHETIC_PREFIX}${id++}` // synthetic id

				if(this.props.uploader) {
					this.setState({
						images: [...this.state.images, {
							nonce: _id,
							data: value,
						}],
					})


					this.props.uploader(value, this.state.images.length).then(asset => {
						const images = this.state.images.slice()
						const assetIndex = images.findIndex(a => a.nonce === _id)
						
						if (assetIndex > -1) {
							images.splice(assetIndex, 1, ...asset)

							this.setState({
								images,
							})
						} else {
							// removed
							this.props.remover &&
							this.props.remover(asset.id)
						}
						
						if(!!closePopup) {
							this.onModalRequestClose()
						}

						this.props.onUpdate &&
						this.props.onUpdate(images)
					})
				} else {
					this.setState({
						images: [...this.state.images, {
							id: _id,
							data: value,
						}],
					}, () => {
						this.props.onUpdate &&
						this.props.onUpdate(this.state.images)
					})
				}
			}
		}

		imageRenderer = (file, index) => {
			const type = file.id !== undefined
				? 'asset' : 'uploading'

			const isAccessibleRight = index !== this.state.images.length - 1
			const isAccessibleLeft = index !== 0

			return type === 'uploading' ? (
				<BoxBit key={index} unflex centering style={[Styles.container, Styles.image, Styles.loading, this.props.styleImage]}>
					<LoaderBit />
				</BoxBit>
			) : (
				<TouchableBit unflex key={index} style={[Styles.container, this.props.containerStyle]} onPress={this.onImagePopUp.bind(this, file)}>
					
					{ this.props.addable ? (
						<InputFileBit
							maxSize={3}
							unflex
							id={index}
							value={type === 'asset'
								? file.url
									? file
									: file.data
								: file.data
							}
							style={[Styles.image, this.props.styleImage]}
							onDataLoaded={this.onDataLoaded}
						/>
					) : (
						<ImageBit resizeMode={ ImageBit.TYPES.COVER } source={type === 'asset' ? file : file.data } style={Styles.image} />
					) }
					{ this.props.reorder ? (
						<React.Fragment>
							<TouchableBit accessible={ isAccessibleLeft } style={Styles.moverLeft} onPress={ this.onChangeOrder.bind(this, index, 'left') }>
								<IconBit
									name="arrow-left"
									color={ isAccessibleLeft ? undefined : Colors.black.palette(2, .3) }
								/>
							</TouchableBit>
							<TouchableBit accessible={ isAccessibleRight } style={Styles.mover} onPress={ this.onChangeOrder.bind(this, index, 'right') }>
								<IconBit
									name="arrow-right"
									color={ isAccessibleRight ? undefined : Colors.black.palette(2, .3) }
								/>
							</TouchableBit>

						</React.Fragment>
					) : false}
				</TouchableBit>
			)
		}

		view() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						headerless: this.props.headerless,
						content: '-',
						children: this.state.images.length || this.props.addable ? (
							<BoxBit row unflex style={Styles.wrap}>
								
								{ this.state.images.map(this.imageRenderer) }

								{ !this.props.addable || (this.props.max_attachment && this.state.images.length >= this.props.max_attachment) ? false : (
									<InputFileBit unflex
										key={ this.state.images.length }
										id={'adder'}
										maxSize={3}
										style={[Styles.container, Styles.image, this.props.styleImage]}
										onDataLoaded={ this.onDataLoaded }
									/>
								)}
							</BoxBit>
						) : false,
					}]}
				/>
			)
		}
	}
)
