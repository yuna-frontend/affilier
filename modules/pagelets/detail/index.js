import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import PageBit from 'modules/bits/page';
import TouchableBit from 'modules/bits/touchable';

import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';

import Styles from './style';


export default ConnectHelper(
	class DetailPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onRequestClose: PropTypes.func,
				paths: PropTypes.array,
				header: PropTypes.node,
				footer: PropTypes.node,
				children: PropTypes.node.isRequired,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'requestClose',
			])
		}

		requestClose() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		headerRenderer() {
			if(this.props.paths) {
				return (
					<BoxBit row unflex style={[Styles.container, Styles.header]}>
						<HeaderBreadcrumbLego
							paths={this.props.paths}
							style={Styles.title}
						/>
						<BoxBit />
						<TouchableBit unflex centering onPress={ this.requestClose } style={Styles.closer}>
							<IconBit
								name="close"
								size={32}
							/>
						</TouchableBit>
					</BoxBit>
				)
			} else {
				return false
			}
		}

		footerRenderer() {
			return this.props.footer && (
				<BoxBit unflex style={[Styles.container, Styles.footer]}>
					{ this.props.footer }
				</BoxBit>
			)
		}

		view() {
			return (
				<PageBit
					headerHeight={ 99 }
					footerHeight={ 76 }
					header={ this.props.header || this.headerRenderer() }
					footer={ this.footerRenderer() }
					style={ [Styles.body, this.props.style] }
					contentContainerStyle={[Styles.container, this.props.contentContainerStyle]}
				>
					{ this.props.children }
				</PageBit>
			)
		}
	}
)
