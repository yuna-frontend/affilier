import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

const Wrap = Sizes.app.width > 1440 ? 1440 : Sizes.app.width

export default StyleSheet.create({

	page: {
		maxWidth: 1440,
		width: Wrap * .8,
		maxHeight: Sizes.app.height * .72,
		backgroundColor: Colors.white.primary,
		borderRadius: 4,
		// overflow: 'hidden',
	},

	narrow: {
		width: Sizes.app.width * .3,
	},

	compact: {
		width: Sizes.app.width * .5,
	},

	content: {
		height: '100%',
	},

	contentContainer: {
		borderRadius: 4,
	},

	header: {
		position: 'absolute',
		top: -34,
		left: 0,
		right: 0,
		zIndex: 2,
		paddingBottom: 8,
		paddingRight: 8,
		paddingLeft: 8,
	},

	center: {
		marginLeft: Sizes.margin.default,
		alignItems: 'center',
	},

	white: {
		color: Colors.white.primary,
	},

	red: {
		color: Colors.white.palette(1, .6),
	},

	close: {
		marginLeft: 4,
	},

	layout3Way0: {
		width: 260,
	},

	layout3Way1: {
		flexGrow: 1,
		flexShrink: 0,
		flexBasis: '25%',
	},

	layout3Way2: {
		flexGrow: 1.2,
		flexShrink: 0,
		flexBasis: '25%',
	},

	...(Defaults.PLATFORM === 'web' ? {

		'@media screen and (max-width: 1279px)': {
			page: {
				width: Wrap * .7,
				maxHeight: Sizes.app.height * .7,
			},
		},
	} : {}),

})
