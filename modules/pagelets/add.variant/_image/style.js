import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({

	container: {
		padding: Sizes.margin.default,
	},

	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	contentContainer: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		top: -8,
	},
})
