import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';
import ProductManager from 'app/managers/product';

import UtilitiesContext from 'coeur/contexts/utilities';

// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';
import BoxRowLego from 'modules/legos/box.row';

// import CardMediaComponent from 'modules/components/card.media';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class EditProductPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				brandId: PropTypes.id.isRequired,
				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, brandId = oP.brandId
				, product = id && ProductManager.get(id) || new ProductManager.record()
				, brand = brandId && BrandManager.get(brandId) || new BrandManager.record()

			return {
				product,
				brand,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.brand._isGetting !== nS.isGettingBrand) {
				return {
					isGettingProduct: nP.product._isGetting,
					data: {
						title: nP.product.title,
						spu: nP.product.spu,
						url: nP.product.url,
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingBrand: p.brand._isGetting,
				data: {
					title: p.product.title,
					spu: p.product.spu,
					url: p.product.url,
				},
				isDone: false,
				isLoading: false,
			}, [
				'onUpdate',
				'onSubmit',
				'onSuccess',
				'onError',
				'onCancel',
			])
		}

		onUpdate(isDone, {
			title,
			spu,
			url,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					title: title.value,
					spu: spu.value,
					url: url.value,
				},
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
					spu,
					url,
				} = this.state.data

				if(this.props.id) {
					// UPDATE
					BrandManager.updateBrand({
						id: this.props.id,
						brandId: this.props.brandId,
						title,
						spu,
						url,
					}).then(this.onSuccess).catch(this.onError)
				} else {
					// CREATE
					ProductManager.createProduct({
						brandId: this.props.brandId,
						title,
						spu,
						url,
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSuccess(product) {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Brand updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onUpdate &&
			this.props.onUpdate(product)

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxRowLego
						title="Brand"
						data={[{
							data: [{
								title: 'Name',
								content: this.props.brand.title,
							}],
						}]}
						contentContainerStyle={Styles.box}
					/>
					<BoxLego title="Product Detail">
						<FormPagelet ignoreNull
							config={[{
								title: 'Name',
								required: true,
								autofocus: true,
								id: 'title',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.title,
								testOnMount: true,
							}, {
								title: 'SPU',
								id: 'spu',
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.spu,
								maxlength: 31,
							}, {
								title: 'URL',
								id: 'url',
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.url,
							}]}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onSubmit }
						/>
					</BoxLego>
				</DetailPagelet>
			)
		}
	}
)
