import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	sizes: {
	},

	item: {
		marginTop: Sizes.margin.default / 2,
		marginBottom: Sizes.margin.default / 2,
	},

	content: {
		marginTop: Sizes.margin.default / 2,
		marginLeft: Sizes.margin.default / 2,
	},

	listCollapsible: {
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
	},

	listSizeCategories: {
		paddingBottom: Sizes.margin.thick,
	},

	listSizeCategoriesTitle: {
		textTransform: 'uppercase',
		color: Colors.grey.palette(12),
		paddingBottom: Sizes.margin.default,
	},

	listSize: {
		paddingBottom: Sizes.margin.default,
		...(Defaults.PLATFORM === 'web' && {
			'&:hover $listSizeCheckbox': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	listSizesBoxes: {
		flexWrap: 'wrap',
	},

	listSizeCheckbox: {
		borderColor: Colors.solid.grey.palette(3),
		...(Defaults.PLATFORM === 'web' && {
			'&:hover': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	listSizeTitle: {
		marginLeft: Sizes.margin.default / 2,
	},

	listSizeBox: {
		padding: 14,
		borderWidth: 2,
		borderColor: Colors.grey.palette(8),
		width: '30%',
		marginBottom: Sizes.margin.default,
		marginRight: '5%',
	},
	
	listSizeBoxActive: {
		borderColor: Colors.black.primary,
		background: Colors.grey.palette(5),
	},

	listSizeBox3N: {
		marginRight: 0,
	},
})
