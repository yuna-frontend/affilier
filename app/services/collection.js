import ServiceModel from 'coeur/models/service';


class CollectionService extends ServiceModel {

	static displayName = 'collection'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/storefront/', 0)
	}

	getCollections(data, token) {
		return this.get('', {
			token,
			query: data,
		})
	}

	getDetail(id, query, token) {
		return this.get(id, {
			query,
			token,
		})
	}

	addItemToCollection(id, { itemId }, token) {
		return this.post(id + '/variant/' + itemId, {
			token,
		})
	}

	uploadCSV(id, data, token) {
		return this.post('upload/csv', {
			token,
			data: {
				data,
				id,
			},
		})
	}

	removeItemFromCollection(id, { itemId }, token) {
		return this.delete(id + '/variant/' + itemId, {
			token,
		})
	}

	getCollection(id, token) {
		return this.get(id, {
			token,
		})
	}

	filterCollections({
		title,
		offset,
		count,
	}, token) {
		return this.get('', {
			query: {
				title,
				offset,
				count,
			},
			token,
		})
	}

	createCollection(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	updateCollection(id, data, token) {
		return this.put(id, {
			data,
			token,
		})
	}

	updateSchedule(id, dates, token) {
		return this.put(id + '/schedule', {
			...(!!dates.length && {
				data: {
					dates,
				},
			}),
			token,
		})
	}
}

export default new CollectionService()
