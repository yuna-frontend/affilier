import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import ModalClosablePagelet from '../modal.closable'


export default ConnectHelper(
	class ModalClosableConfirmationPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				small: PropTypes.bool,
				header: PropTypes.node,
				subheader: PropTypes.node,
				loading: PropTypes.bool,
				disabled: PropTypes.bool,
				cancel: PropTypes.string,
				confirm: PropTypes.string,
				onCancel: PropTypes.func,
				onConfirm: PropTypes.func,
				onClose: PropTypes.func,
				children: PropTypes.node,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			cancel: 'Cancel',
			confirm: 'Change',
		}

		view() {
			return (
				<ModalClosablePagelet
					small={ this.props.small }
					title={ this.props.title }
					header={ this.props.header }
					onClose={ this.props.onClose }
					footer={(
						<React.Fragment>
							{ this.props.onCancel ? (
								<ButtonBit
									title={ this.props.cancel }
									weight="medium"
									size={ ButtonBit.TYPES.SIZES.SMALL }
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									onPress={ this.props.onCancel }
								/>
							) : false }
							<BoxBit />
							{ this.props.onConfirm ? (
								<ButtonBit
									key={ this.props.confirm }
									title={ this.props.confirm }
									weight="medium"
									size={ ButtonBit.TYPES.SIZES.SMALL }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.loading ? ButtonBit.TYPES.STATES.LOADING : !this.props.disabled && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
									onPress={ this.props.onConfirm }
								/>
							) : false }
						</React.Fragment>
					)}
					subheader={ this.props.subheader }
					children={ this.props.children }
					style={ this.props.style }
					contentContainerStyle={ this.props.contentContainerStyle }
				/>
			)
		}
	}
)

