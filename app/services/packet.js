import ServiceModel from 'coeur/models/service';


class PacketService extends ServiceModel {

	static displayName = 'packet'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/packet/', 0)
	}

	deletePacket(id, token) {
		return this.delete(id, {
			token,
		})
	}
}

export default new PacketService()
