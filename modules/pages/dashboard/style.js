import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	notif: {
		backgroundColor: Colors.black.palette(2),
		paddingTop: 6,
		paddingBottom: 6,
		paddingRight: 10,
		paddingLeft: 10,
		borderRadius: 6,
		'&:before': {
			content: '""',
			position: 'absolute',
			bottom: -10,
			left: 0,
			borderStyle: 'solid',
			borderWidth: '0 25px 25px',
			borderColor: `transparent transparent transparent ${Colors.black.palette(2)}`,
		},
		marginBottom: Sizes.margin.default,
	},

	headerContainer: {
		marginTop: Sizes.margin.default,
		marginBottom: Sizes.margin.thick,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	profilePart: {
		marginRight: Sizes.margin.default,
	},

	pad: {
		marginBottom: Sizes.margin.default,
	},

	mt16: {
		marginTop: 16,
	},

	overflow: {
		overflow: 'scroll',
	},

	white: {
		color: Colors.white.primary,
	},

	'@media screen and (max-width: 480px)': {
		headerContainer: {
			flexWrap: 'wrap',
		},

		profilePart: {
			flexBasis: '100%',
			marginRight: 0,
		},

		commissionPart: {
			marginTop: Sizes.margin.thick,
		},
	},
})
