import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import IconBit from '../icon'
import SelectionBit from '../selection'
import TouchableBit from '../touchable'

import Styles from './style'

// function ambiguousValueToObjectValue(val) {
// 	// eslint-disable-next-line no-nested-ternary
// 	return typeof val === 'string' ? {
// 		key: val,
// 		title: val,
// 	} : Array.isArray(val) ? val.map(v => {
// 		return typeof v === 'string' ? {
// 			key: v,
// 			title: v,
// 		} : v
// 	}) : val || []
// }


export default ConnectHelper(
	class SelectionMultipleBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,

				// value: PropTypes.oneOfType([
				// 	PropTypes.string,
				// 	PropTypes.shape({
				// 		key: PropTypes.id,
				// 		title: PropTypes.string,
				// 	}),
				// 	PropTypes.arrayOf(PropTypes.string),
				// 	PropTypes.arrayOf(PropTypes.shape({
				// 		key: PropTypes.id,
				// 		title: PropTypes.string,
				// 	})),
				// ]),

				options: PropTypes.array,
				selectionStyle: PropTypes.style,
				style: PropTypes.style,

				// From Selection Bit

				title: PropTypes.string,
				inputRef: PropTypes.func,
				disabled: PropTypes.bool,
				placeholder: PropTypes.string,
				onChange: PropTypes.func,
				onBlur: PropTypes.func,
				onFocus: PropTypes.func,
				onSubmitEditing: PropTypes.func,
				prefix: PropTypes.node,
				postfix: PropTypes.node,
				inputStyle: PropTypes.style,
				tabindex: PropTypes.number,
			}
		}

		static defaultProps = {
			options: [],
		}

		static getDerivedStateFromProps(nP, nS) {
			if (nP.options !== nS.lastOptions) {
				return {
					values: nP.options.filter(o => !!o.selected),
					options: nP.options.map(o => {
						return {
							key: o.key,
							title: o.title,
						}
					}),
					lastOptions: nP.options,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				values: p.options.filter(o => !!o.selected),
				options: p.options.map(o => {
					return {
						key: o.key,
						title: o.title,
					}
				}),
				lastOptions: p.options,
			})
		}

		onRemoveValue = value => {
			this.setState({
				values: this.state.values.filter(v => v.key !== value.key),
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state.values)
			})
		}

		onToggleValue = (key, value) => {
			if (this.state.values.findIndex(v => v.key === key) > -1) {
				this.onRemoveValue({
					key,
					title: value,
				})
			} else {
				this.setState({
					values: [...this.state.values, {
						key,
						title: value,
					}],
				}, () => {
					this.props.onChange &&
					this.props.onChange(this.state.values)
				})
			}
		}

		valueRenderer = (value, i) => {
			return (
				<TouchableBit key={ i } accessible={ !this.props.disabled } unflex row onPress={ this.onRemoveValue.bind(this, value) } style={ Styles.value }>
					<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 }>
						{ value.title }
					</GeomanistBit>
					{ this.props.disabled ? (
						<IconBit name="close" size={ 12 } style={ Styles.icon } />
					) : false }
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={ this.props.style }>
					{ this.props.title ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold" style={ Styles.title }>
							{ this.props.title }
						</GeomanistBit>
					) : false }
					<BoxBit unflex style={ Styles.container }>
						<BoxBit accessible="box-none" row unflex style={ Styles.values }>
							{ this.state.values.map(this.valueRenderer) }
						</BoxBit>
						<SelectionBit
							key={ this.state.values.length }
							title={ this.props.title }
							options={ this.state.options }
							unflex={ this.props.unflex }
							inputRef={ this.props.inputRef }
							disabled={ this.props.disabled }
							placeholder={ this.state.values.length ? undefined : this.props.placeholder }
							onChange={ this.onToggleValue }
							onBlur={ this.props.onBlur }
							onFocus={ this.props.onFocus }
							onSubmitEditing={ this.props.onSubmitEditing }
							prefix={ this.props.prefix }
							postfix={ this.props.postfix }
							style={[ Styles.selection, this.props.selectionStyle ]}
							inputStyle={ this.props.inputStyle }

							// WEB ONLY
							tabindex={ this.props.tabindex }
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
