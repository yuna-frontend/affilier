import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import ScrollViewBit from 'modules/bits/scroll.view';

import BrowsePart from './_browse';
import BrowseAllPart from './_browse.all'
import FilterPart from './_filter';

import Styles from './style';

import { isEqual, isEmpty } from 'lodash'


export default ConnectHelper(
	class SidebarFilterComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				header: PropTypes.node,
				showAllCategory: PropTypes.bool,
				filter: PropTypes.object,
				categoryId: PropTypes.id,
				onChangeCategory: PropTypes.func,
				onChangeFilter: PropTypes.func,
				onResetFilter: PropTypes.func,
				onCategoryChildrenLoaded: PropTypes.func,
				style: PropTypes.style,
			}
		}

		shouldComponentUpdate(nP) {
			return nP.categoryId !== this.props.categoryId || !isEqual(nP.filter, this.props.filter)
		}

		view() {
			return (
				<ScrollViewBit style={[Styles.container, this.props.style]} contentContainerStyle={Styles.content}>
					{ this.props.header }
					{ this.props.showAllCategory ? (
						<BrowseAllPart
							categoryId={this.props.categoryId}
							onChange={this.props.onChangeCategory}
						/>
					) : (
						<BrowsePart
							categoryId={ this.props.categoryId }
							onChange={ this.props.onChangeCategory }
						/>
					) }
					<FilterPart
						key="persist"
						filter={ this.props.filter }
						isResettable={!(isEmpty(this.props.filter.price) && isEmpty(this.props.filter.brandIds) && isEmpty(this.props.filter.colorIds) && isEmpty(this.props.filter.sizeIds)) }
						categoryId={ this.props.categoryId }
						onChange={ this.props.onChangeFilter }
						onReset={ this.props.onResetFilter }
						onCategoryChildrenLoaded={ this.props.onCategoryChildrenLoaded }
						style={ Styles.padder }
					/>
				</ScrollViewBit>
			);
		}
	}
)
