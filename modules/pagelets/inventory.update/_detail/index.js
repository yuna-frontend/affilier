import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import CheckboxBit from 'modules/bits/checkbox'
import InputCurrencyBit from 'modules/bits/input.currency';
import LoaderBit from 'modules/bits/loader';
import TextInputBit from 'modules/bits/text.input';

import RowLego from 'modules/legos/row';
import RowBrandAddressLego from 'modules/legos/row.brand.address'
import RowStatusInventoryLego from 'modules/legos/row.status.inventory'

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isEditting: PropTypes.bool,
				brandId: PropTypes.id,
				brandAddressId: PropTypes.id,
				note: PropTypes.string,
				price: PropTypes.number,
				quantity: PropTypes.number,
				rack: PropTypes.string,
				isOwnedByYuna: PropTypes.bool,
				status: PropTypes.string,
				statusNote: PropTypes.string,
				onChangeBrand: PropTypes.func,
				onChangeOwnership: PropTypes.func,
				onChangeBrandAddress: PropTypes.func,
				onChangeNote: PropTypes.func,
				onChangePrice: PropTypes.func,
				onChangeRack: PropTypes.func,
				onChangeQuantity: PropTypes.func,
				onChangeStatus: PropTypes.func,
			}
		}

		onChangeBrand = ({ brandId, brandAddressId}) => {
			this.props.onChangeBrand &&
			this.props.onChangeBrand(brandId)

			this.props.onChangeBrandAddress &&
			this.props.onChangeBrandAddress(brandAddressId)
		}

		onChangeStatus = ({ status, note }) => {
			this.props.onChangeStatus &&
			this.props.onChangeStatus(status)

			this.props.onChangeNote &&
			this.props.onChangeNote(note)
		}

		onChangePrice = val => {
			this.props.onChangePrice &&
			this.props.onChangePrice(val)
		}

		onChangeOwnership = (e, val) => {
			this.props.onChangeOwnership &&
			this.props.onChangeOwnership(val)
		}

		onChangeRack = (e, val) => {
			this.props.onChangeRack &&
			this.props.onChangeRack(val)
		}

		onChangeQuantity = (e, val) => {
			this.props.onChangeQuantity &&
			this.props.onChangeQuantity(val)
		}

		view() {
			return !this.props.isLoading ? (
				<BoxBit style={Styles.padder}>
					<RowBrandAddressLego
						brandId={ this.props.brandId }
						brandAddressId={ this.props.brandAddressId }
						onChange={ this.onChangeBrand }
					/>
					{ this.props.isEditting ? (
						<RowLego
							data={[{
								title: 'Quantity',
								children: (
									<TextInputBit disabled
										defaultValue={ 1 }
									/>
								),
							}, {
								title: 'Purchase Price*',
								children: (
									<InputCurrencyBit
										value={ this.props.price }
										onChange={ this.onChangePrice }
									/>
								),
							}]}
						/>
					) : (
						<RowLego
							data={[{
								title: 'Quantity',
								children: (
									<TextInputBit
										defaultValue={ this.props.quantity }
										onChange={ this.onChangeQuantity }
									/>
								),
							}, {
								title: 'Purchase Price*',
								children: (
									<InputCurrencyBit
										value={ this.props.price }
										onChange={ this.onChangePrice }
									/>
								),
							}]}
						/>
					)}
					<RowLego
						data={[{
							title: 'Rack',
							children: (
								<TextInputBit
									placeholder="-"
									defaultValue={ this.props.rack }
									onChange={ this.onChangeRack }
								/>
							),
						}, {
							title: 'Owned By Yuna',
							children: (
								<BoxBit style={{ justifyContent: 'center'}}>
									<CheckboxBit onChange={ this.onChangeOwnership }/>
								</BoxBit>
							),
						}]}
					/>
					<RowStatusInventoryLego
						status={ this.props.status }
						note={ this.props.note }
						onChange={ this.onChangeStatus }
					/>
				</BoxBit>
			) : (
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			)
		}
	}
)
