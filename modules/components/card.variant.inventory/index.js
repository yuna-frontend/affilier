import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import BrandManager from 'app/managers/brand';
import InventoryManager from 'app/managers/inventory';
import ProductManager from 'app/managers/product';
import VariantManager from 'app/managers/variant';

import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
// import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';

import CardMediaComponent from 'modules/components/card.media';

import Styles from './style'


export default ConnectHelper(
	class CardVariantInventoryComponent extends CardComponentModel(VariantManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
			}
		}

		static stateToProps(state, oP) {
			const {
					id,
					data: variant,
				} = super.stateToProps(state, oP)
				, product = variant.productId && ProductManager.get(variant.productId) || new ProductManager.record()
				, brand = product.brandId && BrandManager.get(product.brandId) || new BrandManager.record()
				, quantity = variant.inventoryIds.map(inventoryId => {
					const inventory = InventoryManager.get(inventoryId)

					if(inventory._isGetting) {
						return 'getting'
					}

					return inventory.status.code === 'READY'
				}).filter(inventoryStatus => !!inventoryStatus).reduce((sum, status) => {
					if(status === 'getting' || sum === 'getting') return 'getting'
					return sum + 1
				}, 0)

			return {
				id,
				data: variant,
				product,
				brand,
				quantity,
			}
		}

		view() {
			return (
				<ShadowBit x={0} y={0} blur={2} style={[Styles.container, this.props.style]}>
					<TouchableBit unflex onPress={ this.onPress }>
						<CardMediaComponent
							id={ this.props.data.mediaIds[0] }
							style={ Styles.image }
						/>
						<BoxBit unflex style={Styles.content}>
							<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="medium" style={Styles.aline}>{ this.props.brand.title }</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal" style={[Styles.aline, Styles.padder2]}>{ this.props.product.title }</GeomanistBit>
							<BoxBit unflex row style={Styles.padder2}>
								<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal">IDR { FormatHelper.currencyFormat(this.props.data.basePrice) }</GeomanistBit>
								{ this.props.data.basePrice !== this.props.data.price && (
									<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.discount}>IDR { FormatHelper.currencyFormat(this.props.data.price) }</GeomanistBit>
								) }
							</BoxBit>
							<BoxBit unflex row centering style={[Styles.space, Styles.padder]}>
								<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.size}>{ this.props.data.size.toUpperCase() }</GeomanistBit>
								{}
								<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal">{ this.props.quantity === 'getting' ? '...' : `Qty ${this.props.quantity}` }</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</TouchableBit>
				</ShadowBit>
			)
		}
	}
)
