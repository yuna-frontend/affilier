import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
// import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
// import TouchableBit from 'modules/bits/touchable';

import FilterCollapsibleLego from 'modules/legos/filter.collapsible';
import FilterSelectionLego from 'modules/legos/filter.selection';

import Styles from './style'


export default ConnectHelper(
	class SizePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				sizeIds: PropTypes.arrayOf(PropTypes.id),
				sizes: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					children: PropTypes.arrayOf(PropTypes.shape({
						key: PropTypes.id,
						title: PropTypes.string,
					})),
				})),
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			sizeIds: [],
			sizes: [],
		}

		// componentDidMount() {
		// 	this.props.onChange &&
		// 	this.props.onChange([])
		// }

		sizeRenderer = size => {
			return (
				<BoxBit key={size.id} unflex style={Styles.item}>
					<GeomanistBit type={GeomanistBit.TYPES.CAPTION_1}>{ size.title }</GeomanistBit>
					<BoxBit unflex style={Styles.content}>
						<FilterSelectionLego multiple key={ this.props.sizeIds.join(',') } values={ this.props.sizeIds } selections={ size.children } onChange={ this.props.onChange } />
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<FilterCollapsibleLego title="Size">
					<BoxBit unflex style={Styles.sizes}>
						{ this.props.sizes.map(this.sizeRenderer) }
					</BoxBit>
				</FilterCollapsibleLego>
			)
		}
	}
)
