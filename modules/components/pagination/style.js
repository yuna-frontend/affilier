import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	button: {
		marginLeft: 8,
		marginRight: 8,
	},

	text: {
		color: Colors.black.palette(2, .6),
	},

	nav: {
		backgroundColor: Colors.grey.palette(4),
		marginLeft: 4,
		marginRight: 4,
		marginTop: 8,
		marginBottom: 8,
		paddingLeft: 8,
		paddingRight: 8,
		borderRadius: 2,
	},

})
