import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: 272,
	},

	image: {
		height: 272,
		borderRadius: 4,
	},

	content: {
		backgroundColor: Colors.white.primary,
		padding: 16,
	},

	aline: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},

	space: {
		justifyContent: 'space-between',
	},

	size: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 12,
		paddingRight: 12,
		borderWidth: 1,
		borderRadius: 2,
		borderStyle: 'solid',
		borderColor: Colors.grey.palette(1, .2),
	},

	discount: {
		marginLeft: 8,
		color: Colors.red.primary,
	},

	padder: {
		paddingTop: 8,
	},

	padder2: {
		paddingTop: 2,
	},
})
