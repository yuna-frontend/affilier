import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		height: 44,
		width: 60,
		borderRadius: 2,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	title: {
		color: Colors.black.palette(2, .5),
	},
	left: {
		marginLeft: 4,
	},
	right: {
		marginRight: 4,
	},
})
