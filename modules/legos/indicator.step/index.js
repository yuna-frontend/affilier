import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';

import Styles from './style';


export default ConnectHelper(
	class IndicatorStepLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				total: PropTypes.number,
				index: PropTypes.number,
				style: PropTypes.style,
				// textIndicator: PropTypes.bool,
			}
		}

		// _renderTextIndicator(totalPageNumber, activeIndex) {
		// 	let text;
		// 	const remainingStep = totalPageNumber - activeIndex - 1;
		//
		// 	if (remainingStep === 1) {
		// 		text = 'You’re almost done';
		// 	} else if (remainingStep === 0) {
		// 		text = 'One last bit';
		// 	} else {
		// 		text = remainingStep + ' steps to go';
		// 	}
		//
		// 	return (
		// 		<GeomanistBit
		// 			type={GeomanistBit.TYPES.body1}
		// 			style={ Styles.text }>
		// 			{ text }
		// 		</GeomanistBit>
		// 	);
		// }

		view() {
			return (
				<BoxBit unflex centering row style={[Styles.container, this.props.style]}>
					{ Array(this.props.total).fill().map((u, i) => {
						return (
							<BoxBit unflex key={i} style={[Styles.paging, this.props.index === i ? Styles.pagingActive : Styles.pagingInactive]} />
						)
					}) }
				</BoxBit>
			)
		}
	}
)
