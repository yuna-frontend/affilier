import ConnectHelper from 'coeur/helpers/connect';
import CoreAlertPagelet from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import TouchableBit from 'modules/bits/touchable';

import DefaultPart from './_default';
import ImagePart from './_image';
import ModalPart from './_modal';

import Styles from './style'


export {
	DefaultPart,
	ImagePart,
	ModalPart,
}

export default ConnectHelper(
	class AlertPagelet extends CoreAlertPagelet({
		Styles,
		BoxBit,
		TouchableBit,
		DefaultPart,
		ImagePart,
		ModalPart,
	}) {}
)
