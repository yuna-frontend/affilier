import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageContext from 'coeur/contexts/page';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class BreadcrumbLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onNavigateToHome: PropTypes.func,
				paths: PropTypes.array.isRequired,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			paths: [],
		}

		// static contexts = [
		// 	PageContext,
		// ]

		constructor(p) {
			super(p, {}, [
				'onNavigateToHome',
			])
		}

		onNavigateToHome() {
			// HACK
			// TODO: FIX the ROUTER!!!!!!!!!!!!!!
			// Cannot reload the whole page
			window.location.hash = '/'
			window.location.reload()
		}

		view() {
			const paths = this.props.paths.slice();

			return (
				<BoxBit unflex row style={this.props.style}>
					<TouchableBit unflex onPress={ this.onNavigateToHome } style={ Styles.homeIcon }>
						<IconBit
							name="home"
							size={20}
							color={Colors.black.palette(2, .7)}/>
					</TouchableBit>
					{ paths.map((path, i) => {
						return (
							<TouchableBit key={i} unflex row centering onPress={ path.onPress }>
								<IconBit
									name="arrow-small"
									size={20}
									color={Colors.black.palette(2, .7)}/>
								<GeomanistBit
									type={GeomanistBit.TYPES.SUBHEADER_2}
									weight="medium"
									style={Styles.mainText}>
									{ path.title }
								</GeomanistBit>
							</TouchableBit>
						)
					}) }
				</BoxBit>
			)
		}
	}
)
