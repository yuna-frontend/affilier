import DictionaryModel from 'coeur/models/dictionary'
// import TimeHelper from 'utils/helpers/time'

import MatchboxRecord from 'utils/records/matchbox'

import MatchboxService from 'utils/services/matchbox';


class MatchboxManager extends DictionaryModel {

	static displayName = 'matchboxes'

	constructor() {
		super(MatchboxRecord, {
			// clearOnRehidrate: true,
			// clearOnReset: true,
			expiricy: 1000 * 60 * 5,
		})

		this.__countdown = false
	}

	getTimer() {
		if(this.__countdown && this.__countdown.lastOrderDate > Date.now()) {
			return Promise.resolve(this.__countdown)
		} else {
			return MatchboxService.getTimer().then(timer => {
				this.__countdown = timer
				return timer
			})
		}
	}

	getAll() {
		return MatchboxService.getMatchboxes().then(matchboxes => {
			return this.updateLater(matchboxes)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	dataGetter(id) {
		return MatchboxService.getMatchbox(id).then(matchbox => {
			return this.updateLater(matchbox)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return MatchboxService.getMatchboxes(ids).then(matchboxes => {
			return this.updateLater(matchboxes)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new MatchboxManager()
