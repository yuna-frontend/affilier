import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	content: {
		paddingTop: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	image: {
		width: 90,
		height: 90,
		marginRight: Sizes.margin.default,
	},

	badge: {
		alignSelf: 'flex-end',
		marginTop: Sizes.margin.default,
	},

	input: {
		marginTop: 8,
	},

	button: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	imageContainer: {
		marginTop: Sizes.margin.default,
	},

})
