import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';
import CellLego from '../cell'
import CellEditableLego from '../cell.editable'
import CellHintLego from '../cell.hint'

import Styles from './style';


export default ConnectHelper(
	class RowLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					...CellLego.propTypes,
					hint: PropTypes.string,
					editable: PropTypes.bool,
				})),
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
			row: true,
		}

		static getDerivedStateFromProps(nP) {
			return {
				length: nP.data.length - 1,
			}
		}

		constructor(p) {
			super(p, {
				length: 0,
			})
		}

		cellRenderer = (data, i) => {
			if(data.hint) {
				return (
					<CellHintLego key={i} { ...data } style={[i === this.state.length ? undefined : Styles.separator, data.style]} />
				)
			} else if(data.editable) {
				return (
					<CellEditableLego key={i} { ...data } style={[i === this.state.length ? undefined : Styles.separator, data.style]} />
				)
			} else {
				return (
					<CellLego key={i} { ...data } style={[i === this.state.length ? undefined : Styles.separator, data.style]} />
				)
			}
		}

		view() {
			return (
				<BoxBit unflex row={ this.props.row } style={this.props.style}>
					{ this.props.children ? this.props.children : this.props.data.map(this.cellRenderer) }
				</BoxBit>
			)
		}
	}
)
