import RecordModel from 'coeur/models/record';


function CouponRecordFactory(additionalConfig = {}) {
	return class CouponRecord extends RecordModel('coupon', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,

		type			: RecordModel.TYPES.STRING,
		amount			: RecordModel.TYPES.NUMBER,

		expiry			: RecordModel.TYPES.DATE,
		minimumSpend	: RecordModel.TYPES.NUMBER,
		maximumSpend	: RecordModel.TYPES.NUMBER,
		excludeSale		: RecordModel.TYPES.BOOL.FALSE,
		usageLimit		: RecordModel.TYPES.NUMBER,
		usageLimitItem	: RecordModel.TYPES.NUMBER,
		usageLimitPerUser	: RecordModel.TYPES.NUMBER,

		createdAt		: RecordModel.TYPES.DATE,
		publishedAt		: RecordModel.TYPES.DATE,

		matchboxIds		: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
	}
}


export default CouponRecordFactory
