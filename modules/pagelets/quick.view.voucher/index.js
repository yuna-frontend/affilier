import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'utils/constants/color'

import VoucherService from 'app/services/voucher';

import BoxBit from 'modules/bits/box';
import ClipboardBit from 'modules/bits/clipboard';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import TextAreaBit from 'modules/bits/text.area';

import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';


export default ConnectHelper(
	class QuickViewVoucherPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				data: PropTypes.shape({
					id: PropTypes.id,
					code: PropTypes.string,
					type: PropTypes.oneOf([
						'MATCHBOX',
						'SERVICE',
						'INVENTORY',
						'TOPUP_AMOUNT',
					]),
					ref_id: PropTypes.id,
					status: PropTypes.oneOf([
						'UNPUBLISHED',
						'AVAILABLE',
						'REDEEM_PENDING',
						'REDEEMED',
						'EXCEPTION',
					]),
					free_shipping: PropTypes.bool,
					is_physical: PropTypes.bool,
					name: PropTypes.string,
					email: PropTypes.string,
					note: PropTypes.string,
					expired_at: PropTypes.date,
					item: PropTypes.shape({
						title: PropTypes.string,
						image: PropTypes.object,
						colors: PropTypes.arrayOf(PropTypes.object),
						size: PropTypes.string,
						category: PropTypes.string,
					}),
				}),
			}
		}

		static propsToPromise(state, p) {
			return VoucherService.getDetail(p.id, state.me.token)
		}

		static contexts = [
			UtilitiesContext,
		]

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		viewOnError() {
			return (
				<BoxBit row centering style={ Styles.empty }>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.error }>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalQuickViewPagelet
					title={ `Voucher #VO-${ this.props.id }` }
					layout="1-way"
					onClose={ this.onClose }
				>
					<RowsLego
						data={[{
							data: [{
								headerless: true,
								children: (
									<BadgeStatusLego unflex status={ this.props.data.status } style={ Styles.badge } />
								),
							}],
						}, {
							data: [{
								title: 'Product',
								children: (
									<BoxBit row>
										{ this.props.data.item ? (
											<ImageBit source={ this.props.data.item.image } broken={ !this.props.data.item.image } style={ Styles.image } />
										) : (
											<ImageBit broken style={ Styles.image } />
										) }
										<BoxBit>
											<NavTextTitleBit linkless
												title={ this.props.data.item.brand }
												description={ this.props.data.item.title }
											/>
										</BoxBit>
									</BoxBit>
								),
							}],
						}, {
							data: [{
								title: 'Voucher Code',
								children: (
									<ClipboardBit value={ this.props.data.code } />
								),
							}],
						}, {
							data: [{
								title: 'Receiver',
								content: `${ this.props.data.name } (${ this.props.data.email })`,
							}],
						}, {
							data: [{
								title: 'Note',
								children: (
									<TextAreaBit value={ this.props.data.note } readonly autogrow={ 148 } />
								),
							}],
						}, {
							data: [{
								title: 'Expired At',
								content: TimeHelper.format(this.props.data.expired_at, 'DD MMMM YYYY [—] HH:mm'),
							}],
						}]}
						style={ Styles.content }
					/>
				</ModalQuickViewPagelet>
			)
		}
	}
)
