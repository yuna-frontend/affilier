import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import RowBrandLego from 'modules/legos/row.brand'
import RowCategoryLego from 'modules/legos/row.category'
import RowPacketLego from 'modules/legos/row.packet'
import RowProductLego from 'modules/legos/row.product'

import ProductService from 'app/services/product';

import UtilitiesContext from 'coeur/contexts/utilities';


import Styles from './style';

import HeaderPart from '../__header'


export default ConnectHelper(
	class ProductDataPart extends ConnectedStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propTypes(PropTypes) {
			return {
				onAddNewProduct: PropTypes.func,

				title: PropTypes.string,
				brand: PropTypes.string,
				brandId: PropTypes.number,
				category: PropTypes.string,
				categoryId: PropTypes.number,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			product:{
				brand: '',
				title: '',
				category_id: '',
				category: '',
			},
			productSave:{
				brand_id: '',
				title: '',
				category_id: '',
			},
			packet:{
				weight: 0.25,
				length: 30,
				width: 30,
				height: 5,
			},
		}

		constructor(p) {
			super(p, {
				isLoading:false,
				product:{
					brand: p.brand,
					title: p.title,
					category_id: p.category,
				},
				productSave:{
					brand_id: p.brandId,
					title: p.title,
					category_id: p.categoryId,
				},
				packet:{
					weight: 0.25,
					length: 30,
					width: 30,
					height: 5,
				},
			}, [
			])
		}

		onChange = (key, val, stringVal) => {
			if(key === 'brand' || key === 'category') {
				if(key === 'brand') {
					this.setState((prevState) => ({
						product:{
							...prevState.product,
							[key]:stringVal,
						},
						productSave:{
							...prevState.productSave,
							[key + '_id']:val,
						},
					}))
				} else if(key !== 'category') {
					this.setState((prevState) => ({
						product:{
							...prevState.product,
							[key]:stringVal,
						},
						productSave:{
							...prevState.productSave,
							[key]:val,
						},
					}))
				}
			} else if(key === 'category_id') {
				this.setState( (prevState) => ({
					product:{
						...prevState.product,
						[key]:stringVal,
					},
					productSave:{
						...prevState.productSave,
						[key]:val,
					},
				}))
			} else{
				this.setState( (prevState) => ({
					product:{
						...prevState.product,
						[key]:val,
					},
					productSave:{
						...prevState.productSave,
						[key]:val,
					},
				}))
			}
		}

		onChangePacket = (type) => {
			this.setState({
				packet: {
					weight: type.height,
					length: type.length,
					width: type.width,
					height: type.height,
				},
			})

		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onSubmit = () => {
			this.setState({isLoading:true})
			const data = {
				...this.state.productSave,
				packet:{
					...this.state.packet,
				},
			}

			ProductService.createProduct({
				brand_id: data.brand_id,
				category_id: data.category_id,
				title: data.title,
				packet:data.packet,
			}, this.props.token).then( res => {
				const newSelectedProduct = {
					id: res.id,
					brand: this.state.product.brand,
					title: this.state.product.title,
					category: this.state.product.category_id,
					category_id: this.state.productSave.category_id,
					variants: [],
				}
				this.props.onAddNewProduct(newSelectedProduct)
				this.setState({isLoading:false})
			})
				.catch((err) => {this.onError(err)})
		}

		view() {
			return (
				<BoxBit>
					<HeaderPart title="Product Data" style={Styles.header} />
					<BoxBit style={Styles.contentContainer}>
						<RowBrandLego
							brandId={ this.props.brandId }
							onChange={this.onChange.bind(this, 'brand')}
						/>
						<RowProductLego
							value={ this.props.title}
							onChange={this.onChange.bind(this, 'title')}
						/>
						<RowCategoryLego
							categoryId={ this.props.categoryId }
							onChange={this.onChange.bind(this, 'category_id')}
							onChangeParent={this.onChange.bind(this, 'category')}
						/>
						<RowPacketLego
							onChange={this.onChangePacket}
							packet={this.state.packet}
						/>
						<ButtonBit
							title="Save and Select Product"
							weight="medium"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							style={Styles.button}
							onPress={this.onSubmit}
							state={ this.state.isLoading
								? ButtonBit.TYPES.STATES.LOADING
								: ButtonBit.TYPES.STATES.NORMAL
							}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
