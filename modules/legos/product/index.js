import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class ProductLego extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				imageless: PropTypes.bool,
				image: PropTypes.image,
				id: PropTypes.id,
				title: PropTypes.string,
				description: PropTypes.string,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		content() {
			return (
				<React.Fragment>
					{ this.props.imageless ? false : (
						<ImageBit overlay resizeMode={ ImageBit.TYPES.CONTAIN } source={ this.props.image } style={ Styles.image } />
					) }
					<BoxBit style={ Styles.box }>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
							{ this.props.id }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
							{ this.props.title }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.description }>
							{ this.props.description }
						</GeomanistBit>
					</BoxBit>
				</React.Fragment>
			)
		}

		view() {
			return this.props.onPress ? (
				<TouchableBit row onPress={ this.props.onPress } style={[ Styles.product, this.props.style ]}>
					{ this.content() }
				</TouchableBit>
			) : (
				<BoxBit row style={[ Styles.product, this.props.style ]}>
					{ this.content() }
				</BoxBit>
			)
		}
	}
)
