import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import MailService from 'app/services/mail';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import HintLego from 'modules/legos/hint';
import RowLego from 'modules/legos/row';

import ModalClosablePagelet from 'modules/pagelets/modal.closable'
import ModalPromptPagelet from 'modules/pagelets/modal.prompt'

import Styles from './style'

import {
	isEmpty,
} from 'lodash'

function catcher(err) {
	if(err && err.code === 'ERR_101') {
		return null
	}

	throw err
}


export default ConnectHelper(
	class RowNotificationLego extends PromiseStatefulModel {

		static TYPES = {
			PAYMENT: 'PAYMENT',
			STYLE_PROFILE: 'STYLE_PROFILE',
			SHIPMENT: 'SHIPMENT',
			FEEDBACK: 'FEEDBACK',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.oneOfType([
					PropTypes.id,
					PropTypes.arrayOf(PropTypes.id),
				]).isRequired,
				type: PropTypes.oneOf(this.TYPES).isRequired,
				title: PropTypes.string,
				disabled: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			const key = Array.isArray(oP.id) ? oP.id.join(',') : oP.id

			return [key, Promise.resolve().then(() => {
				switch (oP.type) {
				case 'PAYMENT':
					return MailService.getLatestPaymentNotification(oP.id, state.me.token)
				case 'SHIPMENT':
					return MailService.getLatestShipmentNotification(Array.isArray(oP.id) ? oP.id : [oP.id], state.me.token)
				case 'STYLE_PROFILE':
					return MailService.getLatestSPQNotification(oP.id, state.me.token)
				case 'FEEDBACK':
					return MailService.getLatestFeedbackNotification(oP.id, state.me.token)
				default:
					return null
				}
			}).then(dataOrDatas => {
				if(dataOrDatas) {
					if(Array.isArray(oP.id)) {
						return oP.id.map((id, i) => {
							return dataOrDatas[i] ? {
								[id]: dataOrDatas[i].created_at,
							} : {}
						}).reduce((sum, data) => {
							return {
								...sum,
								...data,
							}
						}, {})
					} else {
						return {
							[oP.id]: dataOrDatas.created_at,
						}
					}
				}

				return dataOrDatas
			}).catch(catcher)]
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nP.data) && !nS.isLoaded) {
				return {
					isLoaded: true,
					lastSent: {
						...nP.data,
						...nS.lastSent,
					},
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoaded: false,
				isSending: false,
				lastSent: {},
			})
		}

		title = () => {
			switch (this.props.type) {
			case 'PAYMENT':
				return 'Send Payment Notification'
			case 'SHIPMENT':
				return 'Send Shipment Notification'
			case 'STYLE_PROFILE':
				return 'Send SPQ Notification'
			case 'FEEDBACK':
				return 'Send Feedback'
			default:
				return null
			}
		}

		hint = () => {
			switch (this.props.type) {
			case 'PAYMENT':
				return 'Enabled only when this order hasn\'t receive any payment yet.'
			case 'SHIPMENT':
				return 'Enabled only when there are \'Processing\' or \'Delivered\' shipments.'
			case 'STYLE_PROFILE':
				return 'Enabled only when user\'s Style Profile is not completed.'
			case 'FEEDBACK':
				return 'Enabled only when there are \'Packed\' order detail.'
			default:
				return null
			}
		}

		haveMultipleResult = () => {
			if(Array.isArray(this.props.id)) {
				return this.props.id.filter(id => {
					return !!this.state.lastSent[id]
				}).length > 1
			} else {
				return false
			}
		}

		lastSent = () => {
			if(!isEmpty(this.state.lastSent)) {
				if(Array.isArray(this.props.id)) {
					if(this.haveMultipleResult()) {
						return (
							<TouchableBit onPress={ this.onViewDates }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right" style={Styles.pressable}>
									{ Object.keys(this.state.lastSent).length } results found
								</GeomanistBit>
							</TouchableBit>
						)
					} else {
						return (
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right" >
								{ TimeHelper.format(Object.values(this.state.lastSent)[0], 'DD MMMM YYYY HH:mm') } (#{Object.keys(this.state.lastSent)[0]})
							</GeomanistBit>
						)
					}
				} else {
					return (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right" >
							{ TimeHelper.format(this.state.lastSent[this.props.id], 'DD MMMM YYYY HH:mm') }
						</GeomanistBit>
					)
				}
			} else {
				return (
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right" >
						-
					</GeomanistBit>
				)
			}
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onSingleIdSendEmail = () => {
			this.onSendEmail(this.props.id)
		}

		onMultipleIdSendEmail = () => {
			if(this.props.id.length > 1) {
				this.props.utilities.menu.show({
					title: 'Select ID',
					actions: this.props.id.map(id => {
						return {
							title: `#${id}`,
							onPress: this.onSendEmail.bind(this, id),
						}
					}),
				})
			} else {
				this.onSendEmail(this.props.id[0])
			}
		}

		onSendEmail = id => {
			this.setState({
				isSending: true,
			}, () => {
				Promise.resolve().then(() => {
					switch (this.props.type) {
					case 'PAYMENT':
						return MailService.sendPaymentNotification(id, this.props.token)
					case 'SHIPMENT':
						return MailService.sendShipmentNotification(id, this.props.token)
					case 'STYLE_PROFILE':
						return MailService.sendSPQNotification(id, this.props.token)
					case 'FEEDBACK':
						return new Promise((res, rej) => {
							this.props.utilities.alert.modal({
								component: (
									<ModalPromptPagelet
										title="URL"
										message="Input feedback URL (required)."
										placeholder="Input here…"
										onCancel={ () => {
											rej({
												detail: 'Cancelled',
											})

											this.onModalRequestClose()
										} }
										onConfirm={ url => {
											res(MailService.sendFeedbackNotification(id, { url }, this.props.token))
										} }
									/>
								),
							})
						})
					default:
						return Promise.reject(null)
					}
				}).then(data => {
					this.setState({
						lastSent: {
							...this.state.lastSent,
							[id]: data.created_at,
						},
						isSending: false,
					})

					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Email sent.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: err && (err.detail || err.message) || 'Something went wrong.',
					})

					this.setState({
						isSending: false,
					})
				})
			})
		}

		onViewDates = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalClosablePagelet title="Last Sent" contentContainerStyle={ Styles.content } onClose={ this.onModalRequestClose }>
						{ Object.keys(this.state.lastSent).map(id => {
							return (
								<BoxBit unflex row key={id} style={Styles.row}>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.title}>
										#{ id }
									</GeomanistBit>
									<BoxBit />
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										{ TimeHelper.format(this.state.lastSent[id], 'DD MMMM YYYY HH:mm') }
									</GeomanistBit>
								</BoxBit>
							)
						}) }
					</ModalClosablePagelet>
				),
			})
		}

		viewOnLoading() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						headerless: !this.props.title,
						children: (
							<BoxBit unflex style={Styles.empty}>
								<LoaderBit simple />
							</BoxBit>
						),
					}]}
				/>
			)
		}

		view() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						headerless: !this.props.title,
						children: (
							<ButtonBit
								title={ this.title() }
								align={ ButtonBit.TYPES.ALIGNS.LEFT }
								weight="normal"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
								onPress={ Array.isArray(this.props.id) ? this.onMultipleIdSendEmail : this.onSingleIdSendEmail }
							/>
						),
					}, {
						blank: !!this.props.title,
						headerless: !this.props.title,
						children: (
							<BoxBit row>
								{ this.props.disabled ? (
									<BoxBit unflex centering style={ Styles.hint}>
										<HintLego title={ this.hint() } />
									</BoxBit>
								) : false }
								<BoxBit>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right">
										Last sent:
									</GeomanistBit>
									{ this.lastSent() }
								</BoxBit>
							</BoxBit>
						),
						style: Styles.center,
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
