import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/connected.stateful'

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box'
import HeaderBit from 'modules/bits/header'
import GeomanistBit from 'modules/bits/geomanist'
import ButtonBit from 'modules/bits/button'
import TextAreaBit from 'modules/bits/text.area'
import TouchableBit from 'modules/bits/touchable'
import IconBit from 'modules/bits/icon'

import ModalPagelet from 'modules/pagelets/modal'
import Styles from './style'

export default ConnectHelper(
	class ShareComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				affiliate: PropTypes.string,
				route: PropTypes.string,
				onClose: PropTypes.func,
				onCopyLink: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				firstName: state.me.firstName,
				userId: state.me.id,
			}
		}

		header = {
			rightActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: () => this.onClose(),
			}],
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()
		}

		onCopyLink = () => {
			this.props.onCopyLink &&
			this.props.onCopyLink(this.shareLink())
		}

		shareLink = () => {
			const siteName = process.env.ENV === 'production' ? 'https://helloyuna.io/' : 'https://staging.helloyuna.io/'
			const route = this.props.route ? this.props.route : 'product/variant/'
			const id = this.props.id
			const affiliate = 'ref=' + this.props.userId
			
			const link = siteName + route + id + '?tr_source=affiliate&' + affiliate

			return link
		}

		headerRenderer = () => {
			return (
				<BoxBit centering unflex row style={[Styles.modalHeader, {height: 45}]}>
					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1}>
						Tautan
					</GeomanistBit>
					<TouchableBit unflex centering enlargeHitSlop onPress={this.onClose}>
						<IconBit
							name="close"
							size={20}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		view() {
			const link = this.shareLink()
			return (
				<ModalPagelet unclosable
					header={this.headerRenderer()}
					style={Styles.container}
					containerStyle={Styles.modalContainer}
					pageStyle={Styles.modalPage}
					contentContainerStyle={Styles.contentContainer}
				>
					<BoxBit unflex centering>
						{/* <GeomanistBit type={GeomanistBit.TYPES.HEADER_5}>
							Tautan
						</GeomanistBit> */}

						<BoxBit unflex>
							<TextAreaBit readonly
								defaultValue={link}
								style={Styles.url}
							/>
							<ButtonBit
								title="Salin"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
								state={ ButtonBit.TYPES.STATES.NORMAL }
								onPress= {this.onCopyLink }
								style={Styles.button}
							/>
						</BoxBit>
					</BoxBit>
				</ModalPagelet>
			)
		}
	}
)

