import ConnectHelper from 'coeur/helpers/connect';
import CoreScrollSnapBit from 'coeur/modules/bits/scroll.snap';

import ScrollViewBit from '../scroll.view';


export default ConnectHelper(
	class ScrollSnapBit extends CoreScrollSnapBit({
		ScrollViewBit,
	}) {}
)
