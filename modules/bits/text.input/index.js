import ConnectHelper from 'coeur/helpers/connect';
import CoreTextInputBit from 'coeur/modules/bits/text.input';

import Colors from 'coeur/constants/color';

import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class TextInputBit extends CoreTextInputBit({
		Styles,
		TouchableBit,
		placeholderColor: Colors.black.palette(2, .6),
	}) {}
)
