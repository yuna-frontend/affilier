/* eslint-disable no-nested-ternary */
import React from 'react'
import Connect from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import CheckboxBit from 'modules/bits/checkbox'
import IconBit from 'modules/bits/icon'
import GeomanistBit from 'modules/bits/geomanist'
import RadioBit from 'modules/bits/radio'
import TouchableBit from 'modules/bits/touchable'
import ButtonBit from 'modules/bits/button'
import ScrollSnapBit from 'modules/bits/scroll.snap'
import ScrollViewBit from 'modules/bits/scroll.view'

import FilterBoxLego from 'modules/legos/filter.box'

import ModalPagelet from 'modules/pagelets/modal'

import { isEmpty, capitalize } from 'lodash'

import Styles from './style'

const chooseCategoriesChildren = ({categoriesChildren}, ids) => {
	if(!isEmpty(ids)) {
		const data = categoriesChildren.find(c => c.id === ids[ids.indexOf(c.id)])
		if(!isEmpty(data)) {
			if(!isEmpty(data.categories)) {
				return {
					categoriesChildren: data.categories,
					isAllowedMultiple: false,
					index: 1,
				}
			}
			const grandChildren = categoriesChildren.find(c => c.id === data.category.id)
			return {
				categoriesChildren: grandChildren.categories,
				isAllowedMultiple: true,
				index: 1,
			}
		}
		// if categoryIds in query manually added and it's wrong id
		return {
			categoriesChildren: [],
			isAllowedMultiple: false,
			index: 0,
		}
	}
	return {
		categoriesChildren: [],
		isAllowedMultiple: false,
		index: 0,
	}
}

export default Connect(
	class CategoryModal extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				categories: PropTypes.object,
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				onChangeCategoryIds: PropTypes.func,
			}
		}

		constructor(p) {
			const { categoriesChildren, isAllowedMultiple, index } = chooseCategoriesChildren(p.categories, p.categoryIds)
			super(p, {
				categoryIdsModal: p.categoryIds,
				categoriesChildren,
				isAllowedMultiple,
				index,
			})
		}

		onClose = () => {
			this.props.handler.onClose()
		}

		onBackToCategoriesParent = () => {
			this.setState({
				...this.state,
				categoriesChildren: [],
				index: 0,
			})
		}

		onResetCategoryIdsModal = () => {
			this.setState({
				...this.state,
				categoryIdsModal: [],
				isAllowedMultiple: false,
				index: 0,
			}, () => {
				this.props.onChangeCategoryIds([], false)
				this.props.handler.onSelect({
					key: 0,
					title: this.props.getSelectionTitle([]),
				})
			})
		}

		onApplyCategoryIdsModal = () => {
			this.props.onChangeCategoryIds(this.state.categoryIdsModal, false)
			this.props.handler.onSelect({
				key: this.state.categoryIdsModal.toString(),
				title: this.props.getSelectionTitle(this.state.categoryIdsModal),
			})
		}

		onSelectCategory = (id, multiple, allItem) => {
			if(multiple) {
				this.setState({
					categoryIdsModal: this.state.categoryIdsModal.indexOf(id) > -1
						? [ ...(this.state.categoryIdsModal.filter(c => c !== id)) ]
						: [ ...this.state.categoryIdsModal, id ],
				})
			} else {
				const { categoriesChildren, isAllowedMultiple, index } = chooseCategoriesChildren(this.props.categories, [id])
				if(allItem || this.state.index) {
					this.setState({
						categoryIdsModal: [ id ],
						categoriesChildren,
						isAllowedMultiple,
					})
				} else {
					this.setState({
						categoriesChildren,
						isAllowedMultiple,
						index,
					})
				}
			}
		}

		onSelectAllItemParent = () => {
			// 'Semua Kategori's button
			this.setState({
				...this.state,
				categoryIdsModal: [],
			})
		}

		listMenuRenderer = ({
			id,
			title,
			category,
		}, isAllowedMultiple, i) => {
			return (
				// Hide Set, Man, Accessory
				<React.Fragment key={ i }>
					{ i === 0 && (
						<TouchableBit centering row unflex style={ Styles.menuListItem } onPress={ () => this.onSelectCategory(category.id, false, true) }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
								Semua { !isEmpty(category) ? category.title : 'Kategori' }
							</GeomanistBit>
							<BoxBit/>
							<RadioBit dumb isActive={ !isEmpty(this.state.categoryIdsModal) ? category.id === this.state.categoryIdsModal.find(c => c === category.id) : false } onPress={ () => this.onSelectCategory(category.id, false, true) } />
						</TouchableBit>
					) }
					<TouchableBit centering row unflex onPress={ () => this.onSelectCategory(id, isAllowedMultiple) } style={ Styles.menuListItem }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
							{ capitalize(title) }
						</GeomanistBit>
						<BoxBit/>
						{ !this.state.index ? (
							<BoxBit unflex>
								<IconBit name="arrow-right" size={ 20 }/>
							</BoxBit>
						) : (
							<CheckboxBit dumb isActive={ !isEmpty(this.state.categoryIdsModal) ? id === this.state.categoryIdsModal.find(c => c === id) : false } onPress={ () => this.onSelectCategory(id, isAllowedMultiple) } />
						) }
					</TouchableBit>
				</React.Fragment>
			)
		}

		listCollapsibleRenderer = (item, isAllowedMultiple, i) => {
			return (
				<React.Fragment key={ i }>
					{ i === 0 && (
						<TouchableBit
							row
							unflex
							style={ Styles.menuListItem }
							onPress={ this.onSelectAllItemParent }
						>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
								Semua Kategori
							</GeomanistBit>
							<BoxBit/>
							<RadioBit dumb isActive={ isEmpty(this.state.categoryIdsModal) } onPress={ this.onSelectAllItemParent }/>
						</TouchableBit>
					) }
					<FilterBoxLego title={ item.title }>
						<BoxBit unflex style={ Styles.menuListItemsCollapsible }>
							{ item.categories.map((data, index) => {
								// Hide Set and Man in clothing
								// Hide accessory in accessory
								if(
									data.title !== 'Set' &&
									data.title !== 'Man' &&
									data.title !== 'accessory'
								) { return this.listMenuRenderer(data, isAllowedMultiple, index) }

								return false
							}) }
						</BoxBit>
					</FilterBoxLego>
				</React.Fragment>
			)
		}

		headerRenderer = () => {
			return (
				<BoxBit centering unflex row style={[Styles.menuListHeader, {height: 55}]}>
					{ this.state.index > 0 &&
						<TouchableBit centering unflex onPress={ this.onBackToCategoriesParent }>
							<IconBit name="arrow-left" size={ 30 }/>
						</TouchableBit>
					}
					<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_1 }>
						PILIH KATEGORI
					</GeomanistBit>
					<TouchableBit unflex centering enlargeHitSlop onPress={ this.onClose }>
						<IconBit
							name="close"
							size={ 20 }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		footerRenderer = () => {
			return (
				<BoxBit row unflex style={Styles.menuListFooter}>
					<BoxBit style={ Styles.menuListFooterBtn }>
						<ButtonBit
							unflex
							title="Reset"
							theme={ ButtonBit.TYPES.THEMES.OUTLINE }
							size={ ButtonBit.TYPES.SIZES.MEDIUM }
							onPress={ this.onResetCategoryIdsModal }
						/>
					</BoxBit>
					<BoxBit style={ Styles.menuListFooterBtn }>
						<ButtonBit
							unflex
							title="Terapkan"
							size={ ButtonBit.TYPES.SIZES.MEDIUM }
							onPress={ this.onApplyCategoryIdsModal }
						/>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<ModalPagelet unclosable header={ this.headerRenderer() } footer={ this.footerRenderer() } style={ Styles.menuListContent } pageStyle={ Styles.modalMenuPage } containerStyle={ Styles.menuListContainer }>
					<ScrollSnapBit disableGesture index={ this.state.index }>
						<ScrollViewBit style={ Styles.menuListItems }>
							{ this.props.categories.categoriesParent.map((item, index) => {
								return this.listCollapsibleRenderer(item, false, index)
							}) }
						</ScrollViewBit>
						<ScrollViewBit style={ Styles.menuListItems }>
							{ !isEmpty(this.state.categoriesChildren) &&
								this.state.categoriesChildren.map((item, index) => {
									return this.listMenuRenderer(item, this.state.isAllowedMultiple, index)
								})
							}
						</ScrollViewBit>
					</ScrollSnapBit>
				</ModalPagelet>
			)
		}
	}
)
