import React from 'react'
import Connect from 'coeur/helpers/connect'
import QueryStatefulModel from 'coeur/models/query.stateful'

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'

import CategoryPart from '../_category'
import ColorPart from './_color'
import SizePart from './_size'
import PricePart from './_price'
import Styles from './style'

export default Connect(
	class FilterPart extends QueryStatefulModel {
		static propTypes(PropTypes) {
			return {
				onChangeFilter: PropTypes.func,
				onPressFilter: PropTypes.func,
				onChangeCategoryIds: PropTypes.func,
				onResetFilter: PropTypes.func,
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				filter: PropTypes.object,
				mobile: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p)
		}

		view() {
			const { mobile } = this.props

			return Sizes.screen.width <= 800 ? (
				<BoxBit unflex={ !mobile } row style={[Styles.container, this.props.styleContainer]}>
					<TouchableBit row centering unflex={ !mobile } onPress={ this.props.onPressFilter } style={[Styles.content, mobile && Styles.contentMobile]}>
						<GeomanistBit type={ !mobile ? GeomanistBit.TYPES.PARAGRAPH_2 : GeomanistBit.TYPES.PARAGRAPH_3} weight={ !mobile ? 'bold' : 'normal' }>
							Filter
						</GeomanistBit>
						<BoxBit unflex style={ Styles.div }/>
						<IconBit name="menu" size={ 16 }/>
					</TouchableBit>
				</BoxBit>
			) : (
				<BoxBit unflex>
					<BoxBit unflex>
						<BoxBit unflex row style={ Styles.headerFilter }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold">
								Filter
							</GeomanistBit>
							<TouchableBit unflex onPress={ this.props.onResetFilter }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.greyBase }>
									Reset
								</GeomanistBit>
							</TouchableBit>
						</BoxBit>

						<CategoryPart categoryIds={this.props.categoryIds} onChangeCategoryIds={this.props.onChangeCategoryIds}/>
						<ColorPart colorIds={this.props.filter.colorIds} onChangeColorIds={val => this.props.onChangeFilter(null, { colorIds: val }, true)}/>
						<SizePart key={ this.props.categoryIds } categoryIds={ this.props.categoryIds } sizeIds={ this.props.filter.sizeIds } onChangeSizeIds={ val => this.props.onChangeFilter('sizeIds', val, true) }/>
						<PricePart price={ this.props.filter.price } onChangePrice={ val => this.props.onChangeFilter('price', val) }/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
