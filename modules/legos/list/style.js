import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderColor: Colors.black.palette(3, .2),
		borderWidth: 0,
		borderBottomWidth: StyleSheet.hairlineWidth,
		justifyContent: 'space-between',
		alignItems: 'flex-start',
	},

	containerEven: {
		backgroundColor: Colors.red.primary,
	},

	center: {
		alignItems: 'center',
		// HACK: cannot truncate if we don't specify width
		width: 100,
	},

	head: {
		borderTopWidth: StyleSheet.hairlineWidth,
	},

	noBorder: {
		borderTopWidth: 0,
		borderBottomWidth: 0,
	},

	text: {
		color: Colors.black.palette(3),
	},

	blueGrey: {
		color: Colors.grey.primary,
	},
})
