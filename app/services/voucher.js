import ServiceModel from 'coeur/models/service';
// import { isEmpty } from 'lodash'


class VoucherService extends ServiceModel {

	static displayName = 'voucher'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/voucher/', 0)
	}

	getDetail(id, token) {
		return this.get(id, {
			token,
		})
	}

}

export default new VoucherService()
