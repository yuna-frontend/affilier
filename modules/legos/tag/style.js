import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	padderBottom16: {
		marginBottom: 16,
	},
	dark90: {
		color: Colors.black.palette(1, .9),
	},
	darkGrey80: {
		color: Colors.black.palette(2, .8),
	},
	wrap: {
		flexWrap: 'wrap',
	},
	choice: {
		paddingBottom: 16,
	},
	title: {
		color: Colors.black.palette(1, .4),
		paddingTop: 8,
		lineHeight: 24,
	},
})
