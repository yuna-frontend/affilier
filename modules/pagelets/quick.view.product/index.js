import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';


import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

import ColorLego from 'modules/legos/color';
import LoaderLego from 'modules/legos/loader';
import QuickViewCellLego from 'modules/legos/quick.view.cell';
import QuickViewImagesGalleryLego from 'modules/legos/quick.view.images.gallery';
import QuickViewOverviewLego from 'modules/legos/quick.view.overview';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';

import { capitalize, groupBy, isEmpty, uniqBy } from 'lodash'


export default ConnectHelper(
	class QuickViewProductPagelet extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				// either id or variantId must be supplied
				id: PropTypes.number,
				variantId: PropTypes.number,
				variants: PropTypes.bool,
				includeZeroInventory: PropTypes.bool,
				mayAddToStylesheet: PropTypes.bool,
				mayAddToStylecard: PropTypes.bool,
				mayBook: PropTypes.bool,
				onClose: PropTypes.func,
				onNavigateToProduct: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToQuery(state, oP) {
			const variants = oP.variantId && oP.variants !== undefined ? oP.variants : true

			return [ oP.id ? `query {
				productById(id: ${ oP.id }) {
					title
					description
					brand { title }
					categoryId
          			category { title category { title } }
          			productTags { tag { id title tag { id title } } }
					variants (condition: { ${ !variants ? `id: ${ oP.variantId }` : 'deletedAt: null' } }) {
						id note price retailPrice isPublished size { id title sizeId }
						variantColors(orderBy: ORDER_ASC) {
							color { hex image title }
						}
						variantAssets(orderBy: ORDER_ASC, condition: { deletedAt: null }) {
							url
              				metadata
						}
            			variantTags { tag { id title tag { id title } } }
						inventories(condition: { deletedAt: null, status: AVAILABLE }) { id }
					}
				}
			}` : `query {
				variantById(id: ${ oP.variantId }) {
					product {
						title
						description
						brand { title }
						categoryId
						category { title category { title } }
						productTags { tag { id title tag { id title } } }
						variants (condition: { ${ !variants ? `id: ${ oP.variantId }` : 'deletedAt: null' } }) {
							id note price retailPrice isPublished size { id title sizeId }
							variantColors(orderBy: ORDER_ASC) {
								color { hex image title }
							}
							variantAssets(orderBy: ORDER_ASC, condition: { deletedAt: null }) {
								url
								metadata
							}
							variantTags { tag { id title tag { id title } } }
							inventories(condition: { deletedAt: null, status: AVAILABLE }) { id }
						}
					}
				}
			}`, {
				fetchPolicy: 'no-cache',
			}, state.me.token]
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {
			if (isEmpty(nP.data)) {
				return null
			} else if (nS.data === null) {
				const product = nP.id ? nP.data.productById : nP.data.variantById.product
				const productTags = product.productTags.map(pT => {
					return {
						id: pT.tag.id,
						title: pT.tag.title,
						tagId: pT.tag.tag ? pT.tag.tag.id : null,
						parent: pT.tag.tag ? pT.tag.tag.title : null,
					}
				})

				const colorByKey = {}
				const sizeById = {}
				const sizeByColorKey = {}

				const variants = product.variants.map(variant => {
					const quantity = variant.inventories.length

					if (!nP.includeZeroInventory && !quantity) {
						return false
					}

					const colors = variant.variantColors.map(vC => {
						return {
							...vC.color,
							title: capitalize(vC.color.title),
						}
					})

					const colorKey = colors.map(c => c.title).join('/')
					colorByKey[colorKey] = colors

					sizeById[variant.size.id] = variant.size

					if (sizeByColorKey[colorKey] === undefined) {
						sizeByColorKey[colorKey] = []
					}

					if (sizeByColorKey[colorKey].indexOf(variant.size.id) === -1) {
						sizeByColorKey[colorKey].push(variant.size.id)
					}

					const tags = uniqBy(productTags.concat(variant.variantTags.map(vT => {
						return {
							id: vT.tag.id,
							title: vT.tag.title,
							tagId: vT.tag.tag ? vT.tag.tag.id : null,
							parent: vT.tag.tag ? vT.tag.tag.title : null,
						}
					})), 'id')

					return {
						id: variant.id,
						isPublished: variant.isPublished,
						note: variant.note,
						price: variant.price,
						retailPrice: variant.retailPrice,
						size: variant.size.title,
						sizeId: variant.size.id,
						colors,
						colorKey,
						assets: variant.variantAssets,
						tags,
						tagsByParent: groupBy(tags, 'parent'),
						quantity,
					}
				}).filter(v => !!v)

				const data = {
					title: product.title,
					brand: product.brand.title,
					description: product.description,
					category: `${ product.category.title }`,
					categoryId: product.categoryId,
					colorKeys: Object.keys(colorByKey),
					colorByKey,
					sizeById,
					sizeByColorKey,
					variants,
					quantity: variants.map(v => v.quantity).reduce((sum, c) => sum + c, 0),
				}

				return {
					...nS,
					data,
					selectedVariantId: nS.selectedVariantId || data.variants[0].id,
				}
			} else {
				return null
			}
		}

		static defaultProps = {
			variants: true,
		}

		constructor(p) {
			super(p, {
				isBooking: false,
				data: null,
				selectedVariantId: p.variantId,
			})
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onChangeVariant = variantId => {
			this.setState({
				selectedVariantId: variantId,
			})
		}

		colorsRenderer = (variant, colorKey) => {
			const isActive = colorKey === variant.colorKey
			const colors = this.state.data.colorByKey[colorKey]
			let selectedVariant = this.state.data.variants.find(v => v.colorKey === colorKey && v.sizeId === variant.sizeId)

			if (!selectedVariant) {
				selectedVariant = this.state.data.variants.find(v => v.colorKey === colorKey)
			}

			return (
				<TouchableBit unflex centering key={ colorKey }
					onPress={ this.onChangeVariant.bind(this, selectedVariant.id) }
					style={[ Styles.color, isActive ? Styles.colorActive : undefined ]}
				>
					<ColorLego radius={ isActive ? 14 : 14 } size={ isActive ? 28 : 28 } colors={ colors } />
				</TouchableBit>
			)
		}

		sizeRenderer = (variant, sizeId) => {
			const size = this.state.data.sizeById[sizeId]
			const isActive = variant.sizeId === sizeId
			const selectedVariant = this.state.data.variants.find(v => v.colorKey === variant.colorKey && v.sizeId === sizeId)

			return (
				<TouchableBit unflex centering key={ sizeId }
					onPress={ this.onChangeVariant.bind(this, selectedVariant.id) }
					style={[ Styles.color, isActive ? Styles.colorActive : undefined ]}
				>
					<GeomanistBit align="center" type={ GeomanistBit.TYPES.SUBHEADER_3 } weight="normal" style={ Styles.size }>
						{ size.title.toUpperCase() }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		tagRenderer = (tags, parent) => {
			return (
				<BoxBit key={ parent } unflex style={ Styles.tags }>
					<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_4 } weight="medium" style={ Styles.type }>
						{ parent.toUpperCase() }
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_3 } style={ Styles.tag }>
						{ tags[parent].map(t => t.title.toUpperCase()).join(', ') }
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			const variant = this.state.data.variants.find(v => v.id === this.state.selectedVariantId)

			return (
				<ModalQuickViewPagelet
					title="Product Preview"
					layout="3-way"
					onClose={ this.props.onClose }
				>
					<ScrollViewBit style={ Styles.scroll }>
						<QuickViewOverviewLego
							id={ `#VA-${ variant.id }` }
							subheader={ this.state.data.brand }
							title={ this.state.data.title }
							mark={ this.state.data.category.toUpperCase() }
							price={ variant.price }
							retailPrice={ variant.retailPrice }
							description={ this.state.data.description }
						/>
					</ScrollViewBit>
					<QuickViewImagesGalleryLego
						key={ variant.colorKey }
						images={ variant.assets }
					/>
					<ScrollViewBit style={ Styles.detail }>
						<BoxBit row unflex>
							<QuickViewCellLego unflex={ false }
								title="COLOR"
								content={ variant.colorKey }
							>
								<BoxBit unflex row style={ Styles.wrapper }>
									{ this.state.data.colorKeys.map(this.colorsRenderer.bind(this, variant)) }
								</BoxBit>
							</QuickViewCellLego>
							<QuickViewCellLego unflex={ false }
								title="SIZE"
								content={ variant.size }
							>
								{ this.state.data.sizeByColorKey[variant.colorKey].length > 1 ? (
									<BoxBit row style={ Styles.wrapper }>
										{ this.state.data.sizeByColorKey[variant.colorKey].map(this.sizeRenderer.bind(this, variant)) }
									</BoxBit>
								) : false }
							</QuickViewCellLego>
						</BoxBit>
						{/* <QuickViewCellLego
							title="QUANTITY"
							content={ `${ variant.quantity }/${ this.state.data.quantity }` }
						/> */}
						<QuickViewCellLego
							title="TAGS"
							content={ `${ variant.tags.length }` }
						>
							{ variant.tags.length ? (
								<BoxBit row style={ Styles.wrapper }>
									{ Object.keys(variant.tagsByParent).map(this.tagRenderer.bind(this, variant.tagsByParent)) }
								</BoxBit>
							) : false }
						</QuickViewCellLego>

						<BoxBit />
					</ScrollViewBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
