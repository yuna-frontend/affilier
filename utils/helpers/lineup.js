export default {
	lineups: [{
		key: null,
		title: 'None',
	}, {
		key: 'Clothing + Clothing + Clothing + Clothing',
		title: 'Clothing + Clothing + Clothing + Clothing',
	}, {
		key: 'Clothing + Clothing + Clothing + Footwear',
		title: 'Clothing + Clothing + Clothing + Footwear',
	}, {
		key: 'Clothing + Clothing + Clothing + Bag',
		title: 'Clothing + Clothing + Clothing + Bag',
	}, {
		key: 'Clothing + Clothing + Clothing + Accessory',
		title: 'Clothing + Clothing + Clothing + Accessory',
	}, {
		key: 'Clothing + Clothing + Bag + Accessory',
		title: 'Clothing + Clothing + Bag + Accessory',
	}, {
		key: 'Clothing + Clothing + Footwear + Accessory',
		title: 'Clothing + Clothing + Footwear + Accessory',
	}, {
		key: 'Clothing + Clothing + Footwear + Bag',
		title: 'Clothing + Clothing + Footwear + Bag',
	}],
}
