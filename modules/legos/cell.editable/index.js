import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import IconBit from 'modules/bits/icon';


export default ConnectHelper(
	class CellLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				content: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.number,
				]),
				description: PropTypes.string,

				blank: PropTypes.bool,
				children: PropTypes.node,
				onPress: PropTypes.func,

				style: PropTypes.style,
				headerStyle: PropTypes.style,
			}
		}

		view() {
			return (
				<CellLego
					header={( <IconBit name="edit" size={20} color={Colors.primary} /> )}
					title={this.props.title}
					content={this.props.content}
					description={this.props.description}
					blank={this.props.blank}
					children={this.props.children}
					onPress={this.props.onPress}
					style={this.props.style}
					headerStyle={this.props.headerStyle}
				/>
			)
		}
	}
)
