import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import TextInputBit from 'modules/bits/text.input';

import LoaderLego from 'modules/legos/loader';
import SelectionStylesheetLego from 'modules/legos/selection.stylesheet';
import SelectStylistLego from 'modules/legos/select.stylist';

import HeaderFilterComponent from 'modules/components/header.filter';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class PopupDuplicateStylecardPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				count: PropTypes.number,
				offset: PropTypes.number,
				limit: PropTypes.number,
				onClose: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			offset: 0,
			limit: 25,
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				isLoading: true,
				isDuplicating: false,
				selectedId: -1,
				data: [],
				offset: p.offset,
				limit: p.limit,
				total: 0,
				stylist: undefined,
				stylistId: undefined,
				search: undefined,
			})
		}

		getterId = 1

		componentDidMount() {
			this.getData()
		}

		getData = () => {
			const {
				offset,
				limit,
				search,
				stylistId,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				StylecardService.getStylecards({
					offset: offset,
					limit: limit,
					search,
					user_id: stylistId,
					with_variant: true,
					status: 'PUBLISHED',
					is_master: true,
				}, this.props.token).then(res => {
					if (id === this.getterId) {
						this.setState({
							isLoaded: true,
							isLoading: false,
							total: res.count,
							limit: res.data.length,
							data: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.props.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onNavigationChange = (offset, limit) => {
			this.setState({
				offset,
				limit,
			}, this.getData)
		}

		onUpdateSearch = (e, val) => {
			this.state.search = val
		}

		onChangeStylist = (id, name) => {
			this.setState({
				stylist: name,
				stylistId: id,
			}, this.getData)
		}

		onToggleSelection = id => {
			this.setState({
				selectedId: id,
			})
		}

		onSelectStylesheet = consent => {
			this.setState({
				isDuplicating: true,
			}, () => {
				StylecardService.duplicate(this.props.id, this.state.selectedId, consent, this.props.token)
					.then(successes => {
						StylecardService.setStatus(this.props.id, 'STYLING', null, this.props.token)
						this.onClose()

						this.props.utilities.alert.show({
							title: 'Duplication Success',
							message: `${ successes.filter(s => s === true).length } out of ${ successes.length } items successfully dulpicated. You are now required to reload the page.`,
							actions: [{
								title: 'Reload',
								onPress: () => {
									window.location.reload()
								},
							}],
						})
					})
					.catch(err => {
						this.warn(err)

						if (err && err.code === 'ERR_109') {
							this.props.utilities.alert.show({
								title: 'Oops…',
								message: err.detail,
								actions: [{
									title: 'YES PLEASE',
									onPress: () => {
										this.onSelectStylesheet(true)
									},
								}, {
									title: 'CANCEL',
									type: 'CANCEL',
								}],
							})
						} else {
							this.props.utilities.notification.show({
								title: 'Oops',
								message: err ? err.detail || err.message || err : 'Something went wrong with your request. Please try again.',
							})

							this.setState({
								isDuplicating: false,
							})
						}
					})
			})
		}

		stylecardRenderer = stylecard => {
			return (
				<SelectionStylesheetLego
					type={ SelectionStylesheetLego.TYPES.STYLECARD }
					key={ stylecard.id }
					id={ stylecard.id }
					user={ stylecard.user }
					stylist={ stylecard.stylist }
					title={ stylecard.title }
					lineup={ stylecard.lineup }
					count={ this.props.count !== undefined ? this.props.count : stylecard.count }
					items={ stylecard.variants.map(variant => {
						return {
							variant_id: variant.variant_id,
							title: variant.title,
							brand: variant.brand,
							image: variant.asset,
							category: variant.category,
							color: variant.color,
							colors: variant.colors,
							size: variant.size,
							price: variant.price,
							retail_price: variant.retail_price,
						}
					}) }
					selected={ this.state.selectedId === stylecard.id }
					percentage={ false }
					addedRetail={ 0 }
					addedPurchase={ 0 }
					addedCount={ 0 }
					onPress={ this.onToggleSelection }
					style={ Styles.stylesheet }
				/>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return !this.state.isLoaded ? this.viewOnLoading() : (
				<ModalClosableConfirmationPagelet
					title="Duplicate Style Card"
					// header: PropTypes.node,
					subheader={(
						<HeaderFilterComponent
							current={ this.state.offset }
							count={ this.state.limit }
							total={ this.state.total }
							onNavigationChange={this.onNavigationChange}
							style={ Styles.header }>
							<TextInputBit
								placeholder={ 'Search…' }
								onChange={ this.onUpdateSearch }
								onSubmitEditing={ this.getData }
								inputStyle={ Styles.input }
								style={ Styles.search }
								postfix={(
									<IconBit
										name="search"
										size={ 20 }
										color={ Colors.black.palette(2, .6) }
										style={ Styles.icon }
									/>
								)}
							/>
							<SelectStylistLego unflex stylist={ this.state.stylist } onChange={ this.onChangeStylist } style={ Styles.stylist } />
						</HeaderFilterComponent>
					)}
					loading={ this.state.isDuplicating }
					disabled={ this.state.selectedId === -1 }
					confirm={ 'Select' }
					onCancel={ this.onClose }
					onClose={ this.onClose }
					onConfirm={ this.onSelectStylesheet.bind(this, undefined) }
					style={ Styles.container }
					contentContainerStyle={ Styles.content }
				>
					{/* eslint-disable-next-line no-nested-ternary */}
					{ this.state.isLoading ? (
						<BoxBit centering>
							<LoaderBit simple />
						</BoxBit>
					) : this.state.data.length ? (
						<BoxBit unflex row style={ this.state.isDuplicating ? Styles.duplicating : Styles.wrapper }>
							{ this.state.data.map(this.stylecardRenderer) }
						</BoxBit>
					) : (
						<BoxBit centering>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								No style card found
							</GeomanistBit>
						</BoxBit>
					) }
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
