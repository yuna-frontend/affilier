import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import TextAreaBit from 'modules/bits/text.area';
import TextBit from 'modules/bits/text';

import NotificationLego from 'modules/legos/notification';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class StylistNotePagelet extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				client: PropTypes.string,
				note: PropTypes.string,
				value: PropTypes.string,
				onClose: PropTypes.func,
				onSubmit: PropTypes.func,
			}
		}

		static defaultProps = {
			title: 'Add Stylist Note',
		}

		constructor(p) {
			super(p, {
				isSubmitting: false,
				len: p.value ? p.value.length : 0,
				value: p.value,
				maxlength: 1500,
			})
		}

		onChange = (e, val) => {
			if(val.length <= this.state.maxlength) {
				this.setState({
					value: val,
					len: val.length,
				})
			}
		}

		onSubmit = () => {
			this.setState({
				isSubmitting: true,
			}, () => {
				this.props.onSubmit &&
				this.props.onSubmit(this.state.value).then(() => {
					this.setState({
						isSubmitting: false,
					})
				})
			})
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ this.props.title }
					confirm="Publish"
					loading={ this.state.isSubmitting }
					onConfirm={ this.onSubmit }
					onClose={ this.props.onClose }
					onCancel={ this.props.onClose }
					contentContainerStyle={ Styles.container }
				>
					<NotificationLego type={ NotificationLego.TYPES.WARNING } message="Please make sure that your note and selections are correct." />
					{ this.props.note ? (
						<React.Fragment>
							<BoxBit unflex style={ Styles.note }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
									<TextBit weight="medium">{ this.props.client } says… </TextBit>{ this.props.note }
								</GeomanistBit>
							</BoxBit>
							<BoxBit unflex style={ Styles.reply }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">
									Your reply:
								</GeomanistBit>
							</BoxBit>
						</React.Fragment>
					) : false }
					<TextAreaBit autogrow={false} disabled={ this.state.isSubmitting }
						defaultValue={ this.props.value }
						maxlength={ this.state.maxlength }
						placeholder={ this.props.note ? 'Input your response here…' : 'Input your note here…' }
						onChange={ this.onChange }
						style={ Styles.content }
						inputStyle={ Styles.textarea }
					/>
					<GeomanistBit
						type={ GeomanistBit.TYPES.NOTE_1 }
						weight="medium"
						align="right"
						style={ Styles.text }
					>
						{ this.state.len }/{ this.state.maxlength }
					</GeomanistBit>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
