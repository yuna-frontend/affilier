import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputCheckboxBit from 'modules/bits/input.checkbox';
import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import RowLego from 'modules/legos/row';
import { isEqual } from 'lodash';

import Styles from './style';


export default ConnectHelper(
	class RowBrandLego extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				isGift: PropTypes.bool,
				isPhysical: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			isGift: false,
			isPhysical: false,
		}

		constructor(p) {
			super(p, {
				isGift: p.isGift,
				isPhysical: p.isPhysical,
			})
		}

		componentDidUpdate(pP, pS) {
			if(!isEqual(pS.isGift, this.state.isGift) || !isEqual(pS.isPhysical, this.state.isPhysical)) {
				this.props.onChange &&
				this.props.onChange({
					...this.state,
				})
			}
		}

		onChangePhysical = (val) => {
			this.setState({
				isPhysical: val,
			})
		}

		onChangeGift = (val) => {
			this.setState({
				isGift: val,
				isPhysical: !val && false,
			})
		}

		view() {
			return (
				<RowLego
					data={[{
						children: (
							<BoxBit row unflex>
								<InputCheckboxBit
									value={this.state.isGift}
									onPress={this.onChangeGift}
								/>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>eGift Card</GeomanistBit>
							</BoxBit>
						),
						headerStyle: Styles.header,
					}, this.state.isGift ? {
						children: (
							<BoxBit row unflex>
								<InputCheckboxBit
									value={this.state.isPhysical}
									onPress={this.onChangePhysical}
								/>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>Physical Gift Card</GeomanistBit>
							</BoxBit>
						),
						headerStyle: Styles.header,
					} : {}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
