import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'
import Defaults from 'coeur/constants/default'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		padding: Sizes.margin.default,
	},

	button: {
		maxWidth: 200,
	},

	page: {
		padding: Sizes.margin.default,
		maxWidth: 560,
	},

	copyIcon: {
		paddingLeft: 6,
	},

	container: {
		maxHeight: '100%',
		padding: Sizes.margin.default,
	},

	modalContainer: {
		height: 'auto',
		maxHeight: '75%',
		width: Sizes.screen.width / 2,
		background: Colors.white.primary,
		borderRadius: 8,
	},

	modalPage: {
		justifyContent: 'center',
	},

	modalHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	contentContainer: {
		width: Sizes.screen.width / 2,
		paddingTop: 0,
	},

	url: {
		marginTop: 6,
		width: 500,
	},

	btnModal: {
		marginTop: 6,
		alignSelf: 'center',
	},

	...(Defaults.PLATFORM === 'web' ? {
		'@media screen and (max-width: 800px)': {
			modalContainer: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
				margin: Sizes.margin.thick,
			},

			contentContainer: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
			},

			header: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
			},
		},

		'@media screen and (max-width: 480px)': {
			url: {
				width: 'auto',
			},
		},
	} : {}),
})
