import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	select: {
		marginLeft: Sizes.margin.default,
	},

	shipments: {
		marginTop: 12,
	},

	shipment: {
		marginTop: 4,
	},

})
