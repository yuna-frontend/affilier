import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	listCollapsible: {
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
	},

	listColorsRow: {
		flexWrap: 'wrap',
		justifyContent: 'space-between',
	},

	listColor: {
		marginBottom: Sizes.margin.default,
		...(Defaults.PLATFORM === 'web' && {
			'&:hover $listColorContainer': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	listColorRow: {
		width: '18%',
		justifyContent: 'space-between',
	},

	listColorContainer: {
		borderRadius: '100%',
		width: 24,
		height: 24,
		padding: 3,
		borderWidth: 2,
		borderColor: Colors.grey.palette(8),
		...(Defaults.PLATFORM === 'web' && {
			'&:hover': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	listColorContainerBig: {
		width: 48,
		height: 48,
		marginBottom: 8,
	},

	listColorContainerActive: {
		borderColor: Colors.black.primary,
	},

	listColorImage: {
		borderRadius: '100%',
		height: '100%',
		overflow: 'hidden',
	},

	listColorBg: {
		borderRadius: '100%',
	},

	listColorTitle: {
		marginLeft: Sizes.margin.default / 2,
		width: '100%',
	},

	listColorTitleRow: {
		marginLeft: 0,
		flex: 1,
		textAlign: 'center',
	},
})
