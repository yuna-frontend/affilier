import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	content: {
		height: 44,
	},

	small: {
		height: 34,
		paddingLeft: 16,
		paddingRight: 16,
	},
})
