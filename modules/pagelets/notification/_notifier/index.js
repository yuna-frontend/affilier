import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	NotifierPart as CoreNotifierPart,
} from 'coeur/modules/pagelets/notification';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'


export default ConnectHelper(
	class NotifierPart extends CoreNotifierPart({
		Styles,
		BoxBit,
		IconBit,
		TextBit: GeomanistBit,
		TouchableBit,
		iconColor: Colors.white.primary,
	}) {

		titleRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_1} style={style}>
					{ this.props.title }
				</GeomanistBit>
			)
		}

		messageRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight={'medium'} style={style}>
					{ this.props.message }
				</GeomanistBit>
			)
		}

	}
)
