import ManagerModel from 'coeur/models/manager'

import ErrorHelper from 'coeur/helpers/error'

import AccountService from 'app/services/account'
import MeService from 'app/services/me'
import MediaService from 'app/services/media'
import TrackingHandler from 'app/handlers/tracking';

import UserRecord from 'app/records/user'


class MeManager extends ManagerModel {

	static displayName = 'me'

	constructor() {
		super(UserRecord, {
			additionalProperties: {
				email			: '',
				phone			: '',
				token			: '',
				// defaultPaymentChannelId		: null,
				// defaultPaymentMethodId		: null,
				// addressIds					: [],
				// batchIds					: [],
				// mediaIds					: [],
				roles						: [],
			},
			expiricy: 1000 * 60 * 60 * 24 * 7,
		})
	}

	// ------------------------------------
	// Login & Register
	// ------------------------------------

	authenticate(token) {
		if(token) {
			// return MeService.authenticate(token).then(response => {
			// 	if(response.roles.length && response.roles.indexOf('admin') > -1) {
			// 		return this.update(response)
			// 	} else {
			// 		return false
			// 	}
			// })
			return MeService.authenticate(token).then(response => {
				return this.update(response)
			})
		} else {
			return Promise.resolve(false)
		}
	}

	login({
		email,
		password,
	}) {
		return AccountService
			.login({
				email,
				password,
			})
			.then(response => {
				return this.update({
					...response,
					firstName: response.first_name,
					lastName: response.last_name,
				})
			})
	}

	register({
		email,
		firstName,
		lastName,
		password,
	}) {
		return AccountService
			.register({
				email,
				firstName,
				lastName,
				password,
			})
			.then(response => {
				TrackingHandler.trackEvent('register', email)
				return this.update(response)
			})
	}

	fbLogin(accessToken) {
		return AccountService
			.fbLogin(accessToken)
			.then(response => {
				TrackingHandler.trackEvent('login')
				return this.update(response)
			})
	}

	// ------------------------------------
	// Data manipulation
	// ------------------------------------

	getBankAccount() {
		return MeService.getBankAccount(this.state.token).then(res => {
			return res
		})
	}
	
	updateProfile(data) {
		return MeService
			.updateData({
				token: this.state.token,
				...data,
			})
			.then(response => {
				return this.update(response)
			})
	}

	verifyToken({
		token,
		email,
	}) {
		return AccountService.verifyToken({
			token,
			email,
		})
	}

	verifyUser({
		token,
		email,
	}) {
		return AccountService.verifyUser({
			token,
			email,
		}).then(response => {
			return this.update(response)
		})
	}

	// ------------------------------------
	// Password manipulation
	// ------------------------------------

	changePassword({
		currentPassword,
		password1,
		password2,
	}) {
		if(password1 !== password2) {
			return Promise.reject(ErrorHelper.create('013'))
		} else {
			return MeService
				.changePassword({
					currentPassword,
					password1,
					password2,
				}, this.state.token)
				.then(response => {
					TrackingHandler.trackEvent('login')
					return this.update(response)
				})
		}
	}

	changePasswordByToken({
		password1,
		password2,
		token,
		email,
	}) {
		if(password1 && password2 && password1 !== password2) {
			return Promise.reject(ErrorHelper.create('013'))
		} else {
			return AccountService
				.changePasswordByToken({
					password1,
					password2,
					token,
					email,
				})
				.then(response => {
					return this.update(response)
				})
		}
	}

	// ------------------------------------
	// Send Email
	// ------------------------------------

	verify(email = this.state.email, redirect) {
		return AccountService.verify(email, redirect)
	}

	resetPassword(email, redirect) {
		return AccountService.resetPassword(email, redirect)
	}

	// ------------------------------------
	// CART
	// ------------------------------------

	addToCart(type, id, metadata = {}) {
		return Promise.resolve().then(() => {
			switch(type) {
			case 'matchbox':
			default:

				const attachments = metadata.attachments.filter(attachment => !!attachment)

				if(attachments.length) {
					return Promise.all(attachments.map(image => {
						return MediaService.upload({
							image,
							type: 'image',
						}, this.state.token)
					})).then(medias => {
						return this.update({
							// cartItems: this.state.cartItems.concat({
							// 	type,
							// 	id,
							// 	note: metadata.note,
							// 	mediaIds: medias.map(media => media.id),
							// }),
							cartItems: [{
								type,
								id,
								note: metadata.note,
								mediaIds: medias.map(media => media.id),
							}],
						})
					})
				} else {
					return this.update({
						cartItems: [{
							type,
							id,
							note: metadata.note,
						}],
					})
				}
			}
		}).then(state => {
			return state.cartItems
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	clearCart() {
		this.update({
			cartItems: [],
		})
	}

	logout() {
		this.dispatch({
			type: this.constants.RESET,
		})
	}
}

export default new MeManager()
