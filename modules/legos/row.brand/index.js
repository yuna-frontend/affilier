import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import SelectionBit from 'modules/bits/selection';
// import Colors from 'coeur/constants/color';

import BrandService from 'app/services/brand'

import ButtonBit from 'modules/bits/button';
import TextInputBit from 'modules/bits/text.input';

import RowLego from 'modules/legos/row';
import SelectBrandLego from 'modules/legos/select.brand';

import Styles from './style';


export default ConnectHelper(
	class RowBrandLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				brandId: PropTypes.number,
				isLoading: PropTypes.bool,
				disabled: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			title: 'Brand',
		}

		constructor(p) {
			super(p, {
				brandId: p.brandId,
				isAdding: false,
				isSaving: false,
			})
		}

		onChange = (id, title) => {
			this.setState({
				brandId: id,
			})

			this.props.onChange &&
			this.props.onChange(id, title)
		}

		onAddBrand = () => {
			this.setState({
				isAdding: true,
			})
		}

		onCreateBrand = (val) => {
			// TODO
			this.setState({
				isSaving: true,
			}, () => {
				BrandService.addBrand(val, this.props.token).then(data => {
					this.setState({
						isSaving: false,
						isAdding: false,
					})

					this.onChange(data.id)
				}).catch(err => {
					this.warn(err)

					this.setState({
						isSaving: false,
					})
				})
			})
		}

		view() {
			return (
				<RowLego
					key={ `${this.state.isAdding}${this.state.brandId}` }
					data={[{
						title: this.props.title,
						children: (
							<SelectBrandLego isRequired
								disabled={ this.props.disabled }
								key={this.state.brandId}
								cache={false}
								brandId={ this.state.brandId }
								isLoading={ this.props.isLoading }
								onChange={ this.onChange }
							/>
						),
					}, {
						blank: true,
						children: this.state.isAdding ? (
							<TextInputBit autofocus onSubmitEditing={ this.onCreateBrand } />
						) : (
							<ButtonBit
								title="CREATE NEW BRAND"
								weight="medium"
								state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
								theme={ ButtonBit.TYPES.THEMES.WHITE_PRIMARY }
								onPress={ this.onAddBrand }
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
