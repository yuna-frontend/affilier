import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		padding: Sizes.margin.default,
	},

	padder: {
		marginTop: Sizes.margin.default,
	}
})
