import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
// import Fonts from 'coeur/constants/font'

export default StyleSheet.create({
	container: {
		height: 160,
		width: 160,
		borderRadius: 4,
		overflow: 'hidden',
		backgroundColor: Colors.grey.palette(1),
	},
	close: {
		position: 'absolute',
		top: 4,
		right: 4,
		width: 24,
		height: 24,
		backgroundColor: Colors.white.primary,
		borderRadius: 2,
	},
})
