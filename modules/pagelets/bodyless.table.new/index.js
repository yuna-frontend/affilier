import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import MeManager from '@yuna/utils/managers/me'
import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import ScrollViewBit from 'modules/bits/scroll.view';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import BoxLego from 'modules/legos/box';
import RowLego from 'modules/legos/row';
import ListLego from 'modules/legos/list';
import TabLego from 'modules/legos/tab';

import Styles from './style';

import { debounce } from 'lodash';

export default ConnectHelper(
	class QueryTableLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				searchable: PropTypes.bool,
				isSearching: PropTypes.bool,
				onSearch: PropTypes.func,
				action: PropTypes.node,
				filter: PropTypes.node,
				tabs: PropTypes.array,

				name: PropTypes.string,
				status: PropTypes.string,
				product: PropTypes.string,
				type: PropTypes.string,
				client: PropTypes.string,
				deadline: PropTypes.string,
				onGoToDetail: PropTypes.node,

				isLoading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			searchable: false,
		}

		constructor(p) {
			super(p, {
			}, [
				'onUpdateSearch',
				'onSearch',
				'contentRenderer',
			])

			this._searchValue = undefined
			this.onSearch = debounce(this.onSearch, 300)
		}

		onUpdateSearch(e, value) {
			this._searchValue = value
		}

		onSearch() {
			this.props.onSearch &&
			this.props.onSearch(this._searchValue)
		}

		contentRenderer(index) {
			return (
				<BoxLego key={index} contentContainerStyle={Styles.content}>
					<TouchableBit onPress={this.props.onGoToDetail}>
						<BoxBit unflex row style={Styles.contentHeader}>
							<GeomanistBit
								type={GeomanistBit.TYPES.HEADER_5}
								style={Styles.name}
							>
								{ this.props.name }
							</GeomanistBit>
							<GeomanistBit
								type={GeomanistBit.TYPES.SECONDARY_3}
								weight="medium"
								style={Styles.status}
							>
								{ this.props.status }
							</GeomanistBit>
						</BoxBit>
						<RowLego
							data={[{
								title: 'PRODUCT',
								content: this.props.product,
							}, {
								title: 'ORDER TYPE',
								content: this.props.type,
							}]}
						/>
						<RowLego
							data={[{
								title: 'CLIENT',
								content: this.props.client,
							}, {
								title: 'DEADLINE',
								content: this.props.deadline,
							}]}
						/>
					</TouchableBit>
				</BoxLego>
			)
		}

		view() {
			return (
				<BoxBit style={this.props.style}>
					<BoxBit unflex row style={Styles.padder}>
						{ this.props.filter }
						<BoxBit />
						{ this.props.searchable && (
							<TextInputBit
								onChange={ this.onUpdateSearch }
								onSubmitEditing={ this.onSearch }
								style={ Styles.inputContainer }
								inputStyle={ Styles.input }
								placeholder="Search orders"
								prefix={(
									<IconBit
										name="search"
										size={24}
										style={Styles.icon}
										color={Colors.black.palette(2, .6)}
									/>
								)}
								postfix={(
									<ButtonBit
										title="Search"
										weight="normal"
										type={ ButtonBit.TYPES.SHAPES.SEARCH }
										size={ ButtonBit.TYPES.SIZES.MEDIUM }
										width={ ButtonBit.TYPES.WIDTHS.FIT }
										state={ this.props.isSearching ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
										onPress={ this.onSearch }
									/>
								)}
							/>
						) }
					</BoxBit>
					<TabLego
						tabs={this.props.tabs}
						style={Styles.tab}
					/>
					<ScrollViewBit>
						{ this.props.isLoading ? (
							<ListLego index={1} unflex={false}>
								<BoxBit centering>
									<LoaderBit />
								</BoxBit>
							</ListLego>
						) :  (
							<BoxBit unflex row style={Styles.container}>
								{ Array(4).fill().map(this.contentRenderer) }
							</BoxBit>
						) || (
							<ListLego index={1} unflex={false}>
								<BoxBit centering>
									<GeomanistBit>No Data</GeomanistBit>
								</BoxBit>
							</ListLego>
						) }
					</ScrollViewBit>
				</BoxBit>
			)
		}
	}
)
