import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/pagelets/alert'

import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		Styles,
		TextBit: GeomanistBit,
		TouchableBit,
	}) {
		titleRenderer(title, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_3} weight="semibold" style={style}>
					{ title }
				</GeomanistBit>
			)
		}
	}
)
