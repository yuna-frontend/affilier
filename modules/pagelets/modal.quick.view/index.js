import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

import LoaderLego from 'modules/legos/loader';

import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';


export default ConnectHelper(
	class ModalQuickViewPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				header: PropTypes.node,
				loading: PropTypes.bool,
				layout: PropTypes.oneOf([
					'free',
					'3-way',
					'2-way',
					'1-way',
				]),
				error: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.bool,
				]),
				children: PropTypes.node,
				onClose: PropTypes.func,
				onEdit: PropTypes.func,
				style: PropTypes.style,
				scrollerStyle: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			layout: 'free',
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		styles = () => {
			switch(this.props.layout) {
			case '1-way':
				return Styles.narrow
			case '2-way':
				return Styles.compact
			default:
				return undefined
			}
		}

		layoutRenderer() {
			switch(this.props.layout) {
			case 'free':
			case '1-way':
			case '2-way':
			default:
				return (
					<ScrollViewBit style={[ Styles.contentContainer, this.props.scrollerStyle ]} contentContainerStyle={[ Styles.content, this.props.contentContainerStyle ]}>
						{ this.props.children }
					</ScrollViewBit>
				)
			case '3-way':
				return (
					<BoxBit row style={[ Styles.content, this.props.contentContainerStyle ]}>
						{ React.Children.map(this.props.children, (child, index) => {
							return (
								<BoxBit unflex style={ Styles[`layout3Way${index}`] }>
									{ child }
								</BoxBit>
							)
						}) }
					</BoxBit>
					// <ScrollViewBit contentContainerStyle={ this.props.contentContainerStyle }>
					// </ScrollViewBit>
				)
			}
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.props.loading ? this.viewOnLoading() : (
				<BoxBit unflex style={[ Styles.page, this.styles(), this.props.style ]}>
					<BoxBit unflex row style={ Styles.header }>
						<BoxBit unflex>
							{ this.props.title ? (
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_1 } weight="normal" style={ Styles.white }>
									{ this.props.title }
								</GeomanistBit>
							) : false }
						</BoxBit>
						<BoxBit />
						{ this.props.header }
						{ this.props.onEdit ? (
							<TouchableBit unflex row onPress={ this.props.onEdit } style={ Styles.center }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="normal" style={ Styles.red }>
									Edit
								</GeomanistBit>
								<BoxBit unflex style={ Styles.close }>
									<IconBit name="edit" size={ 24 } color={ Colors.white.palette(1, .6) } />
								</BoxBit>
							</TouchableBit>
						) : false }
						<TouchableBit unflex row onPress={ this.onClose } style={ Styles.center }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="normal" style={ Styles.white }>
								Close
							</GeomanistBit>
							<BoxBit unflex style={ Styles.close }>
								<IconBit name="close" size={ 24 } color={ Colors.white.primary } />
							</BoxBit>
						</TouchableBit>
					</BoxBit>
					{ this.props.error ? (
						<NotFoundPagelet description={ typeof this.props.error === 'string' ? this.props.error : 'Something went wrong when loading the data. If problem persist, please contact technical team.' } />
					) : this.layoutRenderer() }
				</BoxBit>
			)
		}
	}
)
