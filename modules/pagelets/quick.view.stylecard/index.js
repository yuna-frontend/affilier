import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollViewBit from 'modules/bits/scroll.view';

import ColorLego from 'modules/legos/color';
import InitialLego from 'modules/legos/initial';
import LoaderLego from 'modules/legos/loader';
import QuickViewCellLego from 'modules/legos/quick.view.cell';
import QuickViewImagesMosaicLego from 'modules/legos/quick.view.images.mosaic';
import QuickViewOverviewLego from 'modules/legos/quick.view.overview';
import QuickViewTableLego from 'modules/legos/quick.view.table';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';

import { capitalize } from 'lodash';


export default ConnectHelper(
	class QuickViewStylecardPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onClose: PropTypes.func,
				onNavigate: PropTypes.func,
			}
		}

		static propsToPromise(state, p) {
			return StylecardService.getStylecardDetailed(p.id, state.me.token)
		}

		static contexts = [
			UtilitiesContext,
		]

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onNavigateToClient = () => {
			this.onClose()

			this.props.onNavigate &&
			this.props.onNavigate('client/' + this.props.data.client_id)
		}

		onNavigateToOrder = () => {
			this.onClose()

			this.props.onNavigate &&
			this.props.onNavigate('order/' + this.props.data.order_id)
		}

		onNavigateToStylesheet = () => {
			this.onClose()

			this.props.onNavigate &&
			this.props.onNavigate('stylecard/' + this.props.id)
		}

		colorRenderer = image => {
			const variant = this.props.data.variants.find(i => i.image === image)

			return (
				<React.Fragment>
					<BoxBit unflex centering style={ Styles.id }>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.text }>#SV-{ variant.id }</GeomanistBit>
					</BoxBit>
					<BoxBit />
					<ColorLego colors={ variant.colors } border={ 2 } size={ 28 } radius={ 14 } style={ Styles.color } />
				</React.Fragment>
			)
		}

		variantRenderer = variant => {
			return {
				subheader: `#SV-${ variant.id }`,
				title: `${ variant.brand } – ${ variant.title }`,
				description: `${ variant.category } – ${ variant.size }`,
				status: variant.status === 'UNAVAILABLE' ? 'PACKED' : variant.status,
			}
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalQuickViewPagelet
					title="Style Card Preview"
					layout="3-way"
					onClose={ this.props.onClose }
					onEdit={ this.onNavigateToStylesheet }
				>
					<QuickViewOverviewLego
						id={ `#SC-${ this.props.id }` }
						subheader={ this.props.data.stylist ? (
							<BoxBit unflex row style={ Styles.stylist }>
								<InitialLego
									source={ this.props.data.stylist_profile }
									size={ 24 }
									name={ this.props.data.stylist }
									style={ Styles.initial }
								/>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } style={ Styles.name }>
									{ `${ this.props.data.stylist }'s` }
								</GeomanistBit>
							</BoxBit>
						) : undefined }
						title={ (this.props.data.title || `Stylecard Nº ${ this.props.id }`).toUpperCase() }
						mark={ this.props.data.matchbox.toUpperCase() }
						price={ this.props.data.variants.length ? this.props.data.variants.map(i => i.price).reduce((sum, c) => sum + c, 0) : 0 }
						retailPrice={ this.props.data.variants.length ? this.props.data.variants.map(i => i.retail_price).reduce((sum, c) => sum + c, 0) : 0 }
						description={ this.props.data.stylist_note }
					/>
					<QuickViewImagesMosaicLego row
						images={ this.props.data.variants.map(i => i.image) }
						childRenderer={ this.colorRenderer }
					/>
					<ScrollViewBit contentContainerStyle={ Styles.detail }>
						<QuickViewCellLego unflex={ false }
							title="STATUS"
							content={ capitalize(this.props.data.status) }
						/>
						<BoxBit />
						<QuickViewCellLego
							title="ITEMS"
							content={ `${ this.props.data.variants.length }/${ this.props.data.count }` }
						>
							<QuickViewTableLego
								data={ this.props.data.variants.map(this.variantRenderer) }
								style={ Styles.table }
							/>
						</QuickViewCellLego>
					</ScrollViewBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
