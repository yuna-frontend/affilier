import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	tag: {
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 12,
		paddingRight: 12,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 26,
		marginRight: 8,
		marginTop: 8,
	},

	tagPartialActive: {
		borderColor: Colors.primary,
	},

	tagActive: {
		backgroundColor: Colors.primary,
		borderColor: Colors.primary,
	},

	title: {
		color: Colors.black.palette(1, .9),
	},

	titlePartialActive: {
		color: Colors.primary,
	},

	titleActive: {
		color: Colors.white.primary,
	},
})
