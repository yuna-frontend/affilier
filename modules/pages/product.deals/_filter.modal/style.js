import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

const wrap = Sizes.app.width - (Sizes.margin.thick * 2)

export default StyleSheet.create({
	container: {
		maxHeight: '100%',
		padding: 0,
	},

	content: {
		paddingTop: 8,
		paddingBottom: 8,
	},

	div: {
		width: 8,
	},

	greyBase: {
		color: Colors.grey.palette(12),
	},

	headerFilter: {
		paddingTop: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 8,
	},

	collapsible: {
		borderTopWidth: 0,
		borderColor: Colors.black.palette(2, .1),
	},

	contentColl: {
		paddingLeft: Sizes.margin.thick + 4,
		paddingRight: Sizes.margin.thick + 4,
		paddingBottom: Sizes.margin.default,
	},

	containerHeader: {
		paddingTop: 4,
		paddingBottom: 8,
	},

	contentContainerColl: {
		marginLeft: -4,
		marginRight: -4,
		paddingTop: 4,
		paddingBottom: 4,
		flexWrap: 'wrap',
	},

	containerSelect: {
		paddingTop: 4,
		paddingLeft: 4,
		paddingRight: 4,
		paddingBottom: 4,
	},

	containerList: {
		borderBottomWidth: 0,
	},

	select: {
		minHeight: 48,
		borderWidth: 1,
		borderColor: Colors.grey.palette(16),
		paddingLeft: 12,
		paddingRight: 12,
	},

	col2: {
		width: wrap / 2,
	},

	contentContainerColor: {
		paddingTop: 4,
		paddingBottom: 4,
	},

	containerColor: {
		borderWidth: 2,
		borderColor: Colors.grey.palette(16),
		minWidth: 48,
		minHeight: 48,
		borderRadius: 24,
		paddingTop: 4,
		paddingLeft: 4,
		paddingRight: 4,
		paddingBottom: 4,
	},

	selectActive: {
		borderWidth: 2,
		borderColor: Colors.black.palette(2),
	},

	bgGrey: {
		backgroundColor: Colors.grey.palette(8),
	},

	header: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: Sizes.margin.default,
	},

	contentHeader: {
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	footer: {
		paddingTop: 12,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: 8,
		backgroundColor: Colors.white.primary,
		borderTopWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	btn: {
		paddingTop: 8,
		paddingLeft: 8,
		paddingRight: 8,
		paddingBottom: 8,
	},

	menuListContainer: {
		height: 'auto',
		maxHeight: '75%',
		background: Colors.white.primary,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
	},

	menuListHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	modalMenuPage: {
		justifyContent: 'flex-end',
	},

})
