import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	separator: {
		marginRight: 16,
	},

	containerRedirect: {
		paddingTop: 16,
	},

	pr24: {
		paddingRight: 24,
	},

	redirectText: {
		paddingLeft: 8,
	},
})
