import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'
import Defaults from 'coeur/constants/default'

export default StyleSheet.create({
	container: {
		// paddingTop: 0,
		// paddingLeft: Sizes.margin.default,
		// paddingRight: Sizes.margin.default,
		maxHeight: '100%',
		padding: Sizes.margin.default,
	},

	modalPage: {
		justifyContent: 'center',
	},

	modalContainer: {
		height: 'auto',
		maxHeight: '75%',
		width: Sizes.screen.width / 2,
		background: Colors.white.primary,
		borderRadius: 8,
	},

	modalHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	contentContainer: {
		// borderTopRightRadius: 0,
		// borderTopLeftRadius: 0,
		// borderBottomLeftRadius: 8,
		// borderBottomRightRadius: 8,
		width: Sizes.screen.width / 2,
		paddingTop: 0,
	},

	copyIcon: {
		paddingLeft: 6,
	},

	url: {
		marginTop: 6,
		width: 500,
	},

	button: {
		marginTop: 6,
		alignSelf: 'center',
	},

	header: {
		width: Sizes.screen.width / 2,
		borderTopRightRadius: 8,
		borderTopLeftRadius: 8,
	},

	...(Defaults.PLATFORM === 'web' ? {

		page: {
			height: 'auto',
		},

		'@media screen and (max-width: 800px)': {

			modalContainer: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
				margin: Sizes.margin.thick,
			},
			
			contentContainer: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
			},

			header: {
				width: Sizes.screen.width - (Sizes.margin.thick * 2),
			},
		},

		'@media screen and (max-width: 480px)': {
			page: {
				height: 'unset',
			},

			url: {
				width: 'auto',
			},
		},
	} : {}),
})
