import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'

export default ConnectHelper(
	class ListTableLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				alternating: PropTypes.bool,
				disabled: PropTypes.bool,
				borderless: PropTypes.bool,
				row: PropTypes.bool,
				isHeader: PropTypes.bool,
				index: PropTypes.number,
				children: PropTypes.node,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
			alternating: true,
		}

		view() {
			return this.props.onPress ? (
				<TouchableBit
					unflex={this.props.unflex}
					row={this.props.row}
					onPress={this.props.onPress}
					activeOpacity={this.props.disabled ? 1 : undefined}
					accessible={!this.props.disabled}
					style={[
						Styles.container,
						this.props.index & 1 && this.props.alternating && Styles.containerEven,
						this.props.isHeader && Styles.header,
						this.props.borderless && Styles.noBorder,
						this.props.disabled && Styles.disabled,
						this.props.style,
					]}>
					{this.props.children}
				</TouchableBit>
			) : (
				<BoxBit
					unflex={this.props.unflex}
					row={this.props.row}
					accessible={!this.props.disabled}
					style={[
						Styles.container,
						this.props.index & 1 && this.props.alternating && Styles.containerEven,
						this.props.isHeader && Styles.header,
						this.props.borderless && Styles.noBorder,
						this.props.disabled && Styles.disabled,
						this.props.style,
					]}
				>
					{this.props.children}
				</BoxBit>
			)
		}
	}
)
