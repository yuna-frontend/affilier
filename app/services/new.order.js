import ServiceModel from 'coeur/models/service';


class OrderService extends ServiceModel {

	static displayName = 'order'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/order/', 0)
	}

	getOrderList(data, token) {
		return this.get('', {
			query: data,
			token,
		})
	}

	// renamed from getAddresses to this below, because "getAddresses" is used again in line 46.
	getOrderAddresses(orderId, token) {
		return this.get(orderId + '/address', {
			token,
		})
	}

	getAddress(orderId, addressId, token) {
		return this.get(orderId + '/address/' + addressId, {
			token,
		})
	}

	createAddress(orderId, data, token) {
		return this.post(orderId + '/address', {
			token,
			data,
		})
	}

	updateAddress(orderId, addressId, data, token) {
		return this.post(orderId + '/address/' + addressId, {
			token,
			data,
		})
	}

	getAddresses(orderDetailIds, token) {
		return this.get('detail/' + orderDetailIds.join(',') + '/address', {
			token,
		})
	}

	getOrderHistory(orderIds, token) {
		return this.get(orderIds.join(',') + '/history', {
			token,
		})
	}

	getOrderDetailHistory(orderDetailIds, token) {
		return this.get('detail/' + orderDetailIds.join(',') + '/history', {
			token,
		})
	}

	getShipmentProduct(data, token) {
		return this.get(data.orderId + '/detail/' + data.detailIds.map(id => id).join(','), {
			token,
		})
	}

	getOrderShipmentDetail(data, token) {
		return this.get(data.orderId + '/detail/' + data.detailId, {
			// query: data,
			token,
		})
	}

	getOrderDetail(id, token) {
		return this.get(id, {
			token,
		})
	}

	getOrderHistories(id, token) {
		return this.get(id + '/histories', {
			token,
		})
	}

	payOrder(id, data, token) {
		return this.post(id + '/pay', {
			data: {
				note: data.note,
				metadata: data.metadata,
				payment: data.paymentCode,
				shouldSendEmail: data.shouldSendEmail,
			},
			token,
		})
	}

	processOrder(id, data, token) {
		return this.post(id + '/process', {
			data: {
				note: data.note,
				// shouldSendEmail: data.shouldSendEmail,
			},
			token,
		})
	}

	resolveOrder(id, data, token) {
		return this.post(id + '/resolve', {
			data: {
				note: data.note,
				force: data.force,
				// shouldSendEmail: data.shouldSendEmail,
			},
			token,
		})
	}

	expireOrder(id, data, token) {
		return this.post(id + '/expire', {
			data: {
				note: data.note,
				shouldSendEmail: data.shouldSendEmail,
			},
			token,
		})
	}

	exceptOrder(id, data, token) {
		return this.post(id + '/except', {
			data: {
				note: data.note,
				shouldSendEmail: data.shouldSendEmail,
			},
			token,
		})
	}

	setOrderStatus(id, data, token) {
		return this.post(id + '/status', {
			data,
			token,
		})
	}

	setOrderDetailStatus(id, data, token) {
		return this.post('detail/' + id + '/status', {
			data,
			token,
		})
	}

	craeteOrMockOrder(data, token) {
		return this.post('', {
			data,
			token,
		})
	}
}

export default new OrderService()
