import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	time: {
		width: 48,
		height: 44,
		paddingLeft: 8,
		paddingRight: 8,
		textAlign: 'center',
	},

	text: {
		width: 24,
	},

	separator: {
		width: 8,
	},
})
