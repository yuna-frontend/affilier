import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import SelectionBit from 'modules/bits/selection';

import TabLego from 'modules/legos/tab';

import NotFoundPagelet from 'modules/pagelets/not.found';

import GroupPart from './_group'

import Styles from './style';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class UserAnswersPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				date: PropTypes.date,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		// static propsToPromise(state) {
		// 	return QuestionService.getGroups(state.me.token).catch(() => [])
		// }

		static getDerivedStateFromProps(nP, nS) {
			if(!nS.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					groups: nP.data,
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				date: p.date,
				isLoaded: false,
				groups: [],
				tabIndex: 0,
			})
		}

		tabs = Object.values(GroupPart.TYPES)

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onChangeDate = date => {
			this.setState({
				date,
			})
		}

		headerRenderer() {
			return (
				<BoxBit unflex row centering style={ Styles.header }>
					<TabLego capsule
						activeIndex={ this.state.tabIndex }
						tabs={ this.tabs.map((tab, i) => {
							return {
								title: tab,
								onPress: this.onChangeTab.bind(this, i),
							}
						}) }
					/>
					<BoxBit />
					{/* { this.state.date ? (
						<SelectionBit options={[{
							title: TimeHelper.moment(this.props.date).format('DD MMMM YYYY [at] HH:mm'),
							onPress: this.onChangeDate.bind(this, this.props.date),
							selected: true,
						}, {
							title: 'Latest',
							onPress: this.onChangeDate.bind(this, new Date()),
						}]} />
					) : (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							Version: Latest
						</GeomanistBit>
					) } */}
					{ this.state.date ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							Last Updated : { TimeHelper.moment(this.props.date).format('DD MMM YYYY [at] HH:mm')}
						</GeomanistBit>
					) : (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							Incomplete SPQ
						</GeomanistBit>
					)}
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			const type = this.tabs[this.state.tabIndex]

			return (
				<BoxBit style={Styles.container}>
					{ this.headerRenderer() }
					{ type ? (
						<GroupPart key={ `${this.state.tabIndex}|${this.state.date}` } type={ type } userId={ this.props.id } date={ this.state.date } />
					) : (
						<NotFoundPagelet description="To sum it up, where is the questions??" />
					) }
				</BoxBit>
			)
		}
	}
)
