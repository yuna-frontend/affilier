import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


const col = Math.floor(Sizes.screen.width / 180)

export const width = (Sizes.screen.width - (Sizes.margin.default * 2) - ((4 * 2) * (col - 1)) - 4) / col
export function height(w = width, h) {
	return h / w * width
}

export default StyleSheet.create({
	container: {
		marginTop: - Sizes.margin.default / 4,
		marginLeft: - Sizes.margin.default / 4,
		marginRight: - Sizes.margin.default / 4,
		marginBottom: - Sizes.margin.default / 4,
		display: 'block',
	},
	wrapper: {
		width,
		paddingTop: Sizes.margin.default / 2,
		paddingBottom: Sizes.margin.default / 2,
		paddingLeft: Sizes.margin.default / 2,
		paddingRight: Sizes.margin.default / 2,
		marginTop: Sizes.margin.default / 4,
		marginBottom: Sizes.margin.default / 4,
		marginLeft: Sizes.margin.default / 4,
		marginRight: Sizes.margin.default / 4,
		backgroundColor: Colors.white.primary,
	},
	image: {
		borderRadius: 4,
		backgroundColor: Colors.blue.palette(2, .3),
		width: width - Sizes.margin.default,
		height: width - Sizes.margin.default,
		marginBottom: Sizes.margin.default / 2,
	},
	more: {
		marginTop: 8,
		marginBottom: 8,
	},
})
