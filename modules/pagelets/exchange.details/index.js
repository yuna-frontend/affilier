/* eslint-disable no-nested-ternary */
import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities';

import ExchangeService from 'app/services/exchange';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import InputValidatedBit from 'modules/bits/input.validated';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import BoxLego from 'modules/legos/box';
import LoaderLego from 'modules/legos/loader';
import RowLego from 'modules/legos/row';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';

import VariantPickerPart from './_variant.picker'

import Styles from './style';

import { chunk, debounce, isEmpty, uniq } from 'lodash';


export default ConnectHelper(
	class ExchangeDetailsPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				tab: PropTypes.string,
				details: PropTypes.array,
				title: PropTypes.string,
				comment: PropTypes.bool,
				readonly: PropTypes.bool,
				onUpdated: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!nS.isLoaded) {
				const stylesheet = nP.comment ? nP.details.find(d => d.type === 'STYLESHEET') : null
				const feedback = stylesheet ? stylesheet.feedbacks.find(f => f.title === 'Other thoughts?') : null
				const details = nP.details.filter(d => d.type !== 'STYLESHEET')
				const statuses = uniq(nP.details.map(dtl => dtl.status))
				let status
				if(statuses.length === 1) {
					if(statuses[0] === 'APPROVED') {
						status = 'APPROVED'
					} else {
						status = 'PENDING'
					}
				} else {
					status = 'PENDING'
				}
			
				return {
					isLoaded: true,
					isUpdatable: !nP.readonly && status === 'PENDING',
					comment: feedback ? feedback.answer : undefined,
					details,
					count: {
						kept: details.filter(d => !d.exchanged && !d.is_refund).length,
						exchanged: details.filter(d => d.exchanged && !d.is_refund).length,
						refunded: details.filter(d => d.is_refund === true).length,
					},
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Feedback',
			tab: 'all',
			comment: true,
			readonly: false,
		}

		constructor(p) {
			super(p, {
				isUpdatable: !p.status,
				isLoaded: false,
				isSaving: false,
				activeTabSlug: p.tab,
				details: [],
				comment: undefined,
				count: {
					kept: 0,
					exchanged: 0,
					refunded: 0,
				},
				changes: {},
			})
		}

		tabs = [{
			title: () => 'All',
			slug: 'all',
		}, {
			title: () => `Kept (${ this.state.count.kept })`,
			slug: 'kept',
		}, {
			title: () => `Exchanged (${ this.state.count.exchanged })`,
			slug: 'exchanged',
		}, {
			title: () => `Refunded (${ this.state.count.refunded })`,
			slug: 'refunded',
		}]

		options = [{
			key: 'SUCCESS',
			title: 'Approve',
		}, {
			key: 'FAILED',
			title: 'Reject',
		}]

		feedbackTitleMapper = title => {
			switch(title) {
			case 'How\'s the style?':
				return 'Style'
			case 'How\'s the size? – FIT':
				return 'Size - Fit'
			case 'How\'s the size? – LENGTH':
				return 'Size - Length'
			case 'How\'s the quality?':
				return 'Quality'
			case 'What would you like to do with this piece?':
				return 'Exchange'
			default:
				return title
			}
		}

		isValid = () => {
			// return this.state.isUpdatable
			// 	&& this.state.isLoaded
			// 	&& this.state.details.filter(detail => {
			// 		return !detail.exchanged || (!!this.state.changes[detail.id] && (this.state.changes[detail.id].status === 'SUCCESS'
			// 			|| (this.state.changes[detail.id].status === 'FAILED' && this.state.changes[detail.id].note)))
			// 	}).length === this.state.details.length

			return true
		}

		getStatus = (detail) => {
			if(!detail.exchanged && !detail.is_refund) {
				return 'KEPT'
			} else if(detail.exchanged) {
				return 'EXCHANGED'
			} else {
				return 'REFUNDED'
			}
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onReject = detail => {
			this.state.changes[detail.id] = {
				...(this.state.changes[detail.id] || {}),
				status: 'FAILED',
				// Why? need to change back to false
				pleaseRefund: false,
			}

			this.forceUpdate()
		}

		onApprove = detail => {
			if(detail.is_refund) {
				this.state.changes[detail.id] = {
					...(this.state.changes[detail.id] || {}),
					status: 'SUCCESS',
					// Why? need to change back to false
					pleaseRefund: false,
				}

				this.forceUpdate()
			} else {
				// Need to select the changed variant
				this.props.utilities.alert.modal({
					component: (
						<VariantPickerPart
							search={ detail.variant.product }
							onClose={ this.onModalRequestClose }
							onSelect={ this.onExchangeApprove.bind(this, detail) }
						/>
					),
				})
			}
		}

		onExchangeApprove = (detail, variantId) => {
			this.state.changes[detail.id] = {
				...(this.state.changes[detail.id] || {}),
				status: 'SUCCESS',
				variant_id: variantId,
				// Why? need to change back to false
				pleaseRefund: false,
			}

			this.forceUpdate()
		}

		onRefund = detail => {
			this.state.changes[detail.id] = {
				...(this.state.changes[detail.id] || {}),
				status: 'SUCCESS',
				pleaseRefund: true,
			}

			this.forceUpdate()
		}

		onSubmit = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add Note"
						message="Add a summary of this approval / rejection."
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmSubmit }
					/>
				),
			})
		}

		onConfirmSubmit = note => {
			// check if all is not rejected
			this.setState({
				isSaving: true,
			}, () => {
				Promise.resolve().then(() => {
					const count = this.state.details.filter(d => d.exchanged || d.is_refund).length

					if (!count) {
						throw new Error('There is no details to approve / reject.')
					}

					// if (!this.state.changes.note) {
					// 	throw new Error('Approval / Rejection note must be filled.')
					// }

					// if (count !== Object.keys(this.state.changes.details).length) {
					// 	throw new Error('You need to fill in all of details note and status.')
					// }
					let haveRejected = false
					let haveRejectedAndNoNote = false
					const {
						isAllRejected,
						isAllApproved,
					} = this.state.details.filter(d => d.exchanged).reduce((sum, detail) => {
						const update = this.state.changes[detail.id]

						if(!!update && update.status === 'FAILED') {
							haveRejected = true
						}

						if (!!update && update.status === 'FAILED' && !update.note) {
							haveRejectedAndNoNote = true
						}

						return {
							isAllApproved: sum.isAllApproved ? !!update && update.status === 'SUCCESS' : false,
							isAllRejected: sum.isAllRejected ? !!update && update.status === 'FAILED' : false,
						}
					}, {
						isAllRejected: true,
						isAllApproved: true,
					})

					if(haveRejected && !note) {
						throw new Error('You need to specify a note.')
					}

					if(haveRejectedAndNoNote) {
						throw new Error('You need to specify a note when rejecting a detail.')
					}

					return Promise.all(Object.keys(this.state.changes).map(k => {
						if(this.state.changes[k].pleaseRefund) {
							return ExchangeService.updateToRefund(k, this.props.token).then(() => {
								return {
									exchange_detail_id: parseInt(k, 10),
									status: this.state.changes[k].status,
									note: this.state.changes[k].note,
									variant_id: this.state.changes[k].variant_id,
								}
							})
						} else {
							return {
								exchange_detail_id: parseInt(k, 10),
								status: this.state.changes[k].status,
								note: this.state.changes[k].note,
								variant_id: this.state.changes[k].variant_id,
							}
						}
					})).then(details => {
						return ExchangeService.process(this.props.id, {
							note,
							details,
						// eslint-disable-next-line no-nested-ternary
						}, this.props.token).then(() => [isAllApproved ? 'SUCCESS' : isAllRejected ? 'FAILED' : 'EXCEPTION', details])
					})
				}).then(([status, details]) => {
					// updated
					this.props.onUpdated &&
					this.props.onUpdated(status, note, details)

					this.setState({
						isSaving: false,
					})

					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: `Success ${ status === 'FAILED' ? 'rejecting' : 'approving' } exchange request.`,
						type: 'SUCCESS',
					})
				}).catch(err => {
					this.setState({
						isSaving: false,
					})

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: err ? err.detail || err.message || err : 'Something went wrong.',
					})
				})
			})
		}

		onToggleTab = slug => {
			this.setState({
				activeTabSlug: slug,
			})
		}

		onChangeNote = debounce((id, note) => {
			this.state.changes[id] = {
				...(this.state.changes[id] || {}),
				note,
			}

			this.forceUpdate()
		}, 1000)

		headerRenderer() {
			return (
				<BoxBit row unflex>
					{ this.tabs.map(this.tabRenderer) }
				</BoxBit>
			)
		}

		tabRenderer = tab => {
			const isActive = tab.slug === this.state.activeTabSlug
			return (
				<TouchableBit key={ tab.slug } centering unflex style={[Styles.tab, isActive && Styles.activeTab]} onPress={ this.onToggleTab.bind(this, tab.slug) }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ isActive ? Styles.activeTabText : Styles.inactiveTabText }>
						{ tab.title() }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		detailRenderer = (detail, i) => {
			return (
				<BoxBit accessible={ !this.state.isSaving } key={ detail.id || `key_${i}` } unflex style={[Styles.item]}>
					<BoxBit unflex row>
						<ImageBit overlay resizeMode={ ImageBit.TYPES.COVER } source={ '//' + detail.variant.image.url } style={ Styles.image } />
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ detail.variant.brand }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">
								{ detail.variant.product }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.discounted } >
								IDR { FormatHelper.currency(detail.variant.price) } <TextBit style={ Styles.retail }>IDR { FormatHelper.currency(detail.variant.retail_price) }</TextBit>
							</GeomanistBit>
							<BoxBit />
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ `${detail.variant.colors[0].title} ~ ${detail.variant.size}` }
							</GeomanistBit>
						</BoxBit>
						<BoxBit unflex>
							<BadgeStatusLego status={this.getStatus(detail)}/>
							{/* <BadgeStatusLego status={ detail.exchanged ? detail.is_refund ? 'REFUNDED' : 'EXCHANGED' : 'KEPT' } /> */}
						</BoxBit>
					</BoxBit>
					<BoxBit>
						<BoxBit row style={ Styles.otherPrice }>
							{ !!detail.variant.refund_value && (
								<BoxBit>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
										Refund Price
									</GeomanistBit>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
										IDR { FormatHelper.currency(detail.variant.refund_value) }
									</GeomanistBit>
								</BoxBit>
							)}
							{ !!detail.variant.purchase_price && (
								<BoxBit>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
										Purchase Price
									</GeomanistBit>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
										IDR { FormatHelper.currency(detail.variant.purchase_price) }
									</GeomanistBit>
								</BoxBit>
							)}
						</BoxBit>
					</BoxBit>
					{ this.feedbacksRenderer(detail.feedbacks) }
					{ this.actionsRenderer(detail) }
				</BoxBit>
			)
		}

		feedbacksRenderer = feedbacks => {
			if(feedbacks && feedbacks.length) {
				const comment = feedbacks.find(f => f.title === 'Comment')
				return (
					<BoxBit unflex style={ Styles.feedbacks }>
						{ chunk(feedbacks.filter(f => f.title !== 'Comment'), 3).map((f, i) => (
							<RowLego key={i} data={[this.feedbackRenderer(f[0]), this.feedbackRenderer(f[1]), this.feedbackRenderer(f[2])]} />
						)) }
						{ comment ? (
							<RowLego
								data={[{
									title: comment.title,
									subtle: true,
									content: comment.answer,
									headerStyle: Styles.feedback,
								}]}
							/>
						) : false }
					</BoxBit>
				)
			} else {
				return (
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.empty }>No feedback found</GeomanistBit>
				)
			}
		}

		feedbackRenderer = feedback => {
			if(feedback) {
				return {
					title: this.feedbackTitleMapper(feedback.title),
					subtle: true,
					content: feedback.answer,
					headerStyle: Styles.feedback,
				}
			} else {
				return {
					blank: true,
				}
			}
		}

		actionsRenderer = detail => {
			if(detail.status === 'PENDING') {
				const status = this.state.changes[detail.id] ? this.state.changes[detail.id].status : null
				const note = this.state.changes[detail.id] ? this.state.changes[detail.id].note : null
				const pleaseRefund = this.state.changes[detail.id] ? this.state.changes[detail.id].pleaseRefund : null
				
				return (
					<RowLego
						data={[{
							title: 'Action',
							children: (
								<BoxBit row>
									<ButtonBit
										key={ `approve_${status}` }
										title="Approve"
										weight="medium"
										size={ ButtonBit.TYPES.SIZES.SMALL }
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										theme={ status === 'SUCCESS' && !pleaseRefund ? undefined : ButtonBit.TYPES.THEMES.SECONDARY }
										onPress={ this.onApprove.bind(this, detail) }
										style={ Styles.button }
									/>
									<ButtonBit
										key={ `reject_${status}` }
										title="Reject"
										weight="medium"
										size={ ButtonBit.TYPES.SIZES.SMALL }
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										theme={ status === 'FAILED' ? undefined : ButtonBit.TYPES.THEMES.SECONDARY }
										onPress={ this.onReject.bind(this, detail) }
										style={ Styles.button }
									/>
									{ detail.is_refund === false ? (
										<ButtonBit
											key={ `refund_${status}` }
											title="Refund"
											weight="medium"
											size={ ButtonBit.TYPES.SIZES.SMALL }
											type={ ButtonBit.TYPES.SHAPES.PROGRESS }
											theme={ pleaseRefund ? undefined : ButtonBit.TYPES.THEMES.SECONDARY }
											onPress={ this.onRefund.bind(this, detail) }
											style={ Styles.button }
										/>
									) : false }
									<InputValidatedBit unflex={ false }
										key={ status }
										value={ note }
										autofocus={ status === 'FAILED' }
										placeholder={ 'Note… (Required on rejection)' }
										type={ InputValidatedBit.TYPES.INPUT }
										onChange={ this.onChangeNote.bind(this, detail.id) }
										inputStyle={ Styles.input }
									/>
								</BoxBit>
							),
						}]}
						style={ Styles.actions }
					/>
				)
			} else {
				return (
					<RowLego
						data={[{
							title: 'Status',
							// eslint-disable-next-line no-nested-ternary
							content: detail.status === 'SUCCESS' ? 'Approved' : detail.status === 'FAILED' ? 'Rejected' : detail.status || '-',
						}, {
							title: 'Note',
							content: detail.note || '-',
							span: 2.08,
						}]}
					/>
				)
			}
		}

		viewOnLoading() {
			return (
				<BoxLego
					title={ this.props.title }
					style={ this.props.style }
					contentContainerStyle={ Styles.content } >
					<LoaderLego simple />
				</BoxLego>
			)
		}

		view() {
			const details = this.state.details.filter(d => {
				if (this.state.activeTabSlug === 'kept') {
					return d.exchanged === false && d.is_refund === false
				} else if (this.state.activeTabSlug === 'exchanged') {
					return d.exchanged === true && d.is_refund === false
				} else if (this.state.activeTabSlug === 'refunded') {
					// return d.exchanged === true && d.is_refund === true
					return d.is_refund === true

				} else {
					return true
				}
			})

			
			return (
				<BoxLego
					title={ this.props.title }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content } >
					{ this.props.comment ? (
						<RowLego
							data={[{
								title: 'Comment',
								children: (
									<ClipboardBit value={ this.state.comment || '-' } autogrow />
								),
							}]}
							style={ Styles.comment }
						/>
					) : false }
					{ details.length ? details.map(this.detailRenderer) : (
						<BoxBit unflex style={Styles.item} centering>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.empty}>No items found</GeomanistBit>
						</BoxBit>
					) }
					{ this.state.isUpdatable ? (
						<BoxBit unflex row style={Styles.footer}>
							<ButtonBit
								weight="medium"
								title="Submit Exchange Result"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								// eslint-disable-next-line no-nested-ternary
								state={ this.isValid()
									? this.state.isSaving
										? ButtonBit.TYPES.STATES.LOADING
										: ButtonBit.TYPES.STATES.NORMAL
									: ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onSubmit }
								style={ Styles.button }
							/>
						</BoxBit>
					) : false }
				</BoxLego>
			)
		}
	}
)
