import StyleSheet from 'coeur/libs/style.sheet'

import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	buttonMobile: {
		display: 'none',
	},

	'@media screen and (max-width: 575px)': {
		row: {
			flexWrap: 'wrap',
		},

		col: {
			flexBasis: '100%',
			marginRight: 0,
		},

		colHeaderHide: {
			display: 'none',
		},

		buttonDesktop: {
			display: 'none',
		},

		buttonMobile: {
			display: 'inherit',
		},

		col_mb16: {
			marginBottom: Sizes.margin.default,
		},
	},
})
