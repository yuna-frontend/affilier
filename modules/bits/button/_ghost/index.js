import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.black.primary, Colors.transparent],
				ACTIVE: [Colors.purple.palette(3), Colors.white.primary],
				DISABLED: [Colors.purple.palette(2), Colors.white.primary],
				LOADING: [Colors.purple.palette(2), Colors.white.primary],
			},
			SECONDARY: {
				NORMAL: [Colors.black.primary, Colors.transparent],
				ACTIVE: [Colors.black.primary, Colors.transparent],
				DISABLED: [Colors.grey.palette(1, .04), Colors.transparent],
				LOADING: [Colors.black.primary, Colors.transparent],
			},
			BLACK: {
				NORMAL: [Colors.black.palette(1, .7), Colors.transparent],
				ACTIVE: [Colors.black.primary, Colors.transparent],
				DISABLED: [Colors.grey.palette(1, .04), Colors.transparent],
				LOADING: [Colors.black.primary, Colors.transparent],
			},
		},
		SIZES: {
			NORMAL: Styles.content,
			SMALL: Styles.small,
		},
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.PARAGRAPH_2
				break;
			case this.TYPES.SIZES.SMALL:
				textType = GeomanistBit.TYPES.SECONDARY_3
				break;
			}

			return (
				<GeomanistBit weight={this.props.weights} type={textType} style={[textStyle, { color: this.state.colors}]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}
	}
)
