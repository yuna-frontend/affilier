import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import FilterBoxLego from 'modules/legos/filter.box';

import Styles from './style';

import { isEmpty } from 'lodash';

let allCategoriesCache = null


export default ConnectHelper(
	class BrowseAllPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static propsToQuery(state) {
			return [`query {
				categoriesList(filter: {
					categoryId: {
						isNull: false
					}
				}) {
					id
					title
					category {
						id
					}
				}
				parentCategoriesList: categoriesList(filter: {
					categoryId: {
						isNull: true
					}
				}) {
					id
					title
					categories {
						id
						title
					}
				}
			}`, {
				skip: !!allCategoriesCache,
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				allCategoriesCache = {
					categories: nP.data.categoriesList.concat(nP.data.parentCategoriesList).reduce((sum, category) => {
						return {
							...sum,
							[category.id]: {
								id: category.id,
								title: category.title,
							},
						}
					}, {}),
					childParentMap: nP.data.categoriesList.reduce((sum, category) => {
						return {
							...sum,
							[category.id]: category.category.id,
						}
					}, {}),
					categoryChildren: nP.data.categoriesList.reduce((sum, category) => {
						if (sum[category.category.id] === undefined) {
							sum[category.category.id] = []
						}

						sum[category.category.id].push({
							id: category.id,
							title: category.title,
						})

						return sum
					}, {}),
					rootCategories: [{
						id: null,
						title: 'All categories',
					}].concat(nP.data.parentCategoriesList.reduce((sum, category) => {
						return [...sum, ...category.categories]
					}, [])),
				}
			} else if (allCategoriesCache === null) {
				return null
			}

			const isRoot = allCategoriesCache.rootCategories.findIndex(c => c.id === nP.categoryId) > -1
			const parentId = allCategoriesCache.childParentMap[nP.categoryId]

			return {
				rootId: isRoot ? nP.categoryId : parentId,
				categories: nP.categoryId ? allCategoriesCache.categoryChildren[nP.categoryId] || allCategoriesCache.categoryChildren[parentId] : [],
				rootCategories: allCategoriesCache.rootCategories,
			}
		}

		constructor(p) {
			super(p, {
				rootId: null,
				categories: [],
				rootCategories: [],
			})
		}

		onChange = val => {
			this.props.onChange &&
			this.props.onChange(val)
		}

		categoryRenderer = ({id, title}, i) => {
			return (
				<TouchableBit unflex key={`cat_${i}`} style={Styles.item} onPress={this.onChange.bind(this, id)}>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						weight={id === this.props.categoryId ? 'medium' : 'normal'}
						style={[Styles.text, id === this.props.categoryId && Styles.isActive]}
					>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		rootCategoryRenderer = ({id, title}, i) => {
			return (
				<React.Fragment key={`root_${i}`}>
					<TouchableBit unflex onPress={this.onChange.bind(this, id)}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_2}
							weight={id === this.props.categoryId ? 'medium' : 'normal'}
							style={[Styles.text, id === this.props.categoryId && Styles.isActive]}
						>
							{ title }
						</GeomanistBit>
					</TouchableBit>
					{ id === this.state.rootId ? this.state.categories.map(this.categoryRenderer) : false }
				</React.Fragment>
			)
		}

		viewOnLoading() {
			return !this.state.categories.length ? (
				<FilterBoxLego title="Browse by" isLoading />
			) : this.viewRenderer(true)
		}

		viewRenderer(isLoading = false) {
			return (
				<FilterBoxLego title="Browse by" style={ [isLoading && Styles.isLoading, this.props.style] }>
					{ this.state.rootCategories.map(this.rootCategoryRenderer) }
				</FilterBoxLego>
			)
		}

		view() {
			return (this.viewRenderer())
		}
	}
)
