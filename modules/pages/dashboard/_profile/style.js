import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	initialLego: {
		alignSelf: 'center',
		marginRight: Sizes.margin.default,
		height: 70,
		width: 70,
		borderRadius: 50,
	},

	black2: {
		color: Colors.black.palette(2),
	},

	black2_06: {
		color: Colors.black.palette(2, .6),
	},
})
