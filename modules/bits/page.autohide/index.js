import ConnectHelper from 'coeur/helpers/connect';
import CorePageAutohideBit from 'coeur/modules/bits/page.autohide';

import BoxBit from '../box';
import PageBit from '../page';

import Styles from './style';


export default ConnectHelper(
	class PageAutohideBit extends CorePageAutohideBit({
		Styles,
		BoxBit,
		PageBit,
	}) {}
)
