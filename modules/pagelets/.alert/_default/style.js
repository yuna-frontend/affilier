import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
		width: 270,
		borderRadius: 12,
	},
	description: {
		textAlign: 'center',
		color: Colors.black.palette(2, .4),
		marginTop: 8,
	},
})
