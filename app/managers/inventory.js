import DictionaryModel from 'coeur/models/dictionary'

import InventoryRecord from 'app/records/inventory';

import InventoryService from 'app/services/inventory';

import MeManager from './me';


class InventoryManager extends DictionaryModel {

	static displayName = 'inventories'

	constructor() {
		super(InventoryRecord, {
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		return InventoryService.getInventory(id, MeManager.state.token).then(inventory => {
			return this.updateLater(inventory)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return InventoryService.getInventories(ids, MeManager.state.token).then(inventories => {
			return this.updateLater(inventories)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	filterAll({
		variantId,
		status,
		offset,
		count,
		isConsign = true,
	}) {
		return InventoryService.filterInventory({
			variantId,
			status,
			offset,
			count,
			is_consign: isConsign,
		}, MeManager.state.token).then(result => {
			return {
				count: result.count,
				data: this.updateLater(result.data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll() {
		return InventoryService.getInventories(undefined, MeManager.state.token).then(result => {
			return {
				count: result.count,
				data: this.updateLater(result.data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createInventories(datas = []) {
		return InventoryService.addInventories(datas, MeManager.state.token).then(inventories => {
			return this.updateBulk(inventories)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createInventory({
		productVariantId,
		pickupPointId,
		purchasePrice,
		status,
	}) {
		// TODO
		return InventoryService.addInventories([{
			productVariantId,
			pickupPointId,
			purchasePrice,
			statuses: [{
				code: status,
			}],
		}], MeManager.state.token).then(inventories => {
			return this.updateBulk(inventories)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateInventory(update) {
		// NOTE: MUST CONTAIN ID
		return InventoryService.updateInventories([update], MeManager.state.token).then(inventories => {
			return this.updateBulk(inventories)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	deleteInventories({
		ids = [],
	}) {
		return InventoryService.deleteInventories(ids, MeManager.state.token).then(status => {
			if(status === true) {
				this.clearBulk(ids)
			}

			return status
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateStatus({
		id,
		code,
		note,
	}) {
		return InventoryService.updateStatus({
			id,
			code,
			note,
		}, MeManager.state.token).then(() => {
			return this.dataGetter(id)
		})
	}
}

export default new InventoryManager()
