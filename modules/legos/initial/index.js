import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import UserService from 'app/services/user.new';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import Styles from './style';

const backgrounds = [
		Colors.purple.primary,
		Colors.pink.primary,
		Colors.blue.primary,
		Colors.green.primary,
		Colors.yellow.primary,
		Colors.red.primary,
	], backgroundLen = backgrounds.length


export default ConnectHelper(
	class InitialLego extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				assetId: PropTypes.id,
				source: PropTypes.image,
				size: PropTypes.number,
				accessible: PropTypes.bool,
				name: PropTypes.string.isRequired,
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			if (oP.id && oP.assetId && state.me.token) {
				return UserService.getUserAssets(oP.id, oP.assetId, state.me.token).catch(() => {
					if (oP.source) {
						return Promise.resolve(oP.source)
					} else {
						return Promise.resolve(null)
					}
				})
			} else if (oP.source) {
				return Promise.resolve(oP.source)
			} else {
				return Promise.resolve(null)
			}
		}

		static defaultProps = {
			name: '',
		}

		static getDerivedStateFromProps(nP, nS) {
			return nS.background ? null : {
				background: backgrounds[Math.floor(Math.random() * backgroundLen)],
				name: nP.name ? nP.name.split(' ').map(name => {
					return name[0]
				}).join('').substring(0, 2).toUpperCase() : 'NN',
			}
		}

		constructor(p) {
			super(p, {
				background: null,
				name: '',
			})
		}

		view() {
			return this.props.data ? (
				<BoxBit accessible={ this.props.accessible } unflex centering style={[Styles.container, {
					backgroundColor: this.state.background,
				}, this.props.style]}>
					<ImageBit source={ this.props.data } style={ Styles.image } />
				</BoxBit>
			) : (
				<BoxBit accessible={ this.props.accessible } unflex centering style={[Styles.container, {
					backgroundColor: this.state.background,
				}, this.props.style]}>
					<GeomanistBit type={GeomanistBit.TYPES.HEADER_5} style={[Styles.title, this.props.size ? {
						fontSize: this.props.size,
					} : undefined]}>{ this.state.name }</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
