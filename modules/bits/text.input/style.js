import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';

export default StyleSheet.create({
	container: {
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
		borderRadius: 2,
		transition: '.3s color, .3s border, .3s opacity',
		backgroundColor: Colors.white.primary,
	},
	focused: {
		borderColor: Colors.black.palette(2, .2),
	},
	input: {
		border: 0,
		paddingRight: 16,
		paddingLeft: 16,
		height: 42,
		minWidth: 0,
		width: '100%',
		fontFamily: Fonts.geo,
		fontSize: 14,
		fontWeight: 'normal',
		lineHeight: 20,
		letterSpacing: .3,
		color: Colors.black.palette(2),
	},
})
