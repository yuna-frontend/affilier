import RecordModel from 'coeur/models/record';

// import TimeHelper from 'coeur/helpers/time';


class StatusRecord extends RecordModel('status', {
	code			: RecordModel.TYPES.STRING,
	createdAt		: RecordModel.TYPES.DATE,
}) {}

class OrderAddressRecord extends RecordModel('orderAddress', {
	pic				: RecordModel.TYPES.STRING,
	phone			: RecordModel.TYPES.STRING,
	address			: RecordModel.TYPES.STRING,
	district		: RecordModel.TYPES.STRING,
	postal			: RecordModel.TYPES.STRING,
}) {}

class BatchRecord extends RecordModel('batch', {
	id				: RecordModel.TYPES.ID,
	userId			: RecordModel.TYPES.ID,
	orderNumber		: RecordModel.TYPES.STRING,
	couponId		: RecordModel.TYPES.STRING,
	discount		: RecordModel.TYPES.NUMBER,
	additionalDiscount	: RecordModel.TYPES.NUMBER,
	statuses		: RecordModel.TYPES.ARRAY,
	payments		: RecordModel.TYPES.ARRAY,

	createdAt		: RecordModel.TYPES.DATE,
}) {
	get isExpired() {
		const status = this.statuses[this.statuses.length - 1]

		return status && status.code === 'UNPAID' && Date.parse(this.createdAt) + (1000 * 60 * 60 * 24) < Date.now()
	}

	get status() {
		return this.statuses[this.statuses.length - 1] || new StatusRecord()
	}

	get payment() {
		return this.payments[this.payments.length - 1] || new StatusRecord()
	}
}

class OrderRecord extends RecordModel('order', {
	id 				: RecordModel.TYPES.ID,

	statuses		: RecordModel.TYPES.ARRAY,
	batch			: new BatchRecord(),

	// ADDRESS
	from			: new OrderAddressRecord(),
	to				: new OrderAddressRecord(),

	// FEE
	amount			: RecordModel.TYPES.NUMBER,
	shipping		: RecordModel.TYPES.NUMBER,
	insurance		: RecordModel.TYPES.NUMBER,

	metadata		: RecordModel.TYPES.OBJECT,
}) {
	get status() {
		return this.statuses[this.statuses.length - 1] || new StatusRecord()
	}

	get productCount() {
		return this.details.length
	}

	get listPrice() {
		return this.details.reduce((sum, detail) => sum + detail.listPrice, 0)
	}
}

function OrderDetailRecordFactory(additionalConfig = {}) {
	return class OrderDetailRecord extends RecordModel('orderDetail', {
		id				: RecordModel.TYPES.ID,
		orderId			: RecordModel.TYPES.ID,
		price			: RecordModel.TYPES.NUMBER,
		discount		: RecordModel.TYPES.NUMBER,

		order			: RecordModel.TYPES.OBJECT,
		metadata		: RecordModel.TYPES.OBJECT,

		product			: RecordModel.TYPES.OBJECT,

		createdAt		: RecordModel.TYPES.DATE,

		...additionalConfig,
	}) {

		get image() {
			return this.product.images && this.product.images[0]
		}

		get listPrice() {
			return this.price + this.discount
		}

		get shipmentStartDate() {
			return this.metadata.shipmentStartDate
		}

		get shipmentEndDate() {
			return this.metadata.shipmentEndDate
		}
	}
}

OrderDetailRecordFactory.orderRecord = OrderRecord
OrderDetailRecordFactory.batchRecord = BatchRecord
OrderDetailRecordFactory.addressRecord = OrderAddressRecord
OrderDetailRecordFactory.statusRecord = StatusRecord
OrderDetailRecordFactory.getModel = function(config = {}) {
	return {
		...config,

		metadata		: {
			...(config.metadata || {}),
			mediaIds: config.metadata && config.metadata.mediaIds || (config.medias && config.medias.map(m => m.id)) || [],
		},
		product			: config.product || (config.collection && config.collection.matchbox),

		order			: config.order && new OrderRecord({
			...config.order,
			statuses		: config.order.statuses && config.order.statuses.map(status => new StatusRecord(status)),
			batch			: config.order.batch && new BatchRecord({
				// TODO
				...config.order.batch,
				statuses	: config.order.batch.statuses && config.order.batch.statuses.map(status => new StatusRecord(status)),
				payments	: config.order.batch.payments && config.order.batch.payments.map(payment => new StatusRecord(payment)),
			}),
			// ADDRESS
			from			: new OrderAddressRecord(config.order.from || config.order.addresses.find(address => address.as === 'from')),
			to				: new OrderAddressRecord(config.order.to || config.order.addresses.find(address => address.as === 'to')),
		}) || new OrderRecord(),
	}
}


export default OrderDetailRecordFactory
