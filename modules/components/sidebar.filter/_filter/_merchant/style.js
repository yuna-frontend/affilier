import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	sizes: {
		// marginTop: -Sizes.margin.default * 1.5,
	},

	item: {
		// padding: 0,
		marginTop: Sizes.margin.default / 2,
		marginBottom: Sizes.margin.default / 2,
	},

	content: {
		marginTop: Sizes.margin.default / 2,
		marginLeft: Sizes.margin.default / 2,
	},
})
