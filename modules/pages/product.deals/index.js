/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size';

import CommissionService from 'app/services/commission';

// import AuthenticationHelper from 'utils/helpers/authentication'

import BoxBit from 'modules/bits/box'
// import ButtonBit from 'modules/bits/button'
import IconBit from 'modules/bits/icon'
import LoaderBit from 'modules/bits/loader'
import TextInputBit from 'modules/bits/text.input'
import TouchableBit from 'modules/bits/touchable'
// import CheckboxBit from 'modules/bits/checkbox'
import GeomanistBit from 'modules/bits/geomanist'

// import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
// import TableGalleryLego from 'modules/legos/table.gallery';

import CardProductComponent from 'modules/components/card.product';
// import HeaderFilterComponent from 'modules/components/header.filter';
import PageAuthorizedComponent from 'modules/components/page.authorized';
import PaginationComponent from 'modules/components/pagination';
// import SidebarFilterComponent from 'modules/components/sidebar.filter';
// import ShareComponent from 'modules/components/share'

import QuickViewProductPagelet from 'modules/pagelets/quick.view.product';
import LinkGeneratorPagelet from 'modules/pagelets/link.generator'

import FilterPart from './_filter'
import FilterModalPart from './_filter.modal'
import CategoryPart from './_category'
import SearchBoxPart from './_search.box'
import SearchPart from './_search'
import SortPart from './_sort'

import Styles from './style';

import Linking from 'coeur/libs/linking'

import { isEmpty, isEqual, isArray } from 'lodash';


export default ConnectHelper(
	class ProductDealsPage extends PageModel {

		static routeName = 'product.deals'

		static stateToProps(state) {
			return {
				token: state.me.token,
				userId: state.me.id,
			}
		}

		constructor(p) {
			const query = CommonHelper.getQueryString()

			const page = query.page
				? query.page > 0 ? query.page : 1
				: 1

			const limit = query.limit
				? query.limit <= 100 ? query.limit : 100
				: 40

			super(p, {
				isLoading: true,
				page,
				limit,
				offset: page * limit - limit,
				// count: query.count || (column * 8),
				total: 0,
				products: [],
				categoryIds: query.categoryIds ? ( isArray(query.categoryIds) ? query.categoryIds : [query.categoryIds] ) : [],
				search: query.search || null,
				filter: {
					price: query.price ? query.price : '',
					colorIds: query.colorIds ? ( isArray(query.colorIds) ? query.colorIds : [query.colorIds] ) : [],
					brandIds: query.brandIds ? CommonHelper.asArray(query.brandIds) : [],
					sizeIds: query.sizeIds ? ( isArray(query.sizeIds) ? query.sizeIds : [query.sizeIds] ) : [],
					sort: query.sort_by_created_at
						? { sort_by_created_at: query.sort_by_created_at }
						: query.sort_by_price
							? { sort_by_price: query.sort_by_price }
							: { sort_by_created_at: 'DESC'},
				},
				token: undefined,
				available: true,
				optionsSticky: {
					isActive: true,
					style: Styles.optionsStickyLoggedIn,
				},
			})

			this.getterId = 1
			this.categoryChildren = {}
			this.que = false
			this.isDesktopView = Sizes.screen.width > 800
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP, pS) {
			if ( !isEqual(pS.categoryIds, this.state.categoryIds)
				|| pS.page !== this.state.page
				|| pS.search !== this.state.search
				|| !isEqual(pS.filter, this.state.filter)
				// || pS.available !== this.state.available
			) {
				const query = {
					page: this.state.page,
					...(!!this.state.search && { search: this.state.search }),
					...(!!this.state.limit && { limit: this.state.limit }),
					...(!!this.state.categoryIds.length && { categoryIds: this.state.categoryIds }),
					...(!!this.state.filter.price && { price: this.state.filter.price }),
					...(!!this.state.filter.colorIds.length && { colorIds: this.state.filter.colorIds }),
					...(!!this.state.filter.brandIds.length && { brandIds: this.state.filter.brandIds }),
					...(!!this.state.filter.sizeIds.length && { sizeIds: this.state.filter.sizeIds }),
					...(!!this.state.filter.sort &&
						( !!this.state.filter.sort.sort_by_created_at
							? { sort_by_created_at: this.state.filter.sort.sort_by_created_at }
							: { sort_by_price: this.state.filter.sort.sort_by_price } )),
				}

				const queryKeys = Object.keys(query)

				window.history.replaceState({}, 'Product Deals', !queryKeys.length ? '/deals' : `/deals?${queryKeys.map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}
		
		getData = () => {
			const {
				offset,
				categoryIds,
				filter,
				search,
				limit,
			} = this.state

			this.setState({
				isLoading: true,
			}, () => {
				CommissionService.getProducts({
					limit,
					offset,
					...(!!search ? { search } : {}),
					...(categoryIds.length ? { category_ids: categoryIds.join(',') } : {}),
					...(filter.price.length ? { price: filter.price } : {}),
					...(filter.colorIds.length ? { color_ids: filter.colorIds.join(',') } : {} ),
					...(filter.brandIds.length ? { brand_ids: filter.brandIds.join(',') } : {}),
					...(filter.sizeIds.length ? { size_ids: filter.sizeIds.join(',') } : {}),
					...(filter.sort.sort_by_created_at ? { sort_by_created_at: filter.sort.sort_by_created_at } : {}),
					...(filter.sort.sort_by_price ? { sort_by_price: filter.sort.sort_by_price } : {}),
					inventory: false,
				}, this.props.token)
					.then(res => {
						this.log('get products: ', res)
						this.setState({
							isLoading: false,
							total: res.count,
							products: res.data,
						})
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong with your request, please try again later',
							})
						})
					})
			})
		}

		onChangeFilter = (key, val, multiple) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				filter: {
					...state.filter,
					...(!isEmpty(key) ? {
						[key]: !!multiple
							? ( state.filter[key].indexOf(val) > -1
								? [ ...(state.filter[key].filter(d => d !== val)) ]
								: [ ...state.filter[key], val] )
							: isEqual(state.filter[key], val) ? '' : val,
					} : { // this below is for filter change in filter.modal by apply button
						// also color change by bulk update colorIds
						...val,
					}),
				},
			}))
		}

		onShowSort = () => {
			this.setState(state => ({
				sort: !state.sort,
			}))
		}

		onPressFilter = () => {
			this.utilities.alert.modal({
				component: (
					<FilterModalPart
						filter={this.state.filter}
						categoryIds={this.state.categoryIds}
						onChangeFilter={this.onChangeFilter}
						utilities={this.utilities}
					/>
				),
			})
		}

		onChangeCategoryIds = (id, multiple) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				categoryIds: multiple
					? ( state.categoryIds.indexOf(id) > -1
						? [ ...(state.categoryIds.filter(c => c !== id)) ]
						: [ ...state.categoryIds, id ] )
					: ( !isArray(id) ? [ id ] : id ),
			}))
		}

		onChangeSort = (key, val) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				filter: {
					...state.filter,
					sort: !state.filter.sort[key]
						? { [key]: val }
						: state.filter.sort[key] !== val
							? { [key]: val }
							: {},
				},
				sort: false,
			}))
		}

		// onResetFilter = () => {
		// 	this.setState({
		// 		filter: {
		// 			price: [],
		// 			colorIds: [],
		// 			brandIds: [],
		// 			sizeIds: [],
		// 		},
		// 	})
		// }

		onResetFilter = () => {
			this.setState({
				offset: 0,
				page: 1,
				categoryIds: [],
				filter: {
					price: '',
					colorIds: [],
					brandIds: [],
					sizeIds: [],
					sort: { sort_by_created_at: 'DESC' },
				},
			})
		}

		onCategoryChildrenLoaded = categoryChildren => {
			this.categoryChildren = categoryChildren

			if(this.que) {
				this.que = false
				this.getData()
			}
		}

		onChangeCategory = categoryId => {
			this.setState({
				categoryId,
				offset: 0,
			})
		}

		onPressSearch = () => {
			this.utilities.alert.modal({
				component: (
					<SearchPart onSearch={this.onSearch}/>
				),
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToPrev = () => {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext = () => {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onAddProduct = () => {
			// TODO
			this.navigator.navigate('product/add/')
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onGoToDetail = id => {
			const product = this.state.products.find(prod => prod.id === id)

			if(product.variants.length > 0) {
				this.utilities.alert.modal({
					component: (
						<QuickViewProductPagelet
							id={ id }
							includeZeroInventory
							onNavigateToProduct={ this.props.isInventory ? () => {
								this.onModalRequestClose()
	
								this.navigator.navigate(`product/${id}`)
							} : undefined }
						/>
					),
				})
			} else {
				this.navigator.navigate(`product/${id}`)

			}
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onCopyLink = (url) => {
			navigator.clipboard.writeText(url)

			this.utilities.notification.show({
				message: 'Copied to clipboard',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})
		}

		onNavigateToProductDetail = (id) => {
			const url = 'https://helloyuna.io/product/variant/' + id
			Linking.open(url)
		}

		// onGetLink = (id) => {
		// 	this.utilities.alert.modal({
		// 		component: (
		// 			<ShareComponent
		// 				id={id}
		// 				onClose={this.onClose}
		// 				onCopyLink={this.onCopyLink}
		// 			/>
		// 		),
		// 	})
		// }

		onGetLink = (item) => {
			this.log(item)
			this.utilities.alert.modal({
				component: (
					<LinkGeneratorPagelet
						id={item.id}
						userId={this.props.userId}
						title={item.title}
						brand={item.brand}
						onCopyLink={ this.onCopyLink }
						onClose={this.onClose}
					/>
				),
			})
		}

		onNavigationChange = offset => {
			this.setState(state => ({
				offset,
				page: offset / state.limit + 1,
			}))
		}

		itemRenderer = (item, i) => {
			if(item.variants.length > 0) {
				return (
					<CardProductComponent
						key={ i }
						id={ item.id }
						variantId={ item.variants[0].id }
						image={ item.variants && item.variants[0].asset && item.variants[0].asset.url }
						brand={ item.brand }
						title={ item.title }
						price={ item.prices[1] }
						purchasePrice={ item.prices[0] }
						commission={ item.commission }
						colors={ item.colors }
						onPress={ () => this.onNavigateToProductDetail(item.variants[0].id) }
						onGetUrl={ () => this.onGetLink(item) }
						style={ Styles.cardProduct }
						imageStyle={ Styles.productImage }
					/>
				)
			} else {
				return (
					<CardProductComponent
						key={ i }
						id={ item.id }
						brand={ item.brand }
						title={ item.title }
						onPress={ this.onGetLink }
						style={ Styles.cardProduct }
					/>
				)
			}
		}

		view() {
			this.log(this.props)
			this.log(this.state)
			return (
				<PageAuthorizedComponent
					paths={[{ title: 'Products Deals' }]}
					headerHeight={!this.isDesktopView ? 100 : undefined}
					header={ !this.isDesktopView && (
						<BoxBit unflex style={Styles.containerHeader}>
							<SearchBoxPart search={this.state.search} onPress={this.onPressSearch} />
						</BoxBit>
					)}
				>
					{() => (
						<BoxBit style={Styles.container}>

							{!this.isDesktopView && (<>
								<BoxBit id="optionsPart" row unflex style={[Styles.options, this.state.optionsSticky.isActive && this.state.optionsSticky.style ]}>
									<CategoryPart
										mobile
										categoryIds={this.state.categoryIds}
										onChangeCategoryIds={this.onChangeCategoryIds}
										utilities={this.utilities}
									/>
									<FilterPart
										mobile
										onPressFilter={this.onPressFilter}
										styleContainer={Styles.filterInOption}
									/>
								</BoxBit>
								{ this.state.optionsSticky.isActive && (
									<BoxBit unflex style={{ height: 48 }}/>
								) }
							</>)}

							<BoxBit style={[Styles.padder, Styles.pb48 ]}>
								<BoxBit row>
									{ this.isDesktopView && (<>
										<BoxBit unflex style={Styles.contentLeft}>
											<TextInputBit
												placeholder="Cari"
												prefix={ <IconBit name="search" size={ 16 } color={ Colors.grey.palette(12) } /> }
												defaultValue={ this.state.search }
												onSubmitEditing={ this.onSearch }
												style={ Styles.searchContainer }
												inputStyle={ Styles.searchInput }
											/>
											<FilterPart
												filter={this.state.filter}
												onChangeFilter={this.onChangeFilter}
												categoryIds={this.state.categoryIds}
												onChangeCategoryIds={this.onChangeCategoryIds}
												onResetFilter={this.onResetFilter}
											/>
										</BoxBit>
									</>)}
									<BoxBit style={Styles.content}>
										{ this.isDesktopView && (
											<BoxBit row unflex style={Styles.containerResult}>
												<BoxBit unflex>
													<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={Styles.greyBase}>
														{ this.state.total } produk
													</GeomanistBit>
												</BoxBit>
												<TouchableBit unflex row centering onPress={this.onShowSort}>
													<IconBit name="menu" size={16}/>
													<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2}>
														Urutkan
													</GeomanistBit>
												</TouchableBit>
												{this.state.sort &&
													<SortPart onChangeSort={this.onChangeSort} sort={this.state.filter.sort}/>
												}
											</BoxBit>
										) }

										{ this.state.isLoading ? (
											<BoxBit centering style={Styles.wrap}>
												<LoaderBit simple/>
											</BoxBit>
										) : (
											<BoxBit row unflex style={Styles.wrap}>
												{ this.state.products.map(this.itemRenderer) }
											</BoxBit>
										) }
										<BoxBit unflex row>
											<BoxBit />
											<PaginationComponent
												current={this.state.offset}
												count={this.state.limit}
												total={this.state.total}
												onNavigationChange={this.onNavigationChange}
											/>
										</BoxBit>
									</BoxBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
