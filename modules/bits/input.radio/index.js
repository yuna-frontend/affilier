import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import GeomanistBit from '../geomanist'
import RadioBit from '../radio'
import TouchableBit from '../touchable'

import Styles from './style'


export default ConnectHelper(
	class InputRadioBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				dumb: PropTypes.bool,
				value: PropTypes.bool,
				title: PropTypes.string,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
			dumb: false,
		}

		static getDerivedStateFromProps(nP) {
			if(nP.dumb) {
				return {
					value: nP.value,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				value: p.value,
			})
		}

		onChange = () => {
			const value = !this.state.value

			this.setState({
				value,
			})

			this.props.onPress &&
			this.props.onPress(value)
		}


		view() {
			return (
				<TouchableBit unflex={ this.props.unflex } row onPress={ this.onChange } style={[Styles.container, this.props.style]}>
					<RadioBit dumb isActive={ this.state.value } onPress={ this.onChange } style={ Styles.radio } />
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ this.props.title }
					</GeomanistBit>
				</TouchableBit>
			)
		}
	}
)
