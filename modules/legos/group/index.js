import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';


export default ConnectHelper(
	class GroupLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit type={BoxBit.TYPES.ALL_THICK} style={[Styles.container, this.props.style]}>
					<GeomanistBit weight="normal" type={GeomanistBit.TYPES.SECONDARY_2} style={[
						Styles.title,
					]}>{ this.props.title }</GeomanistBit>
					{ this.props.children}
				</BoxBit>
			)
		}
	}
)
