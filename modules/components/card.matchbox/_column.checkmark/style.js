import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		marginLeft: -6,
		marginRight:  -6,
	},

	column: {
		marginRight: 6,
		marginLeft: 6,
	},

	row: {
		marginBottom: 8,
	},

	title: {
		marginLeft: 8,
	},

	footnote: {
		padding: [8, 0, 6],
		color: Colors.black.palette(1, .3),
	},
})
