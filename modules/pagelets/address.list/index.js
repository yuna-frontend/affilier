import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import GeomanistBit from 'modules/bits/geomanist';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import SelectionAddressLego from 'modules/legos/selection.address';

import AddressUpdateComponent from 'modules/components/address.update';

import AddressUpdatePagelet from 'modules/pagelets/address.update';
import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class AddressListPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				type: PropTypes.oneOf([
					'user',
					'order',
					'brand',
				]),
				typeId: PropTypes.id,
				selectedId: PropTypes.id,
				selectable: PropTypes.bool,
				onConfirm: PropTypes.func,
				addable: PropTypes.bool,
				onClose: PropTypes.func,
				addresses: PropTypes.array.isRequired,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			selectedId: -1,
			addresses: [],
		}

		constructor(p) {
			super(p, {
				selectedId: p.selectedId,
				isValid: false,
				isSaving: false,
			})

			this.submit = null
		}

		getSubmitter = submitter => {
			this.submit = submitter
		}

		onConfirm = () => {
			this.setState({
				isSaving: true,
			}, () => {
				if(this.state.selectedId === null) {
					this.submit().then(address => {
						this.props.utilities.notification.show({
							title: 'Yeay!',
							message: 'Successfully create an address.',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.addresses.unshift(address)

						if(this.props.onConfirm) {
							this.props.onConfirm(address)
						} else {
							this.setState({
								isSaving: false,
								selectedId: -1,
							})
						}
					})
				} else {
					const address = this.props.addresses.find(a => a.id === this.state.selectedId)

					this.props.onConfirm &&
					this.props.onConfirm(address)

					this.setState({
						isSaving: false,
					})
				}
			})
		}

		onChangeSelected = id => {
			this.setState({
				selectedId: id,
			})
		}

		onUpdate = (data, isValid) => {
			if(this.state.isValid !== isValid) {
				this.setState({
					isValid,
				})
			}
		}

		onEdit = address => {
			this.props.utilities.alert.modal({
				component: (
					<AddressUpdatePagelet
						id={ address.id }
						type={ this.props.type }
						typeId={ this.props.typeId }
						onClose={ this.onModalRequestClose }
						onConfirm={ this.onAddressUpdated }
					/>
				),
			})
		}

		onAddressUpdated = update => {
			const address = this.props.addresses.find(a => a.id === update.id)

			address.location_id = update.location_id
			address.title = update.title
			address.receiver = update.receiver
			address.phone = update.phone
			address.address = update.address
			address.district = update.district
			address.postal = update.postal
			address.coords = update.coords
			address.metadata = update.metadata
			address.location = update.location

			this.onModalRequestClose()

			this.forceUpdate()
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering row style={Styles.empty}>
					<IconBit
						size={16}
						name="circle-info"
						color={Colors.black.palette(2, .6)}
					/>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text}>
						Address not found.
					</GeomanistBit>
				</BoxBit>
			)
		}

		addRenderer = () => {
			return (
				<BoxBit unflex style={Styles.adder}>
					<TouchableBit row style={Styles.header} onPress={this.onChangeSelected.bind(this, null)}>
						{ this.props.addable ? (
							<BoxBit unflex style={ Styles.icon }>
								<IconBit
									name="expand"
									color={ Colors.primary }
								/>
							</BoxBit>
						) : false }
						<BoxBit style={Styles.button}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.primary} >
								Add New Address
							</GeomanistBit>
						</BoxBit>
						{ this.props.selectable ? (
							<RadioBit dumb
								isActive={this.state.selectedId === null}
								onPress={this.onChangeSelected.bind(this, null)}
							/>
						) : false }
					</TouchableBit>
					{ this.state.selectedId === null ? (
						<AddressUpdateComponent
							getSubmitter={ this.getSubmitter }
							type={ this.props.type }
							typeId={ this.props.typeId }
							onUpdate={ this.onUpdate }
							style={ Styles.add }
						/>
					) : false }
				</BoxBit>
			)
		}

		rowRenderer = (address, i) => {
			return (
				<SelectionAddressLego
					editable={ this.props.addable }
					key={ address.id }
					odd={ !(i & 1) }
					selectable={ !!this.props.selectable }
					isSelected={ address.id === this.state.selectedId }
					{ ...address }
					onPress={ this.onChangeSelected }
					onEdit={ this.onEdit.bind(this, address) }
				/>
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ this.props.title || ( this.props.selectable ? 'Select Shipping Address' : 'Shipping Addresses' ) }
					// eslint-disable-next-line no-nested-ternary
					confirm={ this.state.selectedId === null ? 'Add' : this.props.selectable ? 'Select' : 'Okay' }
					cancel={ 'Cancel' }
					loading={ this.state.isSaving }
					// eslint-disable-next-line no-nested-ternary
					disabled={ this.state.selectedId === null ? !this.state.isValid : this.props.selectable ? this.state.selectedId === -1 : false }
					onClose={ this.props.onClose }
					onCancel={ this.props.selectable ? this.props.onClose : undefined }
					onConfirm={ this.props.selectable || this.state.selectedId === null ? this.onConfirm : this.props.onClose }
					contentContainerStyle={Styles.content}
				>
					{ this.props.addresses.length ? this.props.addresses.map(this.rowRenderer) : this.emptyRenderer() }
					{ this.props.addable ? this.addRenderer() : false }
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
