import ServiceModel from 'coeur/models/service';


class BrandService extends ServiceModel {

	static displayName = 'brand'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	getClientList(data, token) {
		return this.get('user/client', {
			token,
			query: data,
		})
	}
}

export default new BrandService()
