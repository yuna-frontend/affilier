import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
	},

	categoryGallery: {
		paddingTop: 8,
		paddingBottom: 8,
		width: 72,
	},

	image: {
		width: 72,
		height: 72,
	},

	containerText: {
		marginTop: 8,
		marginBottom: 8,
		textTransform: 'capitalize',
	},

	categoryList: {
		paddingTop: 12,
		paddingLeft: 12,
		paddingRight: 12,
		paddingBottom: 12,
		borderWidth: 2,
		borderColor: Colors.grey.palette(16),
		textTransform: 'capitalize',
	},

	categoryListActive: {
		borderColor: Colors.black.primary,
	},

	selection: {
		flexShrink: 0,
		flexBasis: '50%',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		textTransform: 'capitalize',
	},

	selectionInLoading: {
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	modalMenuPage: {
		justifyContent: 'flex-end',
	},

	menuListContainer: {
		height: 'auto',
		maxHeight: '75%',
		background: Colors.white.primary,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
	},

	menuListHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	menuListFooter: {
		paddingTop: 12,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: 8,
		backgroundColor: Colors.white.primary,
		borderTopWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	menuListFooterBtn: {
		paddingTop: 8,
		paddingLeft: 8,
		paddingRight: 8,
		paddingBottom: 8,
	},

	menuListContent: {
		maxHeight: '100%',
		padding: 0,
	},
	
	menuListItems: {
		// width: 480,
		width: Math.min(480, Sizes.screen.width),
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		textTransform: 'capitalize',
	},

	menuListItemsCollapsible: {
		paddingLeft: Sizes.margin.default,
	},

	menuListItem: {
		marginBottom: Sizes.margin.default,
	},

	menuListItemDot: {
		width: 18,
		height: 18,
		...(Defaults.PLATFORM === 'web' ? {
			borderRadius: '100%',
		} : {
			borderRadius: 18,
		}),
		borderWidth: 1,
		borderColor: Colors.black.primary,
	},

	menuListItemDotActive: {
		width: 10,
		height: 10,
		borderWidth: 1,
		...(Defaults.PLATFORM === 'web' ? {
			borderRadius: '100%',
		} : {
			borderRadius: 10,
		}),
	},

	menuListItemDesktop: {
		alignItems: 'center',
	},

	menuListItemTitleDesktop: {
		marginLeft: Sizes.margin.default / 2,
	},

	...(Defaults.PLATFORM === 'web' ? {
		'@media screen and (max-width: 800px)': {
			menuListItems: {
				width: Sizes.app.width,
				paddingRight: Sizes.margin.thick,
				paddingLeft: Sizes.margin.thick,
				paddingTop: Sizes.margin.default,
				paddingBottom: Sizes.margin.default,
				textTransform: 'capitalize',
			},
		},
	} : {} ),
})
