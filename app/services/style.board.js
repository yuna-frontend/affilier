import ServiceModel from 'coeur/models/service';

class StyleBoardService extends ServiceModel {
	static displayName = 'styleboard'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	getStyleBoardDetail(id, token) {
		return this.get('styleboard/' + id, {
			token,
			query: {
				deep:true,
				with_request:true,
				with_user: true,
				with_stylist:true,
				with_service: true,
				with_styleboard_assets: true,
				with_history: true,

			},
		})
	}

	publish(id, stylistNote, token) {
		return this.post('styleboard/' + id + '/publish', {
			data: stylistNote ? {
				stylist_note: stylistNote,
			} : undefined,
			token,
		})
	}

	resolve(id, token) {
		return this.post('styleboard/' + id + '/resolve', {
			token,
		})
	}

	setStyling(id, note, token) {
		return this.post('styleboard/' + id + '/styling', {
			data: {
				note,
			},
			token,
		})
	}

	getStyleBoards(data, token) {
		return this.get('styleboard', {
			token,
			query: data,
		})
	}

	filterStyleboard(filter, token) {
		return this.get('styleboard', {
			query: {
				deep: true,
				with_count: true,
				with_stylist: true,
				with_inventories: true,
				...filter,
			},
			token,
		})
	}

	assignStylist(id, stylistId, token, note = '') {
		return this.post('styleboard/' + id + '/stylist', {
			data: {
				stylist_id: stylistId,
				note: note,
			},
			token,
		})
	}

	update(id, data, token) {
		return this.post('styleboard/' + id, {
			data: {
				...data,
			},
			token,
		})
	}

}

export default new StyleBoardService()
