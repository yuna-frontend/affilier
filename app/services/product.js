import ServiceModel from 'coeur/models/service';


class ProductService extends ServiceModel {

	static displayName = 'product'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'product/', 0)
	}

	getAllProduct(product, token) {
		return this.get('', {
			query: product,
			token,
		})
	}

	getProductTags(id, token) {
		return this.get(id + '/tag', {
			token,
		})
	}

	getProductVariants(id, token) {
		return this.get(id + '/variant', {
			token,
		})
	}

	getProductById(id, token) {
		return this.get(id, {
			token,
		})
	}

	getProductTag(id, token) {
		return this.get(id + '/tag', {
			token,
		})
	}

	createProduct(product, token) {
		return this.post('', {
			data: product,
			token,
		})
	}

	updateProduct(product, token) {
		return this.post(product.id, {
			data: product,
			token,
		})
	}
}

export default new ProductService()
