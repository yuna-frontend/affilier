import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		// borderRadius: 2,
		// backgroundColor: Colors.solid.grey.palette(1),
		// alignItems: 'center',
	},

	locked: {
		opacity: .4,
		filter: 'saturate(.3)',
	},

	borderless: {
		borderBottomWidth: 0,
	},

	empty: {
		height: 100,
	},

	text: {
		color: Colors.black.palette(2, .9),
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	image: {
		width: 60,
		height: 76,
		marginRight: Sizes.margin.default,
		// borderWidth: 1,
		// borderColor: Colors.black.palette(2, .16),
	},

	price: {
		color: Colors.black.palette(1, .7),
	},

	discounted: {
		marginRight: 8,
		color: Colors.red.primary,
	},

	retail: {
		color: Colors.black.palette(1, .7),
		textDecoration: 'line-through',
	},

	trash: {
		marginRight: 4,
	},

	edit: {
		marginRight: 4,
	},

	lock: {
		marginRight: 4,
		marginTop: 1,
	},

	exceptioned: {
		backgroundColor: Colors.red.palette(3, .06),
		// borderLeftColor: Colors.red.palette(7),
		// borderLeftWidth: 3,
	},

})
