import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	time: {
		paddingTop: 8,
		color: Colors.black.palette(2, .6),
	},

	button: {
		marginTop: 0,
	},

	input: {
		marginTop: 8,
	},

})
