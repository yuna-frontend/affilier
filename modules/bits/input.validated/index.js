import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreInputValidatedBit from 'coeur/modules/bits/input.validated';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import GeomanistBit from '../geomanist';
import IconBit from '../icon';
import SelectionBit from '../selection';
import TextAreaBit from '../text.area';
import TextInputBit from '../text.input';
import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class InputValidatedBit extends CoreInputValidatedBit({
		Styles,
		BoxBit,
		IconBit,
		SelectionBit,
		TextBit: GeomanistBit,
		TextAreaBit,
		TextInputBit,
		TouchableBit,
		checkmarkColor: Colors.green.primary,
	}) {
		titleRenderer(title, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={style}>{ title }</GeomanistBit>
			)
		}

		counterRenderer(counter, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="medium" style={style}>
					{ counter }
				</GeomanistBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_1} style={style}>{ this.props.description }</GeomanistBit>
			)
		}

		prefixTextRenderer(prefix, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={style}>{ prefix }</GeomanistBit>
			)
		}
	}
)
