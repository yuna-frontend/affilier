import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'

const optionsSticky = {
	position: 'fixed',
	left: 50,
	width: Sizes.screen.width - 50,
	zIndex: 1,
}

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},

	sidebarFilterTitle: {
		color: Colors.black.palette(2),
	},
	
	wrap: {
		flexWrap: 'wrap',
	},

	filter: {
		top: 80,
	},

	header: {
		marginTop: 16,
	},

	input: {
		marginTop: 4,
		marginBottom: Sizes.margin.default,
	},

	checker: {
		alignItems: 'center',
		marginTop: 8,
		marginBottom: 8,
	},

	optionsStickyLoggedIn: {
		...optionsSticky,
		top: 100,
	},

	check: {
		marginLeft: 8,
	},

	pb48: {
		paddingBottom: Sizes.margin.thick * 2,
	},

	pt44: {
		paddingTop: Sizes.margin.default * 2 - 4,
	},

	padder: {
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	containerResult: {
		justifyContent: 'space-between',
		paddingLeft: 8,
		paddingRight: 8,
		paddingBottom: 8,
	},

	content: {
		paddingTop: Sizes.margin.default,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: Sizes.margin.default * 2,
	},
	
	contentLeft: {
		width: '15%',
		minWidth: 216,
		paddingTop: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingLeft: 8,
	},

	filterInOption: {
		width: 'auto',
		minWidth: 'auto',
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .1),
		justifyContent: 'space-between',
		flexBasis: '50%',
	},

	cardProduct: {
		width: '25%',
		paddingRight: 8,
		paddingLeft: 8,
	},

	productImage: {
		width: '100%',
	},

	searchContainer: {
		height: 36,
		backgroundColor: Colors.grey.palette(8),
		borderColor: Colors.solid.grey.palette(3),
		marginBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	searchInput: {
		backgroundColor: Colors.transparent,
		paddingLeft: 8,
	},

	containerHeader: {
		backgroundColor: Colors.white.primary,
		// width: Sizes.app.width,
		paddingTop: 8,
		paddingBottom: 8,
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
	},

	greyBase: {
		color: Colors.grey.palette(9),
	},

	'@media screen and (min-width: 1440px)': {
		cardProduct: {
			width: '20%',
		},
	},

	'@media screen and (max-width: 800px)': {
		cardProduct: {
			width: '33.3333%',
		},

		padder: {
			paddingRight: Sizes.margin.default,
			paddingLeft: Sizes.margin.default,
		},
	},

	'@media screen and (max-width: 600px)': {
		cardProduct: {
			width: '50%',
		},

		optionsStickyLoggedIn: {
			left: 0,
			width: Sizes.screen.width,

		},
	},
})
