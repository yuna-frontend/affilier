import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import HintLego from 'modules/legos/hint';

import ModalPagelet from '../modal'


export default ConnectHelper(
	class ModalHintPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				hint: PropTypes.string,
				footer: PropTypes.node,
				children: PropTypes.node,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		view() {
			return (
				<ModalPagelet
					title={ this.props.title }
					{ ...this.props.hint ? {
						header: ( <HintLego title={ this.props.hint } /> )
					} : false }
					footer={ this.props.footer }
					children={ this.props.children }
					style={ this.props.style }
					contentContainerStyle={ this.props.contentContainerStyle }
				/>
			)
		}
	}
)

