import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: 20,
		height: 20,
		borderRadius: 10,
	},
	inactive: {
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderStyle: 'solid',
		backgroundColor: Colors.black.palette(2, .1),
	},
	active: {
		backgroundColor: Colors.primary,
	},
})
