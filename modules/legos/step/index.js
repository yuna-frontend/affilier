import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import RadioBit from 'modules/bits/radio'


import Styles from './style';


export default ConnectHelper(
	class StepLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				activeIndex: PropTypes.number,
				steps: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			activeIndex: 0,
			isDone: false,
		}

		constructor(p) {
			super(p, {
				style: p.capsule && Styles,
			})
		}

		shouldComponentUpdate(nP) {
			return nP.activeIndex !== this.state.activeIndex
		}

		tabRenderer = (tab, i) => {
			const isActive = this.props.activeIndex === i
			return (
				<BoxBit key={i} activeOpacity={1} accessible={ !tab.disabled } row unflex style={[ Styles.tab, tab.disabled && Styles.disabled ]}>
					{ tab.isDone ? (
						<RadioBit isActive style={Styles.radio} />
					) : (
						<BoxBit unflex centering style={[Styles.count, !isActive && Styles.countInactive]}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={!isActive ? Styles.isInactive : undefined}>
								{ tab.count }
							</GeomanistBit>
						</BoxBit>
					) }
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={[ isActive ? undefined : Styles.isInactive, tab.isDone ? Styles.isActive : undefined ]}>
						{ tab.title }
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					{ this.props.steps.map(this.tabRenderer) }
				</BoxBit>
			)
		}
	}
)
