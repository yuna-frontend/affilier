import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import ButtonBit from 'modules/bits/button'
import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';


import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import Styles from './style';


export default ConnectHelper(
	class ImageEditorPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				variantId: PropTypes.id,
				source: PropTypes.image,
				onPress: PropTypes.func,
				style: PropTypes.style,
				newIndex: PropTypes.number,
				onDataLoaded: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				crop: {
					unit: '%',
					width: 0,
					aspect: 3 / 4,
				},
				isLoading: false,
			})
		}

		onImageLoaded = image => {
			this.imageRef = image;
			this.imageRef.setAttribute('crossorigin', 'anonymous')
		};

		onCropComplete = crop => {
			this.makeClientCrop(crop);
		};

		makeClientCrop(crop) {
			if (this.imageRef && crop.width && crop.height) {
			  	this.getCroppedImg(
					this.imageRef,
					crop,
					'newFile.jpeg'
				)
			}
		}

		onCropChange = (crop) => {
			this.setState({ crop });
		}

		onSaveImage = () => {
			this.setState({
				isLoading: true,
			})

			this.props.onDataLoaded &&
			this.props.onDataLoaded('adder', this.state.croppedImg, true)
		}

		getCroppedImg(image, crop) {
			const canvas = document.createElement('canvas');
			const scaleX = image.naturalWidth / image.width;
			const scaleY = image.naturalHeight / image.height;
			canvas.width = crop.width;
			canvas.height = crop.height;
			const ctx = canvas.getContext('2d');
			ctx.drawImage(
			  image,
			  crop.x * scaleX,
			  crop.y * scaleY,
			  crop.width * scaleX,
			  crop.height * scaleY,
			  0,
			  0,
			  crop.width,
			  crop.height
			);

			this.setState({
				croppedImg: canvas.toDataURL('image/jpeg'),
			})
		}

		view() {
			return(
				<BoxBit style={[Styles.container, this.props.style]}>
					<BoxBit>
						<BoxBit>
							<ReactCrop
								src={ this.props.source.metadata.secureUrl }
								crop={this.state.crop}
								onImageLoaded={this.onImageLoaded}
								onComplete={this.onCropComplete}
								onChange={this.onCropChange}
								imageStyle={{
									maxWidth: '500px',
									borderWidth: 1,
									borderStyle: 'solid',
									objectFit: 'contain',
								}}
							/>
						</BoxBit>
					</BoxBit>

					{
						this.state.croppedImg &&
						<BoxBit>
							<ButtonBit
								title={'Save'}
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								onPress={ this.onSaveImage }

								state={this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL}
							/>
							<ImageBit
								source={ this.state.croppedImg }
								transform={{crop: 'fit'}}

							/>
						</BoxBit>

					}

					<TouchableBit unflex style={Styles.icon} onPress={this.props.onPress}>
						<IconBit name="close" size={36}/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
