import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

import Colors from 'coeur/constants/color'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class GeomanistBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.geo,
		weights: [
			'normal',
			'medium',
			'semibold',
			'bold',
		],
		TYPES: {
			HERO: {
				fontSize: 56,
				lineHeight: 62,
				fontWeight: '700',
				letterSpacing: -.4,
				color: Colors.black.primary,
			},

			HEADER_1: {
				fontSize: 45,
				lineHeight: 55,
				fontWeight: '600',
				letterSpacing: -.2,
				color: Colors.black.primary,
			},

			HEADER_2: {
				fontSize: 34,
				lineHeight: 42,
				fontWeight: '600',
				letterSpacing: 0,
				color: Colors.black.primary,
			},

			HEADER_3: {
				fontSize: 28,
				lineHeight: 36,
				fontWeight: '600',
				letterSpacing: 0,
				color: Colors.black.primary,
			},

			HEADER_4: {
				fontSize: 24,
				lineHeight: 31,
				fontWeight: '600',
				letterSpacing: .2,
				color: Colors.black.primary,
			},

			HEADER_5: {
				fontSize: 20,
				lineHeight: 24,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SUBHEADER_1: {
				fontSize: 16,
				lineHeight: 20,
				fontWeight: '600',
				letterSpacing: .5,
				color: Colors.black.primary,
			},

			SUBHEADER_2: {
				fontSize: 12,
				lineHeight: 14,
				fontWeight: '600',
				letterSpacing: 1.2,
				color: Colors.black.primary,
			},

			SUBHEADER_3: {
				fontSize: 11,
				lineHeight: 13,
				fontWeight: '600',
				letterSpacing: 1.6,
				color: Colors.black.primary,
			},

			SUBHEADER_4: {
				fontSize: 8,
				lineHeight: 10,
				fontWeight: '600',
				letterSpacing: 1.6,
				color: Colors.black.primary,
			},

			SUBHEADER_5: {
				fontSize: 6,
				lineHeight: 8,
				fontWeight: '600',
				letterSpacing: 1.6,
				color: Colors.black.primary,
			},

			PARAGRAPH_1: {
				fontSize: 18,
				lineHeight: 26,
				fontWeight: '500',
				letterSpacing: .4,
			},

			// SAME AS P1
			DEFAULT: {
				fontSize: 18,
				lineHeight: 26,
				fontWeight: '500',
				letterSpacing: .4,
			},

			PARAGRAPH_2: {
				fontSize: 16,
				lineHeight: 24,
				letterSpacing: .4,
			},

			PARAGRAPH_3: {
				fontSize: 14,
				lineHeight: 20,
				letterSpacing: .3,
			},

			CAPTION_1: {
				fontSize: 12,
				lineHeight: 16,
				fontWeight: '500',
				letterSpacing: .4,
			},

			CAPTION_2: {
				fontSize: 11,
				lineHeight: 14,
				fontWeight: '500',
				letterSpacing: .4,
			},

			NOTE_1: {
				fontSize: 12,
				lineHeight: 16,
				letterSpacing: .4,
			},

			NOTE_2: {
				fontSize: 11,
				lineHeight: 14,
				letterSpacing: .4,
			},

			NOTE_3: {
				fontSize: 9,
				lineHeight: 12,
				letterSpacing: .4,
			},

			NOTE_4: {
				fontSize: 8,
				lineHeight: 11,
				letterSpacing: .4,
			},

			NOTE_5: {
				fontSize: 7,
				lineHeight: 10,
				letterSpacing: .4,
			},

			PRIMARY_1: {
				fontSize: 15,
				lineHeight: 18,
				fontWeight: '500',
				letterSpacing: 1.4,
				color: Colors.black.primary,
			},

			PRIMARY_2: {
				fontSize: 13,
				lineHeight: 16,
				fontWeight: '700',
				letterSpacing: 1.2,
				color: Colors.black.primary,
			},

			PRIMARY_3: {
				fontSize: 11,
				lineHeight: 13,
				fontWeight: '700',
				letterSpacing: 1,
				color: Colors.black.primary,
			},

			SECONDARY_1: {
				fontSize: 18,
				lineHeight: 23,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SECONDARY_2: {
				fontSize: 16,
				lineHeight: 24,
				fontWeight: '500',
				letterSpacing: .5,
				color: Colors.black.primary,
			},

			SECONDARY_3: {
				fontSize: 14,
				lineHeight: 16,
				fontWeight: '400',
				letterSpacing: .4,
				color: Colors.black.primary,
			},
		},
	}) {}
)
