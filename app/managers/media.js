import DictionaryModel from 'coeur/models/dictionary';

import MediaRecord from 'app/records/media';

import MediaService from 'app/services/media';

import MeManager from './me';


class MediaManager extends DictionaryModel {

	static displayName = 'medias'

	constructor() {
		super(MediaRecord, {
			// clearOnRehidrate: true,
			// clearOnReset: true,
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		return MediaService.getMedia(id, MeManager.state.token).then(media => {
			return this.updateLater(media)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return MediaService.getMedias(ids, MeManager.state.token).then(medias => {
			return this.updateLater(medias)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	upload(image, {
		onUploadProgress,
		onDownloadProgress,
	} = {}) {
		return MediaService.upload({
			image,
			onUploadProgress,
			onDownloadProgress,
		}, MeManager.state.token).then(media => {
			return this.update(media)
		})
	}
}

export default new MediaManager()
