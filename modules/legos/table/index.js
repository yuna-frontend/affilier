import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import ScrollViewBit from 'modules/bits/scroll.view';
import ListTableLego from 'modules/legos/list.table';

import DefaultStyles from './style';
import CompactStyles from './style.compact';
import SmallStyles from './style.small';


export default ConnectHelper(
	class TableLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				compact: PropTypes.bool,
				unflex: PropTypes.bool,
				headers: PropTypes.array.isRequired,
				headerless: PropTypes.bool,
				bodyless: PropTypes.bool,
				footers: PropTypes.array,
				rows: PropTypes.array.isRequired,
				headerKey: PropTypes.updater,
				lastBorder: PropTypes.bool,
				alternating: PropTypes.bool,
				style: PropTypes.style,
				contentStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			lastBorder: true,
			alternating: true,
		}

		constructor(p) {
			super(p, {
				rowLen: -1,
				// eslint-disable-next-line no-nested-ternary
				styles: p.small ? SmallStyles : p.compact ? CompactStyles : DefaultStyles,
			})
		}

		getFlex(width) {
			return width > 0 ? `${width * 100}%` : 'auto'
		}

		rowRenderer = ({
			key,
			data,
			children,
			disabled,
			onPress,
			style,
		}, index) => {
			return (
				<ListTableLego index={ index } key={ key || index } onPress={ onPress } disabled={ disabled } style={[this.state.styles.body, style]} borderless={ this.props.lastBorder ? false : index === this.state.rowLen } alternating={ this.props.alternating }>
					<BoxBit unflex row>
						{ data.map((row, i) => {
							return (
								<BoxBit key={i} style={[this.state.styles.row, {
									flexBasis: this.getFlex(this.props.headers[i].width),
								}]}>
									{ row.children ? row.children : (
										<GeomanistBit type={ this.props.compact ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } align={this.props.headers[i].align} style={ row.style }>{ row.title }</GeomanistBit>
									) }
								</BoxBit>
							)
						}) }
					</BoxBit>
					{ children }
				</ListTableLego>
			)
		}

		view() {
			this.state.rowLen = this.props.rows.length - 1

			return (
				<BoxBit unflex={ this.props.unflex } style={this.props.style}>
					{ this.props.headerless ? false : (
						<ListTableLego key={this.props.headerKey} isHeader row style={this.state.styles.header}>
							{ this.props.headers.map((header, i) => {
								return (
									<BoxBit key={i} style={[this.state.styles.row, {
										flexBasis: this.getFlex(header.width),
									}]}>
										{ header.children ? header.children : (
											<GeomanistBit type={ this.props.compact ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } align={this.props.headers[i].align}  weight="semibold">{ header.title }</GeomanistBit>
										) }
									</BoxBit>
								)
							}) }
						</ListTableLego>
					) }
					<ScrollViewBit unflex={ this.props.unflex } style={this.props.contentStyle}>
						{ /* eslint-disable-next-line no-nested-ternary */ }
						{ this.props.isLoading ? (
							<ListTableLego index={1} unflex={false}>
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							</ListTableLego>
							/* eslint-disable-next-line no-nested-ternary */
						) : !this.props.rows.length ? !this.props.bodyless ? (
							<ListTableLego index={1} unflex={false} style={this.state.styles.empty}>
								<BoxBit centering>
									<GeomanistBit type={ this.props.compact ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } style={this.state.styles.note}>
										No Data
									</GeomanistBit>
								</BoxBit>
							</ListTableLego>
						) : false : this.props.rows.map(this.rowRenderer) }
					</ScrollViewBit>
					{ this.props.footers && (
						<ListTableLego row isHeader style={this.state.styles.footer}>
							{ this.props.footers.map((footer, i) => {
								return (
									<BoxBit key={i} style={[this.state.styles.row, {
										flexBasis: this.getFlex(this.props.headers[i].width),
									}]}>
										{ footer.children ? footer.children : (
											<GeomanistBit type={ this.props.compact ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } align={this.props.headers[i].align}  weight="semibold">{ footer.title }</GeomanistBit>
										)}
									</BoxBit>
								)
							}) }
						</ListTableLego>
					)}
				</BoxBit>
			)
		}
	}
)
