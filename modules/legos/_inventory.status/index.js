import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import RefService from 'app/services/ref';
import GeomanistBit from 'modules/bits/geomanist';
import SelectionBit from 'modules/bits/selection';

import {
} from 'modules/components/box.detail';
import Styles from './style';

export default ConnectHelper(
	class InventoryStatusPart extends PromiseStatefulModel {
		static TYPES = {
			INVENTORY_STATUS: 'INVENTORY_STATUS',
		}
		static propsToPromise() {
			return RefService.getRefInventory()
		}

		static getDerivedStateFromProps(nP) {
			return !nP.isLoading && nP.data && ({
				status: nP.data.map((status, i) => {
					return {
						key: i + 1,
						title: status,
						selected: nP.defaultValue && nP.defaultValue === status,
					}
				}),
			}) || false;
		}

		constructor(p) {
			super(p, {
				status: [],
			}, [])
		}

		onChange(key, id, val) {
			this.props.onChange &&
			this.props.onChange(key, id, val)
		}

		view() {
			return (
				<SelectionBit
					placeholder="Select status"
					options={this.state.status || []}
					onChange={ this.onChange.bind(this, this.TYPES.INVENTORY_STATUS) }
				/>
			)
		}
	}
)
