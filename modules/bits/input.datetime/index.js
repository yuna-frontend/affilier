import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import InputDateBit from '../input.date'
import TextInputBit from '../text.input'

import Styles from './style'


export default ConnectHelper(
	class InputDatetimeBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				title: PropTypes.string,
				date: PropTypes.date,
				description: PropTypes.string,
				format: PropTypes.string,
				readonly: PropTypes.bool,
				disabled: PropTypes.bool,
				children: PropTypes.node,
				onChange: PropTypes.func,
				style: PropTypes.style,

				timeStyle: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Date',
			format: 'DD/MM/YYYY',
		}

		constructor(p) {
			super(p, {
				date: p.date ? TimeHelper.moment(p.date || undefined).set('second', 0).set('millisecond', 0) : null,
			})

			this._hour = null
			this._minute = null
		}

		componentDidUpdate() {
			// this.props.onChange &&
			// this.props.onChange(this.state.date ? this.state.date.toDate() : null, this.state.date)
		}

		onUpdate = () => {
			this.props.onChange &&
			this.props.onChange(this.state.date ? this.state.date.toDate() : null, this.state.date)
		}

		bindHour = hour => {
			this._hour = hour
		}

		bindMinute = minute => {
			this._minute = minute
		}

		initiateDate = date => {
			return date ? date : TimeHelper.moment(date || undefined).set('second', 0).set('millisecond', 0)
		}

		onChange = (date, moment) => {
			// date & moment could be null
			if(date && moment) {
				this.setState({
					date: this.initiateDate(this.state.date)
						.set('year', moment.year())
						.set('month', moment.month())
						.set('date', moment.date()),
				}, this.onUpdate)
			} else {
				this.setState({
					date: null,
				}, this.onUpdate)
			}
		}

		onChangeHour = (e, hour) => {
			const h = parseInt(hour, 10)
			if((h || h === 0) && h < 24) {
				this.setState({
					date: this.initiateDate(this.state.date).set('hour', h),
				}, this.onUpdate)

				if(hour.length === 2 || (
					hour.length === 1 && h > 2
				)) {
					// autofocus next
					this._minute.focus()
				}
			}
		}

		onChangeMinute = (e, minute) => {
			const m = parseInt(minute, 10)
			if((m || m === 0) && m < 60) {
				this.setState({
					date: this.initiateDate(this.state.date).set('minute', m),
				}, this.onUpdate)
			}
		}

		view() {
			return (
				<InputDateBit key={ this.props.date } unflex
					unflex={ this.props.unflex }
					title={ this.props.title }
					date={ this.state.date }
					description={ this.props.description }
					format={ this.props.format }
					readonly={ this.props.readonly }
					disabled={ this.props.disabled }
					children={ (
						<BoxBit unflex row centering style={[Styles.children, this.props.timeStyle]}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} align="center" style={Styles.text}>
								at
							</GeomanistBit>
							<TextInputBit key={ this.props.date + 'x' }
								inputRef={ this.bindHour }
								readonly={ this.props.readonly }
								disabled={ this.props.disabled }
								defaultValue={ this.state.date ? this.state.date.format('HH') : undefined }
								type={ TextInputBit.TYPES.TEL }
								onChange={ this.onChangeHour }
								maxlength={ 2 }
								placeholder={ 'HH' }
								inputStyle={ Styles.time }
							/>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} align="center" style={Styles.separator}>
								:
							</GeomanistBit>
							<TextInputBit key={ this.props.date }
								inputRef={ this.bindMinute }
								readonly={ this.props.readonly }
								disabled={ this.props.disabled }
								defaultValue={ this.state.date ? this.state.date.format('mm') : undefined }
								type={ TextInputBit.TYPES.TEL }
								onChange={ this.onChangeMinute }
								maxlength={ 2 }
								placeholder={ 'MM' }
								inputStyle={ Styles.time }
							/>
						</BoxBit>
					) }
					onChange={ this.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
