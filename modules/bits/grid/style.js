import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from '../../../constants/color'
// import Sizes from '../../../constants/size'


export default StyleSheet.create({
	content: {
		flexWrap: 'wrap',
		justifyContent: 'space-between',
		flex: 1,
		flexDirection: 'row',
	},
	emptyBox: {
		height: 1,
	},
	itemContainer: {
	},
})
