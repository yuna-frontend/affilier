import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
		paddingBottom: 8,
		backgroundColor: Colors.solid.grey.palette(1),
	},

	content: {
		paddingRight: Sizes.margin.default,
	},

	breadcrumb: {
		paddingBottom: Sizes.margin.default,
	},

	title: {
		paddingTop: 8,
		textTransform: 'capitalize',
		color: Colors.black.palette(2),
	},
})
