import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from '../box'
import InputValidatedBit from '../input.validated'

import UnitLego from 'modules/legos/unit'

import Styles from './style'


export default ConnectHelper(
	class InputCurrencyBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				value: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.number,
				]),
				readonly: PropTypes.bool,
				placeholder: PropTypes.string,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			placeholder: 'Input here',
			title: 'IDR'
		}

		onChange = (val, isValid) => {
			this.props.onChange &&
			this.props.onChange(val ? parseInt(val, 10) : val, isValid)
		}

		view() {
			return (
				<BoxBit row style={ this.props.style }>
					{ this.props.title !== '' && (
						<UnitLego title={this.props.title} right />
					) }
					<InputValidatedBit unflex={ false } type="CURRENCY"
						readonly={ this.props.readonly }
						key={ this.props.value }
						value={ this.props.value }
						placeholder={ this.props.placeholder }
						onChange={ this.onChange }
						inputContainerStyle={ Styles.container }
					/>
				</BoxBit>
			)
		}
	}
)
