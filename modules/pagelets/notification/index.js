import ConnectHelper from 'coeur/helpers/connect';
import CoreNotificationPagelet from 'coeur/modules/pagelets/notification';

import BoxBit from 'modules/bits/box'

import NotifierPart from './_notifier'

export {
	NotifierPart,
}


export default ConnectHelper(
	class NotificationPagelet extends CoreNotificationPagelet({
		BoxBit,
		NotifierPart,
	}) {}
)
