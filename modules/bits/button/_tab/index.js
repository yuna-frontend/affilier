import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonTab extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.black.palette(1, .7), Colors.solid.grey.palette(3), Colors.solid.grey.palette(5)],
				ACTIVE: [Colors.black.primary, Colors.transparent, Colors.solid.grey.palette(5)],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(3), Colors.solid.grey.palette(5)],
				LOADING: [Colors.white.primary, Colors.primary],
			},
			SECONDARY: {
				NORMAL: [Colors.black.primary, Colors.solid.grey.palette(3), Colors.solid.grey.palette(5)],
				ACTIVE: [Colors.black.primary, Colors.solid.grey.palette(5), Colors.solid.grey.palette(5)],
				DISABLED: [Colors.black.primary, Colors.solid.grey.palette(5), Colors.solid.grey.palette(5)],
				LOADING: [Colors.white.primary, Colors.primary],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.SUBHEADER_1
				break;
			}

			return (
				<GeomanistBit weight={this.props.weight} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
