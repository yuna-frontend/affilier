import React from 'react'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'
import ConnectHelper from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon'
import GeomanistBit from 'modules/bits/geomanist'

import BoxLego from 'modules/legos/box'
import TableLego from 'modules/legos/table'

import Styles from './style'

export default ConnectHelper (
	class RankPart extends ConnectedStatefulModel {

		constructor(p) {
			super(p, {
				isLoading: false,
			})
		}

		productsHeader = [{
			title: 'Product Id',
			width: 1,
		}, {
			title: 'Product Name',
			width: 2,
		}, {
			title: 'Price',
			width: 1.5,
		}, {
			title: 'Sold',
			width: .5,
		}, {
			title: 'Total Price',
			width: 1.5,
		}]

		categoriesHeader = [{
			title: 'Category Id',
			width: 1.5,
		}, {
			title: 'Category Name',
			width: 1.5,
		}, {
			title: 'Sold',
			width: .5,
		}, {
			title: 'Total Price',
			width: 1.5,
		}]

		productsRows = [{
			id: 123,
			data: [{
				title: '12348',
			}, {
				title: 'RAMUNE - HARA CROPPED SWEATER',
			}, {
				title: '275000',
			}, {
				title: '2',
			}, {
				title: '550000',
				// -----------------------------------
			}],
		}, {
			id: 456,
			data: [{
				title: '14508',
			}, {
				title: 'BLZR.ID - Luxee Denim Blazer',
			}, {
				title: '409000',
			}, {
				title: '1',
			}, {
				title: '409000',
				// -----------------------------------
			}],
		}, {
			id: 789,
			data: [{
				title: '14506',
			}, {
				title: 'BLZR.ID - Quinn Crop Blazer',
			}, {
				title: '429000',
			}, {
				title: '1',
			}, {
				title: '429000',
				// -----------------------------------
			}],
		}, {
			id: 134,
			data: [{
				title: '9763',
			}, {
				title: 'RAMUNE - Mia Loose Dress',
			}, {
				title: '339000',
			}, {
				title: '1',
			}, {
				title: '339000',
				// -----------------------------------
			}],
		}, {
			id: 124,
			data: [{
				title: '375',
			}, {
				title: 'Havva - Naflah Square Gold',
			}, {
				title: '129000',
			}, {
				title: '1',
			}, {
				title: '129000',
				// -----------------------------------
			}],
		}]

		categoriesRows = [{
			id: 456,
			data: [{
				title: '7',
			}, {
				title: 'Crop Top',
			}, {
				title: '1',
			}, {
				title: '275000',
			}],
		}, {
			id: 111,
			data: [{
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}],
		}, {
			id: 112,
			data: [{
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}],
		}, {
			id: 114,
			data: [{
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}],
		}, {
			id: 115,
			data: [{
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}, {
				title: '-',
			}],
		}]

		topProductsRenderer = () => {
			return (
				<TableLego unflex
					key="Products"
					isLoading={ this.state.isLoading }
					headers={ this.productsHeader }
					rows={ this.productsRows }
				/>
			)
		}

		topCategoriesRenderer = () => {
			return (
				<TableLego unflex
					key="Categories"
					isLoading={ this.state.isLoading }
					headers={ this.categoriesHeader }
					rows={ this.categoriesRows }
				/>
			)
		}

		view() {
			return (
				<BoxBit row>
					<BoxLego unflex={false} title="My Top 5 Products" style={Styles.container} contentContainerStyle={Styles.padder}>
						{ this.topProductsRenderer() }
					</BoxLego>
					<BoxLego unflex={false} title="My Top 5 Categories" style={Styles.container} contentContainerStyle={Styles.padder}>
						{ this.topCategoriesRenderer() }
					</BoxLego>
				</BoxBit>
			)
		}
	}
)
