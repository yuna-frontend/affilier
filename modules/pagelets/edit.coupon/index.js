import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import CouponManager from 'app/managers/coupon';

import UtilitiesContext from 'coeur/contexts/utilities';

// import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class EditCouponPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, coupon = id && CouponManager.get(id) || new CouponManager.record()

			return {
				coupon,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.coupon._isGetting !== nS.isGettingCoupon) {
				return {
					isGettingCoupon: nP.coupon._isGetting,
					data: {
						title: nP.coupon.title,
						description: nP.coupon.description,
						type: nP.coupon.type,
						amount: nP.coupon.amount,
						expiry: nP.coupon.expiry && TimeHelper.moment(nP.coupon.expiry).format('DD / MM / YYYY'),
						minimumSpend: nP.coupon.minimumSpend,
						maximumSpend: nP.coupon.maximumSpend,
						excludeSale: nP.coupon.excludeSale,
						usageLimit: nP.coupon.usageLimit,
						usageLimitItem: nP.coupon.usageLimitItem,
						usageLimitPerUser: nP.coupon.usageLimitPerUser,
						matchboxIds: nP.coupon.matchboxIds.join(','),
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingCoupon: p.coupon._isGetting,
				data: {
					title: p.coupon.title,
					description: p.coupon.description,
					type: p.coupon.type,
					amount: p.coupon.amount,
					expiry: p.coupon.expiry && TimeHelper.moment(p.coupon.expiry).format('DD / MM / YYYY'),
					minimumSpend: p.coupon.minimumSpend || '0',
					maximumSpend: p.coupon.maximumSpend || '-1',
					excludeSale: p.coupon.excludeSale,
					usageLimit: p.coupon.usageLimit || '-1',
					usageLimitItem: p.coupon.usageLimitItem || '-1',
					usageLimitPerUser: p.coupon.usageLimitPerUser || '-1',
					matchboxIds: p.coupon.matchboxIds.join(','),
				},
				isDone: false,
				isLoading: false,
			}, [
				'uppercaseValidator',
				'onUpdate',
				'onSubmit',
				'onSuccess',
				'onError',
				'onCancel',
			])

			this._couponTypes = [{
				title: this.getTitle('percent'),
				key: 'percent',
			}, {
				title: this.getTitle('total'),
				key: 'total',
			}, {
				title: this.getTitle('item'),
				key: 'item',
			}, {
				title: this.getTitle('shipping'),
				key: 'shipping',
			}]

			this._saleTypes = [{
				title: 'No',
				key: false,
			}, {
				title: 'Yes',
				key: true,
			}]

			this._matchboxes = [{
				title: this.getMatchboxTitle(''),
				key: '',
			}, {
				title: this.getMatchboxTitle('basic'),
				key: 'basic',
			}, {
				title: this.getMatchboxTitle('mini'),
				key: 'mini',
			}]

			this._upperCaseRegex = /[a-z]/
		}

		onUpdate(isDone, {
			title,
			description,
			type,
			amount,
			expiry,
			minimumSpend,
			maximumSpend,
			excludeSale,
			usageLimit,
			usageLimitItem,
			usageLimitPerUser,
			matchboxIds,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					title: title.value,
					description: description.value,
					type: type.value,
					amount: amount.value,
					expiry: expiry.value,
					minimumSpend: minimumSpend.value,
					maximumSpend: maximumSpend.value,
					excludeSale: excludeSale.value,
					usageLimit: usageLimit.value,
					usageLimitItem: usageLimitItem.value,
					usageLimitPerUser: usageLimitPerUser.value,
					matchboxIds: matchboxIds.value,
				},
			})
		}

		uppercaseValidator(input) {
			return !this._upperCaseRegex.test(input)
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
					description,
					type,
					amount,
					expiry,
					minimumSpend,
					maximumSpend,
					excludeSale,
					usageLimit,
					usageLimitItem,
					usageLimitPerUser,
					matchboxIds,
				} = this.state.data

				if(this.props.id) {
					// UPDATE
					CouponManager.updateCoupon({
						id: this.props.id,
						title,
						description,
						type: this.getKey(type),
						amount,
						expiry: expiry && TimeHelper.moment(expiry, 'DD/MM/YYYY').toDate(),
						minimumSpend,
						maximumSpend,
						excludeSale: this.getKey(excludeSale),
						usageLimit,
						usageLimitItem,
						usageLimitPerUser,
						matchboxIds: this.getKey(matchboxIds),
					}).then(this.onSuccess).catch(this.onError)
				} else {
					// CREATE
					CouponManager.createCoupon({
						title,
						description,
						type: this.getKey(type),
						amount,
						expiry: expiry && TimeHelper.moment(expiry, 'DD/MM/YYYY').toDate(),
						minimumSpend,
						maximumSpend,
						excludeSale: this.getKey(excludeSale),
						usageLimit,
						usageLimitItem,
						usageLimitPerUser,
						matchboxIds: this.getKey(matchboxIds),
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSuccess(product) {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Coupon updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onUpdate &&
			this.props.onUpdate(product)

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		getTitle(type) {
			switch(type) {
			case 'shipping':
				return 'Shipping Amount';
			case 'item':
				return 'Each Applicable Item';
			case 'total':
				return 'Total Amount';
			case 'percent':
				return 'Percentage';
			default:
				return type
			}
		}

		getKey(title) {
			switch(title) {
			case 'Shipping Amount':
				return 'shipping';
			case 'Each Applicable Item':
				return 'item';
			case 'Total Amount':
				return 'total';
			case 'Percentage':
				return 'percent';
			case 'Basic Matchbox':
				return ['basic'];
			case 'Mini Matchbox':
				return ['mini'];
			case 'Matchbox X Catwomanizer':
				return ['x-catwomanizer'];
			case 'All':
				return [];
			case 'Yes':
				return true;
			case 'No':
				return false;
			default:
				return title
			}
		}

		getMatchboxTitle(type) {
			switch(type) {
			case 'basic':
				return 'Basic Matchbox'
			case 'mini':
				return 'Mini Matchbox'
			case 'x-catwomanizer':
				return 'Matchbox X Catwomanizer'
			default:
				return 'All'
			}
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxLego title="Coupon Detail">
						<FormPagelet
							config={[{
								title: 'Title',
								required: true,
								autofocus: true,
								id: 'title',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.title,
								validator: this.uppercaseValidator,
								description: 'Please use upper case.',
							}, {
								title: 'Description',
								id: 'description',
								type: FormPagelet.TYPES.FREE_TAREA,
								value: this.state.data.description,
							}, {
								title: 'Type',
								id: 'type',
								type: FormPagelet.TYPES.SELECTION,
								options: this._couponTypes,
								value: this.getTitle(this.state.data.type),
							}, {
								title: 'Amount',
								id: 'amount',
								type: FormPagelet.TYPES.CURRENCY,
								value: this.state.data.amount,
							}, {
								title: 'Expiry',
								id: 'expiry',
								type: FormPagelet.TYPES.BIRTHDATE,
								value: this.state.data.expiry,
							}, {
								title: 'Minimum Spend',
								id: 'minimumSpend',
								type: FormPagelet.TYPES.CURRENCY_NEGATIVE,
								description: 'Minimum total cart amount (before shipping) that must be fulfilled before coupon made applicable.',
								value: this.state.data.minimumSpend,
							}, {
								title: 'Maximum Spend',
								id: 'maximumSpend',
								type: FormPagelet.TYPES.CURRENCY_NEGATIVE,
								description: 'If coupon type is "Percentage", this will affect total applicable discount.\nEx: Provided cart amount is 10m, coupon type is "Percentage", coupon amount is 10%, maximum spend is 1m, then the applicable discount will be 100k, not 1m.',
								value: this.state.data.maximumSpend,
							}, {
								title: 'Exclude Sale',
								id: 'excludeSale',
								type: FormPagelet.TYPES.SELECTION,
								options: this._saleTypes,
								description: 'Option to exclude sale products (products that have its retail price not equal with its discounted price). If true, those products wouldn\'t be applicable for this coupon.',
								value: this.state.data.excludeSale ? 'Yes' : 'No',
							}, {
								title: 'Usage Limit',
								id: 'usageLimit',
								type: FormPagelet.TYPES.CURRENCY_NEGATIVE,
								description: 'Total usage limit, each checkout made by user counted as a coupon usage. Blank or -1 means Infinity.',
								value: this.state.data.usageLimit,
							}, {
								title: 'Usage Limit Item',
								id: 'usageLimitItem',
								type: FormPagelet.TYPES.CURRENCY_NEGATIVE,
								description: 'If coupon type is "Each Applicable Item", setting this will limit how many applicable products on each cart. Blank or -1 means Infinity',
								value: this.state.data.usageLimitItem,
							}, {
								title: 'Usage Limit Per User',
								id: 'usageLimitPerUser',
								type: FormPagelet.TYPES.CURRENCY_NEGATIVE,
								description: 'Self explanatory. Blank or -1 means Infinity',
								value: this.state.data.usageLimitPerUser,
							}, {
								title: 'Valid For',
								id: 'matchboxIds',
								type: FormPagelet.TYPES.SELECTION,
								options: this._matchboxes,
								value: this.getMatchboxTitle(this.state.data.matchboxIds),
							}]}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onSubmit }
						/>
					</BoxLego>
				</DetailPagelet>
			)
		}
	}
)
