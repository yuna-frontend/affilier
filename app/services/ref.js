import ServiceModel from 'coeur/models/service';


class RefService extends ServiceModel {

	static displayName = 'ref'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'ref/enum/', 0)
	}

	getEnum(enums) {
		return this.get(enums)
	}

	getRefStyleSheets() {
		return this.get('stylesheets')
	}

	getRefOrders() {
		return this.get('orders')
	}

	getRefInventory() {
		return this.get('inventories')
	}

	// getRefShipmentService() {
	// 	return this.get('shipments')
	// }

	getRefShipmentStatus() {
		return this.get('shipment_statuses')
	}

	getRefOrderDetailStatus() {
		return this.get('order_detail_statuses')
	}

	getRefStyleSheetsStatus() {
		return this.get('stylesheet_statuses')
	}

	getRefCoupon() {
		return this.get('coupons')
	}

	getRefAvailableCouponValidity() {
		return this.get('coupon_validity')
	}
}

export default new RefService()
