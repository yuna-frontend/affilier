import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	content: {
		// marginLeft: 0,
		// marginRight: 0,
		// marginBottom: 0,
		marginTop: Sizes.margin.default,
	},

	item: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		// borderBottomWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.black.palette(2, .16),
		// borderRadius: 2,
		backgroundColor: Colors.grey.palette(2),
		marginBottom: 4,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	id: {
		color: Colors.primary,
	},

	empty: {
		height: 88,
	},

	header: {
		alignItems: 'center',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	button: {
		marginTop: -6,
		marginBottom: -6,
		marginLeft: Sizes.margin.default,
	},

	variativePrice: {
		marginRight: 8,
		marginLeft: 5,
	}

})
