import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'
import Colors from 'coeur/constants/color'

import TouchableBit from 'modules/bits/touchable'
import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'

import Styles from './style'

export default ConnectHelper(
	class SearchBoxPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				search: PropTypes.string,
				onPress: PropTypes.func,
			}
		}

		view() {
			return (
				<TouchableBit onPress={this.props.onPress} style={Styles.padderInput}>
					<BoxBit row unflex style={Styles.containerInput}>
						<BoxBit unflex style={Styles.pr8}>
							<IconBit name="search" size={16} color={Colors.grey.palette(12)}/>
						</BoxBit>
						<BoxBit unflex>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={[Styles.inputText, { lineHeight: 18} ]}>
								{ this.props.search || 'Cari' }
							</GeomanistBit>
						</BoxBit>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)