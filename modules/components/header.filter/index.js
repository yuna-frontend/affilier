import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';

import PaginationComponent from 'modules/components/pagination';

import Styles from './style';

import { chunk } from 'lodash'


export default ConnectHelper(
	class HeaderFilterComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				pagination: PropTypes.bool,
				current: PropTypes.number,
				count: PropTypes.number,
				total: PropTypes.number,
				children: PropTypes.node,
				onNavigationChange: PropTypes.func,
				style: PropTypes.style,
				wrapperFilterStyle: PropTypes.style,
				filterStyle: PropTypes.style,
				paginationStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			pagination: true,
		}

		filterRenderer = (filter, i) => {
			return filter === null ? false : (
				<BoxBit key={i} style={[Styles.filter, this.props.filterStyle]}>
					{ filter }
				</BoxBit>
			)
		}

		boxRenderer = (d, i) => {
			return (
				<BoxBit key={i} style={Styles.filter} />
			)
		}

		view() {
			const filters = chunk(React.Children.map(this.props.children, this.filterRenderer), 4)

			if(filters.length && filters[filters.length - 1].length < 4) {
				filters[filters.length - 1].push(...Array(4 - filters[filters.length - 1].length).fill(undefined).map(this.boxRenderer))
			}

			return (
				<BoxBit row unflex style={[Styles.container, this.props.style]}>
					<BoxBit row style={[Styles.wrapper, this.props.wrapperFilterStyle]}>
						{ React.Children.map(this.props.children, this.filterRenderer) }
						{/* { Array(Math.max(0, 3 - React.Children.count(this.props.children))).fill(undefined).map(this.boxRenderer) } */}
					</BoxBit>
					{ this.props.pagination ? (
						<BoxBit style={[Styles.pagination, this.props.paginationStyle]}>
							<PaginationComponent
								current={ this.props.current }
								count={ this.props.count }
								total={ this.props.total }
								onNavigationChange={ this.props.onNavigationChange }
							/>
						</BoxBit>
					) : false }
				</BoxBit>
			);
		}
	}
)
