import ConnectHelper from 'coeur/helpers/connect';
import CoreGridBit from 'coeur/modules/bits/grid';

import BoxBit from '../box';
import ScrollViewBit from '../scroll.view';

import Styles from './style';

export default ConnectHelper(
	class GridBit extends CoreGridBit({
		BoxBit,
		ScrollViewBit,
		Styles,
	}) {}
)
