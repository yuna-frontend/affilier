import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},

	tabActive: {
		paddingBottom: 18,
		paddingTop: 10,
		borderBottomWidth: 2,
		borderColor: Colors.primary,
		marginBottom: -StyleSheet.hairlineWidth,
		zIndex: 1,
	},

	tab: {
		height: 56,
		paddingRight: 16,
		alignItems: 'center',
	},

	count: {
		height: 24,
		width: 24,
		borderWidth: 1,
		borderColor: Colors.black.palette(2),
		borderRadius: 12,
		marginRight: 8,
	},

	countInactive: {
		borderColor: Colors.grey.palette(6),
	},

	radio: {
		marginRight: 8,
	},

	disabled: {
		opacity: .6,
	},

	isActive: {
		color: Colors.primary,
	},

	isInactive: {
		color: Colors.grey.palette(6),
	},
})
