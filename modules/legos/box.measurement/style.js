import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
		marginTop: 16,
	},

	padder: {
		paddingBottom: 0,
		marginTop: 8,
	},

	row: {
		marginBottom: 16,
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 16,
		paddingRight: 16,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		justifyContent: 'space-between',
	},
})
