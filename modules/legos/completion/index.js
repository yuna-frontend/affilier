import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

// import HintLego from 'modules/legos/hint';

import Styles from './style'


export default ConnectHelper(
	class CompletionLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				percentage: PropTypes.bool,

				count: PropTypes.number.isRequired,
				purchase: PropTypes.number.isRequired,
				retail: PropTypes.number.isRequired,

				targetCount: PropTypes.number.isRequired,
				targetRetail: PropTypes.number,
				maxPurchase: PropTypes.number,
				minRetail: PropTypes.number,

				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
			percentage: true,
			targetRetail: 0,
			maxPurchase: Infinity,
			minRetail: 0,
		}

		static getDerivedStateFromProps(nP) {
			let retailStatus = 0
			let purchaseStatus = 0
			let countStatus = 0

			if (nP.retail < nP.targetRetail) {
				if (nP.retail >= nP.minRetail) {
					retailStatus = 2
				} else {
					retailStatus = 1
				}
			} else {
				retailStatus = 3
			}

			if (nP.purchase <= nP.maxPurchase) {
				if (nP.purchase >= nP.maxPurchase * .85) {
					purchaseStatus = 2
				} else {
					purchaseStatus = 3
				}
			} else {
				purchaseStatus = 1
			}

			if (nP.count < nP.targetCount) {
				if (nP.count > nP.targetCount * .7) {
					countStatus = 2
				} else {
					countStatus = 1
				}
			} else if (nP.count === nP.targetCount) {
				countStatus = 3
			} else {
				countStatus = 1
			}

			return {
				retailStatus,
				purchaseStatus,
				countStatus,
				// eslint-disable-next-line no-nested-ternary
				retail: nP.percentage ? `${ (isNaN(100 * nP.retail / nP.targetRetail) ? 0 : (100 * nP.retail / nP.targetRetail) === Infinity ? 100 : (100 * nP.retail / nP.targetRetail)).toFixed(0) }%` : FormatHelper.number(nP.retail),
				purchase: nP.percentage ? `${isNaN(100 * nP.purchase / nP.maxPurchase) ? 0 : (100 * nP.purchase / nP.maxPurchase).toFixed(0) }%` : FormatHelper.number(nP.purchase),
				count: `${ nP.count }/${ nP.targetCount }`,
			}
		}

		constructor(p) {
			super(p, {
				purchase: '',
				purchaseStatus: 0,
				retail: '',
				retailStatus: 0,
				count: '',
				countStatus: 0,

			})
		}

		completionRenderer = (title, value, status, flex = 1) => {
			return (
				<BoxBit centering style={[ Styles.box, {
					flexGrow: flex,
				} ]}>
					<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_5 }>
						{ title }
					</GeomanistBit>
					<GeomanistBit type={ this.props.percentage ? GeomanistBit.TYPES.SUBHEADER_1 : GeomanistBit.TYPES.SUBHEADER_2 } weight="normal" style={[ Styles.text, Styles[`text_${ status }`] ]}>
						{ value }
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit row unflex={ this.props.unflex } style={[ Styles.container, this.props.style ]}>
					{ this.completionRenderer('COUNT', this.state.count, this.state.countStatus) }
					{ this.completionRenderer('RETAIL', this.state.retail, this.state.retailStatus) }
					{ this.completionRenderer('PURCHASE', this.state.purchase, this.state.purchaseStatus) }
				</BoxBit>
			)
		}
	}
)
