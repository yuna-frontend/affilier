import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		// borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		overflow: 'hidden',
	},
	content: {
		transform: 'scale(2) rotateZ(-45deg)',
	},
})
