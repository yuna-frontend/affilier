import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import TimeHelper from 'coeur/helpers/time';
import StringHelper from 'coeur/helpers/string';

// import ShipmentService from 'app/services/admin.shipment';

import BoxBit from 'modules/bits/box';
import InputCurrencyBit from 'modules/bits/input.currency';
import InputDateTimeBit from 'modules/bits/input.datetime';
import InputDateBit from 'modules/bits/input.date';
import InputValidatedBit from 'modules/bits/input.validated';
import SelectionBit from 'modules/bits/selection';

import RowPacketLego from 'modules/legos/row.packet';

import RowLego from 'modules/legos/row';
import RowsLego from 'modules/legos/rows';

// import Styles from './style'


export default ConnectHelper(
	class DetailPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				editable: PropTypes.bool,
				packet: PropTypes.object,
				data: PropTypes.shape({
					service: PropTypes.string,
					awb: PropTypes.string,
					url: PropTypes.string,
					amount: PropTypes.number,
					packetId: PropTypes.number,
					metadata: PropTypes.object,
					createdAt: PropTypes.date,
				}),
				couriers: PropTypes.array,
				rates: PropTypes.array,
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			couriers: [],
			rates: [],
			data: {
				metadata: {},
			},
		}

		shouldComponentUpdate() {
			return false
		}

		packet = { ...this.props.packet }
		data = { ...this.props.data }

		onChangeCourier = value => {
			this.data.courier = value

			this.props.onChange &&
			this.props.onChange('courier', this.data.courier)

			if(this.props.couriers.length > 1) {
				const rate = this.props.rates.filter(r => r.courier === value)[0]

				this.data.service = rate.service
				this.data.amount = rate.rate
				this.data.metadata = {
					max_delivery_day: rate.max_day,
					min_delivery_day: rate.min_day,
				}

				this.props.onChange &&
				this.props.onChange('service', this.data.service)

				this.props.onChange &&
				this.props.onChange('amount', this.data.amount)

				this.props.onChange &&
				this.props.onChange('metadata', this.data.metadata)

				this.forceUpdate()
			}
		}

		onChangeService = value => {
			this.data.service = value

			this.props.onChange &&
			this.props.onChange('service', this.data.service)

			if(this.props.rates.length) {
				const rate = this.props.rates.find(r => r.service === value)

				this.data.amount = rate.rate
				this.data.metadata = {
					max_delivery_day: rate.max_day,
					min_delivery_day: rate.min_day,
				}

				this.props.onChange &&
				this.props.onChange('amount', this.data.amount)

				this.props.onChange &&
				this.props.onChange('metadata', this.data.metadata)

				this.forceUpdate()
			}
		}

		onChangeEstimate = (date, moment) => {
			const diff = moment.diff(this.data.createdAt, 'd')

			this.data.metadata = {
				max_delivery_day: diff,
				min_delivery_day: diff,
			}

			this.props.onChange &&
			this.props.onChange('metadata', this.data.metadata)

			this.forceUpdate()
		}

		onChange = (key, value) => {
			this.data[key] = value

			this.props.onChange &&
			this.props.onChange(key, value)
		}

		onChangePacket = packet => {
			this.packet = { ...packet }

			this.props.onChange &&
			this.props.onChange('packet', packet)
		}

		view() {
			return (
				<React.Fragment>
					<RowsLego data={[{
						data: [{
							headerless: true,
							children: (
								<RowLego
									data={[{
										title: 'Courier',
										children: this.props.couriers.length > 1 ? (
											<SelectionBit
												readonly={!this.props.editable}
												options={this.props.couriers.map(courier => {
													return {
														key: courier,
														title: courier,
														selected: courier === this.data.courier,
													}
												})}
												onChange={this.onChangeService}
											/>
										) : (
											<InputValidatedBit readonly={ !this.props.editable } type="INPUT" value={ this.data.courier } onChange={this.onChange.bind(this, 'courier')} />
										),
									}, {
										title: 'Service',
										children: (
											<BoxBit unflex>
												{ this.props.rates.length === 0 ? (
													<InputValidatedBit readonly={ !this.props.editable } type="INPUT" value={ this.data.service } onChange={this.onChange.bind(this, 'service')} />
												) : (
													<SelectionBit
														readonly={!this.props.editable}
														options={this.props.rates.filter(rate => this.data.courier ? rate.courier === this.data.courier : true).map(rate => {
															return {
																key: rate.service,
																title: rate.service,
																selected: rate.service === this.data.service,
															}
														})}
														onChange={this.onChangeService}
													/>
												) }
											</BoxBit>
										),
									}]}
								/>
							),
						}, {
							title: 'Shipping Cost',
							children: (
								<InputCurrencyBit readonly={ !this.props.editable } value={ this.data.amount } onChange={this.onChange.bind(this, 'amount')} />
							),
						}],
					}, {
						data: [{
							title: 'AWB',
							children: (
								<InputValidatedBit readonly={ !this.props.editable } type="INPUT" value={ this.data.awb } onChange={ this.onChange.bind(this, 'awb') } />
							),
						}, {
							title: 'URL',
							children: (
								<InputValidatedBit readonly={ !this.props.editable } type="INPUT" value={ this.data.url } onChange={this.onChange.bind(this, 'url')} />
							),
						}],
					}]} />

					<RowPacketLego
						packet={ this.props.packet }
						readonly={ !this.props.editable }
						packetId={ this.props.packetId }
						onChange={ this.onChangePacket }
					/>

					<RowsLego
						data={[{
							data: [{
								title: 'Manifest Date',
								children: (
									<InputDateTimeBit title={ null } date={ this.data.createdAt } readonly={ !this.props.editable } onChange={ this.onChange.bind(this, 'createdAt') } />
								),
							}, {
								title: 'Estimated Delivery Date',
								children: (
									<InputDateBit key={ this.data.metadata.max_delivery_day } title={null} date={ TimeHelper.moment(this.data.createdAt).add(this.data.metadata.max_delivery_day, 'd') } readonly={ !this.props.editable } onChange={ this.onChangeEstimate } />
								),
								description: this.data.metadata.max_delivery_day !== this.data.metadata.min_delivery_day ? `Estimation: ${this.data.metadata.min_delivery_day} - ${StringHelper.pluralize(this.data.metadata.max_delivery_day, 'day', '0 day', 's')} shipment` : `Estimation: ${StringHelper.pluralize(this.data.metadata.max_delivery_day, 'day', '0 day', 's')} shipment`,
							}],
						}]}
					/>

				</React.Fragment>
			)
		}
	}
)
