import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import PromiseStatefulModel from 'coeur/models/promise.stateful'

import FormatHelper from 'coeur/helpers/format'

import CommissionService from 'app/services/commission'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'

import Styles from './style'

export default ConnectHelper(
	class CommissionPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			return CommissionService.getBalance(state.me.id, state.me.token)
				.then(res => {
					if(res && typeof res.total_commission !== 'undefined' && typeof res.total_wallet !== 'undefined') {
						return res
					} else {
						oP.onError &&
						oP.onError()
						return false
					}
				})
				.catch(err => {
					oP.onError &&
					oP.onError(err)
					return false
				})
		}

		viewOnLoading() {
			return (
				<BoxBit style={ this.props.style }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
						Komisi
					</GeomanistBit>
					<BoxBit unflex row style={ Styles.alignItemsCenter }>
						<GeomanistBit type={ GeomanistBit.TYPES.HEADER_3 } style={ Styles.mr8 }>
							{/* IDR { FormatHelper.currencyFormat(this.props.data.totalCommission) } */}
							IDR
						</GeomanistBit>
						<LoaderBit simple/>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			if(this.props.isLoading) return this.viewOnLoading()

			return (
				<BoxBit style={ this.props.style }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } weight="medium">
						Komisi
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.HEADER_3 }>
						IDR { FormatHelper.currencyFormat(this.props.data.total_commission) }
					</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
