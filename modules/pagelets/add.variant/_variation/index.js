import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import RadioBit from 'modules/bits/radio'

import VariantListPart from './_varian.list'

import ListLego from 'modules/legos/list'

import Styles from './style';

import VariationDataPart from '../__variation.data'


export default ConnectHelper(
	class VariationPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
				
				product: PropTypes.array,
				selectedProductId: PropTypes.number,

				size: PropTypes.string,
				sizeId: PropTypes.number,
				colors: PropTypes.array,
				colorsIds: PropTypes.array,
				
				onAddNewVariant: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				showForm: false,
				select: false,
			}, [
			])

			this.getData = true;
		}

		onSelect = () => {
			this.setState({
				select: true,
				showForm: false,
			})
		}

		onAddNew = () => {
			this.setState({
				showForm: !this.state.showForm,
				select: false,
			})
		}

		closeForm = () => {
			this.setState({
				showForm: false,
			})
		}

		variantRenderer = (variant, i) => {
			return(
				<VariantListPart
					key={ i }
					// selectedProductId={ this.state.selectedProductId }
					id={ variant.id }
					size={ variant.size.title }
					colors= {variant.colors}
				/>
			)
		}

		onChange = (key, val) => {
			this.setState({
				[key]: val,
			})
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					{ this.props.product[0].variants.map(this.variantRenderer) }

					<ListLego isHeader row
						onPress={this.onAddNew}
						style={Styles.containerAddNew}
					>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.primary}>
							Add New Variation
						</GeomanistBit>
						<BoxBit />
					</ListLego>
					{ this.state.showForm && (
						<VariationDataPart
							onChange={this.onChange}
							selectedProductId={this.props.selectedProductId}
							onAddNewVariant={this.props.onAddNewVariant}

							size={ this.props.size }
							sizeId={ this.props.sizeId }
							colors={ this.props.colors }
							colorsIds={ this.props.colorsIds }					closeForm={this.closeForm}
						/>
					) }
				</BoxBit>
			)
		}
	}
)
