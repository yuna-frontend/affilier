import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from '../box'
import CheckboxBit from '../checkbox'
import GeomanistBit from '../geomanist'
import TouchableBit from '../touchable'

import Styles from './style'


export default ConnectHelper(
	class InputCheckboxBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				disabled: PropTypes.bool,
				dumb: PropTypes.bool,
				value: PropTypes.bool,
				title: PropTypes.string,
				label: PropTypes.string,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
			dumb: false,
		}

		static getDerivedStateFromProps(nP) {
			if (nP.dumb) {
				return {
					value: nP.value,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				value: p.value,
			})
		}

		onChange = () => {
			const value = !this.state.value

			this.setState({
				value,
			})

			this.props.onChange &&
			this.props.onChange(value)
		}


		view() {
			return (
				<BoxBit unflex style={ this.props.style }>
					{ this.props.label ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" style={ Styles.label }>
							{ this.props.label }
						</GeomanistBit>
					) : false }
					<TouchableBit accessible={ !this.props.disabled } unflex={ this.props.unflex } row onPress={ this.onChange } style={ Styles.container }>
						<CheckboxBit disabled={ this.props.disabled } dumb isActive={ this.state.value } onPress={ this.onChange } style={ Styles.radio } />
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ this.props.title }
							</GeomanistBit>
							{ this.props.description ? (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
									{ this.props.description }
								</GeomanistBit>
							) : false }
						</BoxBit>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
