import ColorModel from 'coeur/models/color';

/*
 * 08 is .03
 * 33 is .2
 * 66 is .4
 * 80 is .5
 * 99 is .6
 * B3 is .7
 * CC is .8
 * E5 is .9
 */

const White = new ColorModel([
		'FFFFFF', // #FFFFFF
	], 1)
	, Black = new ColorModel([
		'0F0F14', // #0F0F14
		'27272B', // #27272B
	], 1)
	, Purple = new ColorModel([
		'5267E6', // #5267E6
		'5B5BFF', // #5B5BFF
		'3333FF', // #3333FF
	], 1)
	, Pink = new ColorModel([
		'FFD1E5', // #FFD1E5
		'FF99AA', // #FF99AA
	], 2)
	, Blue = new ColorModel([
		'0068FF', // #0068FF
		'91ABB0', // #91ABB0 BLUE GREY TWO
	], 1, {
		facebook: '#3B5998', // #3B5998
	})
	, Green = new ColorModel([
		'00AA5A', // #00AA5A
		'345A45', // #345A45 PINE
	], 1)
	, Yellow = new ColorModel([
		'FFB900', // #FFBF00
		'FF9400', // #FF9400
		'D8BA6E', // #D8BA6E TAN
	], 1)
	, Red = new ColorModel([
		'FFABAB', // #FFABAB
		'FF6666', // #FF6666
		'E84040', // #E84040 TOMATO
		'FF7055', // #FF7055 ORANGE-PINK
		'986C79', // #986C79
		'DC8167', // #DC8167 DARK PEACH
		'AD321B', // #AD321B BRICK
		'BD3A21', // #BD3A21 rgb(189, 58, 33) SIENNA

	], 2)
	, Grey = new ColorModel([
		'8993A4', // #8993A4
		'F4F6F8', // #F4F6F8 PALE GREY
		'C1C7D0', // #C1C7D0
		'DFE3E8', // #DFE3E8
		'F9FAFB', // #F9FAFB PALE GREY TWO
		'C4CDD5', // #C4CDD5 SILVER
		'F3F3F5', // #F3F3F5 rgb(243, 243, 245) PALE GREY SIX
		'EEEEEE', // #EEEEEE rgb(238, 238, 238) GALLERY
		'969AA3', // #969AA3 rgb 150 154 163 COOL GREY
	], 1)
	, GreySolid = new ColorModel([
		'F4F6F8', // #F4F6F8 PALE GREY
		'C1C7D0', // #C1C7D0
		'DFE3E8', // #DFE3E8
		'F9FAFB', // #F9FAFB PALE GREY TWO
		'C4CDD5', // #C4CDD5 SILVER
	], 2)


const COLORS = {
	transparent: 'transparent',

	primary: Red.palette(5),

	white: White,
	black: Black,
	purple: Purple,
	pink: Pink,
	blue: Blue,
	green: Green,
	yellow: Yellow,
	red: Red,
	grey: Grey,

	solid: {
		grey: GreySolid,
	},
	opacity: {
		black: {
			overlay: Black.palette(2, .02),
		},
	},
	gradient: {},
};


export default COLORS
