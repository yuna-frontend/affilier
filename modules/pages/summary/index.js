import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'
import AuthenticationHelper from 'utils/helpers/authentication'

import UserService from 'app/services/user.new'

import BoxBit from 'modules/bits/box'
import LoaderBit from 'modules/bits/loader'

import TabLego from 'modules/legos/tab'

import PageAuthorizedComponent from 'modules/components/page.authorized'
import HeaderPageComponent from 'modules/components/header.page'

import SummaryPart from './_summary'
import RankPart from './_rank'

import Styles from './style'

export default ConnectHelper(
	class SummaryPage extends PageModel {
		
		static routeName = 'summary'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				isUpdating: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
				data: {
					addresses: [],
				},
				changes: {},
			})
		}

		roles = ['stylist', 'headstylist', 'analyst', 'finance', 'fulfillment', 'purchasing']

		onNavigateToSummary = () => {
			this.navigator.navigate('summary')
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					// onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Summary',
						onPress: this.onNavigateToSummary,
					}]}
				>
					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Summary"
							/>
							<BoxBit unflex>
								<BoxBit>
									<SummaryPart/>
								</BoxBit>
								{/* <BoxBit>
									<RankPart/>
								</BoxBit> */}
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
