import ConnectHelper from 'coeur/helpers/connect';
import CoreProgressBit from 'coeur/modules/bits/progress';

import BoxBit from '../box';

import Styles from './style'


export default ConnectHelper(
	class ProgressBit extends CoreProgressBit({
		BoxBit,
		Styles,
	}) {}
)
