import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonProgressBit from 'coeur/modules/bits/button.progress'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonSearch extends CoreButtonProgressBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.primary],
				ACTIVE: [Colors.white.primary, Colors.primary],
				DISABLED: [Colors.white.primary, Colors.grey.palette(1, .7)],
				LOADING: [Colors.white.primary, Colors.primary, undefined, Colors.palette],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
			COMPACT: Styles.compact,
			MEDIUM: Styles.medium,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.COMPACT:
				textType = GeomanistBit.TYPES.PRIMARY_3
				break;
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.SECONDARY_2
				break;
			}

			return (
				<GeomanistBit weight={this.props.weight} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
