import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';

import Styles from './style';


export default ConnectHelper(
	class NotificationLego extends StatefulModel {

		static TYPES = {
			DEFAULT: 'DEFAULT',
			WARNING: 'WARNING',
			ERROR: 'ERROR',
			SUCCESS: 'SUCCESS',
		}

		static propTypes(PropTypes) {
			return {
				message: PropTypes.string.isRequired,
				type: PropTypes.oneOf(this.TYPES),
				icon: PropTypes.oneOf(IconBit.TYPES),
				iconless: PropTypes.bool,
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			type: 'DEFAULT',
			icon: 'circle-info',
		}

		getBackgroundColor() {
			switch(this.props.type) {
			case this.TYPES.WARNING:
				return Colors.yellow.palette(1)
			case this.TYPES.ERROR:
				return Colors.red.palette(7)
			case this.TYPES.SUCCESS:
				return Colors.green.palette(2)
			case this.TYPES.DEFAULT:
			default:
				return Colors.black.palette(2, .7)
			}
		}

		view() {
			return (
				<BoxBit unflex row centering style={[Styles.container, {
					backgroundColor: this.getBackgroundColor(),
				}, this.props.style]}>
					{ !this.props.iconless ? (
						<IconBit
							name={ this.props.icon }
							color={ Colors.white.primary }
						/>
					) : false }
					<BoxBit style={Styles.titleContainer}>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="medium" style={Styles.title}>{ this.props.message }</GeomanistBit>
					</BoxBit>
					{ this.props.children }
				</BoxBit>
			)
		}
	}
)
