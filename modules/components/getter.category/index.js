import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

// import Colors from 'coeur/constants/color'

// import MeManager from 'app/managers/me'
import CategoryService from 'app/services/category'

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

const cache = {}

export default ConnectHelper(
	class GetterCategoryComponent extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				children: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.categoryId) {
				if(cache[oP.categoryId]) {
					return Promise.resolve({
						categoryId: cache[oP.categoryId],
					})
				} else {
					return CategoryService.getCategory(oP.categoryId, state.me.token).then(category => {
						cache[oP.categoryId] = category.category_id
						return {
							categoryId: category.category_id,
						}
					})
				}
			} else {
				return null
			}
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (this.props.children(this.props.data))
		}
	}
)
