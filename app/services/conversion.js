import ServiceModel from 'coeur/models/service';


class ConversionService extends ServiceModel {

	static displayName = 'conversion'

	constructor() {
		super(process.env.CONNECTER_URL + 'affiliate/conversion/', 0)
	}

	getConversion(userId, query, token) {
		return this.get(userId, {
			query,
			token,
		})
	}
}

export default new ConversionService()
