import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonProgressBit from 'coeur/modules/bits/button.progress'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonProgressBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.primary, Colors.primary],
				ACTIVE: [Colors.white.primary, Colors.red.palette(5, .9), Colors.primary],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(5), Colors.solid.grey.palette(5)],
				LOADING: [Colors.white.primary, Colors.primary, Colors.primary, Colors.red.palette(1, .3)],
			},
			SECONDARY: {
				NORMAL: [Colors.black.palette(2), Colors.white.primary, Colors.black.palette(2, .2)],
				ACTIVE: [Colors.black.palette(2, .2), Colors.white.primary, Colors.black.palette(2, .2)],
				DISABLED: [Colors.black.palette(2, .4), Colors.white.primary, Colors.black.palette(2, .2)],
				LOADING: [Colors.black.palette(2), Colors.white.primary, Colors.black.palette(2, .2), Colors.black.palette(2, .16)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
			MEDIUM: Styles.medium,
			SMALL: Styles.small,
			TINY: Styles.tiny,
			MIDGET: Styles.midget,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.PARAGRAPH_3
				break;
			case this.TYPES.SIZES.MEDIUM:
				textType = GeomanistBit.TYPES.PRIMARY_2
				break;
			case this.TYPES.SIZES.TINY:
			case this.TYPES.SIZES.MIDGET:
				textType = GeomanistBit.TYPES.NOTE_1
				break
			case this.TYPES.SIZES.SMALL:
				textType = GeomanistBit.TYPES.SECONDARY_3
				break;
			}

			return (
				<GeomanistBit weight={this.props.weight} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={this.props.iconSize || 24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
