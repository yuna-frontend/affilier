import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import RowLego from 'modules/legos/row';
import SelectColorLego from 'modules/legos/select.color'

import Styles from './style'


export default ConnectHelper(
	class RowColorLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				colorIds: PropTypes.arrayOf(PropTypes.id),
				onChange: PropTypes.func,
			}
		}

		constructor(p) {
			super(p)

			this.colorIds = p.colorIds
			this.colorStrings = []
		}

		static defaultProps = {
			colorIds: [],
		}

		onChangeColor = (index, colorId, colorString) => {
			this.colorIds[index] = colorId
			if(colorString === 'No color') {
				this.colorStrings[index] = undefined
			} else{
				this.colorStrings[index] = colorString
			}
			

			this.props.onChange &&
			this.props.onChange(this.colorIds.filter(cId => !!cId), this.colorStrings.filter(cStr => !!cStr) )
		}

		view() {
			return (
				<RowLego
					key={ this.props.colorIds.join(',') }
					data={[{
						title: 'Primary Color',
						children: (
							<SelectColorLego isRequired
								placeholder={ 'Select color' }
								colorId={ this.props.colorIds[0] }
								onChange={ this.onChangeColor.bind(this, 0) }
							/>
						),
					}, {
						title: 'Secondary Color',
						children: (
							<SelectColorLego
								placeholder={ 'Select color' }
								emptyTitle={ 'No color' }
								colorId={ this.props.colorIds[1] }
								onChange={ this.onChangeColor.bind(this, 1) }
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
