import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		maxHeight: '100%',
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	modalPage: {
		justifyContent: 'center',
	},

	modalContainer: {
		maxHeight: '75%',
		height: 'auto',
		width: Sizes.screen.width - (Sizes.margin.thick * 2),
		background: Colors.white.primary,
		borderRadius: 8,
	},

	modalHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	contentContainer: {
		width: Sizes.screen.width - (Sizes.margin.thick * 2),
	},

	pt16: {
		paddingTop: Sizes.margin.default,
	},

})
