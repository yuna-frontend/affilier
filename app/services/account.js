import ServiceModel from 'coeur/models/service';


class AccountService extends ServiceModel {

	static displayName = 'account'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'account/')
	}

	login({
		email,
		password,
	}) {
		return this.post('login', {
			data: {
				email,
				password,
			},
		})
	}
}

export default new AccountService()
