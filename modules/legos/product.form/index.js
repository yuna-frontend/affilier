import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import TextBit from 'modules/bits/text';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';

import AddressUpdateComponent from 'modules/components/address.update';

import BoxLego from 'modules/legos/box';
import DropdownSearchLego from 'modules/legos/dropdown.search';
import RowImageUserLego from 'modules/legos/row.image.user';
import RowMatchboxTypeLego from 'modules/legos/row.matchbox.type';

import FormPagelet from 'modules/pagelets/form';
import ModalCloseableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';
import HeaderPart from './_header';
import ResultItemPart from './_result.item';
import { isEqual, isEmpty } from 'lodash';


export default ConnectHelper(
	class ProductFormLego extends StatefulModel {
		static TYPES = {
			MATCHBOX: 'MATCHBOX',
			INVENTORY: 'INVENTORY',
			VARIANT: 'VARIANT',
			VOUCHER: 'VOUCHER',
			SERVICE: 'SERVICE',
		}

		static propTypes(PropTypes) {
			return {
				userId: PropTypes.id,

				isEditing: PropTypes.bool,
				item: PropTypes.object,
				data: PropTypes.object,

				// TODO: item category validation

				onUpdate: PropTypes.func,
				onUpdateDetail: PropTypes.func,

				onAdd: PropTypes.func,
				onCancel: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			item: {},
			data: {},
			metadata: {},
			title: 'Items',
			addTitle: 'Add Item',
			type: 'MATCHBOX',
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				item: p.item,
				metadata: p.metadata,
				assetIds: [],

				isGift: p.isGift,
				isPhysical: p.isPhysical,
			})
			this.submit = null

			this.lineup = [
				'#01 - Clothing - Clothing - Clothing - Clothing',
				'#02 - Clothing - Clothing - Clothing - Bag',
				'#03 - Clothing - Clothing - Clothing - Footwear',
				'#04 - Clothing - Clothing - Clothing - Accessory',
				'#05 - Clothing - Clothing - Bag - Accessory',
				'#06 - Clothing - Clothing - Footwear - Accessory',
			]

			this.data = {
				'NAME': {
					id: 'receiver',
					key: 'receiver',
					title: 'Recipient\'s Name',
					required: true,
					value: this.props.metadata.receiver,
					style: Styles.border,
				},
				'EMAIL': {
					id: 'email',
					key: 'email',
					title: 'Recipient\'s Email',
					type: FormPagelet.TYPES.EMAIL,
					required: true,
					value: this.props.metadata.email,
					description: 'We will only contact you by phone if there is a problem with your order',
					style: [Styles.input, Styles.padderBottom],
				},
				'PHONE': {
					id: 'phone',
					key: 'phone',
					title: 'Recipient\'s Phone Number',
					type: FormPagelet.TYPES.PHONE,
					required: true,
					value: this.props.metadata.phone,
					description: 'We will only contact you by phone if there is a problem with your order',
				},
				'GIFTNOTE': {
					id: 'giftNote',
					key: 'giftNote',
					required: true,
					title: 'Gift Message',
					type: FormPagelet.TYPES.TEXTAREA,
					value: this.props.metadata.giftNote,
				},
				'NOTE': {
					id: 'note',
					key: 'note',
					title: 'Matchbox Note',
					placeholder: 'Write your note here',
					type: FormPagelet.TYPES.TEXTAREA,
					value: this.props.metadata.note,
					required: true,
					style: Styles.padder,
				},
				'RECIPIENT': {
					id: 'address',
					key: 'address',
					title: 'Recipent\'s Shipping Info',
					value: this.state.metadata.address,
					children: (
						<AddressUpdateComponent
							type="user"
							address={this.props.metadata.address}
							onUpdate={this.onUpdateAddress }
							style={ Styles.add }
						/>
					),
				},
				'ATTACHMENT': {
					id: 'asset_ids',
					key: 'asset_ids',
					title: 'Reference Attachment',
					children: this.props.userId ? (
						<RowImageUserLego id={this.props.userId}
							assetIds={this.props.metadata.asset_ids}
							key="attachment" addable
							onUpdate={this.onDataLoaded}
						/>
					) : (
						<BoxBit unflex>
							<TextBit weight="medium" style={Styles.attachment}>Attachment</TextBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Set user first
							</GeomanistBit>
						</BoxBit>
					),
				},
				'LINEUP': {
					id: 'lineup',
					key: 'lineup',
					title: 'Lineup',
					placeholder: 'Select lineup',
					required: true,
					type: FormPagelet.TYPES.SELECTION,
					options: this.lineup.map(title => {
						return {
							key: title,
							title: title,
							selected: title === this.props.metadata.lineup,
						}
					}),
					style: { marginTop: 0 },
				},
			}
		}
		
		transposeFormData(data) {
			let newData = {}

			for(const [key, value] of Object.entries(data)) {
				newData = {
					...newData,
					[key]: value.value,
				}
			}

			return newData
		}

		onUpdate = (isValid, data) => {
			this.state.metadata = {
				...this.transposeFormData(data),
			}
		}

		onUpdateAddress = data => {
			this.state.metadata = {
				...(this.state.metadata.isPhysical ? {
					...this.state.metadata,
				} : {
					isPhysical: true,
				}),
				address: data,
			}
		}

		onUpdatePhysicalNote = (isValid, { giftNote }) => {
			this.state.metadata = {
				...this.state.metadata,
				giftNote: giftNote.value,
			}
		}

		onSubmit = () => {
			this.state.product = {
				...this.state.item,
				asVoucher: this.state.isGift,
				metadata: {
					...this.state.metadata,
					isPhysical: this.state.isPhysical,
					asset_ids: this.state.assetIds,
				},
			};

			this.props.onAdd &&
			this.props.onAdd(this.state.product)
		}

		onDataLoaded = (val) => {
			this.state.assetIds = val.map(v => v.id)
		}

		onChangeHeader = (val) => {
			if(!isEqual(this.state.item, val)) {
				this.state.item = val
			}
		}

		onChangeMatchboxType = (val) => {
			this.setState({
				isGift: val.isGift,
				isPhysical: val.isPhysical,
			})
		}

		onSelectProduct = (val) => {
			this.setState({
				isLoading: false,
				item: {
					id: val.id,
					title: val.title,
					type: val.type,
				},
			})
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering style={ Styles.empty }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.text }>
						No Data
					</GeomanistBit>
				</BoxBit>
			)
		}

		formRenderer = (config = []) => {
			return (
				<FormPagelet
					config={config}
					onUpdate={this.state.isPhysical ? this.onUpdatePhysicalNote : this.onUpdate}
				/>
			)
		}

		contentRenderer = () => {
			let config;

			const {
				type,
				id,
				title,
			} = this.state.item

			if(type === this.TYPES.MATCHBOX) {
				if(this.state.isPhysical) {
					config = ['RECIPIENT', 'GIFTNOTE']
				} else if(this.state.isGift) {
					config = ['NAME', 'EMAIL', 'PHONE', 'GIFTNOTE']
				} else {
					config = ['LINEUP', 'NOTE', 'ATTACHMENT']
				}
			} else if(type === this.TYPES.SERVICE) {
				config = ['NOTE', 'ATTACHMENT']
			}

			return (
				<BoxBit>
					<HeaderPart
						id={id}
						type={type}
						title={title}
						onClear={this.onClear}
						onChange={this.onChangeHeader}
					/>

					<BoxBit unflex style={Styles.content}>
						{ type === this.TYPES.MATCHBOX && (
							<BoxBit unflex style={Styles.type}>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									weight="semibold"
									style={Styles.title}
								>
									Matchbox Type
								</GeomanistBit>

								<RowMatchboxTypeLego
									isGift={this.state.isGift}
									isPhysical={this.state.isPhysical}
									onChange={this.onChangeMatchboxType}
								/>
							</BoxBit>
						) }
						<BoxBit key={`${type} ${this.state.isGift} ${this.state.isPhysical}`}>
							{ config && this.formRenderer(config.map(c => this.data[c])) }
						</BoxBit>
					</BoxBit>

				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxLego
					title={ this.props.title }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.noMargin }
				>
					<BoxBit unflex centering style={ Styles.empty }>
						<LoaderBit simple />
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
							Fetching result ...
						</GeomanistBit>
					</BoxBit>
				</BoxLego>
			)
		}

		view() {
			return (
				<ModalCloseableConfirmationPagelet
					title={this.props.title}
					confirm={this.props.isEditing ? 'Edit' : 'Add'}
					disabled={isEmpty(this.state.item)}
					onCancel={ this.props.onCancel }
					onConfirm={ this.onSubmit }
					contentContainerStyle={Styles.container}
					children={(
						<BoxBit style={Styles.padderTop}>
							{ !this.props.isEditing && (
								<DropdownSearchLego
									placeholder="Search product by brand or product name"
									onSelect={ this.onSelectProduct }
									children={(
										<ResultItemPart
											search={this.state.search}
											{ ...this.props }
										/>
									)}
								/>
							)}

							{ !isEmpty(this.state.item) && this.contentRenderer() }
						</BoxBit>
					)}
				/>
			)
		}
	}
)
