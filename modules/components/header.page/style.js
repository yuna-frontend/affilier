import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	breadcrumb: {
		paddingTop: 16,
		paddingBottom: 16,
	},

	content: {
		marginBottom: 16,
	},

	header: {
		height: 44,
		justifyContent: 'center',
	},

	title: {
		textTransform: 'capitalize',
		color: Colors.black.palette(2),
	},

	search: {
		borderRadius: 2,
		overflow: 'hidden',
		marginLeft: 16,
	},

	input: {
		height: 34,
	},

	icon: {
		marginRight: 8,
	},
})
