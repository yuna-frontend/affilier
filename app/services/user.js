import ServiceModel from 'coeur/models/service';


class UserService extends ServiceModel {

	static displayName = 'user'

	constructor() {
		super(process.env.API_URL + 'admin/user/', 0)
	}

	getUsers(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getUser(id, token) {
		return this.get(id, {
			token,
		})
	}

	getFeedbackLike(id, query, token) {
		return this.get(id + '/like', {
			query,
			token,
		})
	}
}

export default new UserService()
