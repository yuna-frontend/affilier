import ServiceModel from 'coeur/models/service';


class ItemService extends ServiceModel {

	static displayName = 'item'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/item/', 0)
	}

	getAllItem(data, token) {
		return this.get('', {
			query: {
				...data,
				available: true,
			},
			token,
		})
	}
}

export default new ItemService()
