import React from 'react';
import Loadable from 'react-loadable';
import LoaderPage from 'modules/pages/loader';

import MeManager from 'app/managers/me';


import {
	BrowserRouter,
	HashRouter,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';

const PrivateRoute = function({ component: Component, /* roles = [], */ ...rest }) {
	return (
		<Route {...rest} render={function(props) {
			const isAuthorized = !!MeManager.state.token

			return isAuthorized ? (
				<Component { ...props } />
			) : (
				<Redirect to="/login" />
			)
		}} />
	)
}

export default {
	BrowserRouter,
	HashRouter,
	Route() {
		return (
			<Switch>
				<Route exact path="/login" component={ Loadable({
					loader: () => import('modules/pages/login').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/" component={ Loadable({
					loader: () => import('modules/pages/dashboard').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/deals" component={ Loadable({
					loader: () => import('modules/pages/product.deals').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/custom" component={ Loadable({
					loader: () => import('modules/pages/custom.link').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/clicks" component={ Loadable({
					loader: () => import('modules/pages/clicks').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/dashboard" component={ Loadable({
					loader: () => import('modules/pages/dashboard').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<Route component={ Loadable({
					loader: () => import('modules/pages/four.o.four').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				
				{/* <Redirect from="*" to="/404" /> */}

			</Switch>
		)
	},
}
