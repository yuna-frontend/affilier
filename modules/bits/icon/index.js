import ConnectHelper from 'coeur/helpers/connect';
import CoreIconBit from 'coeur/modules/bits/icon';

import Colors from 'coeur/constants/color';

import Config from './config.json'

export default ConnectHelper(
	class IconBit extends CoreIconBit({
		Config,
		iconColor: Colors.black.primary,
	}) {}
)
