import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		width: 100,
		height: 100,
	},

	uploading: {
		opacity: .7,
		pointerEvents: 'none',
	},

	progress: {
		height: 4,
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
	},

	progressBar: {
		backgroundColor: Colors.green.primary,
	},
})
