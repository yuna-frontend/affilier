import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import FilterBoxLego from 'modules/legos/filter.box';

import Styles from './style';

import { isEmpty } from 'lodash';

const cache = {}


export default ConnectHelper(
	class BrowsePart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			if (oP.categoryId) {
				return [`query {
						categoryById (id: ${ oP.categoryId}) {
							id
							title
							category {
								id
							}
							categories {
								id
								title
							}
						}
					}`, {
					skip: !!cache[oP.categoryId],
				}, state.me.token]
			} else {
				return null
			}
		}

		static getDerivedStateFromProps(nP) {
			if (!nP.categoryId) {
				return null
			} else if (!isEmpty(nP.data)) {
				cache[nP.categoryId] = nP.data
			} else if (cache[nP.categoryId]) {
				// check for cache
			} else {
				return null
			}

			if (cache[nP.categoryId].categoryById.categories.length) {
				return {
					id: cache[nP.categoryId].categoryById.id,
					title: cache[nP.categoryId].categoryById.title,
					categories: cache[nP.categoryId].categoryById.categories,
				}
			} else if (cache[cache[nP.categoryId].categoryById.category.id]) {
				return {
					id: cache[cache[nP.categoryId].categoryById.category.id].categoryById.id,
					title: cache[cache[nP.categoryId].categoryById.category.id].categoryById.title,
					categories: cache[cache[nP.categoryId].categoryById.category.id].categoryById.categories,
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				id: null,
				title: 'No data',
				categories: [],
			})
		}

		onChange = val => {
			this.props.onChange &&
			this.props.onChange(val)
		}

		categoryRenderer = ({id, title}, i) => {
			return (
				<TouchableBit unflex key={i} style={Styles.item} onPress={this.onChange.bind(this, id)}>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						weight={id === this.props.categoryId ? 'medium' : 'normal'}
						style={[Styles.text, id === this.props.categoryId && Styles.isActive]}
					>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		viewOnLoading() {
			return !this.state.categories.length ? (
				<FilterBoxLego title="Browse by" isLoading />
			) : this.viewRenderer(true)
		}

		viewRenderer(isLoading = false) {
			return (
				<FilterBoxLego title="Browse by" style={ [isLoading && Styles.isLoading, this.props.style] }>
					<TouchableBit accessible={!!this.state.id} onPress={ this.onChange.bind(this, this.state.id) }>
						<GeomanistBit
							type={ GeomanistBit.TYPES.PARAGRAPH_2 }
							weight="medium"
							style={ [Styles.text, this.state.id !== null && this.state.id === this.props.categoryId && Styles.isActive] }
						>
							{ this.state.title }
						</GeomanistBit>
					</TouchableBit>
					{ this.state.categories.map(this.categoryRenderer) }
				</FilterBoxLego>
			)
		}

		view() {
			return (this.viewRenderer())
		}
	}
)
