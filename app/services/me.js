import ServiceModel from 'coeur/models/service';


class MeService extends ServiceModel {

	static displayName = 'me'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'user/')
	}

	authenticate(token) {
		return this.get('', {
			token,
		})
	}

	getBankAccount(id, token) {
		return this.get(`bank/${ id || ''}`, {
			token,
		})
	}

	updateBankAccount(id, data, token) {
		if(id) {
			return this.post('bank/' + id, {
				data: {
					...data,
				},
				token,
			})
		} else {
			return this.post('bank', {
				data: {
					...data,
				},
				token,
			})
		}
	}
}

export default new MeService()
