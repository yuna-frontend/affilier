import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import AuthenticationHelper from 'utils/helpers/authentication';

import BoxBit from 'modules/bits/box'
import TextInputBit from 'modules/bits/text.input'

import RowLego from 'modules/legos/row';
import SelectStatusLego from 'modules/legos/select.status'

import Styles from './style'


export default ConnectHelper(
	class RowStatusBroadcastLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
				note: PropTypes.string,
				description: PropTypes.string,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				isSuperAdmin: state.me.roles.indexOf('superadmin') > -1,
				isNotFulfillmentNorInventory: AuthenticationHelper.isWithout(state.me.roles, 'inventory', 'fulfillment'),
			}
		}

		constructor(p) {
			super(p, {
				status: p.status,
				note: p.note,
			})
		}

		onChangeStatus = status => {
			this.setState({
				status,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state)
			})

		}

		onChangeNote = (e, note) => {
			this.state.note = note

			this.props.onChange &&
			this.props.onChange(this.state)
		}

		view() {
			return (
				<RowLego
					key={ `${this.state.status}${this.props.note}` }
					data={[{
						title: 'Inventory Status*',
						children: (
							<BoxBit row>
								<SelectStatusLego isRequired
									type={ SelectStatusLego.TYPES.INVENTORIES }
									disabledStatuses={ this.disabledStatuses(this.props.status, this.props.isSuperAdmin, this.props.isNotFulfillmentNorInventory) }
									status={ this.state.status }
									onChange={ this.onChangeStatus }
								/>
							</BoxBit>
						),
					}, {
						title: 'Note',
						children: (
							<TextInputBit
								defaultValue={ this.props.note }
								onChange={ this.onChangeNote }
							/>
						),
						description: this.props.description,
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
