import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		marginBottom: Sizes.margin.default,
	},
	pagination: {
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
	},
	filter: {
		marginRight: Sizes.margin.default,
		height: 44,
	},
	wrapper: {
		flexGrow: 4,
	},
})
