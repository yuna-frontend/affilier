import React from 'react'
import StatefulModel from 'coeur/models/stateful'
import Connect from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import RadioBit from 'modules/bits/radio'
import TouchableBit from 'modules/bits/touchable'
import GeomanistBit from 'modules/bits/geomanist'

import FilterBoxLego from 'modules/legos/filter.box'

import { isEmpty } from 'lodash'

import Styles from './style'

export default Connect(
	class PricePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onChangePrice: PropTypes.func,
				price: PropTypes.string,
				inFilterModal: PropTypes.bool,
			}
		}

		static defaultProps = {
			price: '',
		}

		constructor(p) {
			super(p, {
				price: decodeURIComponent(p.price),
			})

			this.prices = {
				['0-250000']: { text: '0 - 250,000' },
				['250000-500000']: { text: '250,000 - 500,000'},
				['500000-1000000']: { text: '500,000 - 1,000,000' },
				['1000000+']: { text: '1,000,000+' },
			}
		}

		onSelectPrice = key => {
			this.setState({
				price: this.state.price !== key ? key : '',
			}, () => this.props.onChangePrice(encodeURIComponent(key)) )
		}

		view() {
			return (
				<FilterBoxLego title="Harga" style={ this.props.inFilterModal ? Styles.listCollapsible : {} }>
					<BoxBit unflex style={ Styles.prices }> { Object.keys(this.prices).map((key, i) => {
						return !this.props.inFilterModal ? (
							<TouchableBit
								key={ i }
								row
								onPress={ () => this.onSelectPrice(key) }
								style={ Styles.price }>
								<RadioBit
									dumb
									isActive={ key === decodeURIComponent(this.props.price) }
									onPress={ () => this.onSelectPrice(key) }
									style={ Styles.priceRadio }
								/>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.priceTitle }>
									{ this.prices[key].text }
								</GeomanistBit>
							</TouchableBit>
						) : (
							<TouchableBit
								centering
								key={ i }
								onPress={ () => this.onSelectPrice(key) }
								style={[
									Styles.priceBox,
									!isEmpty(this.state.price) &&
									key === this.state.price &&
									Styles.priceBoxActive,
								]}
							>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
									{ this.prices[key].text }
								</GeomanistBit>
							</TouchableBit>
						)
					}) } </BoxBit>
				</FilterBoxLego>
			)
		}
	}
)
