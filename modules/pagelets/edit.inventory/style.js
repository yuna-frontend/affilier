import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	rightButton: {
		marginLeft: 12,
	},

	box: {
		marginBottom: 16,
	},

	images: {
		flexWrap: 'wrap',
	},

	media: {
		width: 128,
		height: 128,
		margin: 6,
	},

	statuses: {
		minHeight: 240,
	},
})
