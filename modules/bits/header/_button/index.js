import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/bits/header';

import GeomanistBit from '../../geomanist'
import IconBit from '../../icon'
import TouchableBit from '../../touchable'

import Styles from './style'

export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		IconBit,
		TouchableBit,
	}) {
		static TYPES = {
			BACK: 'BACK_BUTTON',
			BLANK: 'BLANK',
			CLOSE: 'CLOSE_BUTTON',

			LOGO: 'LOGO',
			LOGO_YUNA: 'LOGO_YUNA',
			MENU: 'MENU_BUTTON',
			RESET: 'RESET_BUTTON',
			TEXT: 'TEXT',
		}

		getIconName(type) {
			switch(type) {
			case this.TYPES.MENU:
				return 'menu';
			case this.TYPES.LOGO:
				return 'yuna';
			case this.TYPES.LOGO_YUNA:
				return 'yuna-gram';
			case this.TYPES.RESET:
				return 'refresh';
			case this.TYPES.CLOSE:
				return 'close';
			default:
				return super.getIconName(type)
				// return 'tail-left';
			}
		}

		buttonRenderer() {
			switch(this.props.type) {

			case this.TYPES.TEXT:
				return (
					<GeomanistBit type={GeomanistBit.TYPES.PRIMARY_2} weight={'semibold'} style={ this.state.isLoading || !this.state.isActive ? Styles.textInactive : Styles.textActive }>
						{ this.state.data }
					</GeomanistBit>
				)
			case this.TYPES.LOGO:
			case this.TYPES.LOGO_YUNA:
				return (
					<IconBit
						name={ this.getIconName(this.props.type) }
						color={ this.props.data && this.props.data.color }
						size={ 32 }
					/>
				)
			default:
				return super.buttonRenderer()
			}
		}

	}
)
