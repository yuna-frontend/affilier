import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import MeManager from 'app/managers/me'

import PageContext from 'coeur/contexts/page'
import UtilitiesContext from 'coeur/contexts/utilities'

import AuthenticationHelper from 'utils/helpers/authentication';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import HeaderLoggedLego from 'modules/legos/header.logged';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import Styles from './style';


export default ConnectHelper(
	class PageAuthorizedComponent extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				roles: PropTypes.oneOfType([
					PropTypes.arrayOf(PropTypes.string),
					PropTypes.string,
				]),
				type: PropTypes.oneOf(PageBit.TYPES),
				scroll: PropTypes.bool,
				header: PropTypes.node,
				sidebar: PropTypes.node,
				footer: PropTypes.node,
				sticky: PropTypes.node,
				headerHeight: PropTypes.number,
				footerHeight: PropTypes.number,
				onAuthorized: PropTypes.func,
				paths: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string.isRequired,
					onPress: PropTypes.func,
				})), // use this to show breadcrumb in navigation
				children: PropTypes.func,
				contentContainerStyle: PropTypes.style,
				style: PropTypes.style,
			}
		}

		static stateToProps(state, oP) {
			const roles = oP.roles && Array.isArray(oP.roles) ? oP.roles : [oP.roles]

			return {
				me: state.me,
				token: state.me.token,
				isAuthorized: AuthenticationHelper.isAuthorized(state.me.roles, ...roles),
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP) {
			if(nP.isMenuVisible === undefined) {
				return null
			} else {
				return {
					isVisible: nP.isMenuVisible,
				}
			}
		}

		constructor(p) {
			super(p, {
				isVisible: false,
			})
		}

		componentDidMount() {
			if(!this.props.token) {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Session expired. You\'ve been logged out automatically.',
				})

				this.onLogout()
			} else {
				if(this.props.isAuthorized) {
					this.props.onAuthorized &&
					this.props.onAuthorized(this.props.token, this.props.me)
				}
			}
		}

		onToggleVisibility = () => {
			this.setState({
				isVisible: !this.state.isVisible,
			})
		}

		onLogout = () => {
			this.props.page.navigator.navigate('login')
			MeManager.logout()
		}

		viewOnError() {
			if(this.props.isCrashing && this.props.isCrashing instanceof Error) {
				if(this.props.isCrashing.message.match(/401/)) {
					this.props.utilities.notification.show({
						title: 'Oops…',
						message: 'Session expired. You\'ve been logged out automatically.',
					})

					this.onLogout()
				}
			}

			return (
				<PageBit
					type={ this.props.type }
					unflex={ this.props.unflex }
					scroll={ this.props.scroll }
					header={ this.props.header || (
						<BoxBit style={ Styles.innerContainerHeader }>
							<HeaderLoggedLego/>
						</BoxBit>
					) }
					footer={ this.props.footer }
					sticky={ this.props.sidebar || (
						<SidebarNavigationComponent />
					) }
					headerHeight={ this.props.headerHeight }
					footerHeight={ this.props.footerHeight }
					contentContainerStyle={[ Styles.content, this.props.contentContainerStyle ]}
					style={[ Styles.container, this.props.style ]}
				>
					<BoxBit centering>
						<BoxBit unflex row centering>
							<IconBit
								color={Colors.primary}
								name={'circle-info'}
								style={Styles.icon}
							/>
							<GeomanistBit style={Styles.title}>Sorry, you are not authorized to access this page.</GeomanistBit>
						</BoxBit>
						<GeomanistBit align="center" type={GeomanistBit.TYPES.PARAGRAPH_2} style={Styles.desc}>
							If you believe this is a mistake, please try to re-login.
							{'\n'}
							If problem persist, please contact your system administrator.
						</GeomanistBit>
						<BoxBit unflex>
							<ButtonBit
								title="RE-LOGIN"
								width={ButtonBit.TYPES.WIDTHS.FIT}
								onPress={this.onLogout}
							/>
						</BoxBit>
					</BoxBit>
				</PageBit>
			)
		}

		viewOnLoading() {
			return (
				<PageBit
					headerHeight={this.props.headerHeight || 56}
					type={this.props.type}
					unflex={this.props.unflex}
					scroll={this.props.scroll}
					header={this.props.header || (
						<BoxBit style={ Styles.innerContainerHeader }>
							<HeaderLoggedLego closer={ this.state.isVisible } onShowMenu={ this.onToggleVisibility } paths={ this.props.paths } />
						</BoxBit>
					)}
					footer={this.props.footer}
					sticky={this.props.sidebar || (
						<SidebarNavigationComponent mobileVisible={ this.state.isVisible } onClose={ this.onToggleVisibility }/>
					)}
					contentContainerStyle={[ Styles.content, this.props.contentContainerStyle ]}
					style={[Styles.container, this.props.style]}
				>
					<BoxBit centering>
						<LoaderBit />
					</BoxBit>
				</PageBit>
			)
		}

		view() {
			// eslint-disable-next-line no-nested-ternary
			return this.props.isAuthorized
				? typeof this.props.children === 'function'
					? (
						<PageBit
							headerHeight={ this.props.headerHeight || 56 }
							type={ this.props.type }
							unflex={ this.props.unflex }
							scroll={ this.props.scroll }
							header={(
								<BoxBit style={ Styles.innerContainerHeader }>
									<HeaderLoggedLego closer={ this.state.isVisible } onShowMenu={ this.onToggleVisibility } paths={ this.props.paths } />
									{this.props.header}
								</BoxBit>
							)}
							footer={ this.props.footer }
							sticky={ this.props.sidebar || (
								<SidebarNavigationComponent mobileVisible={ this.state.isVisible } onClose={ this.onToggleVisibility }/>
							) }
							contentContainerStyle={[ Styles.content, this.props.contentContainerStyle ]}
							style={[ Styles.container, this.props.style ]}
						>
							{ this.props.children() }
						</PageBit>
					) : false
				: this.viewOnError()
		}
	}
)
