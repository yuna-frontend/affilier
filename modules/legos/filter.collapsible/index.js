import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import CollapsibleBit from 'modules/bits/collapsible';
import IconBit from 'modules/bits/icon';
// import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
// import TouchableBit from 'modules/bits/touchable';

// import { without } from 'lodash';

import Styles from './style';


export default ConnectHelper(
	class FilterCollapsibleLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				disabled: PropTypes.bool,
				children: PropTypes.node,
				noTopBorder: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		headerRenderer = isActive => {
			return (
				<BoxBit accessible={!this.props.disabled} row style={this.props.disabled ? Styles.disabled : Styles.space}>
					<GeomanistBit
						type={GeomanistBit.TYPES.SECONDARY_2}
						weight="medium"
						style={Styles.title}
					>
						{ this.props.title }
					</GeomanistBit>
					{ this.props.disabled ? false : (
						<IconBit name={isActive ? 'substract' : 'expand'} color={Colors.black.palette(2, .4)} />
					) }
				</BoxBit>
			);
		}

		view() {
			return (
				<CollapsibleBit headerRenderer={this.headerRenderer} headerStyle={Styles.header} style={this.props.noTopBorder ? Styles.noBorder : undefined}>
					<BoxBit unflex style={Styles.content}>
						{ this.props.children }
					</BoxBit>
				</CollapsibleBit>
			)
		}
	}
)
