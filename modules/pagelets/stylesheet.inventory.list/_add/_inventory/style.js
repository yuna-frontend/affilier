import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	header: {
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	// content: {
	// 	paddingTop: Sizes.margin.default,
	// 	paddingBottom: Sizes.margin.default,
	// },

	loader: {
		marginTop: 8,
		color: Colors.black.palette(2, .6),
	},

	padderLeft16: {
		paddingLeft: 16,
		flexWrap: 'wrap',
	},

	dark70: {
		color: Colors.black.palette(1, .7),
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},

	wrapper: {
		flexWrap: 'wrap',
	},

	itemsContainer: {
		padding: 16,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 4,
		marginTop: 16,
	},
	selectContainer: {
		padding: 16,
		borderWidth: 1,
		borderColor: Colors.primary,
		borderRadius: 4,
		marginTop: 16,
	},
	capital: {
		textTransform: 'capitalize',
	},
	image: {
		width: 96,
		height: 96,
		marginRight: 16,
	},
	discount: {
		color: Colors.red.palette(4),
	},
	retail: {
		marginRight: 8,
		color: Colors.black.palette(1, .7),
	},
	empty: {
		paddingTop: 24,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},
})
