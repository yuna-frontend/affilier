import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({

	select: {
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.white.primary,
		borderRadius: 2,
	},

	list: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},

	isHovered: {
		backgroundColor: Colors.primary,
		justifyContent: 'space-between',
	},

	white: {
		color: Colors.white.primary,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},

	searchBar: {
		maxHeight: 168,
	},

	desc: {
		height: 24,
		alignItems: 'center',
	},

	loader: {
		paddingLeft: 16,
	},
})
