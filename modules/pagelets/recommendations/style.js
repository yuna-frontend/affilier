import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	featuredImage: {
		width: 350,
		height: 481,
	},

	gridBox: {
		width: 450,
		height: 482,
	},

	contentContainer: {
		backgroundColor: 'white',
		width: '95%',
	},

	loadingContainer: {
		marginTop: 56,
		marginBottom: 56,
	},

	icon: {
		position: 'absolute',
		right: 24,
		top: 24,
	},

	title: {
		marginLeft: 'auto',
		marginRight: 'auto',
		marginTop: 'auto',
		marginBottom: 'auto',
		textAlign: 'center',
	},

	grid: {
		marginTop: 'auto',
	},

	noPadding: {
		padding: 0,
	},
	
	imageTarget: {
		width: '100%',
	},

	caption: {
		width: '100%',
		paddingTop: 8,
		paddingBottom: 8,
		backgroundColor: Colors.grey.palette(2),
	},

	itemCount: {
		alignItems: 'flex-end',
		justifyContent: 'center',
		padding: 16,
	},

	croppedItem: {
		alignItems: 'center',
		marginBottom: Sizes.margin.default,
	},

	croppedImage: {
		marginRight: 12,
		width: 73,
		height: 73,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	recommendWrapper: {
		justifyContent: 'space-between',
		maxHeight: '100%',
	},

	recommendLeftSide: {
		width: 300,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderRightWidth: 1,
		borderColor: Colors.black.palette(2, .1),
		overflowY: 'auto',
	},

	methodTab: {
		paddingTop: 0,
		paddingLeft: 15,
		paddingRight: 15,
		justifyContent: 'center',
	},

	containerScroll: {
		paddingTop: Sizes.margin.default,
		overflowX: 'hidden',
		overflowY: 'auto',
	},

	categoryTabs: {
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .1),
		paddingTop: Sizes.margin.default,
	},

	categoryTab: {
		paddingTop: 0,
	},

	containerHeaderItems: {
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	filterTab: {
		border: `1px solid ${Colors.black.primary}`,
		borderRadius: 4,
		paddingTop: 10,
		paddingBottom: 10,
		paddingRight: 8,
		paddingLeft: Sizes.margin.default,
		marginRight: 8,
		justifyContent: 'center',
	},

	filterTabUseButton: {
		border: 0,
		padding: 0,
	},

	filterTabTitleContainer: {
		alignItems: 'center',
	},

	filterTabTitle: {
		textTransform: 'capitalize',
	},

	filterTabItems: {
		position: 'absolute',
		top: '102%',
		left: 0,
		zIndex: 1,
		backgroundColor: Colors.white.primary,
		width: 150,
		maxHeight: 280,
		overflowY: 'auto',
		padding: 8,
	},

	filterTabItemsFit: {
		width: 'auto',
	},

	filterTabItem: {
		marginTop: 10,
		marginBottom: 10,
	},

	filterTabItemAlign: {
		alignItems: 'center',
	},

	filterTabItemSubs: {
		paddingLeft: 12,
	},
	
	filterTabItemSub: {
		alignItems: 'center',
		marginTop: 10,
	},

	filterTabItemColor: {
		width: 24,
		height: 24,
		borderRadius: 12,
		borderWidth: 2,
		borderColor: Colors.black.palette(2, .1),
		padding: 3,
	},

	filterTabItemColorGroup: {
		borderRadius: 0,
	},

	filterTabItemColorSelected: {
		borderColor: Colors.black.palette(2, .75),
		'& $filterTabItemColorNoBG': {
			borderColor: Colors.black.palette(2, .75),
		},
	},

	filterTabItemSelected: {
		'& $filterTabItemColor': {
			borderColor: Colors.black.palette(2, .75),
		},
	},

	filterTabItemColorBG: {
		borderRadius: '100%',
		overflow: 'hidden',
	},

	filterTabItemColorNoBG: {
		borderRadius: '100%',
		overflow: 'hidden',
		borderWidth: 2,
		borderColor: Colors.black.palette(2, .1),
	},

	filterTabItemTitle: {
		marginLeft: 8,
		textTransform: 'capitalize',
	},

	filterTabItemCheckbox: {
		width: 24,
		height: 24,
		borderRadius: 4,
		borderWidth: 2,
	},

	filterTabItemPriceInput: {
		width: 150,
	},

	containerCardItems: {
		marginTop: Sizes.margin.default,
		flexWrap: 'wrap',
	},

	cardItem: {
		width: '25%',
	},

	mr8: {
		marginRight: 8,
	},

	mb8: {
		marginBottom: 8,
	},

	mb16: {
		marginBottom: Sizes.margin.default,
	},

	prl16: {
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	prl32: {
		paddingRight: Sizes.margin.default * 2,
		paddingLeft: Sizes.margin.default * 2,
	},

	'@media screen and (min-width: 1600px)': {
		cardItem: {
			width: '20%',
		},
	},

	'@media screen and (max-width: 1024px)': {
		recommendLeftSide: {
			width: 240,
		},

		containerHeaderItems: {
			flexWrap: 'wrap',
		},

		itemsAdded: {
			width: '100%',
			marginTop: Sizes.margin.default,
		},

		cardItem: {
			width: '50%',
		},
	},

	'@media screen and (max-width: 720px)': {
		cardItem: {
			width: '50%',
		},

		prl32: {
			paddingRight: Sizes.margin.thick,
			paddingLeft: Sizes.margin.thick,
		},
	},
})
