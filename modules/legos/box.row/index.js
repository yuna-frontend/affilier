import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxLego from '../box'
import RowLego from '../row'


export default ConnectHelper(
	class BoxRowLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				borderless: PropTypes.bool,
				unflex: PropTypes.bool,
				small: PropTypes.bool,
				title: PropTypes.string.isRequired,
				header: PropTypes.node,
				data: PropTypes.array,
				children: PropTypes.node,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
				useSwitch: PropTypes.bool,
				switchValue: PropTypes.bool,
				switchOnChange: PropTypes.func,
			}
		}

		static defaultProps = {
			data: [],
		}

		rowRenderer = (data, i) => {
			return data === null ? false : (
				<RowLego key={ i } { ...data } />
			)
		}

		view() {
			return (
				<BoxLego
					unflex={ this.props.unflex }
					borderless={ this.props.borderless }
					small={ this.props.small }
					title={ this.props.title }
					header={ this.props.header }
					children={ this.props.children || this.props.data.map(this.rowRenderer) }
					style={ this.props.style }
					contentContainerStyle={ this.props.contentContainerStyle }
					useSwitch={ this.props.useSwitch }
					switchValue={ this.props.switchValue }
					switchOnChange={ this.props.switchOnChange }
				/>
			)
		}
	}
)
