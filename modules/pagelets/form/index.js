import ConnectHelper from 'coeur/helpers/connect';
import CoreFormPagelet from 'coeur/modules/pagelets/form';

import BoxBit from 'modules/bits/box';
import InputValidatedBit from 'modules/bits/input.validated';


export default ConnectHelper(
	class FormPagelet extends CoreFormPagelet({
		BoxBit,
		InputValidatedBit,
	}) {}
)
