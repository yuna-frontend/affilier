/* eslint-disable no-nested-ternary */
import React from 'react'
import Connect from 'coeur/helpers/connect'
import QueryStatefulModel from 'coeur/models/query.stateful'

import BoxBit from 'modules/bits/box'
import CheckboxBit from 'modules/bits/checkbox'
import LoaderBit from 'modules/bits/loader'
import GeomanistBit from 'modules/bits/geomanist'
import RadioBit from 'modules/bits/radio'
import SelectionBit from 'modules/bits/selection'
import TouchableBit from 'modules/bits/touchable'

import FilterBoxLego from 'modules/legos/filter.box'

import CategoryModal from './_category.modal'

import { isEmpty, capitalize } from 'lodash'

import Styles from  './style'

let categoriesCache = null
// let lastCategoryIds = []
let categoriesLevel2 = []

const setDataCategory = ({categoriesParent, categoriesChildren}, ids) => {
	if(!!ids.length) {
		const dataFromParent = categoriesParent.find(p => {
			return p.id === ids[ ids.indexOf(p.id) ]
		})

		if(!!dataFromParent) {
			categoriesLevel2 = dataFromParent.categories.filter(c => {
				return (
					// Hide Set and Man in clothing
					// Hide accessory in accessory
					c.title !== 'Set' &&
					c.title !== 'Man' &&
					c.title !== 'accessory'
				)
			})
			return {
				isLoading: false,
				categoriesLevel: 2,
				level3Selected: false,
				categoriesLevelData: {
					1: {
						selected: {
							id: dataFromParent.id,
							title: capitalize(dataFromParent.title),
						},
						categories: categoriesParent,
					},
					2: {
						selected: {
							id: null,
							title: null,
						},
						categories: categoriesLevel2,
					},
					3: {
						// selected from selectedIds props
						categories: [],
					},
				},
			}
		}

		const dataFromChildrens = categoriesChildren.find(c => {
			return ids.indexOf(c.id) > -1
		})

		if(!!dataFromChildrens) {
			if(!categoriesLevel2.length) {
				categoriesLevel2 = categoriesParent.find(c => {
					return !!dataFromChildrens.category.category
						? c.id === dataFromChildrens.category.category.id
						: c.id === dataFromChildrens.category.id
				}).categories.filter(c => {
					return (
						// Hide Set and Man in clothing
						// Hide accessory in accessory
						c.title !== 'Set' &&
						c.title !== 'Man' &&
						c.title !== 'accessory'
					)
				})
			}

			if(!!dataFromChildrens.categories.length) {
				return {
					isLoading: false,
					categoriesLevel: 3,
					level3Selected: false,
					categoriesLevelData: {
						1: {
							selected: {
								id: dataFromChildrens.category.id,
								title: capitalize(dataFromChildrens.category.title),
							},
							categories: categoriesParent,
						},
						2: {
							selected: {
								id: dataFromChildrens.id,
								title: capitalize(dataFromChildrens.title),
							},
							categories: categoriesLevel2,
						},
						3: {
							// selected from selectedIds props
							categories: dataFromChildrens.categories,
						},
					},
				}
			} else {
				const categoriesLevel3 = categoriesChildren.find(c => {
					return c.id === dataFromChildrens.category.id
				})
				return {
					isLoading: false,
					categoriesLevel: 3,
					level3Selected: true,
					categoriesLevelData: {
						1: {
							selected: {
								id: categoriesLevel3.category.id,
								title: capitalize(categoriesLevel3.category.title),
							},
							categories: categoriesParent,
						},
						2: {
							selected: {
								id: dataFromChildrens.category.id,
								title: capitalize(dataFromChildrens.category.title),
							},
							categories: categoriesLevel2,
						},
						3: {
							// selected from selectedIds props
							categories: categoriesLevel3.categories,
						},
					},
				}
			}
		}

		// if categoryIds in query manually added and it's wrong id
		return false
	}

	return {
		isLoading: false,
		categoriesLevelData: {
			1: {
				selected: { id: null, title: null },
				categories: categoriesParent,
			},
			2: {
				selected: { id: null, title: null },
				categories: [],
			},
			3: {
				selected: { id: null, title: null },
				categories: [],
			},
		},
		categoriesLevel: 1,
		level3Selected: false,
	}
}

export default Connect(
	class CategoryPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				onChangeCategoryIds: PropTypes.func,
				mobile: PropTypes.bool,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				categoriesLevel: 1,
				categoriesLevelData: {},
				level3Selected: false,
			})
		}

		static propsToQuery(state) {
			return [`query {
				categoriesList(filter: {
					categoryId: {
						isNull: false
					}
				}) {
					id
					title
					category {
						id
						title
						category {
							id
							title
						}
					}
					categories {
						id
						title
						category {
							id
							title
						}
					}
				}
				parentCategoriesList: categoriesList(filter: {
					categoryId: {
						isNull: true
					}
				}) {
					id
					title
					categories {
						id
						title
						category {
							id
							title
						}
					}
				}
			}`, {
				skip: !!categoriesCache,
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				categoriesCache = {
					categoriesParent: nP.data.parentCategoriesList,
					categoriesChildren: nP.data.categoriesList,
				}

				return setDataCategory(categoriesCache, nP.categoryIds)
			} else if(!!categoriesCache) {
				return setDataCategory(categoriesCache, nP.categoryIds)
			} else {
				return {
					data: [],
					isLoading: false,
				}
			}
		}

		onPressListItem = (id, multiple) => {
			this.props.onChangeCategoryIds(id, multiple)

			// for desktop to update height collapsible immediately after category change
			// lastCategoryIds = this.props.categoryIds
		}

		listMenuRenderer = ({
			id,
			title,
			category,
		}, isAllowedMultiple, level, selectedId, i) => {
			const RadioORCheckbox = isAllowedMultiple ? CheckboxBit : RadioBit

			let isActive = id === selectedId

			if(level === 3) {
				isActive = this.props.categoryIds.indexOf(id) > -1
			}

			const isActiveAll = !!category
				? this.props.categoryIds.indexOf(category.id) > -1
				: level === 1 && this.state.categoriesLevel > 1 ? false : true

			return (
				<React.Fragment key={ i }>
					{ i === 0 && (
						<TouchableBit row unflex style={ Styles.menuListItem } onPress={ () => this.onPressListItem( !!category ? category.id : [], false ) }>
							<RadioBit
								key={ this.props.categoryIds.toString() }
								isActive={ isActiveAll }
								onPress={() => this.onPressListItem(category.id, false)}
								style={[Styles.menuListItemDot, !isActiveAll ? Styles.menuListItemDotInactive : {}]}
							/>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.menuListItemTitle }>
								Semua { !!category ? category.title : 'Kategori' }
							</GeomanistBit>
						</TouchableBit>
					) }
					<TouchableBit row unflex style={ Styles.menuListItem } onPress={ () => this.onPressListItem(id, level === 3 ? this.state.level3Selected : isAllowedMultiple) }>
						<RadioORCheckbox
							key={ this.props.categoryIds.toString() }
							isActive={ isActive }
							onPress={() => this.onPressListItem(id, isAllowedMultiple)}
							style={[Styles.menuListItemDot, !isActive ? Styles.menuListItemDotInactive : {}]}
						/>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.menuListItemTitle }>
							{ title }
						</GeomanistBit>
					</TouchableBit>
				</React.Fragment>
			)
		}

		categoryModalRenderer = handler => {
			return (
				<CategoryModal
					categories={ categoriesCache }
					categoryIds={ this.props.categoryIds }
					onChangeCategoryIds={ this.props.onChangeCategoryIds }
					handler={ handler }
					getSelectionTitle={ ids => this.getSelectionTitle(ids) }
				/>
			)
		}

		getSelectionTitle = ids => {
			const {categoriesParent, categoriesChildren} = categoriesCache

			if(!ids.length) return 'Semua Kategori'

			const multipleTitle = data => {
				const collections = []
				data.forEach(id => {
					collections.push(categoriesChildren.find(c => c.id === id).title)
				})
				return collections.toString().replace(/,/g, ', ')
			}

			// multiple category
			if(ids.length > 1) return multipleTitle(ids)

			// single category
			return !!categoriesParent.find(p => p.id === ids[0])
				? categoriesParent.find(p => p.id === ids[0]).title
				: categoriesChildren.find(c => c.id === ids[0]).title
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={this.props.mobile && [Styles.selection, Styles.selectionInLoading]}>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			if(this.state.isLoading) return this.viewOnLoading()

			if(!categoriesCache) return false

			if(!this.props.mobile) {
				const { categoriesLevelData, categoriesLevel } = this.state

				return (<>
					<BoxBit>
						<FilterBoxLego title="Kategori" key={ categoriesLevelData[1].selected.id }>
							<BoxBit unflex style={ Styles.listMenus } key={ categoriesLevelData[1].selected.id }>
								{ categoriesLevelData[1].categories.map((item, index) => {
									return this.listMenuRenderer(item, false, 1, categoriesLevelData[1].selected.id, index)
								}) }
							</BoxBit>
						</FilterBoxLego>
					</BoxBit>

					{ categoriesLevel > 1 && (
						<BoxBit>
							<FilterBoxLego title={ categoriesLevelData[1].selected.title } key={ `${categoriesLevelData[1].selected.id}${categoriesLevelData[2].selected.id}` } isActive={ !categoriesLevelData[2].selected.id && categoriesLevelData[1].selected.id }>
								<BoxBit unflex style={ Styles.listMenus }>
									{ categoriesLevelData[2].categories.map((item, index) => {
										return this.listMenuRenderer(item, false, 2, categoriesLevelData[2].selected.id, index)
									}) }
								</BoxBit>
							</FilterBoxLego>
						</BoxBit>
					) }

					{ categoriesLevel === 3 && (
						<BoxBit>
							<FilterBoxLego title={ categoriesLevelData[2].selected.title } key={ categoriesLevelData[2].selected.id } isActive>
								<BoxBit unflex style={ Styles.listMenus }>
									{ categoriesLevelData[3].categories.map((item, index) => {
										return this.listMenuRenderer(item, true, 3, null, index)
									}) }
								</BoxBit>
							</FilterBoxLego>
						</BoxBit>
					) }
				</>)
			}

			return (
				<SelectionBit
					unflex={ false }
					value={ this.getSelectionTitle(this.props.categoryIds) }
					menuRenderer={ handler => this.categoryModalRenderer(handler) }
					style={ Styles.selection }
					options={[]}
				/>
			)
		}
	}
)
