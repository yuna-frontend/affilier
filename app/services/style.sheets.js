import ServiceModel from 'coeur/models/service';


class StyleSheetsService extends ServiceModel {

	static displayName = 'stylesheets'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	getInventoryDetail = this._createSmartGetters((ids, params) => {
		return this.get('stylesheet/inventory/' + ids.join(','), {
			token: params.token,
		})
	}, (id, params) => {
		return this.get('stylesheet/inventory/' + id, {
			token: params.token,
		})
	}, function(id, [ids, datas]) {
		if (ids.length) {
			return datas.find(d => d.id === id)
		} else {
			return datas[0]
		}
	})

	updateInventory(id, data, token) {
		return this.post('stylesheet/inventory/' + id, {
			data,
			token,
		})
	}

	// getPrintInvoice = this._createSmartGetters((ids, params) => {
	// 	return this.get('stylesheet/' + ids.join(',') + '/print/invoice', {
	// 		token: params.token,
	// 	})
	// }, (id, params) => {
	// 	return this.get('stylesheet/' + id + '/print/invoice', {
	// 		token: params.token,
	// 	})
	// }, (id, [ids, datas]) => {
	// 	return datas
	// })

	getNotes(ids, token) {
		return this.get('stylesheet/' + ids + '/print/note', {
			token,
		})
	}

	getAddresses(ids, token) {
		return this.get('stylesheet/' + ids + '/print/address', {
			token,
		})
	}

	getStyleSheets(data, token) {
		return this.get('stylesheet', {
			token,
			query: data,
		})
	}

	getStyleSheetShallow(id, token) {
		return this.get('stylesheet/' + id, {
			token,
			query: {
				shallow: true,
			},
		})
	}

	getStyleSheetDetailed(id, token) {
		return this.get('stylesheet/' + id, {
			token,
			query: {
				detailed: true,
			},
		})
	}

	getStyleSheetDetail(id, token) {
		return this.get('stylesheet/' + id, {
			token,
		})
	}

	onPutInventoryToStylesheet(id, data, token) {
		return this.post('stylesheet/' + id + '/inventory', {
			data,
			token,
		})
	}

	onRemoveInventoryFromStylesheet(id, token) {
		return this.delete('stylesheet/inventory/' + id, {
			query: {
				force: true,
			},
			token,
		})
	}

	onRefundInventory(id, status, note, token) {
		return this.post('stylesheet/inventory/' + id + '/refund', {
			data: {
				status,
				note,
			},
			token,
		})
	}

	getStyleSheetVariant(data, token) {
		return this.get('variant', {
			query: {
				search: data.search,
				exclude_matchbox: true,
			},
			token,
		})
	}

	onSave(data, token) {
		return this.post('stylesheet/' + data.id + '/note', {
			data: {
				data,
			},
			token,
		})
	}

	assignStylist(id, stylistId, token) {
		return this.post('stylesheet/' + id + '/assign', {
			data: {
				stylist_id: stylistId,
			},
			token,
		})
	}

	duplicate(id, sourceId, consent, token) {
		return this.post('stylesheet/' + id + '/duplicate', {
			data: {
				source_stylesheet_id: sourceId,
				consent,
			},
			token,
		})
	}

	setStylesheetStatus(id, status, note, token) {
		return this.post('stylesheet/' + id + '/status', {
			data: {
				code: status,
				note,
			},
			token,
		})
	}

	publish(id, status, stylistNote, data, token) {
		return this.post('stylesheet/' + id + '/status', {
			data: {
				...data,
				code: status,
				stylist_note: stylistNote,
			},
			token,
		})
	}

	update(id, data, note, token) {
		return this.post('stylesheet/' + id, {
			data: {
				...data,
				changelog: note,
			},
			token,
		})
	}
}

export default new StyleSheetsService()
