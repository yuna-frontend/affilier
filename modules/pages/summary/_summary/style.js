import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	lineChart: {
		minHeight: 500,
		width: '100%',
		backgroundColor: Colors.red.palette(1),
		marginTop: 8,
		marginBottom: 8,
	},
})
