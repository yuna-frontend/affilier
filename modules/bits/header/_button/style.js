import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	textActive: {
		color: Colors.black.primary,
	},
	textInactive: {
		color: Colors.black.palette(1, .3),
	},
})
