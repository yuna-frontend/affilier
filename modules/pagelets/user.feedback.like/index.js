import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import TimeHelper from 'coeur/helpers/time';

import UserService from 'app/services/user.new';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader'
import NavTextBit from 'modules/bits/nav.text';
import GeomanistBit from 'modules/bits/geomanist';

import ProductLego from 'modules/legos/product'
import TableLego from 'modules/legos/table';
import TabLego from 'modules/legos/tab'

import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';
import { isEmpty } from 'lodash'

let cache = {}

export default ConnectHelper(
	class UserFeedbackLikePagelet extends PromiseStatefulModel {
		static TYPES = {
			VARIANT: 'VARIANT',
			STYLECARD: 'STYLECARD',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				tabIndex: 0,
				data: {
					variant: {
						likes: [],
						dislikes: [],
					},
					stylecard: {
						likes: [],
						dislikes: [],
					},
				},
			})
		}

		tabs = Object.keys(this.TYPES).map(title => title.toLowerCase())


		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP, pS) {
			if(this.state.tabIndex !== pS.tabIndex) {
				this.getData()
			}
		}

		getData = () => {
			if(!isEmpty(cache[this.tabs[this.state.tabIndex]])) {
				this.setState({
					data: {
						...this.state.data,
						[this.tabs[this.state.tabIndex]]: cache[this.tabs[this.state.tabIndex]],
					},
				})
			} else {
				this.setState({
					isLoading: true,
				}, () => {
					UserService.getFeedbackLike(this.props.id, { type: this.tabs[this.state.tabIndex] }, this.props.token)
						.then(res => {
							cache = {
								...cache,
								[this.tabs[this.state.tabIndex]]: res,
							}
	
							this.setState({
								isLoading: false,
								data: {
									...this.state.data,
									[this.tabs[this.state.tabIndex]]: res,
								},
							})
						})
						.catch(err => this.onError(err))
				})
			}
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					message: err ? err.detail || err.message || err : 'Oops… Something went wrong. Please try again later.',
				})
			})
		}

		onNavigateToProduct = (pId, vId) => {
			this.props.page.navigator.navigate('product/' + pId + '/variant/' + vId)
		}

		onChangeTab = i => {
			this.setState({
				tabIndex: i,
			})
		}

		likeRenderer = data => {
			if(this.state.tabIndex === 0) {
				const {
					id,
					product_id: pId,
					product: title,
					...others
				} = data

				return {
					data: [{
						title: '',
						children: this.state.isLoading ? <LoaderBit /> : (
							<ProductLego title={title} { ...others } onPress={ this.onNavigateToProduct.bind(this, pId, id) } />
						),
					}],
				}
			} else {
				return {
					data: [{
						title: '',
						children: this.state.isLoading ? <LoaderBit /> : (
							<React.Fragment>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{ data.title }
								</GeomanistBit>
								<NavTextBit
									title={ `Order Date: ${ data.created_at ? TimeHelper.format(data.created_at, 'DD MMM YYYY') : '-' }` }
									link={ 'View Stylecard' }
									href={ `stylecard/${data.id}` }
									textStyle={ Styles.note }
								/>
							</React.Fragment>
						),
					}],
				}
			}
		}

		dislikeRenderer = data => {
			if(this.state.tabIndex === 0) {
				const {
					id,
					product_id: pId,
					product: title,
					...others
				} = data

				return {
					data: [{
						title: '',
						children: this.state.isLoading ? <LoaderBit /> : (
							<ProductLego title={title} { ...others } onPress={ this.onNavigateToProduct.bind(this, pId, id) } />
						),
					}, {
						title: '',
						children: <GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={Styles.reason}>{ !!data.reason ? data.reason : '-' }</GeomanistBit>,
					}],
				}
			} else {
				return {
					data: [{
						title: '',
						children: this.state.isLoading ? <LoaderBit /> : (
							<React.Fragment>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{ data.title }
								</GeomanistBit>
								<NavTextBit
									title={ `Order Date: ${ data.created_at ? TimeHelper.format(data.created_at, 'DD MMM YYYY') : '-' }` }
									link={ 'View Stylecard' }
									href={ `stylecard/${data.id}` }
									textStyle={ Styles.note }
								/>
							</React.Fragment>
						),
					}, {
						title: '',
						children: <GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} style={Styles.reason}>{ !!data.reason ? data.reason : '-' }</GeomanistBit>,
					}],
				}
			}
		}

		viewOnError() {
			return !this.props.isSuperAdmin ? (
				<NotFoundPagelet
					title="Sorry"
					description="You are not authorized to view this page."
				/>
			) : (
				<NotFoundPagelet
					title="There's an error"
					description="Contact tech team to look for"
				/>
			)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return (
				<BoxBit row style={[Styles.container, this.props.style]}>
					<BoxBit>
						<TabLego capsule
							activeIndex={ this.state.tabIndex }
							tabs={ this.tabs.map((tab, i) => {
								return {
									title: tab.toUpperCase(),
									onPress: this.onChangeTab.bind(this, i),
								}
							}) }
						/>
						{ this.state.tabIndex === 0 ? (
							<BoxBit row style={Styles.content}>
								<TableLego
									isLoading={ this.state.isLoading }
									headers={[{ title: 'Liked', width: 1 }]}
									rows={ this.state.data[this.tabs[this.state.tabIndex]].likes.map(this.likeRenderer) }
								/>
								<TableLego
									isLoading={ this.state.isLoading }
									headers={[{
										title: 'Disliked', width: 1,
									}, {
										title: 'Reason', width: .5,
									}]}
									rows={ this.state.data[this.tabs[this.state.tabIndex]].dislikes.map(this.dislikeRenderer) }
								/>
							</BoxBit>
						) : (
							<BoxBit row style={ Styles.content }>
								<TableLego
									isLoading={ this.state.isLoading }
									headers={[{ title: 'Liked' }]}
									rows={ this.state.data[this.tabs[this.state.tabIndex]].likes.map(this.likeRenderer)}
								/>
								<TableLego
									isLoading={ this.state.isLoading }
									headers={[{
										title: 'Disliked', width: 1,
									}, {
										title: 'Reason', width: .5,
									}]}
									rows={ this.state.data[this.tabs[this.state.tabIndex]].dislikes.map(this.dislikeRenderer)}
								/>
							</BoxBit>
						)}

					</BoxBit>
				</BoxBit>
			)
		}
	}
)
