import {
	// difference,
	intersection,
	xor,
} from 'lodash'

export default {
	isAuthorized(userRoles, ...roles) {
		return !!intersection(userRoles, [...roles, 'superadmin']).length
	},
	isWithout(userRoles, ...roles) {
		return [...roles, 'superadmin'].findIndex(role => userRoles.indexOf(role) > -1) === -1
	},
	// isOnly(userRoles, ...roles) {
	// 	return !difference(userRoles, [...roles, 'admin']).length
	// },
	isExact(userRoles, ...roles) {
		return !xor(userRoles, [...roles, 'admin']).length
	},
}
