import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { orderBy, isEmpty } from 'lodash';

let cache = null


export default ConnectHelper(
	class SelectBrandLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				brandId: PropTypes.number,
				hasPickupPoint: PropTypes.bool,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				disabled: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache ? [oP.hasPickupPoint ? `
				query {
					brandAddressesList(filter: {
						isPickupPoint: {
							equalTo: true
						}
					}) {
						brand {
							id
							title
						}
					}
				}
			` : `
				query {
					brandsList {
						id
						title
					}
				}
			`, {
				fetchPolicy: _cache ? 'cache-first' : 'no-cache',
			}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				cache = nP.data
			} else if(cache) {
				// check for cache
			} else {
				return null
			}

			return {
				brands: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(orderBy((nP.hasPickupPoint ? cache.brandAddressesList : cache.brandsList).map(b => {
					return nP.hasPickupPoint ? {
						selected: b.brand.id === nP.brandId,
						key: b.brand.id,
						title: b.brand.title,
					} : {
						selected: b.id === nP.brandId,
						key: b.id,
						title: b.title,
					}
				}), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			cache: true,
			unflex: false,
			placeholder: 'Filter by brand',
			emptyTitle: 'All Brands',
		}

		constructor(p) {
			super(p, {
				brands: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit unflex={ this.props.unflex } disabled
					options={[]}
					placeholder={'Loading ...'}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit
					key={ this.props.brandId }
					disabled={ this.props.disabled }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={this.state.brands}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
					onChange={this.props.onChange}
				/>
			)
		}
	}
)
