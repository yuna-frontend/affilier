import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import InputValidatedBit from 'modules/bits/input.validated'
import IconBit from 'modules/bits/icon'
import TouchableBit from 'modules/bits/touchable'
import TextAreaBit from 'modules/bits/text.area'

import PageAuthorizedComponent from 'modules/components/page.authorized'

import BoxRowLego from 'modules/legos/box.row'

import ModalPagelet from 'modules/pagelets/modal'

import { isEmpty } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class CustomLinkPage extends PageModel {
		static routeName = 'custom.link'

		constructor(p) {
			super(p, {
				isLoading: false,
				// url: 'https://helloyuna.io/product/variant/',
				// url: '',
			})
		}

		static stateToProps(state) {
			return {
				firstName: state.me.firstName,
				userId: state.me.id,
			}
		}

		getUrl = () => {
			let { url } = {...this.state}

			url = url + '?tr_source=affiliate&ref=' + this.props.userId

			const tags = [this.state.tag1, this.state.tag2, this.state.tag3, this.state.tag4, this.state.tag5]

			for(let i = 1; i <= tags.length; i++) {
				if(!isEmpty(tags[i - 1])) {
					url = url + '&tag' + i + '=' + tags[i - 1]
				}
			}

			// this.setState({
			// 	url: url,
			// })

			this.onShowModal(url)
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onCopyLink = url => {
			navigator.clipboard.writeText(url)

			this.props.utilities.notification.show({
				message: 'Copied to clipboard',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})
		}

		headerRenderer = () => {
			return (
				<BoxBit centering unflex row style={[Styles.modalHeader, {height: 45}]}>
					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1}>
						Tautan
					</GeomanistBit>
					<TouchableBit unflex centering enlargeHitSlop onPress={this.onClose}>
						<IconBit
							name="close"
							size={20}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		onShowModal = url => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPagelet unclosable
						header={this.headerRenderer()}
						style={Styles.container}
						containerStyle={Styles.modalContainer}
						pageStyle={Styles.modalPage}
						contentContainerStyle={Styles.contentContainer}
						// onClose={this.onClose}
					>
						<BoxBit unflex centering>
							<TextAreaBit readonly
								defaultValue={url}
								style={Styles.url}
							/>
							<ButtonBit
								title="Salin"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
								state={ ButtonBit.TYPES.STATES.NORMAL }
								onPress= {this.onCopyLink.bind(this, url) }
								style={[Styles.button, Styles.btnModal]}
							/>
						</BoxBit>
					</ModalPagelet>
				),
			})
		}

		onChange = (key, value) => {
			this.setState({
				[key]: value,
			})
			// this.getUrl()
		}

		view() {
			// this.log(this.state.url)
			return (
				<PageAuthorizedComponent
					paths={[{ title: 'Tautan Khusus' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<BoxBit unflex>
								<BoxRowLego
									title="Tautan Khusus"
									data={[{
										data: [{
											title: 'Masukkan Variant ID Produk',
											children: (
												<InputValidatedBit
													defaultValue="https://helloyuna.io/product/variant/"
													type={InputValidatedBit.TYPES.INPUT}
													placeholder="Example: https://helloyuna.io/products/variants/:id"
													onChange={this.onChange.bind(this, 'url')}
												/>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 1',
											children: (
												<BoxBit unflex>
													<InputValidatedBit
														type={InputValidatedBit.TYPES.INPUT}
														placeholder="Example: Skirts"
														onChange={this.onChange.bind(this, 'tag1')}
													/>
												</BoxBit>
											),
										}, {
											title: 'Tag Link 2',
											children: (
												<BoxBit unflex>
													<InputValidatedBit
														type={InputValidatedBit.TYPES.INPUT}
														placeholder="Example: Feeds"
														onChange={this.onChange.bind(this, 'tag2')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 3',
											children: (
												<BoxBit unflex>
													<InputValidatedBit
														type={InputValidatedBit.TYPES.INPUT}
														placeholder="Example: Feeds"
														onChange={this.onChange.bind(this, 'tag3')}
													/>
												</BoxBit>
											),
										}, {
											title: 'Tag Link 4',
											children: (
												<BoxBit unflex>
													<InputValidatedBit
														type={InputValidatedBit.TYPES.INPUT}
														placeholder="Example: Feeds"
														onChange={this.onChange.bind(this, 'tag4')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: '',
											children: (
												<ButtonBit
													title="Salin Tautan"
													weight="medium"
													type={ ButtonBit.TYPES.SHAPES.PROGRESS }
													width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
													state={ ButtonBit.TYPES.STATES.NORMAL }
													onPress= {this.getUrl }
													style={Styles.button}
												/>
											),
											description: 'Catatan:\n1. Komisi untuk produk yang berhasil di-checkout melalui tautan khusus ini, akan dihitung berdasarkan tarif yang tertera di bagian Penawaran Produk.\n2. Pembuatan tautan khusus hanya berlaku untuk 1 produk / variant saja dalam sekali waktu.',
										}],
									}]}
								/>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
