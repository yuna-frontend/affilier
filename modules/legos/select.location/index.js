import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import CommonHelper from 'coeur/helpers/common';

import LocationService from 'app/services/location';

import SelectionBit from 'modules/bits/selection';
import InputValidatedBit from 'modules/bits/input.validated';

import { isEmpty } from 'lodash';

const cache = {
	PROVINCE: {},
	CITY: {},
	SUBURB: {},
}


export default ConnectHelper(
	class SelectLocationLego extends PromiseStatefulModel {

		static TYPES = {
			PROVINCE: 'PROVINCE',
			CITY: 'CITY',
			SUBURB: 'SUBURB',
		}

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES),
				id: PropTypes.number,
				placeholder: PropTypes.string,
				onChange: PropTypes.func.isRequired,
				value: PropTypes.number,
				disabled: PropTypes.bool,

				form: PropTypes.boolean,
				title: PropTypes.string,

				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id && !cache[oP.type][oP.id] ? Promise.resolve().then(() => {
				switch(oP.type) {
				case 'PROVINCE':
				default:
					return LocationService.getProvinces()
				case 'CITY':
					return LocationService.getCities(oP.id)
				case 'SUBURB':
					return LocationService.getSuburbs(oP.id)
				}
			}) : null
		}

		static getDerivedStateFromProps(nP) {
			const id = nP.id

			if(!isEmpty(nP.data)) {
				cache[nP.type][id] = nP.data
			} else if(cache[nP.type] && cache[nP.type][id]) {
				// check for cache
			} else {
				return null
			}

			return {
				locations: cache[nP.type][id].map(l => {
					return {
						key: l.id,
						title: l.alias ? `${l.title} (${l.alias})` : l.title,
						selected: l.id === nP.value,
						onPress: nP.onChange,
					}
				}),
			}
		}

		constructor(p) {
			super(p, {
				locations: [],
			}, [])
		}

		viewOnLoading() {
			return  this.props.form ? (
				<InputValidatedBit disabled
					type="SELECTION"
					title={this.props.title}
					placeholder={'Loading…'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			) : (
				<SelectionBit unflex={false} disabled
					options={[]}
					placeholder={'Loading…'}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		getPlaceholder() {
			switch( this.props.type ) {
			case this.TYPES.PROVINCE:
			default:
				return 'Select province'
			case this.TYPES.CITY:
				return 'Select city'
			case this.TYPES.SUBURB:
				return 'Select district'
			}
		}

		view() {
			return this.props.form ? (
				<InputValidatedBit
					key={ this.props.value }
					type="SELECTION"
					disabled={ !this.state.locations.length || this.props.disabled }
					title={ this.props.title }
					placeholder={ this.props.placeholder || this.getPlaceholder() }
					options={ this.state.locations }
					// do not use on change, we use onPress on options instead
					// onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			) : (
				<SelectionBit
					key={ this.props.value }
					disabled={ !this.state.locations.length || this.props.disabled }
					unflex={false}
					placeholder={ this.props.placeholder || this.getPlaceholder() }
					options={ this.state.locations }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
					onChange={ this.props.onChange }
				/>
			)
		}
	}
)
