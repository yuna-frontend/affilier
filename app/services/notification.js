import ServiceModel from 'coeur/models/service';


class NotificationService extends ServiceModel {

	static displayName = 'notification'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/notification/', 0)
	}
	
	addNotification = (data, token) => {
		return this.post('', {
			data,
			token,
		})
	}

	updateNotification = (id, data, token) => {
		return this.post(`${id}/`, {
			data,
			token,
		})
	}

	getNotification = (id, token) => {
		return this.get(`${id}/`, {
			token,
		})
	}

	getNotifications = (token, query) => {
		return this.get('', {
			token,
			query,
		})
	}

	
}

export default new NotificationService()
