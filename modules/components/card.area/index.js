import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import AddressManager from 'app/managers/address';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
// import BoxLoadingBit from 'modules/bits/box.loading';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';


import Styles from './style';


export default ConnectHelper(
	class CardAddressComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				data: PropTypes.object,
				selectionMode: PropTypes.bool,
				isActive: PropTypes.bool,
				isDefaultAddress: PropTypes.bool,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {}, [
			])
		}
		viewOnLoading() {
			return (
				<BoxBit type={BoxBit.TYPES.ALL_THICK} unflex centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (
				<TouchableBit key={this.props.isActive} unflex onPress={ this.props.onPress } style={[Styles.container, this.props.style]}>
					<BoxBit unflex row style={Styles.header}>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} weight="medium" style={[Styles.darkGrey, Styles.capital]}>
								Province: {this.props.data.province.toLowerCase() }
							</GeomanistBit>
						</BoxBit>
						{ this.props.selectionMode ? (
							<RadioBit isActive={this.props.isActive} onPress={ this.onPress } />
						) : (
							<IconBit
								name="edit"
								size={20}
								color={Colors.primary}
							/>
						) }
					</BoxBit>
					<BoxBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.darkGrey60, Styles.capital]}>
							District: {this.props.data.dist.toLowerCase()}
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.darkGrey60, Styles.capital]}>
							Sub District: {this.props.data.sub_dist.toLowerCase()}
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.darkGrey60, Styles.capital]}>
							City: { this.props.data.city_county.toLowerCase() }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.darkGrey60, Styles.capital]}>
							Postal: {this.props.data.zip_code.toLowerCase() }
						</GeomanistBit>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
