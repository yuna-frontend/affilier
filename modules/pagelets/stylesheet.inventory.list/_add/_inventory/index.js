import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import VariantService from 'app/services/variant'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
// import ScrollViewBit from 'modules/bits/scroll.view';

import SelectionInventoryLego from 'modules/legos/selection.inventory'

import Styles from './style';

let __id = 1

export default ConnectHelper(
	class InventoryPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				keyword: PropTypes.string,
				onSelect: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isUpdating: false,
				inventories: [],
				selectedId: -1,
			})
		}

		componentDidMount() {
			if(this.props.keyword) {
				this.getData()
			}
		}

		componentDidUpdate(pP) {
			if(pP.keyword !== this.props.keyword) {
				this.getData()
			}
		}

		getData = () => {
			__id++

			const _id = __id

			// eslint-disable-next-line react/no-did-update-set-state
			this.setState({
				isUpdating: true,
			}, () => {
				VariantService.search(this.props.keyword, 0, 12, true, this.props.token).then(res => {
					if (_id === __id) {
						this.setState({
							isUpdating: false,
							inventories: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong, please try again later',
					})

					this.setState({
						isUpdating: false,
						inventories: [],
					})
				})
			})
		}

		onSelect = id => {
			this.setState({
				selectedId: id,
			})

			this.props.onSelect &&
			this.props.onSelect(id)
		}

		inventoryRenderer = (variant, i) => {
			return (
				<SelectionInventoryLego dumb selectable key={`${i}|${variant.id}`}
					id={ variant.id }
					borderless={ i === this.state.inventories.length - 1 }
					data={ variant }
					isSelected={ variant.id === this.state.selectedId }
					onSelect={ this.onSelect }
				/>
			);
		}

		loadingRenderer() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.loader}>
						Fetching your search detail...
					</GeomanistBit>
				</BoxBit>
			);
		}

		view() {
			return this.state.isUpdating ? this.loadingRenderer() : (
				<BoxBit style={ Styles.content }>
					{ this.state.inventories.map(this.inventoryRenderer) }
				</BoxBit>
			)
		}
	}
)
