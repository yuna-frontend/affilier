import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import SelectionBit from 'modules/bits/selection';


export default ConnectHelper(
	class SelectTypeLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				role: PropTypes.string,
				disabledRoles: PropTypes.arrayOf(PropTypes.string),
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
				values: PropTypes.arrayOf(PropTypes.shape({
					key: PropTypes.oneOf(
						PropTypes.string,
						PropTypes.number,
					),
					title: PropTypes.string,
				})),
				selected: PropTypes.string,
			}
		}

		static defaultProps = {
			unflex: false,
			placeholder: '',
			emptyTitle: '',
			disabledRoles: [],
		}

		constructor(p) {
			super(p, {
				roles: (!p.isRequired ? [{
					key: '',
					title: p.emptyTitle,
				}] : []).concat(p.values),
			}, [])
		}

		view() {
			return (
				<SelectionBit unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={ this.state.roles }
					onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
