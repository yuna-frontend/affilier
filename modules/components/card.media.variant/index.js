import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStateful from 'coeur/models/promise.stateful';

import MeManager from 'app/managers/me';

import VariantService from 'app/services/variant';

import ImageBit from 'modules/bits/image';


export default ConnectHelper(
	class CardMediaVariantComponent extends PromiseStateful {
		static propsToPromise(state, p) {
			return VariantService.getMediaById(p.id || undefined, state.me.token)
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				data: PropTypes.object,

				unflex: PropTypes.bool,
				accessible: PropTypes.bool,

				source: PropTypes.image,
				transform: PropTypes.object,
				broken: PropTypes.bool,
				resizeMode: PropTypes.oneOf(ImageBit.TYPES),
				loader: PropTypes.bool,
				overlay: PropTypes.bool,
				alt: PropTypes.string,
				onLayout: PropTypes.func,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {}, [])
		}

		view() {
			return (
				<ImageBit
					source={this.props.isLoading ? undefined : this.props.data}
					unflex={this.props.unflex}
					accessible={this.props.accessible}
					transform={this.props.transform}
					broken={this.props.broken}
					resizeMode={this.props.resizeMode}
					loader={this.props.loader}
					overlay={this.props.overlay}
					alt={this.props.alt}
					onLayout={this.props.onLayout}
					style={this.props.style}
				/>
			)
		}
	}
)
