import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	title: {
		marginTop: 4,
		marginBottom: 4,
	},

	container: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		borderRadius: 2,
		minHeight: 44,
		backgroundColor: Colors.white.primary,
	},

	values: {
		paddingTop: 8,
		paddingLeft: 8,
		paddingRight: 32,
		paddingBottom: 4,
		flexWrap: 'wrap',
		zIndex: 1,
	},

	value: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		borderRadius: 2,
		alignItems: 'center',
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 4,
		paddingBottom: 4,
		marginRight: 4,
		marginBottom: 4,
	},

	selection: {
		borderWidth: 0,
		position: 'absolute',
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		height: 'auto',
		backgroundColor: Colors.transparent,
	},

	icon: {
		marginLeft: 4,
	},

})
