import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';
import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

import { isEmpty } from 'lodash'

export default ConnectHelper(
	class GetterInventoryComponent extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				children: PropTypes.func,
			}
		}

		static propsToQuery(state, oP) {
			if (oP.id) {
				return [`query {
					inventoryById(id: ${oP.id}){
						note
						price
						status
						rack
						brand {
							id
						}
						brandAddress {
							id
							title
						}
						status
					}
				}`,
				// THIS QUERY IS BREAKING
				// inventoryStatuses(orderBy: CREATED_AT_DESC, first: 1){
				// id
				// note
				// code
				// createdAt
				{
					fetchPolicy: 'no-cache',
				}, state.me.token]
			} else {
				return null
			}
		}

		static getDerivedStateFromProps(nP) {
			if (!nP.id) {
				return null
			} else if (isEmpty(nP.data)) {
				return null
			} else if (nP.isLoading) {
				return null
			}

			return {
				id: nP.id,
				brandId: nP.data.inventoryById.brand.id,
				brandAddressId: nP.data.inventoryById.brandAddress.id,
				brandAddressTitle: nP.data.inventoryById.brandAddress.title,
				note: nP.data.inventoryById.note,
				price: nP.data.inventoryById.price,
				rack: nP.data.inventoryById.rack,
				status: nP.data.inventoryById.status,
			}
		}

		constructor(p) {
			super(p, {
				id: null,
				brandId: null,
				brandAddressId: null,
				brandAddressTitle: undefined,
				note: undefined,
				price: undefined,
				rack: undefined,
				status: null,
			})
		}

		viewOnError() {
			return (
				<BoxBit centering>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_2}
						style={{ color: Colors.black.palette(1, .9) }}
					>
						There's an error with load the data
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (this.props.children(this.state))
		}
	}
)
