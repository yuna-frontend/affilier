import ServiceModel from 'coeur/models/service';


class CouponService extends ServiceModel {

	static displayName = 'coupon'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/coupon/', 0)
	}

	getCoupons(data, token) {
		return this.get('', {
			token,
			query: data,
		})
	}

	getDetail(id, query, token) {
		return this.get(id, {
			query,
			token,
		})
	}

	getDetailOfType({ id, type }, token) {
		return this.get(id + '/' + type, {
			token,
		})
	}

	addItemToCoupon(id, { type, typeId }, token) {
		return this.post(id + '/' + type + '/' + typeId, {
			token,
		})
	}

	removeItemFromCoupon(id, { type, typeId }, token) {
		return this.delete(id + '/' + type + '/' + typeId, {
			token,
		})
	}

	getCoupon(id, token) {
		return this.get(id, {
			token,
		})
	}

	filterCoupons({
		title,
		offset,
		count,
	}, token) {
		return this.get('', {
			query: {
				title,
				offset,
				count,
			},
			token,
		})
	}

	createCoupon(coupon, token) {
		return this.post('', {
			data: {
				data: coupon,
			},
			token,
		})
	}

	updateCoupon(id, coupon, token) {
		return this.post(id, {
			data: {
				data: coupon,
			},
			token,
		})
	}
}

export default new CouponService()
