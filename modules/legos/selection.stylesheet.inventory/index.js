import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import FormatHelper from 'coeur/helpers/format';

import AuthenticationHelper from 'utils/helpers/authentication';
import StylesheetService from 'app/services/style.sheets';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import LinkBit from 'modules/bits/link';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusSmallLego from 'modules/legos/badge.status.small';
import HintLego from 'modules/legos/hint';

import Styles from './style';

import { isEmpty, includes } from 'lodash'


export default ConnectHelper(
	class SelectionStylesheetInventoryLego extends PromiseStatefulModel {

		static TYPES = {
			BOOKED: 'BOOKED',
			PACKED: 'PACKED',
			INVENTORY_EXCEPTION: 'INVENTORY_EXCEPTION',
			INVENTORY_INVALID: 'INVENTORY_INVALID',
			// INVENTORY_LOCKED: 'INVENTORY_LOCKED',
			REQUESTING: 'REQUESTING',
			PURCHASING: 'PURCHASING',
			PURCHASED: 'PURCHASED',
			PURCHASE_CANCELLED: 'PURCHASE_CANCELLED',
			PURCHASE_COMPLETED: 'PURCHASE_COMPLETED',
			PURCHASE_EXCEPTION: 'PURCHASE_EXCEPTION',
			REQUEST_APPROVED: 'REQUEST_APPROVED',
			REQUEST_REJECTED: 'REQUEST_REJECTED',
			REQUEST_COMPLETED: 'REQUEST_COMPLETED',
			REQUEST_EXCEPTION: 'REQUEST_EXCEPTION',
			INVALID: 'INVALID',
			LOCKED: 'LOCKED',
			REPLACED: 'REPLACED',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				editable: PropTypes.bool,
				removable: PropTypes.bool,
				borderless: PropTypes.bool,
				data: PropTypes.shape({
					id: PropTypes.id,
					inventory_id: PropTypes.id,
					purchase_request_id: PropTypes.id,
					is_locked: PropTypes.bool,
					image: PropTypes.object,
					brand: PropTypes.string,
					title: PropTypes.string,
					category: PropTypes.string,
					size: PropTypes.string,
					color: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
					status: PropTypes.oneOf(this.TYPES),
					changelog: PropTypes.string,
					stylesheet_inventory_id: PropTypes.id,
				}),
				onRemove: PropTypes.func,
				onEditPurchaseRequest: PropTypes.func,
				onDuplicatePurchaseRequest: PropTypes.func,
				onEditStylesheetInventory: PropTypes.func,
				onDataLoaded: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				isSuperAdmin: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? StylesheetService.getInventoryDetail(oP.id, {
				token: state.me.token,
			}).then(data => {
				oP.onDataLoaded &&
				oP.onDataLoaded(data)

				return data
			}) : null
		}

		constructor(p) {
			super(p, {

			})
		}

		editables = [
			this.TYPES.REQUESTING,
			this.TYPES.LOCKED,
		]

		duplicatables = [
			this.TYPES.PURCHASE_CANCELLED,
			this.TYPES.PURCHASE_EXCEPTION,
			this.TYPES.REQUEST_REJECTED,
			this.TYPES.REQUEST_EXCEPTION,
		]

		removables = [
			this.TYPES.BOOKED,
			this.TYPES.REQUESTING,
			this.TYPES.PURCHASE_CANCELLED,
			this.TYPES.PURCHASE_EXCEPTION,
			this.TYPES.REQUEST_REJECTED,
			this.TYPES.REQUEST_EXCEPTION,
			this.TYPES.INVENTORY_EXCEPTION,
			this.TYPES.INVENTORY_INVALID,
			this.TYPES.INVALID,
			this.TYPES.LOCKED,
		]

		editable() {
			return this.props.editable &&
				!this.props.data.inventory_id &&
				includes(this.editables, this.props.data.status)
		}

		duplicatable() {
			return this.props.editable &&
				!this.props.data.inventory_id &&
				includes(this.duplicatables, this.props.data.status)
		}

		removable() {
			return this.props.removable &&
				!this.props.data.is_locked &&
				includes(this.removables, this.props.data.status)
		}

		note() {
			switch(this.props.data.status) {
			case this.TYPES.BOOKED:
				return 'Inventory successfuly booked. Hoooray…'
			case this.TYPES.PACKED:
				return 'Inventory packed into stylesheet. You may not remove this inventory.'
			case this.TYPES.INVENTORY_EXCEPTION:
				return `Please change this inventory.${ this.props.data.changelog ? ` Note: ${ this.props.data.changelog }` : '' }`
			case this.TYPES.REQUESTING:
				return 'Requesting. Be patient or contact fulfillment team.'
			case this.TYPES.PURCHASING:
				return 'Purchasing is on going.'
			case this.TYPES.PURCHASED:
				return 'Request purchased. Waiting for delivery of the product.'
			case this.TYPES.PURCHASE_CANCELLED:
				return `Purchase cancelled. Please change this request.${ this.props.data.changelog ? ` Note: ${ this.props.data.changelog }` : '' }`
			case this.TYPES.PURCHASE_COMPLETED:
				return 'Purchase has been made and should be converted into inventory soon.'
			case this.TYPES.PURCHASE_EXCEPTION:
				return `Purchase exceptioned. Please change this item.${ this.props.data.changelog ? ` Note: ${ this.props.data.changelog }` : '' }`
			case this.TYPES.REQUEST_APPROVED:
				return 'Request approved. Purchasing soon.'
			case this.TYPES.REQUEST_REJECTED:
				return `Request rejected. Please change this item.${ this.props.data.changelog ? ` Note: ${ this.props.data.changelog }` : '' }`
			case this.TYPES.REQUEST_COMPLETED:
				return 'Request done. You should have seen an inventory by now.'
			case this.TYPES.REQUEST_EXCEPTION:
				return `Request exceptioned. Please change this item.${ this.props.data.changelog ? ` Note: ${ this.props.data.changelog }` : '' }`
			case this.TYPES.LOCKED:
				return 'Request locked. It will not be bought. You still can delete or edit this request.'
			case this.TYPES.INVENTORY_LOCKED:
				return 'Inventory / Request locked. You cannot change this inventory'
			case this.TYPES.REPLACED:
				return 'Request replaced. It is moved into a new stylesheet.'
			case this.TYPES.INVALID:
			default:
				return 'Request invalid. If you are currently styling, please change this inventory.'
			}
		}

		statusStyle() {
			switch(this.props.data.status) {
			case this.TYPES.INVENTORY_EXCEPTION:
			case this.TYPES.PURCHASE_CANCELLED:
			case this.TYPES.PURCHASE_EXCEPTION:
			case this.TYPES.REQUEST_REJECTED:
			case this.TYPES.REQUEST_EXCEPTION:
				return Styles.exceptioned
			default:
				return false
			}
		}

		onEditInventory = () => {
			this.props.onEditStylesheetInventory &&
			this.props.onEditStylesheetInventory(this.props.id, this.props.refresh)
		}

		onEdit = () => {
			this.props.onEditPurchaseRequest &&
			this.props.onEditPurchaseRequest(this.props.data.purchase_request_id, this.props.refresh)
		}

		onDuplicate = () => {
			this.props.onDuplicatePurchaseRequest &&
			this.props.onDuplicatePurchaseRequest(this.props.data.purchase_request_id)
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={ [Styles.container, Styles.empty, this.props.style ] }>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return isEmpty(this.props.data) ? this.viewOnLoading() : (
				<BoxBit unflex row style={[Styles.container, this.props.borderless && Styles.borderless, this.statusStyle(), this.props.style]}>
					<ImageBit overlay
						resizeMode={ ImageBit.TYPES.COVER }
						broken={ !this.props.data.image }
						source={ this.props.data.image || undefined }
						style={[ Styles.image, this.props.data.is_locked && Styles.locked ]}
					/>
					<BoxBit style={ this.props.data.is_locked ? Styles.locked : undefined }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={Styles.text}>
							{ this.props.data.inventory_id ? `#IN-${ this.props.data.inventory_id }` : `#PR-${this.props.data.purchase_request_id}` }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.text}>
							{ this.props.data.brand || '-' } – { this.props.data.title || '-' }
						</GeomanistBit>
						{ this.props.data.price === this.props.data.retail_price ? (
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.price}>
								IDR { FormatHelper.currencyFormat(this.props.data.price) || '-' }
							</GeomanistBit>
						) : (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.discounted}>
									IDR { FormatHelper.currencyFormat(this.props.data.price) || '-' }
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.retail}>
									IDR { FormatHelper.currencyFormat(this.props.data.retail_price) || '-' }
								</GeomanistBit>
							</BoxBit>
						) }
						<BoxBit />
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={Styles.note}>
							{ this.props.data.category || '-' } – Color: { this.props.data.color } – Size: { this.props.data.size }
						</GeomanistBit>
						{ this.props.data.inventory_id ? false : (
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={Styles.note}>
								<LinkBit underline href={ this.props.data.url } target={ LinkBit.TYPES.BLANK }>{ this.props.data.url ? `${ this.props.data.url.substr(0, 20) }…` : false }</LinkBit>
							</GeomanistBit>
						) }
					</BoxBit>
					<BoxBit unflex>
						<BoxBit row>
							<BoxBit />
							<BoxBit unflex row>
								{ this.props.isSuperAdmin ? (
									<TouchableBit unflex onPress={ this.onEditInventory } style={ Styles.trash }>
										<IconBit
											name="edit-2"
											size={20}
											color={Colors.primary}
										/>
									</TouchableBit>
								) : false }
								{ this.editable() ? (
									<TouchableBit unflex onPress={ this.onEdit } style={Styles.trash}>
										<IconBit
											name="edit"
											size={20}
											color={Colors.primary}
										/>
									</TouchableBit>
								) : false }
								{ this.duplicatable() ? (
									<TouchableBit unflex onPress={ this.onDuplicate } style={Styles.trash}>
										<IconBit
											name="copy"
											size={16}
											color={Colors.primary}
										/>
									</TouchableBit>
								) : false }
								{ this.removable() ? (
									<TouchableBit unflex onPress={ this.props.onRemove } style={Styles.trash}>
										<IconBit
											name="trash"
											size={20}
											color={Colors.primary}
										/>
									</TouchableBit>
								) : false }
								{ this.props.data.is_locked ? (
									<BoxBit unflex style={Styles.lock}>
										<IconBit
											name="lock"
											size={16}
											color={Colors.primary}
										/>
									</BoxBit>
								) : false }
								<HintLego size={20} title={ this.note() } />
							</BoxBit>
						</BoxBit>
						<BoxBit unflex row centering style={ this.props.data.is_locked || this.props.data.stylesheet_inventory_id ? Styles.locked : undefined }>
							<BadgeStatusSmallLego status={ this.props.data.status } style={ Styles.status } />
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
