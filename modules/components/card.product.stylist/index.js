import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/connected.stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';
import IconBit from 'modules/bits/icon'


import ColorLego from 'modules/legos/color';

// import CardComponentModel from 'coeur/models/components/card';
// import CardMediaComponent from 'modules/components/card.media';

import Styles from './style'

import { isEmpty, uniqBy } from 'lodash'


export default ConnectHelper(
	class CardProductStylistComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				width: PropTypes.number,
				brand: PropTypes.string,
				title: PropTypes.string,
				description: PropTypes.string,
				variants: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					asset: PropTypes.object,
					prices: PropTypes.arrayOf(PropTypes.number),
					colors: PropTypes.arrayOf(PropTypes.object),
					size: PropTypes.object,
				})),
				categoryId: PropTypes.id,
				// images: PropTypes.arrayOf(PropTypes.string),
				// price: PropTypes.number,
				// purchasePrice: PropTypes.number,
				sizes: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
				})),
				colors: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					hex: PropTypes.string,
					image: PropTypes.string,
				}))),
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(isEmpty(nS.sizeByColorKey)) {
				return {
					sizeByColorKey: nP.variants.reduce((sum, variant) => {
						const colorKey = variant.colors.map(c => c.id).join('|')
						return {
							...sum,
							[colorKey]: uniqBy([
								...(sum[colorKey] || []),
								variant.size,
							], size => size.id),
						}
					}, {}),
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				sizeByColorKey: {},
				selectedColorIndex: 0,
				selectedSizeIndex: 0,
			})
		}

		onPress = variant => {
			this.props.onPress &&
			this.props.onPress(this.props.id, variant.id, this.props.categoryId, this.props.brand, this.props.title, this.props.description, this.props.colors, this.props.variants)
		}

		onSelectColorIndex = index => {
			this.setState({
				selectedColorIndex: index,
			})
		}

		onSelectSizeIndex = index => {
			this.setState({
				selectedSizeIndex: index,
			})
		}

		colorRenderer = (colors, index) => {
			return (
				<TouchableBit unflex onPress={ this.onSelectColorIndex.bind(this, index) } key={index} style={[Styles.color, index === this.state.selectedColorIndex && Styles.colorActive]}>
					<ColorLego radius={12} size={24} colors={colors} />
				</TouchableBit>
			)
		}

		sizeRenderer = (size, index) => {
			const isActive = false
			// const isActive = index === this.state.selectedSizeIndex
			return (
				<BoxBit centering unflex key={index} style={[Styles.size, isActive && Styles.sizeActive]}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_2} weight={ 'medium' } style={isActive ? Styles.sizeTitleActive : Styles.sizeTitle}>
						{ size.title }
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			const selectedColorIdKey = this.props.colors[this.state.selectedColorIndex].map(c => c.id).join('|')
			const sizes = this.state.sizeByColorKey[selectedColorIdKey]
			const variant = this.props.variants.find(v => {
				return v.colors.map(c => c.id).join('|') === selectedColorIdKey
			})

			return (
				<TouchableBit unflex onPress={ this.onPress.bind(this, variant) } style={[Styles.container, {
					width: this.props.width,
				}, this.props.style]}>
					{
						this.props.haveConsignmentStock &&
						(
							<IconBit
								style={Styles.icon}
								size={24}
								name={'shipments'}
							/>
						)
					}
					<BoxImageBit unflex key={ variant && variant.asset && variant.asset.url }
						resizeMode={ BoxImageBit.TYPES.COVER }
						broken={ !variant.asset }
						source={ variant.asset }
						style={ [Styles.image, {
							height: this.props.width * 1.33,
						}] }
					>
						<BoxBit />
						<BoxBit unflex style={Styles.sizes}>
							{ sizes.map(this.sizeRenderer) }
						</BoxBit>
					</BoxImageBit>
					<BoxBit unflex style={Styles.content}>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.aline, Styles.brand]}>
							{ this.props.brand }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.aline, Styles.title]}>
							{ this.props.title }
						</GeomanistBit>
						{ variant.prices[0] !== variant.prices[1] ? (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_2} weight="normal" style={Styles.discount}>IDR { FormatHelper.currencyFormat(variant.prices[0]) }</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_2} weight="normal" style={Styles.retail}>IDR { FormatHelper.currencyFormat(variant.prices[1]) }</GeomanistBit>
							</BoxBit>
						) : (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_2} weight="normal" style={Styles.price}>IDR { FormatHelper.currencyFormat(variant.prices[0]) }</GeomanistBit>
								<BoxBit unflex style={Styles.empty} />
							</BoxBit>
						) }
						<BoxBit unflex row style={Styles.colors}>
							{ this.props.colors.map(this.colorRenderer) }
						</BoxBit>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
