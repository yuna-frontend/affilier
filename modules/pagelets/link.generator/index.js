import React from 'react'
import PromiseSatefulModel from 'coeur/models/promise.stateful'
import ConnectHelper from 'coeur/helpers/connect'

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import InputValidatedBit from 'modules/bits/input.validated'
import TouchableBit from 'modules/bits/touchable'
import TextAreaBit from 'modules/bits/text.area'

import ModalPagelet from 'modules/pagelets/modal'

import { isEmpty } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class LinkGeneratorPagelet extends PromiseSatefulModel {
		

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				userId: PropTypes.id,
				title: PropTypes.string,
				brand: PropTypes.string,
				onCopyLink: PropTypes.func,
				onClose: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				baseLink: 'https://helloyuna.io/product/variant/' + p.id + '?ref=' + p.userId,
				customLink: 'https://helloyuna.io/product/variant/' + p.id + '?ref=' + p.userId,
				tags: null,
			})
		}

		desc = 'Catatan:\n1. Komisi untuk produk yang berhasil di-checkout melalui tautan khusus ini, akan dihitung berdasarkan tarif yang tertera di bagian Penawaran Produk.\n2. Pembuatan tautan khusus hanya berlaku untuk 1 produk / variant saja dalam sekali waktu.'

		onChange = (key, val) => {
			if(isEmpty(this.state[key]) && !isEmpty(val)) {
				this.setState(state => ({
					...state,
					[key]: val,
					customLink: encodeURI(state.customLink + '?tags=' + val),
				}))
			} else if (!isEmpty(this.state[key]) && !isEmpty(val)) {
				this.setState(state => ({
					...state,
					[key]: val,
					customLink: encodeURI(state.baseLink + '?tags=' + val),
				}))
			} else {
				this.setState(state => ({
					...state,
					[key]: val,
					customLink: state.baseLink,
				}))
			}
			console.log(this.state.tags)
		}

		onCopyLink = () => {
			this.props.onCopyLink &&
			this.props.onCopyLink(this.state.customLink)
		}

		headerRenderer = () => {
			return (
				<BoxBit centering unflex row style={[Styles.modalHeader, {height: 45}]}>
					<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1}>
						Tautan • {this.props.brand} - {this.props.title}
					</GeomanistBit>
					<TouchableBit unflex centering enlargeHitSlop onPress={this.props.onClose}>
						<IconBit name="close" size={20} />
					</TouchableBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<ModalPagelet
					header={this.headerRenderer()}
					style={Styles.container}
					containerStyle={Styles.modalContainer}
					pageStyle={Styles.modalPage}
					contentContainerStyle={Styles.contentContainer}
				>
					<BoxBit unflex>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Tautan
							</GeomanistBit>
							<TextAreaBit readonly
								value={this.state.customLink}
								style={Styles.url}
							/>
						</BoxBit>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Tags
							</GeomanistBit>
							<InputValidatedBit
								placeholder="Contoh: Sale, IG"
								onChange={this.onChange.bind(this, 'tags')}
							/>
							<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} weight="normal">
								Gunakan koma untuk memisahkan tag
							</GeomanistBit>
						</BoxBit>

						<BoxBit unflex style={Styles.pt16}>
							<ButtonBit
								title="Salin"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								state={ ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onCopyLink }
								style={ Styles.button }
							/>
						</BoxBit>

						<BoxBit style={Styles.pt16}>
							<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} weight="normal">
								{ this.desc }
							</GeomanistBit>
						</BoxBit>
					</BoxBit>
				</ModalPagelet>
			)
		}
	}
)
