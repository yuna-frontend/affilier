import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputValidatedBit from 'modules/bits/input.validated'

import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class RowProductLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				valie: PropTypes.string,
			}
		}

		static defaultProps = {
			title: 'Product',
		}

		constructor(p) {
			super(p, {
			})
		}

		onChange = (val) => {
			// this.setState({
			// 	title: val,
			// })

			this.props.onChange &&
			this.props.onChange(val)
		}

		view() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						children: (
							<InputValidatedBit required
								placeholder="Enter product name"
								value={ this.props.value }
								onChange={ this.onChange }
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
