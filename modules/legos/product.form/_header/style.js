import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	text: {
		color: Colors.black.palette(2, .6),
	},
	clearIcon: {
		justifyContent: 'center'
	},
	image: {
		height: 36,
		width: 36,
		marginRight: 8,
	},

	header: {
		alignItems: 'center',
	},

	flex: {
		flexGrow: 2,
		justifyContent: 'center',
	},

	table: {
		paddingTop: Sizes.margin.default,
	},

	noBorder: {
		borderBottomWidth: 0,
	},

	input: {
		height: 32,
		paddingRight: 6,
		paddingLeft: 8,
	},

	button: {
		marginTop: -6,
		marginBottom: -6,
		marginLeft: Sizes.margin.default,
	},

})
