import DictionaryModel from 'coeur/models/dictionary';

import AddressRecord from 'app/records/address';

import AddressService from 'app/services/address';
// import ShipperService from 'app/services/shipper';

import MeManager from './me';


class AddressManager extends DictionaryModel {

	static displayName = 'addresses'

	constructor() {
		super(AddressRecord, {
			expiricy: 1000 * 60 * 60,
		})
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }

	dataGetter(id) {
		return AddressService.getAddress(id, MeManager.state.token).then(address => {
			return this.updateLater(address)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return AddressService.getAddresses(ids, MeManager.state.token).then(addresses => {
			return this.updateLater(addresses)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createAddress({
		title,
		pic,
		phone,
		district,
		address,
		postal,
	}) {
		// TODO
		return AddressService.addAddresses([{
			title,
			pic,
			phone,
			district,
			address,
			postal,
		}], MeManager.state.token).then(addresses => {
			return this.updateBulk(addresses)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateAddress(update) {
		// NOTE: MUST CONTAIN ID
		return AddressService.updateAddresses([update], MeManager.state.token).then(addresses => {
			return this.updateBulk(addresses)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}


	// validatePostalAndRetrieveAreas(postal) {
	// 	return ShipperService.getAreas(postal).then(result => {
	// 		return result && result.data && result.data.rows
	// 	})
	// }
}

export default new AddressManager()
