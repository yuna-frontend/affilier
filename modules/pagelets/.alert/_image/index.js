import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	ImagePart as CoreImagePart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import ButtonPart from '../__button';

import Styles from './style'


export default ConnectHelper(
	class ImagePart extends CoreImagePart({
		Styles,
		BoxBit,
		ImageBit,
		TextBit: GeomanistBit,
		ButtonPart,
	}) {
		titleRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.subheading2} style={style}>
					{ this.props.title }
				</GeomanistBit>
			)
		}

		messageRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.body1} style={style}>
					{ this.props.message }
				</GeomanistBit>
			)
		}
	}
)
