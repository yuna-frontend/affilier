import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import MeManager from 'app/managers/me';

// import AuthenticationHelper from 'utils/helpers/authentication'

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

// import InitialLego from '../initial';

import Styles from './style';

// import { uniqBy, sortBy } from 'lodash'


export default ConnectHelper(
	class SidebarNavigationComponent extends ConnectedStatefulModel {

		static TYPES = {
			PRODUCT_DEALS: 'PRODUCT_DEALS',
			SUMMARY: 'SUMMARY',
			CUSTOM_LINK: 'CUSTOM_LINK',
			REPORT: 'REPORT',
			DASHBOARD: 'DASHBOARD',
		}

		static propTypes(PropTypes) {
			return {
				mobileVisible: PropTypes.bool,
				onClose: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
				roles: state.me.roles,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p)

			// this.extVoucher = ['external-voucher', 'external-voucher.detail']
			// this.paymentLink = ['add.payment.link', 'payment-link']

			this.autoCloser = null
			this.tab = []
			this.configurations = {
				[this.TYPES.PRODUCT_DEALS]: {
					title: 'Penawaran Produk',
					icon: 'product',
					onPress: this.onNavigateToProductDeals,
					isActive: ['product.deals'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.SUMMARY]: {
					title: 'Summary',
					icon: 'purchases',
					onPress: this.onNavigateToSummary,
					isActive: ['summary'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.CUSTOM_LINK]: {
					title: 'Tautan Khusus',
					icon: 'edit',
					onPress: this.onNavigateToCustomLink,
					isActive: ['custom.link'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.REPORT]: {
					title: 'Clicks Report',
					icon: 'requests',
					onPress: this.onNavigateToReport,
					isActive: ['clicks'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.DASHBOARD]: {
					title: 'Dashboard',
					icon: 'address-book',
					onPress: this.onNavigateToDashboard,
					isActive: ['dashboard'].indexOf(p.page.routeName) > -1,
				},
			}

			this.tab.push(
				this.configurations.DASHBOARD,
				// this.configurations.CUSTOM_LINK,
				this.configurations.PRODUCT_DEALS,
				// this.configurations.SUMMARY,
				this.configurations.REPORT,
			)

			// =========================================================================================
			//  INVENTORY
			// =========================================================================================
			// if (AuthenticationHelper.isAuthorized(this.props.roles, 'inventory')) {
			// 	this.tab.push(
			// 		this.configurations.PRODUCT_DEALS,
			// 		this.configurations.CUSTOM_LINK,
			// 	)
			// }

			// =========================================================================================
			//  STYLIST
			// =========================================================================================
			// if(AuthenticationHelper.isAuthorized(this.props.roles, 'stylist')) {
			// 	this.tab.push(
			// 		this.configurations.PRODUCT_DEALS,

			// 	)
			// }

			// =========================================================================================
			//  ANALYST
			// =========================================================================================
			// if (AuthenticationHelper.isAuthorized(this.props.roles, 'analyst')) {
			// 	this.tab.push(
			// 		this.configurations.PRODUCT_DEALS,
			// 		this.configurations.SUMMARY,
			// 		this.configurations.CUSTOM_LINK,
			// 		this.configurations.REPORT,
			// 	)
			// }

			// =========================================================================================
			//  MARKETING
			// =========================================================================================
			// if (AuthenticationHelper.isAuthorized(this.props.roles, 'marketing')) {
			// 	this.tab.push(
			// 		this.configurations.SUMMARY,
			// 		this.configurations.CUSTOM_LINK,
			// 	)
			// }

			// =========================================================================================
			//  PACKING
			// =========================================================================================
			// if (AuthenticationHelper.isAuthorized(this.props.roles, 'packing')) {
			// 	this.tab.push(
			// 		this.configurations.PRODUCT_DEALS,
			// 		this.configurations.CUSTOM_LINK,
			// 	)
			// }

			// this.tab = sortBy(uniqBy(this.tab, t => t.title), ['title'])
		}

		logout = {
			title: 'Kembali',
			icon: 'logout',
			onPress: () => {
				this.props.page.navigator.navigate('login')
				MeManager.logout()
			},
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()
		}

		onNavigateToProductDeals = () => {
			this.props.page.navigator.navigate('deals')
		}


		onNavigateToSummary = () => {
			this.props.page.navigator.navigate('summary')
		}

		onNavigateToCustomLink = () => {
			this.props.page.navigator.navigate('custom')
		}

		onNavigateToReport = () => {
			this.props.page.navigator.navigate('clicks')
		}

		onNavigateToDashboard = () => {
			this.props.page.navigator.navigate('dashboard')
		}

		onNavigateToProfile = () => {
			this.props.page.navigator.navigate('profile')
		}

		tabRenderer = ({title, icon, onPress, isActive}, i) => {
			return (
				<TouchableBit key={i} unflex row style={[Styles.nav, isActive && Styles.backgroundGrey]} onPress={onPress} >
					<IconBit
						name={ icon }
						size={ 20 }
						color={ isActive ? Colors.white.primary : Colors.white.palette(1, .4) }
						style={ Styles.icon }
					/>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_2}
						weight="medium"
						style={ isActive ? Styles.text : Styles.textInactive }
					>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit unflex accessible style={[Styles.container, typeof this.props.mobileVisible === 'boolean' ? Styles.containerMobile : {}, this.props.mobileVisible ? Styles.containerMobileVisible : {}, this.props.style]}>
					<BoxBit style={ Styles.innerContainer }>
						<BoxBit row unflex style={Styles.header}>
							<IconBit size={32} name="yuna-gram" color={Colors.white.primary} />
							{ typeof this.props.mobileVisible === 'boolean' && (
								<TouchableBit unflex onPress={this.props.onClose} style={ Styles.closeIcon }>
									<IconBit size={32} name="close" color={Colors.white.primary}/>
								</TouchableBit>
							) }
						</BoxBit>
						<ScrollViewBit>
							{ this.tab.map(this.tabRenderer) }
						</ScrollViewBit>
						{ this.tabRenderer(this.logout, -1) }
					</BoxBit>
				</BoxBit>
			);
		}
	}
)
