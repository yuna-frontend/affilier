import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
import LoaderBit from 'modules/bits/loader';
import TextInputBit from 'modules/bits/text.input';

import BoxLego from 'modules/legos/box';
import HintLego from 'modules/legos/hint';
import RowLego from 'modules/legos/row';
import UnitLego from 'modules/legos/unit';

import AdditionalsPart from './_additionals'

import Styles from './style';

import { isEmpty } from 'lodash'

const cache = {}


export default ConnectHelper(
	class BoxMeasurementLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				measurements: PropTypes.object,
				style: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			if(!oP.categoryId) {
				return null
			} else {
				return !cache[oP.categoryId] ? [`
					query {
						categoryMeasurementsList(
							filter: {
								categoryId: {
									equalTo: ${oP.categoryId || 0}
								}
							}) {
							measurement {
								id
								title
								isRequired
								measurements {
									id
									title
								}
							}
						}
					}
				`, {}, state.me.token] : null
			}
		}

		static getDerivedStateFromProps(nP) {

			if (!nP.categoryId) {
				return null
			} else if (!isEmpty(nP.data)) {
				cache[nP.categoryId] = nP.data
			} else if (cache[nP.categoryId]) {
				// check for cache
			} else {
				return null
			}

			const measurements = cache[nP.categoryId].categoryMeasurementsList.map(({ measurement }) => {
				return {
					title: measurement.title,
					isRequired: measurement.isRequired,
					measurements: measurement.measurements,
				}
			})

			return {
				requiredMeasurements: measurements.filter(m => m.isRequired),
				optionalMeasurements: measurements.filter(m => !m.isRequired).reduce((sum, c) => {
					return [...sum, ...c.measurements]
				}, []),
			}
		}

		constructor(p) {
			super(p, {
				requiredMeasurements: [],
				optionalMeasurements: [],
			})

			this.measurements = p.measurements
		}

		onChange = (id, e, value) => {
			this.measurements[id] = value

			Object.keys(this.measurements).forEach(key => {
				if (this.measurements[key] === undefined || this.measurements[key] === '') {
					delete this.measurements[key]
				}
			})

			this.props.onChange &&
			this.props.onChange(this.measurements)
		}

		rowRenderer = (measurement, index) => {
			return (
				<RowLego
					key={ index }
					data={ [{
						headerless: true,
						children: (
							<TextInputBit disabled defaultValue={measurement.title} />
						),
					}, {
						headerless: true,
						children: (
							<BoxBit row>
								<TextInputBit unflex={false}
									onChange={this.onChange.bind(this, measurement.id)}
									defaultValue={this.props.measurements[measurement.id]}
								/>
								<UnitLego title="cm" left />
							</BoxBit>
						),
					}] }
					style={Styles.row}
				/>
			)
		}

		measurementRenderer = ({ measurements }, i) => {
			if (i === 0) {
				return [(
					<RowLego key={ 'title' } data={[{
						title: 'Key',
					}, {
						title: 'Value',
					}]} />
				), (
					measurements.map(this.rowRenderer)
				)]
			} else {
				return measurements.map(this.rowRenderer)
			}
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<BoxLego
						title="Measurements"
						header={( <HintLego title="Search and use predefined measurement key from the options before creating a new key." /> )}
						contentContainerStyle={Styles.padder}
					>
						{ this.state.requiredMeasurements.map(this.measurementRenderer) }
					</BoxLego>
					<AdditionalsPart
						key={this.state.optionalMeasurements.map(m => m.id).join(',')}
						measurements={ this.state.optionalMeasurements }
						answers={ this.props.measurements }
						onChange={ this.props.onChange }
					/>
				</BoxBit>
			)
		}
	}
)

