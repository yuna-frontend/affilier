import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	headerBackground: {
		backgroundColor: Colors.white.primary,
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0,
	},

	content: {
		flexShrink: 0,
		flexGrow: 1,
		zIndex: 1,
		backgroundColor: Colors.white.primary,
	},
})
