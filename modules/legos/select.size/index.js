import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { flattenDeep, uniqBy, isEmpty } from 'lodash'

const cache = {}


export default ConnectHelper(
	class SelectSizeLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				sizeId: PropTypes.number,
				categoryId: PropTypes.number,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache[oP.categoryId || 'all'] ? [`
				query {
					categorySizesList ${oP.categoryId ? `(
						filter: {
							categoryId: {
								equalTo: ${oP.categoryId}
							}
						})` : ''} {
						size {
							id
							sizes {
								id
								title
							}
						}
					}
				}
			`, {
				fetchPolicy: _cache ? 'cache-first' : 'no-cache',
			}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {

			if (!isEmpty(nP.data)) {
				cache[nP.categoryId || 'all'] = nP.data
			} else if (cache[nP.categoryId || 'all']) {
				// check for cache
			} else {
				return null
			}

			return {
				sizes: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(uniqBy(flattenDeep(cache[nP.categoryId || 'all'].categorySizesList.map(s => {
					return s.size.sizes.map(cS => {
						return {
							selected: cS.id === nP.sizeId,
							key: cS.id,
							title: cS.title,
						}
					})
				})), 'key')),
			}
		}

		static defaultProps = {
			cache: true,
			unflex: false,
			placeholder: 'Filter by size',
			emptyTitle: 'All Categories',
		}

		constructor(p) {
			super(p, {
				sizes: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit unflex={false} disabled
					placeholder={'Loading ...'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit unflex={ this.props.unflex }
					disabled={ this.props.disabled }
					key={ this.props.sizeId }
					placeholder={ this.props.placeholder }
					options={ this.state.sizes || [] }
					onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
