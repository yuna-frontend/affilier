import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'
import Defaults from 'coeur/constants/default'

export default StyleSheet.create({

	page: {
		width: '100%',
		height: '100%',
	},

	modal: {
		height: Sizes.screen.height + Sizes.safe.bottom,
		width: Math.min(480, Sizes.screen.width),
		overflow: 'hidden',
	},

	header: {
		backgroundColor: Colors.transparent,
		height: 48,
	},

	headerContainer: {
		marginTop: -Sizes.safe.top,
	},

	container: {
		width: Sizes.app.width,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		backgroundColor: Colors.white.primary,
		transform: Defaults.PLATFORM === 'web' ? 'translate3d(0, 0, 0)' : undefined,
	},

	slideContainer: {
		width: Sizes.app.width,
		backgroundColor: Colors.white.primary,
		transform: Defaults.PLATFORM === 'web' ? 'translate3d(0, 0, 0)' : undefined,
	},

	content: {
		paddingTop: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 32,
	},

	slideContent: {
		paddingTop: 0,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 32,
	},

	shorten: {
		minHeight: Sizes.app.height * .3,
	},

	footer: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(3, .16),
	},

	grabber: {
		paddingTop: 6,
		paddingBottom: 16,
		alignItems: 'center',
		width: Sizes.app.width,
		zIndex: 1,
		backgroundColor: Colors.white.primary,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		marginBottom: -2,
		...( Defaults.PLATFORM === 'web' ? {
			cursor: 'grab',
		} : {} ),
	},

	handle: {
		height: 4,
		width: 48,
		borderRadius: 2,
		backgroundColor: Colors.solid.grey.palette(3),
	},

	touchable: {
		width: '100%',
		height: '100%',
		position:'absolute',
		top: 0,
		left: 0,
	},

	...(Defaults.PLATFORM === 'web' ? {
		'@media screen and (max-width: 800px)': {
			modal: {
				width: Sizes.app.width,
			},
		},
	} : {}),
})
