import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import RadioBit from 'modules/bits/radio'
import TouchableBit from 'modules/bits/touchable'
import GeomanistBit from 'modules/bits/geomanist'
import ShadowBit from 'modules/bits/shadow'

import { isEmpty, isNil } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class SortPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				onChangeSort: PropTypes.func,
				sort: PropTypes.object,
				inFilterModal: PropTypes.bool,
			}
		}

		constructor(p) {
			super(p, {
				sort: p.sort,
			})

			this._list = [{
				title: 'Terbaru',
				sort: 'sort_by_created_at',
				value: 'DESC',
			}, {
				title: 'Harga Tertinggi',
				sort: 'sort_by_price',
				value: 'DESC',
			}, {
				title: 'Harga Terendah',
				sort: 'sort_by_price',
				value: 'ASC',
			}, {
				title: 'Komisi Tertinggi',
				sort: 'sort_by_commission',
				value: 'DESC',
			}]
		}

		onSelectSort = (sort, value) => {
			if(isNil(this.state.sort[sort])) {
				this.setState({
					sort: { [sort]: value },
				})
			} else {
				this.setState({
					sort: this.state.sort[sort] !== value
						? { [sort]: value }
						: {},
				})
			}
			this.props.onChangeSort(sort, value)
		}

		listRenderer = ({ title, sort, value }, i) => {
			return (
				<TouchableBit row key={ i } style={[Styles.listItem, this.props.inFilterModal && Styles.listItemInFilterModal]} onPress={() => this.onSelectSort(sort, value)}>
					{ !this.props.inFilterModal ? (<>
						<RadioBit dumb isActive={
							!isEmpty(this.props.sort)
								? !!Object.keys(this.props.sort).find(key => key === sort && this.props.sort[key] === value)
								: false
						} onPress={() => this.onSelectSort(sort, value)} />
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold" style={ Styles.listItemTitle }>
							{ title }
						</GeomanistBit>
					</>) : (<>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } style={ Styles.listItemTitle }>
							{ title }
						</GeomanistBit>
						<RadioBit dumb isActive={
							!isEmpty(this.state.sort)
								? !!Object.keys(this.state.sort).find(key => key === sort && this.state.sort[key] === value)
								: false
						} onPress={() => this.onSelectSort(sort, value)} />
					</>) }
				</TouchableBit>
			)
		}

		view() {
			return !this.props.inFilterModal ? (
				<ShadowBit y={ -10 } blur={ 24 } unflex style={ Styles.container }>
					{ this._list.map(this.listRenderer) }
				</ShadowBit>
			) : (
				<BoxBit>
					{ this._list.map(this.listRenderer) }
				</BoxBit>
			)
		}
	}
)
