import ServiceModel from 'coeur/models/service';


class RequestService extends ServiceModel {

	static displayName = 'request'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/request/')
	}

	getRequests(filter, token) {
		return this.get('', {
			query: filter,
			token,
		})
	}

	getDetail(id, token) {
		return this.get(id, {
			token,
		})
	}

	approveWithdraw(id, note, assetId, token) {
		return this.post(id + '/approve', {
			data: {
				note,
				data:{
					asset_id: assetId,
				},
			},
			token,
		})
	}

	approveChange(id, stylistId, note, token) {
		return this.post(id + '/approve', {
			data: {
				note,
				data: {
					stylist_id: stylistId,
				},
			},
			token,
		})
	}

	reject(id, note, token) {
		return this.post(id + '/reject', {
			data: {
				note,
			},
			token,
		})
	}
}

export default new RequestService()
