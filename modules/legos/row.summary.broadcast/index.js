import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import InputValidatedBit from 'modules/bits/input.validated'

import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class RowProductLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				value: PropTypes.string,
				maxlength: PropTypes.number,
			}
		}

		static defaultProps = {
			title: 'Summary',
			maxlength: 240,
		}

		constructor(p) {
			super(p, {
				len: p.value ? p.value.length : 0,
			})
		}

		onChange = (val) => {
			this.setState({
				len: val.length,
			})

			this.props.onChange &&
			this.props.onChange(val)
		}

		view() {
			return (
				<RowLego
					data={[{
						title: this.props.title,
						children: (
							<BoxBit>
								<InputValidatedBit required
									placeholder="-"
									value={ this.props.value }
									onChange={ this.onChange }
									maxlength={ this.props.maxlength }
								/>
								<BoxBit row style={Styles.containerMaxlength}>
									<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.darkGrey60}>
										Summary. Max { this.props.maxlength } characters.
									</GeomanistBit>
									<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align="right" style={Styles.darkGrey60}>
										{ this.state.len }/{ this.props.maxlength }
									</GeomanistBit>
								</BoxBit>
							</BoxBit>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
