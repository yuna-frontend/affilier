import RecordModel from 'coeur/models/record'


function VariantRecordFactory(additionalConfig = {}) {
	return class VariantRecord extends RecordModel('variant', {
		id			: RecordModel.TYPES.ID,
		productId	: RecordModel.TYPES.ID,
		size		: RecordModel.TYPES.STRING,
		color		: RecordModel.TYPES.STRING,
		url			: RecordModel.TYPES.STRING,

		basePrice	: RecordModel.TYPES.NUMBER,
		price		: RecordModel.TYPES.NUMBER,
		sku			: RecordModel.TYPES.STRING,

		metadata	: RecordModel.TYPES.OBJECT,

		inventoryIds: RecordModel.TYPES.ARRAY,
		mediaIds	: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
	}
}

VariantRecordFactory.getModel = function(config = {}) {
	return {
		...config,
		inventoryIds: config.inventoryIds || (config.inventories && config.inventories.map(inventory => inventory.id)),
	}
}


export default VariantRecordFactory
