import ConnectHelper from 'coeur/helpers/connect';
import CoreInputFileBit from 'coeur/modules/bits/input.file';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import IconBit from '../icon';
import ImageBit from '../image';
import LoaderBit from '../loader';
import TouchableBit from '../touchable';

import Styles from './style';

export default ConnectHelper(
	class InputFileBigBit extends CoreInputFileBit({
		Styles,
		BoxBit,
		IconBit,
		ImageBit,
		LoaderBit,
		TouchableBit,
		closeIcon: 'trash',
		closeSize: 18,
		closeColor: Colors.red.palette(3),
		loaderColor: Colors.black.palette(2, .4),
		iconColor: Colors.black.palette(2, .8),
	}) {}
)
