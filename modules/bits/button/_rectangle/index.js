import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.black.palette(2), Colors.white.primary, Colors.black.palette(2, .2)],
				ACTIVE: [Colors.black.palette(2, .7), Colors.white.palette(1, .8), Colors.black.palette(2, .2)],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(3), Colors.solid.grey.palette(3)],
				LOADING: [Colors.white.primary, Colors.primary],
			},
			WHITE_PRIMARY: {
				NORMAL: [Colors.primary, Colors.white.primary, Colors.primary],
				ACTIVE: [Colors.primary, Colors.white.palette(1, .8), Colors.primary],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(3), Colors.solid.grey.palette(3)],
				LOADING: [Colors.white.primary, Colors.primary],
			},
			BLACK: {
				NORMAL: [Colors.white.primary, Colors.black.palette(2), Colors.black.palette(2)],
				ACTIVE: [Colors.white.primary, Colors.black.palette(2, .7), Colors.black.palette(2, .7)],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(3), Colors.solid.grey.palette(3)],
				LOADING: [Colors.white.primary, Colors.black.palette(2, .9), Colors.black.palette(2)],
			},
			SECONDARY: {
				NORMAL: [Colors.black.palette(2, .8), Colors.solid.grey.palette(1), Colors.black.palette(2, .2)],
				ACTIVE: [Colors.black.palette(2, .8), Colors.solid.grey.palette(2), Colors.black.palette(2, .2)],
				DISABLED: [Colors.white.primary, Colors.solid.grey.palette(3)],
			},
			TRASH: {
				NORMAL: [Colors.red.palette(4), Colors.white.primary, Colors.red.palette(4)],
				DISABLED: [Colors.red.palette(3), Colors.solid.grey.palette(2), Colors.red.palette(3)],
				ACTIVE: [Colors.red.palette(1), Colors.white.primary, Colors.red.palette(1)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
			MEDIUM: Styles.medium,
			SMALL: Styles.small,
			TINY: Styles.tiny,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.PARAGRAPH_3
				break;
			case this.TYPES.SIZES.MEDIUM:
				textType = GeomanistBit.TYPES.PRIMARY_2
				break;
			case this.TYPES.SIZES.SMALL:
				textType = GeomanistBit.TYPES.SECONDARY_3
				break;
			case this.TYPES.SIZES.TINY:
				textType = GeomanistBit.TYPES.CAPTION_2
				break;
			}

			return (
				<GeomanistBit type={textType} weight={this.props.weight} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
