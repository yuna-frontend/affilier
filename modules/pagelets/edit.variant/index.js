import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import MediaManager from 'app/managers/media';
import ProductManager from 'app/managers/product';
import VariantManager from 'app/managers/variant';

import UtilitiesContext from 'coeur/contexts/utilities';

import CommonHelper from 'coeur/helpers/common';
// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';
// import StringHelper from 'coeur/helpers/string';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import LinkBit from 'modules/bits/link';
import LoaderBit from 'modules/bits/loader';

import BoxLego from 'modules/legos/box';
import BoxRowLego from 'modules/legos/box.row';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import EditVariantMediaPart from './_media';

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class EditVariantPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				productId: PropTypes.id.isRequired,
				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, productId = oP.productId
				, variant = id && VariantManager.get(id) || new VariantManager.record()
				, product = productId && ProductManager.get(productId) || new ProductManager.record()

			return {
				product,
				variant,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.variant._isGetting !== nS.isGettingVariant) {
				return {
					isGettingVariant: nP.variant._isGetting,
					data: {
						size		: nP.variant.size,
						color		: nP.variant.color,
						url			: nP.variant.url,

						basePrice	: nP.variant.basePrice,
						price		: nP.variant.price,
						sku			: nP.variant.sku,
					},
					mediaIds		: nP.variant.mediaIds,
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingVariant: p.variant._isGetting,
				isDone: false,
				isLoading: false,
				data: {
					size		: p.variant.size,
					color		: p.variant.color,
					url			: p.variant.url,
					basePrice	: p.variant.basePrice,
					price		: p.variant.price,
					sku			: p.variant.sku,
				},
				mediaIds: p.variant.mediaIds || [],
				tempIds: [],
				files: {},
			}, [
				'lowerCaseValidator',
				'onUpdate',
				'onSubmit',
				'onSetMedias',
				'onSuccess',
				'onError',
				'onCancel',
				'onMediaReady',
				'onMediaChange',
				'onMediaRemoved',
			])

			this._tempId = 0
			this._uploadPromises = []
			this._upperCaseRegex = /[A-Z]/
		}

		lowerCaseValidator(input) {
			return !this._upperCaseRegex.test(input)
		}

		onUpdate(isDone, {
			size,
			color,
			url,
			basePrice,
			price,
			sku,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					size: size.value,
					color: color.value,
					url: url.value,
					basePrice: basePrice.value,
					price: price.value,
					sku: sku.value,
				},
			})
		}

		onSubmit() {
			if(!this.state.isLoading) {
				this.setState({
					isLoading: true,
				}, () => {
					const {
						size,
						color,
						url,
						basePrice,
						price,
						sku,
					} = this.state.data

					Promise.all(this._uploadPromises).then(() => {
						if(this.props.id) {
							// UPDATE
							VariantManager.updateVariant({
								id: this.props.id,
								productId: this.props.productId,
								size,
								color,
								url,
								basePrice,
								price,
								sku,
							}).then(this.onSetMedias).catch(this.onError)
						} else {
							// CREATE
							VariantManager.createVariant({
								productId: this.props.productId,
								size,
								color,
								url,
								basePrice,
								price,
								sku,
							}).then(this.onSetMedias).catch(this.onError)
						}
					}).catch(this.onError)
				})
			}
		}

		onSetMedias(variant) {
			if(this.state.mediaIds.length) {
				VariantManager.updateVariantMedias({
					id: variant.id,
					mediaIds: this.state.mediaIds,
				}).then(this.onSuccess).catch(this.onError)
			} else {
				this.onSuccess(variant)
			}
		}

		onSuccess(variant) {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Variant updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onUpdate &&
			this.props.onUpdate(variant)

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		onMediaChange(id, file) {
			if(file) {
				this._tempId++;
				if(this.state.tempIds.indexOf(id) === -1) {
					this.setState({
						files: {
							...this.state.files,
							[id]: file,
						},
						tempIds: [...this.state.tempIds, id],
					})
				} else {
					this.setState({
						files: {
							...this.state.files,
							[id]: file,
						},
					})
				}
			} else {
				// undefined
				if(this.state.tempIds.indexOf(id) > -1) {
					this.setState({
						files: CommonHelper.stripUndefined({
							...this.state.files,
							[id]: undefined,
						}),
						tempIds: _.without(this.state.tempIds, id),
					})
				}
			}
		}

		onMediaReady(id, base64) {
			this._uploadPromises.push(MediaManager.upload(base64).then(media => {
				return new Promise(res => {
					if(this.state.tempIds.indexOf(id) > -1) {
						this.setState({
							mediaIds: [...this.state.mediaIds, media.id],
							tempIds: _.without(this.state.tempIds, id),
							files: CommonHelper.stripUndefined({
								...this.state.files,
								[id]: undefined,
							}),
						}, () => {
							res(media)
						})
					} else {
						// id removed
						res(media)
					}
				})
			}))
		}

		onMediaRemoved(id) {
			if(this.state.mediaIds.indexOf(id) > -1) {
				this.setState({
					mediaIds: _.without(this.state.mediaIds, id),
				})
			}
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxRowLego
						title="Product"
						data={[{
							data: [{
								title: 'Name',
								content: this.props.product.title,
							}],
						}, {
							data: [{
								title: 'SPU',
								content: this.props.product.spu || '-',
							}],
						}, {
							data: [{
								title: 'URL',
								children: this.props.product.url ? (
									<LinkBit target={LinkBit.TYPES.BLANK} href={this.props.product.url}>
										{ this.props.product.url }
									</LinkBit>
								) : undefined,
								content: '-',
							}],
						}]}
						contentContainerStyle={Styles.box}
					/>
					<BoxLego
						title="Variant Detail"
						contentContainerStyle={Styles.box}>
						<FormPagelet ignoreNull
							config={[{
								title: 'Color',
								description: 'Please use lower case.',
								id: 'color',
								autofocus: true,
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.color,
								validator: this.lowerCaseValidator,
							}, {
								title: 'Size',
								description: 'Please use lower case. Insert "one size" if the product have no sizing variables.',
								required: true,
								id: 'size',
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.size,
								validator: this.lowerCaseValidator,
								testOnMount: true,
							}, {
								title: 'SKU',
								id: 'sku',
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.sku,
								maxlength: 31,
							}, {
								title: 'URL',
								id: 'url',
								type: FormPagelet.TYPES.FREE_INPUT,
								value: this.state.data.url,
							}, {
								title: 'Retail Price',
								required: true,
								id: 'basePrice',
								type: FormPagelet.TYPES.CURRENCY,
								value: this.state.data.basePrice + '',
								testOnMount: true,
							}, {
								title: 'Discounted Price',
								required: true,
								id: 'price',
								type: FormPagelet.TYPES.CURRENCY,
								value: this.state.data.price + '',
								testOnMount: true,
							}]}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onSubmit }
						/>
					</BoxLego>
					<BoxLego title="Images">
						{ this.state.isGettingVariant ? (
							<BoxBit unflex centering type={BoxBit.TYPES.ALL_THICK}>
								<LoaderBit />
							</BoxBit>
						) : (
							<BoxBit unflex row style={Styles.images}>
								{ this.state.mediaIds.concat(this.state.tempIds).map(mediaId => {
									return (
										<EditVariantMediaPart
											key={ mediaId }
											id={ mediaId }
											value={ this.state.files[mediaId] }
											progress={ this.state.files[mediaId] ? 10 : undefined }
											onChange={ this.onMediaChange }
											onDataLoaded={ this.onMediaReady }
											onRemoveMedia={ this.onMediaRemoved }
											style={ Styles.image }
										/>
									)
								}) }
								<EditVariantMediaPart
									key={ `temp[${this._tempId}]` }
									id={ `temp[${this._tempId}]` }
									onDataLoaded={ this.onMediaReady }
									onChange={ this.onMediaChange }
									style={ Styles.image }
								/>
							</BoxBit>
						) }
					</BoxLego>
				</DetailPagelet>
			)
		}
	}
)
