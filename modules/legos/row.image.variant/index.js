import React from 'react';
import ConnectedStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities';

import VariantService from 'app/services/variant';

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import LoaderLego from 'modules/legos/loader';
import RowImageLego from 'modules/legos/row.image';

import Styles from './style'


export default ConnectHelper(
	class RowImageVariantLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				headerless: PropTypes.bool,
				addable: PropTypes.bool,
				reorder: PropTypes.bool,
				variantId: PropTypes.id,
				onUpdate: PropTypes.func,
				styleImage: PropTypes.style,
				containerStyle: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.variantId ? VariantService.getVariantAssets(oP.variantId, state.me.token).catch(() => []) : Promise.resolve([])
		}

		static contexts = [ UtilitiesContext ]

		static defaultProps = {
			reorder: true,
			addable: true,
		}

		constructor(p) {
			super(p, {
				images: [],
				isChangedOrder: false,
				isLoading: false,
			})
		}

		uploader = (image, index) => {
			return VariantService.upload({
				variantId: this.props.variantId,
				image,
				order: index,
			}, this.props.token)
		}

		remover = id => {
			return VariantService.removeVariantAssets(
				this.props.variantId,
				id,
				this.props.token,
			)
		}

		orderer = images => {
			if(!this.state.isChangedOrder) {
				this.state.isChangedOrder = true

				this.forceUpdate()
			}

			this.state.images = images
		}

		onSubmitReorder = () => {
			this.setState({
				isLoading: true,
			}, () => {
				return VariantService.reorderAssets(this.props.variantId, this.state.images.map(image => image.id), this.props.token)
					.then(res => {
						let isSaved = true

						if(!res) {
							this.props.utilities.notification.show({
								message: 'Failed to save reorder image',
							})

							isSaved = false
						}

						this.props.utilities.notification.show({
							type: 'SUCCESS',
							message: 'Reorder Saved',
						})

						this.setState({
							isChangedOrder: isSaved,
							isLoading: false,
						})
					})
					.catch(err => {
						this.warn(err)

						this.props.utilities.notification.show({
							type: 'SUCCESS',
							message: 'Reorder Saved',
						})

						this.setState({
							isLoading: false,
						})
					})
			})
		}

		viewOnLoading() {
			return (
				<LoaderLego simple style={ Styles.empty } />
			)
		}

		view() {
			console.log('test ', this.props.data)
			return (
				<BoxBit unflex>
					<RowImageLego
						title={ this.props.title }
						headerless={ this.props.headerless }
						variantId={ this.props.variantId }
						addable={ this.props.addable }
						reorder={ this.props.reorder }
						images={ this.props.data }
						uploader={ this.uploader }
						remover={ this.remover }
						orderer={ this.orderer }
						onUpdate={ this.props.onUpdate }
						styleImage={ this.props.styleImage }
						containerStyle={ this.props.styleImage }
					/>
					{ this.props.reorder && (
						<ButtonBit unflex title="Save Reorder"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS}
							state={ this.state.isLoading ? ButtonBit.TYPES.LOADING : !!this.state.isChangedOrder && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onSubmitReorder }
						/>
					)}
				</BoxBit>
			)
		}
	}
)
