import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	listCollapsible: {
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
	},

	price: {
		marginBottom: Sizes.margin.default,
		alignItems: 'center',
		...(Defaults.PLATFORM === 'web' && {
			'&:hover $priceRadio': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	priceRadio: {
		width: 20,
		height: 20,
		borderColor: Colors.solid.grey.palette(3),
		...(Defaults.PLATFORM === 'web' && {
			'&:hover': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	priceBox: {
		paddingTop: 14,
		paddingBottom: 14,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		marginBottom: Sizes.margin.default,
		borderWidth: 2,
		borderColor: Colors.grey.palette(8),
	},

	priceBoxActive: {
		borderColor: Colors.black.primary,
		background: Colors.grey.palette(5),
	},

	priceTitle: {
		marginLeft: Sizes.margin.default / 2,
	},
})
