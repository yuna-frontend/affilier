import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';
import ScrollViewBit from 'modules/bits/scroll.view'
import TouchableBit from 'modules/bits/touchable';

import HeaderLoggedLego from '../header.logged';

import Styles from './style';

import {  flatten, isEmpty } from 'lodash';

let cache = null


export default ConnectHelper(
	class HeaderStylistLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				search: PropTypes.string,
				closer: PropTypes.bool,
				onSearch: PropTypes.func,
				onChangeCategory: PropTypes.func,
				onShowMenu: PropTypes.func,
			}
		}

		static propsToQuery(state) {
			return !cache ? [
				`query {
					categoriesList (filter: {
						categoryId : {
							isNull: true
						}
					}){
						categories {
							id
							title
							categories {
								id
							}
						}
					}
				}`, {}, state.me.token,
			] : null
		}

		static stateToProps(state) {
			return {
				name: state.me.fullname,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if (!isEmpty(nP.data)) {
				cache = nP.data
			} else if (cache) {
				// check for cache
			} else {
				return null
			}

			if(cache && !nS.categories.length) {
				const categories = flatten(cache.categoriesList.map(category => category.categories))

				if(nP.categoryId) {
					const category = categories.find(c => {
						return c.id === nP.categoryId || (c.categories.findIndex(cc => cc.id === nP.categoryId) > -1)
					})

					return {
						categories,
						selectedCategoryId: category.id,
					}

				} else {
					return {
						categories,
						selectedCategoryId: categories[0].id,
					}
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				categories: [],
				selectedCategoryId: null,
			}, [
				'onChange',
			])
		}

		shouldComponentUpdate(nP, nS) {
			return nS.selectedCategoryId !== this.state.selectedCategoryId
				|| nS.categories.length !== this.state.categories.length
		}

		onChange(id) {
			this.setState({
				selectedCategoryId: id,
			})
		}

		componentDidMount() {
			if (this.state.selectedCategoryId !== null) {
				this.props.onChangeCategory &&
				this.props.onChangeCategory(this.state.selectedCategoryId)
			}
		}

		componentDidUpdate() {
			this.props.onChangeCategory &&
			this.props.onChangeCategory(this.state.selectedCategoryId)
		}

		tabRenderer = category => {
			return (
				<TouchableBit unflex key={category.id} onPress={this.onChange.bind(this, category.id)}>
					<GeomanistBit
						type={GeomanistBit.TYPES.SUBHEADER_1}
						weight="medium"
						style={Styles.title}
					>
						{ category.title }
					</GeomanistBit>
					<BoxBit unflex style={ category.id === this.state.selectedCategoryId ? Styles.tabActive : undefined } />
				</TouchableBit>
			);
		}

		view() {
			return (
				<HeaderLoggedLego closer={ this.props.closer } onShowMenu={ this.props.onShowMenu } header={(
					<BoxBit row centering unflex style={Styles.header}>
						<IconBit name="yuna-gram" size={32} />
						<BoxBit unflex style={Styles.line} />
						<GeomanistBit type={GeomanistBit.TYPES.HEADER_5} weight="normal" >
							INVENTORY
						</GeomanistBit>
						<TextInputBit
							prefix={(
								<IconBit
									name="search"
									color={ Colors.primary }
								/>
							)}
							defaultValue={ this.props.search }
							onSubmitEditing={ this.props.onSearch }
							style={Styles.search}
						/>
					</BoxBit>
				)}>
					<ScrollViewBit horizontal style={Styles.tabs}>
						{ this.state.categories.map(this.tabRenderer) }
					</ScrollViewBit>
				</HeaderLoggedLego>
			);
		}
	}
)
