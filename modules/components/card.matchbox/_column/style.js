import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		marginLeft: -6,
		marginRight:  -6,
	},

	column: {
		borderTopWidth: 1,
		borderTopStyle: 'solid',
		borderTopColor: Colors.pink.palette(1),
		paddingTop: 8,
		marginRight: 6,
		marginLeft: 6,
	},

	point: {
		marginBottom: 8,
	},
})
