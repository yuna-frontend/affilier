import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'

import UtilitiesContext from 'coeur/contexts/utilities'

import CommonHelper from 'coeur/helpers/common'
import FormatHelper from 'coeur/helpers/format'
import TimeHelper from 'coeur/helpers/time'

import Linking from 'coeur/libs/linking'

import ConversionService from 'app/services/conversion'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'

import TableLego from 'modules/legos/table'

import PaginationComponent from 'modules/components/pagination'

import SearchPart from './_search'

import Styles from './style'

// const data = [{
// 	timestamp: '2021-09-16T06:50:11.628Z',
// 	id: 'YCO2109160015006',
// 	product: [{
// 		id: 13764,
// 		brand: 'Modemaya',
// 		title: 'Olive Shirt',
// 		price: [269910, 299900],
// 		commission: 20000,
// 		quantity: 1,
// 	}, {
// 		id: 12347,
// 		brand: 'Ramune',
// 		title: 'Chaerin Frill Top',
// 		price: [191400, 319000],
// 		commission: 15000,
// 		quantity: 2,
// 	}],
// }, {
// 	timestamp: '2021-09-16T04:07:28.154Z',
// 	id: 'YCO2109160010704',
// 	product: {
// 		id: 12347,
// 		brand: 'RAMUNE',
// 		title: 'Chaerin Frill Top',
// 		price: [191400, 319000],
// 		commission: 15000,
// 		quantity: 3,
// 	},
// }, {
// 	timestamp: '2021-09-10T16:52:26.962Z',
// 	id: 'YCO2109100015216',
// 	product: {
// 		id: 14660,
// 		brand: 'RAMUNE',
// 		title: 'Haru Loose Top',
// 		price: [129000, 249000],
// 		commission: 18500,
// 		quantity: 7,
// 	},
// }]

export default ConnectHelper(
	class TablePart extends ConnectedStatefulModel {

		static stateToProps(state) {
			return {
				userId: state.me.id,
				token: state.me.token,
			}
		}

		static contexts = [ UtilitiesContext ]

		constructor(p) {
			const query = CommonHelper.getQueryString()

			const page = query.page
				? (query.page > 0 && query.page) || 1
				: 1

			const limit = query.limit
				? (query.limit <= 100 && query.limit) || 100
				: 50

			super(p, {
				page,
				offset: page * limit - limit,
				limit,

				total: 0,
				data: [],

				searchTrigger: 0,
			})

			this.mounted = null

			this.filter = {
				product: query.search || null,
				startDate: (query.startDate && new Date(query.startDate)) || null,
				endDate: (query.endDate && new Date(query.endDate)) || null,
			}
		}

		header = [{
			title: 'Waktu Pembelian',
			width: 1,
		}, {
			title: 'Produk',
			width: 2,
		}, {
			title: 'Harga Pembelian',
			width: 1,
		}, {
			title: 'Komisi',
			width: 1,
		}]

		componentDidMount() {
			this.mounted = true
			this.getData()
		}

		componentWillUnmount() {
			this.mounted = false
		}

		componentDidUpdate(prevProps, prevState) {
			if(prevState.page !== this.state.page || prevState.searchTrigger !== this.state.searchTrigger) {
				const query = {
					page: this.state.page,
					limit: this.state.limit,
					...(!!this.filter.product && { search: this.filter.product }),
					...(!!this.filter.startDate && { startDate: new Date(this.filter.startDate).getTime() }),
					...(!!this.filter.endDate && { endDate: new Date(this.filter.endDate).getTime() }),
				}

				const queryKeys = Object.keys(query)

				window.history.pushState({}, 'Conversion', !queryKeys.length ? '/dashboard' : `/dashboard?${queryKeys.map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		getData = () => {
			if(this.mounted) {
				this.setState({ isLoading: true }, () => {
					ConversionService.getConversion(this.props.userId, {
						offset: this.state.offset,
						limit: this.state.limit,
						...(!!this.filter.product && { search: this.filter.product }),
						...(!!this.filter.startDate && { start_date: this.filter.startDate }),
						...(!!this.filter.endDate && { end_date: this.filter.endDate }),
					}, this.props.token).then(res => {
						if(res && res.data) {
							this.setState({
								isLoading: false,
								data: res.data,
								total: res.count,
							})
						} else {
							this.onError()
						}
					}).catch(this.onError)
				})
			}
		}

		onError = err => {
			this.setState({
				isLoading: false,
			}, () => {
				this.warn(err)

				this.props.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onNavigateToVariant = id => {
			Linking.open('https://helloyuna.io/product/variant/' + id)
		}

		onNavigationChange = offset => {
			this.setState(state => ({
				offset,
				page: offset / state.limit + 1,
			}))
		}

		onSearch = (product, startDate, endDate) => {
			this.filter.product = product
			this.filter.startDate = startDate
			this.filter.endDate = endDate

			this.setState(state => ({
				searchTrigger: state.searchTrigger + 1,
			}))
		}

		rowRenderer = data => {
			return {
				id: data.id,
				data: [{
					title: `${TimeHelper.format(data.created_at, 'DD MMMM YYYY — HH:mm')}`,
				}, {
					children: data.order_detail.title ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							{ data.order_detail.title }
						</GeomanistBit>
					) : (
						<BoxBit unflex/>
					),
				}, {
					title: `IDR ${FormatHelper.currency(data.price)}`,
				}, {
					title: `IDR ${FormatHelper.currency(data.commission)}`,
				}],
				onPress: data.order_detail.id ? this.onNavigateToVariant.bind(this, data.order_detail.id) : undefined,
			}
		}

		emptyRowRenderer = () => {
			return {
				data: [{
					children: (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="center">
							No data.
						</GeomanistBit>
					),
				}],
			}
		}

		view() {
			return (
				<BoxBit unflex>
					<SearchPart isLoading={ this.state.isLoading } product={ this.filter.product } startDate={ this.filter.startDate } endDate={ this.filter.endDate } onSearch={ this.onSearch }/>

					<BoxBit unflex style={ Styles.outerTableWrapper }>
						<BoxBit style={ Styles.innerTableWrapper }>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.header }
								rows={ !!this.state.data.length ? this.state.data.map(this.rowRenderer) : [] }
							/>
						</BoxBit>
					</BoxBit>

					<BoxBit unflex style={ Styles.footer }>
						<PaginationComponent
							current={ this.state.offset }
							count={ this.state.limit }
							total={ this.state.total }
							onNavigationChange={ this.onNavigationChange }
						/>
					</BoxBit>
				</BoxBit>
			)
		}

	}
)
