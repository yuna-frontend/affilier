import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	broken: {
		height: '100%',
		backgroundColor: Colors.grey.palette(1, .2),
	},

	backgroundOverlay: {
		backgroundColor: Colors.opacity.black.overlay,
	},
})
