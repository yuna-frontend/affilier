import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		padding: Sizes.margin.default,
	},

	disabled: {
		opacity: .6,
	},

	noBorder: {
		borderTopWidth: 0,
	},

	content: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	title: {
		color: Colors.black.palette(2, .8),
	},

	space: {
		justifyContent: 'space-between',
	},
})
