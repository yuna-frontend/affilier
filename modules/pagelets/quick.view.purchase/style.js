import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	container: {
		paddingLeft: 0,
		paddingRight: 0,
	},

	box: {
		backgroundColor: Colors.grey.palette(2),
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	content: {
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	button: {
		marginRight: 8,
	},

})
