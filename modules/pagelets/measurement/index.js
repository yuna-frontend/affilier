import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import PageContext from 'coeur/contexts/page'

import BoxMeasurementLego from 'modules/legos/box.measurement';

import GetterCategoryComponent from 'modules/components/getter.category';


export default ConnectHelper(
	class MeasurementPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				variantId: PropTypes.id,
				categoryId: PropTypes.id,
				measurements: PropTypes.object,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			PageContext,
		]

		viewRenderer = p => {
			return (
				<BoxMeasurementLego
					categoryId={ p.categoryId }
					measurements={this.props.measurements}
					onChange={this.props.onChange}
					style={this.props.style}
				/>
			)
		}

		view() {
			return (
				<GetterCategoryComponent
					key={ this.props.categoryId }
					categoryId={ this.props.categoryId }
					children={ this.viewRenderer }
				/>
			)
		}
	}
)
