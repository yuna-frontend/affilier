import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';


import UtilitiesContext from 'coeur/contexts/utilities'

import TouchableBit from 'modules/bits/touchable';

import CardMediaComponent from 'modules/components/card.media';

import ImagePagelet from 'modules/pagelets/image';

import Styles from './style';


export default ConnectHelper(
	class CardMediaPopupComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,

				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {}, [
				'onImagePress',
				'onRequestClose',
			])
		}

		onImagePress() {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet
						id={this.props.id}
						onPress={this.onRequestClose}
					/>
				),
			})
		}

		onRequestClose() {
			this.props.utilities.alert.hide()
		}

		view() {
			return (
				<TouchableBit unflex onPress={this.onImagePress} style={this.props.style}>
					<CardMediaComponent id={ this.props.id } style={Styles.image} />
				</TouchableBit>
			)
		}
	}
)
