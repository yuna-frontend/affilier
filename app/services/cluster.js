import ServiceModel from 'coeur/models/service';


class ClusterService extends ServiceModel {

	static displayName = 'cluster'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/cluster', 0)
	}

	getClusters(token) {
		return this.get('', {
			token,
		})
	}
	
	addCluster(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	getClusterDetail(id, query, token) {
		return this.get(`/${id}`, {
			query: {
				with_stylecard: true,
				...query,
			},
			token,

		})
	}

	publishMultiple(clusterId, stylecardIds, token) {
		return this.post('/stylecard', {
			data: {
				action: 'publish',
				cluster_id: clusterId,
				stylecard_ids: stylecardIds,
			},
			token,
		})
	}
}

export default new ClusterService()
