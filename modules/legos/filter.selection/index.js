import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

// import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import { without } from 'lodash';

import Styles from './style';


export default ConnectHelper(
	class FilterSelectionLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				selections: PropTypes.arrayOf(PropTypes.shape({
					key: PropTypes.any,
					title: PropTypes.string,
				})),
				values: PropTypes.arrayOf(PropTypes.any),
				multiple: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			selections: [],
			values: [],
		}

		constructor(p) {
			super(p, {
				selectedIndexes: p.selections.map((sel, i) => {
					return p.values.indexOf(sel.key) > -1 ? i : false
				}).filter(sel => sel !== false),
			})
		}

		onChange = (index) => {
			if(this.props.multiple) {
				if(this.state.selectedIndexes.indexOf(index) === -1) {
					this.setState({
						selectedIndexes: [...this.state.selectedIndexes, index],
					}, this.onUpdate)
				} else {
					this.setState({
						selectedIndexes: without(this.state.selectedIndexes, index),
					}, this.onUpdate)
				}
			} else {
				if(this.state.selectedIndexes.indexOf(index) > -1) {
					this.setState({
						selectedIndexes: [],
					}, this.onUpdate)
				} else {
					this.setState({
						selectedIndexes: [index],
					}, this.onUpdate)
				}
			}
		}

		onUpdate = () => {
			if(this.props.multiple) {
				this.props.onChange &&
				this.props.onChange(this.state.selectedIndexes.map(i => {
					return this.props.selections[i].key
				}))
			} else {
				this.props.onChange &&
				this.props.onChange(this.state.selectedIndexes.length ? this.props.selections[this.state.selectedIndexes[0]].key : undefined)
			}
		}

		selectionRenderer = ({title}, index) => {
			return (
				<TouchableBit row unflex key={index} onPress={this.onChange.bind(this, index)} style={Styles.item}>
					<CheckboxBit dumb isActive={ this.state.selectedIndexes.indexOf(index) > -1 } onPress={ this.onChange.bind(this, index) } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			);
		}

		view() {
			return (this.props.selections.map(this.selectionRenderer))
		}
	}
)
