import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'utils/constants/color'

import UtilitiesContext from 'coeur/contexts/utilities';

import AuthenticationHelper from 'utils/helpers/authentication';
import StyleSheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputCurrencyBit from 'modules/bits/input.currency';
import ScrollViewBit from 'modules/bits/scroll.view';

import LoaderLego from 'modules/legos/loader';
import NotificationLego from 'modules/legos/notification';
import RowsLego from 'modules/legos/rows';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';


export default ConnectHelper(
	class QuickEditStylesheetInventoryPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onUpdated: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				editable: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return StyleSheetService.getInventoryDetail(oP.id, {
				token: state.me.token,
			})
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
				data: {},
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onUpdate = (key, value) => {
			this.state.data[key] = value

			this.setState({
				data: { ...this.state.data },
			})
		}

		onSubmit = () => {
			this.setState({
				isUpdating: true,
			})

			StyleSheetService.updateInventory(this.props.id, this.state.data, this.props.token).then(() => {
				this.props.utilities.notification.show({
					title: 'Success',
					message: 'Style sheet inventory updated',
					type: 'SUCCESS',
				})

				this.props.onUpdated &&
				this.props.onUpdated(this.state.data)

				this.onClose()
			}).catch(this.onError).catch(err => {
				this.warn(err)

				this.props.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong. Please try again.',
				})

				this.setState({
					isUpdating: false,
				})
			})
		}

		viewOnError() {
			return (
				<BoxBit row centering style={ Styles.empty }>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.error }>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalQuickViewPagelet
					title={ 'Edit Style Sheet Inventory' }
					layout="1-way"
					onClose={ this.onClose }
				>
					<ScrollViewBit style={ Styles.content }>
						<RowsLego
							data={[{
								data: [{
									headerless: true,
									children: (
										<NotificationLego type={ NotificationLego.TYPES.WARNING } message="WARNING: You are about to edit and make changes to style sheet inventory. Any price difference after customer checkout may become a dispute." />
									),
								}],
							}, {
								data: [{
									title: 'Retail Price',
									children: (
										<InputCurrencyBit readonly={ !this.props.editable }
											placeholder="Input here…"
											value={ this.props.data.retail_price }
											onChange={ this.onUpdate.bind(this, 'retail_price') }
										/>
									),
								}],
							}, {
								data: [{
									title: 'Discounted Price',
									children: (
										<InputCurrencyBit readonly={ !this.props.editable }
											placeholder="Input here…"
											value={ this.props.data.price }
											onChange={ this.onUpdate.bind(this, 'price') }
										/>
									),
								}],
							}]}
							style={ Styles.container }
						/>
					</ScrollViewBit>
					<BoxBit unflex style={ Styles.button }>
						<ButtonBit
							title={ 'Update' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.state.isUpdating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onSubmit }
						/>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
