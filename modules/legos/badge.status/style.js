import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	status: {
		backgroundColor: Colors.black.palette(2),
		borderRadius: 2,
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 12,
		paddingRight: 12,
		overflow: 'hidden',
	},

	spacer: {
		flexGrow: .2,
	},

	row: {
		alignItems: 'center',
	},

	text: {
		color: Colors.white.primary,
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	statusGreen: {
		backgroundColor: Colors.green.palette(2),
	},

	statusRed: {
		backgroundColor: Colors.red.palette(7),
	},

	statusYellow: {
		backgroundColor: Colors.yellow.palette(3),
	},

	statusOrange: {
		backgroundColor: Colors.red.palette(6),
	},

	statusBlue: {
		backgroundColor: Colors.blue.palette(2),
	},

	statusGrey: {
		backgroundColor: Colors.solid.grey.palette(5),
	},
})
