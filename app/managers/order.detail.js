import DictionaryModel from 'coeur/models/dictionary'

import OrderDetailRecord from 'app/records/order.detail';

import OrderDetailService from 'app/services/order.detail';
import OrderService from 'app/services/order';

import MeManager from './me';

import CommonHelper from 'coeur/helpers/common';


class OrderDetailManager extends DictionaryModel {

	static displayName = 'orderdetails'

	constructor() {
		super(OrderDetailRecord, {
			expiricy: 1000 * 60 * 30,
		})
	}

	dataGetter(id) {
		return OrderDetailService.getOrderDetail(id, MeManager.state.token).then(batch => {
			return this.updateLater(batch)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return OrderDetailService.getOrderDetails(ids, MeManager.state.token).then(batches => {
			return this.updateLater(batches)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll({
		status = undefined,
		date = undefined,
		search = undefined,
		offset = 0,
		count = 20,
	} = {}) {
		return OrderDetailService.getFiltered(CommonHelper.stripUndefined({
			status,
			date,
			search,
			offset,
			count,
		}), MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateOrderStatus({
		id,
		orderId,
		code,
	}) {
		return OrderService.updateStatus({
			id: orderId,
			code,
		}, MeManager.state.token).then(() => {
			return this.dataGetter(id)
		})
	}

	updateOrderShipment({
		id,
		orderId,
		awb,
	}) {
		return OrderService.updateShipment({
			id: orderId,
			awb,
		}, MeManager.state.token).then(() => {
			return this.dataGetter(id)
		})
	}
}

export default new OrderDetailManager()
