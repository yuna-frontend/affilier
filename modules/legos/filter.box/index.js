import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import CollapsibleBit from 'modules/bits/collapsible'
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import IconBit from 'modules/bits/icon'

import Styles from './style';


export default ConnectHelper(
	class FilterBoxLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				title: PropTypes.string,
				children: PropTypes.node,
				style: PropTypes.style,
				contentStyle: PropTypes.style,
				isActive: PropTypes.bool,
			}
		}

		viewOnLoading() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					{ this.headerRenderer() }
					<BoxBit unflex centering style={Styles.content}>
						<LoaderBit />
					</BoxBit>
				</BoxBit>
			)
		}

		headerRenderer = (isActive) => {
			return (
				<BoxBit row style={Styles.contentHeader}>
					{!!this.props.title && (
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2}>
							{ this.props.title }
						</GeomanistBit>
					)}
					<BoxBit unflex>
						<IconBit name={ isActive ? 'substract' : 'expand' } size={20}/>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			// return this.props.isLoading ? this.viewOnLoading() : (
			// 	<BoxBit unflex style={[Styles.container, this.props.style]}>
			// 		{ this.headerRenderer() }
			// 		<BoxBit unflex style={[Styles.content, this.props.contentStyle]}>
			// 			{ this.props.children }
			// 		</BoxBit>
			// 	</BoxBit>
			// )
			return (
				<CollapsibleBit
					isActive={ !!this.props.isActive }
					headerRenderer={ this.headerRenderer }
					headerStyle={ Styles.header }
					style={[Styles.collapsible, this.props.style]}
				>
					{this.props.children}
				</CollapsibleBit>
			)
		}
	}
)
