import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import CardMediaComponent from 'modules/components/card.media'

import Styles from './style';


export default ConnectHelper(
	class ImagePagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				source: PropTypes.image,
				onPress: PropTypes.func,
				style: PropTypes.style,
				onEdit: PropTypes.func,
				uploader: PropTypes.func,
			}
		}

		view() {
			return(
				<BoxBit accessible unflex style={[Styles.container, this.props.style]}>
					{ this.props.id ? (
						<CardMediaComponent transform={{
							crop: 'fit',
						}} id={ this.props.id } style={Styles.image} />
					) : (
						<ImageBit
							transform={{
								crop: 'fit',
							}}
							source={this.props.source}
							style={ Styles.image }
						/>
					) }
					{
						!!this.props.onEdit && (
							<TouchableBit unflex style={Styles.edit} onPress={this.props.onEdit}>
								<IconBit name="edit" size={36}/>
							</TouchableBit>
						)

					}
					
					<TouchableBit unflex style={Styles.icon} onPress={this.props.onPress}>
						<IconBit name="close" size={36}/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
