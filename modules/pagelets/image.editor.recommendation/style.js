import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		background: Colors.white.primary,
		minHeight: '100%',
		// width: Styles.size.fluidWidth,
		// height: Styles.size.fluidHeight,
	},

	heightAddition: {
		height: Styles.size.fluidHeight + 200,
	},

	image: {
		width: '50',
		height: '50',
		borderColor: Colors.black.palette(1),
		borderWidth: 1,
	},

	imageCropper: {
		height: '25%',
		width: '25%',
		borderWidth: 1,
	},

	icon: {
		position: 'absolute',
		right: 24,
		top: 24,
	},
})
