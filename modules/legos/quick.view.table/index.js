import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import BadgeStatusSmallLego from 'modules/legos/badge.status.small';

import Styles from './style';


export default ConnectHelper(
	class QuickViewTableLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				data: PropTypes.arrayOf(PropTypes.shape({
					subheader: PropTypes.string,
					title: PropTypes.string,
					description: PropTypes.string,
					status: PropTypes.string,
					onPress: PropTypes.func,
				})),
				style: PropTypes.style,
			}
		}

		dataRenderer = (data, i) => {
			return (
				<BoxBit key={ i } unflex row style={ Styles.row }>
					<BoxBit style={ Styles.data }>
						{ data.subheader ? (
							<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_2 } weight="normal" style={ Styles.primary }>
								{ data.subheader }
							</GeomanistBit>
						) : false }
						<BoxBit row>
							<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 } weight="normal" style={ Styles.title }>
								{ data.title }
							</GeomanistBit>
						</BoxBit>
						{ data.description ? (
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.note }>
								{ data.description }
							</GeomanistBit>
						) : false }
					</BoxBit>
					{ data.status ? (
						<BadgeStatusSmallLego status={ data.status } style={ Styles.status } />
					) : false }
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={ this.props.style }>
					{ this.props.data.map(this.dataRenderer) }
				</BoxBit>
			)
		}
	}
)
