import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CollapsibleBit from 'modules/bits/collapsible';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';

import BoxLego from '../box'
import HintLego from '../hint';

import TagTogglerPart from './_toggler'

import Styles from './style';

import { isEmpty, isEqual, without, intersectionWith, intersection } from 'lodash';

const cache = {}


export default ConnectHelper(
	class BoxTagLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				categoryId: PropTypes.number,
				tagIds: PropTypes.arrayOf(PropTypes.number),
				parentTagIds: PropTypes.arrayOf(PropTypes.number),
				onChange: PropTypes.func,
			}
		}

		static propsToQuery(state, oP) {
			if(oP.categoryId) {
				const _cache = CommonHelper.default(oP.cache, true)

				return !_cache || !cache[oP.categoryId] ? [`query {
					categoryTagsList(filter: {
						categoryId: {
							equalTo: ${oP.categoryId}
						}
					}) {
						tag {
							id
							title
							tags {
								id
								title
							}
						}
					}
				}`, {
					fetchPolicy: 'no-cache',
				}, state.me.token] : null
			} else {
				return null
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {

			if (!nP.categoryId) {
				return null
			} else if (!isEmpty(nP.data)) {
				cache[nP.categoryId] = nP.data
			} else if (cache[nP.categoryId]) {
				// check for cache
			} else {
				return null
			}

			return {
				tags: cache[nP.categoryId].categoryTagsList.map(({tag}, i) => {
					return {
						header: tag.title,
						tags: tag.tags,
						isActive: nS.tags[i] ? nS.tags[i].isActive : false,
					}
				}),
				tagIds: intersection(nS.tagIds, cache[nP.categoryId].categoryTagsList.reduce( (sum, { tag }) => {
					return [...sum, ...tag.tags.map(t => t.id)]
				}, [])),
			}
		}

		static defaultProps = {
			cache: true,
			tagIds: [],
			parentTagIds: [],
		}

		constructor(p) {
			super(p, {
				tags: [],
				tagIds: p.tagIds,
			}, [])
		}

		componentDidUpdate(pP, pS) {
			if(!isEqual(pS.tags, this.state.tags)) {
				this.update()
			}
		}

		update = () => {
			this.props.onChange &&
			this.props.onChange(this.state.tagIds)
		}

		onToggle = (id, isActive) => {
			// TODO
			if(isActive) {
				this.setState({
					tagIds: [...this.state.tagIds, id],
				}, this.update)
			} else {
				this.setState({
					tagIds: without(this.state.tagIds, id),
				}, this.update)
			}
		}

		onToggleCollapsible = (index, isActive) => {
			this.state.tags[index].isActive = !isActive
			this.setState({
				tags: [...this.state.tags],
			})
		}

		onToggleAll = state => {
			if (state) {
				this.state.tags.forEach(tag => {
					tag.isActive = false
				})
			} else {
				this.state.tags.forEach(tag => {
					tag.isActive = true
				})
			}

			this.setState({
				tags: [...this.state.tags],
			})
		}

		count = (tag, id) => {
			return tag.id === id
		}

		viewOnLoading() {
			return (
				<BoxLego title={'Tags'} contentContainerStyle={this.props.style}>
					<BoxBit type={BoxBit.TYPES.ALL_THICK} centering>
						<LoaderBit simple />
					</BoxBit>
				</BoxLego>
			)
		}

		tagRenderer = ({
			id,
			title,
		}) => {
			return (
				<TagTogglerPart key={id} title={title} isActive={this.state.tagIds.indexOf(id) > -1} isSelected={this.props.parentTagIds.indexOf(id) > -1} onPress={this.onToggle.bind(this, id)} />
			)
		}

		headerRenderer = (header, count, isActive) => {
			return (
				<BoxBit row>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.title} >
						{ header }
					</GeomanistBit>
					<BoxBit />
					{ count > 0 ? (
						<GeomanistBit
							type={GeomanistBit.TYPES.NOTE_1}
							weight="medium"
							style={Styles.count}
						>
							{ count }
						</GeomanistBit>
					) : false }
					<IconBit
						name={ isActive ? 'arrow-upward' : 'arrow-downward' }
						size={20}
						color={ Colors.black.palette(2, .4) }
					/>
				</BoxBit>
			)
		}

		groupRenderer = ({
			header,
			tags,
			isActive,
		}, i) => {
			return (
				<CollapsibleBit key={i} dumb isActive={isActive} onPress={this.onToggleCollapsible.bind(this, i)}
					headerRenderer={ this.headerRenderer.bind(this, header, intersectionWith(tags, this.state.tagIds, this.count).length) }
					headerStyle={ Styles.header }
					style={ Styles.collapsible }
				>
					<BoxBit row style={Styles.wrap}>
						{ tags.map(this.tagRenderer) }
					</BoxBit>
				</CollapsibleBit>
			);
		}

		view() {
			const toggleState = this.state.tags.findIndex(tag => tag.isActive === true) > -1

			return (
				<BoxLego borderless title="Tags" header={(
					<BoxBit unflex row style={Styles.buttons}>
						<ButtonBit
							key={toggleState}
							title={toggleState ? 'Close All' : 'Open All'}
							theme={ButtonBit.TYPES.THEMES.SECONDARY}
							size={ButtonBit.TYPES.SIZES.SMALL}
							onPress={this.onToggleAll.bind(this, toggleState)}
							style={Styles.button}
						/>
						<HintLego title="Select relevant tags according to product category" />
					</BoxBit>
				)} contentContainerStyle={[Styles.container, this.props.style]}>
					{ this.state.tags.map(this.groupRenderer) }
				</BoxLego>
			)
		}
	}
)
