import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'

// import ButtonBit from 'modules/bits/button';
import TextInputBit from 'modules/bits/text.input';

import RowLego from 'modules/legos/row';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import VariantsPart from './_variants';

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class VariantPickerPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				search: PropTypes.string,
				onClose: PropTypes.func,
				onSelect: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				selectedId: null,
				search: p.search,
			})
		}

		onSearch = debounce((e, val) => {
			this.setState({
				search: val,
			})
		}, 300)

		onClose = () => {
			this.props.utilities.alert.hide()

			this.props.onClose &&
			this.props.onClose()
		}

		onSelect = id => {
			this.setState({
				selectedId: id,
			})
		}

		onConfirm = () => {
			this.props.utilities.alert.hide()

			this.props.onSelect &&
			this.props.onSelect(this.state.selectedId)
		}

		onAddNewVariation = () => {
			this.props.onNavigateToInventory &&
			this.props.onNavigateToInventory()
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Select Variation"
					subheader={(
						<RowLego data={[{
							title: 'Product Title',
							children: (
								<TextInputBit defaultValue={ this.state.search } placeholder="Input here" onChange={this.onSearch} />
							),
						}, {
							blank: true,
						}]} style={Styles.header} />
					)}
					loading={ this.state.isLoading }
					disabled={ this.state.selectedId === null }
					confirm={ 'Select' }
					onCancel={ this.onClose }
					onConfirm={ this.onConfirm }
					contentContainerStyle={ Styles.content }
				>
					<VariantsPart keyword={ this.state.search } onSelect={ this.onSelect } />
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
