import ServiceModel from 'coeur/models/service';


class LinkService extends ServiceModel {

	static displayName = 'link'

	constructor() {
		super(process.env.CONNECTER_URL + 'affiliate/link/', 0)
	}

	getClicks(userId, query, token) {
		return this.get('click/' + userId, {
			query,
			token,
		})
	}

}

export default new LinkService()
