import React from 'react';
import ConnectedStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UserService from 'app/services/user.new'

import ImagesPagelet from 'modules/pagelets/images';
import NotFoundPagelet from 'modules/pagelets/not.found';


export default ConnectHelper(
	class ImagesUserPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				addable: PropTypes.bool,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? UserService.getUserAssets(oP.id, '', state.me.token).catch(() => []) : Promise.resolve([])
		}

		static defaultProps = {
			addable: true,
		}

		uploader = (image, index) => {
			return UserService.upload(this.props.id, {
				image,
				order: index,
			}, this.props.token)
		}

		remover = id => {
			return UserService.removeAsset(this.props.id, id, this.props.token)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return (
				<ImagesPagelet
					prefix="UA"
					addable={ this.props.addable }
					reorder={ false }
					images={ this.props.data.map(images => {
						return images
					}) }
					uploader={ this.uploader }
					remover={ this.remover }
					orderer={ this.orderer }
					onUpdate={ this.props.onUpdate }
				/>
			)
		}
	}
)
