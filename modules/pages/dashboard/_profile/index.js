import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'

import InitialLego from 'modules/legos/initial'

import Styles from './style'

export default ConnectHelper(
	class ProfilePart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				fullName: state.me.fullName,
				email: state.me.email,
				phone: state.me.phone,
				token: state.me.token,
			}
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={ this.props.style }>
					<BoxBit row>
						<InitialLego
							name={ this.props.fullName }
							style={ Styles.initialLego }
						/>
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.HEADER_4 } style={ Styles.black2 }>
								{ this.props.fullName }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={ Styles.black2_06 }>
								Affiliate Partner
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ this.props.email }
							</GeomanistBit>
						</BoxBit>
					</BoxBit>

				</BoxBit>
			)
		}
	}
)
