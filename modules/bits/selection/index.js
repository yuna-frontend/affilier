import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreSelectionBit from 'coeur/modules/bits/selection';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import TextBit from '../text'
import IconBit from '../icon';
import GeomanistBit from '../geomanist';
import TextInputBit from '../text.input';
import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class SelectionBit extends CoreSelectionBit({
		BoxBit,
		TextBit,
		TouchableBit,
		IconBit,
		TextInputBit,
		iconDisabledColor: Colors.black.palette(2, .5),
		Styles,
	}) {
		textRenderer(value, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={style}>{ value }</GeomanistBit>
			)
		}
	}
)
