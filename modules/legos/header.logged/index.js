/* eslint-disable no-nested-ternary */
import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import MeManager from 'app/managers/me';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BreadcrumbLego from 'modules/legos/breadcrumb';
// import InitialLego from '../initial';

import Styles from './style';


export default ConnectHelper(
	class HeaderLoggedLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				closer: PropTypes.bool,
				header: PropTypes.node,
				onShowMenu: PropTypes.func,
				children: PropTypes.node,
			}
		}

		static stateToProps(state) {
			return {
				name: state.me.fullName,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		static defaultProps = {
		}

		constructor(p) {
			super(p, {
				isVisible: true,
			})

			this.autoCloser = null
		}

		// componentDidMount() {
		// 	this.autoCloser = this.setTimeout(this.onHide, 500)
		// }

		onShowMenu = () => {
			if(this.props.onShowMenu) {
				this.props.onShowMenu()
			} else {
				this.props.utilities.menu.show({
					actions: [{
						title: 'Logout',
						onPress: () => {
							this.props.page.navigator.navigate('login')
							MeManager.logout()
						},
					}],
				})
			}
		}

		// onToggleVisibility = () => {
		// 	if(this.state.isVisible) {
		// 		this.onHide()
		// 	} else {
		// 		this.onShow()
		// 	}
		// }

		// onShow = () => {
		// 	if (this.autoCloser) {
		// 		clearTimeout(this.autoCloser)
		// 	}

		// 	this.setState({
		// 		isVisible: true,
		// 	})
		// }

		// onHide = () => {
		// 	if (this.autoCloser) {
		// 		clearTimeout(this.autoCloser)

		// 		this.autoCloser = this.setTimeout(() => {
		// 			this.setState({
		// 				isVisible: false,
		// 			})
		// 		}, 100)
		// 	}
		// }

		onNavigateToDashboard = () => {
			this.props.page.navigator.navigate('dashboard')
		}

		view() {
			return (
				<BoxBit unflex accessible style={[Styles.container, this.state.isVisible && Styles.visible, this.props.style]}>
					<BoxBit row style={Styles.content}>
						{ this.props.header
							? this.props.header
							: this.props.paths
								? <BreadcrumbLego paths={ this.props.paths } style={ Styles.breadcrumb }/>
								: false
						}
						<BoxBit />
						<BoxBit unflex row centering>
							<TouchableBit unflex row centering onPress={this.onNavigateToDashboard}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_2} weight="medium" style={Styles.title} >
									{ this.props.name }
								</GeomanistBit>
								{/* <InitialLego key={1} name={ this.props.name } style={Styles.initial} /> */}
							</TouchableBit>
							{ this.props.onShowMenu ? (
								<TouchableBit unflex enlargeHitSlop onPress={this.onShowMenu} style={ Styles.toggleMenuBtn }>
									<IconBit
										key={ this.props.closer }
										name={ this.props.closer ? 'close' : 'more' }
										size={20}
										style={Styles.darkGrey80}
									/>
								</TouchableBit>
							) : false }
						</BoxBit>
					</BoxBit>
					{ this.props.children }
				</BoxBit>
			);
		}
	}
)
