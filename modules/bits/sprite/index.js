import ConnectHelper from 'coeur/helpers/connect';
import CoreSpriteBit from 'coeur/modules/bits/sprite';

import ImageBit from '../image';


export default ConnectHelper(
	class SpriteBit extends CoreSpriteBit({
		ImageBit,
	}) {}
)
