import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	label: {
		marginTop: 4,
		marginBottom: 4,
	},

	url: {
		marginBottom: 8,
	},

	image: {
		height: 160,
		width: '100%',
		backgroundColor: Colors.grey.palette(2),
	},

	note: {
		color: Colors.grey.palette(3),
		marginTop: 8,
		marginBottom: 8,
	},
})
