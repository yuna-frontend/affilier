import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import ImageBit from '../image'
import TextInputBit from '../text.input'

import Styles from './style'


export default ConnectHelper(
	class InputImageUrlBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				description: PropTypes.string,
				value: PropTypes.string,
				disabled: PropTypes.bool,
				onChange: PropTypes.func,
				style: PropTypes.style,
				imageStyle: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				value: p.value,
			})
		}

		onUpdate = () => {
			this.forceUpdate()
		}

		onChange = (e, val) => {
			this.state.value = val

			this.props.onChange &&
			this.props.onChange(val)
		}


		view() {
			return (
				<BoxBit unflex style={ this.props.style }>
					{ this.props.title ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" style={ Styles.label }>
							{ this.props.title }
						</GeomanistBit>
					) : false }
					<TextInputBit disabled={ this.props.disabled } placeholder={ this.props.placeholder } defaultValue={ this.props.value } onChange={ this.onChange } onBlur={ this.onUpdate } style={ Styles.url } />
					<ImageBit key={ this.state.value } broken={ !this.state.value } source={ this.state.value } style={[ Styles.image, this.props.imageStyle ]} />
					{ this.props.description ? (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
							{ this.props.description }
						</GeomanistBit>
					) : false }
				</BoxBit>
			)
		}
	}
)
