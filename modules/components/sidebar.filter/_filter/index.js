import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import Colors from 'coeur/constants/color'

// import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

import FilterBoxLego from 'modules/legos/filter.box';

import PricePart from './_price';
import BrandPart from './_brand';
import ColorPart from './_color';
import SizePart from './_size';
import MerchantPart from './_merchant'

import { isEmpty } from 'lodash';

import Styles from './style';

let cache = null

function sumObject(sum, object) {
	return {
		...sum,
		...object,
	}
}

export default ConnectHelper(
	class FilterPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				// brandIds: PropTypes.arrayOf(PropTypes.id),
				categoryId: PropTypes.id,
				filter: PropTypes.object,
				isResettable: PropTypes.bool,
				onReset: PropTypes.func,
				onChange: PropTypes.func,
				onCategoryChildrenLoaded: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static propsToQuery(state) {
			return [`query {
				categoriesList(filter: {
					categoryId: {
						isNull: false
					}
				}) {
					id
					categoryId
				}
				categorySizesList {
					categoryId
					sizeId
				}
				sizesList(filter: {
					sizeId: {
						isNull: true
					}
				}) {
					id
					title
					sizes {
						id
						title
					}
				}
			}`, {
				skip: cache !== null,
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP, nS) {
			if (!isEmpty(nP.data) && cache === null) {
				const categorySizeMap = nP.data.categorySizesList.map(categorySize => {
					return {
						[categorySize.categoryId]: categorySize.sizeId,
					}
				}).reduce(sumObject, {})

				cache = {
					categoryChildren: nP.data.categoriesList.reduce((sum, category) => {
						if(sum[category.categoryId] === undefined) {
							sum[category.categoryId] = []
						}

						sum[category.categoryId].push(category.id)

						return sum
					}, {}),
					categorySize: nP.data.categoriesList.map(category => {
						return {
							[category.id]: categorySizeMap[category.id] || categorySizeMap[category.categoryId],
						}
					}).reduce(sumObject, {}),
					sizes: nP.data.sizesList.map(size => {
						return {
							...size,
							children: size.sizes.map(c => {
								return {
									key: c.id,
									title: c.title,
								}
							}),
						}
					}),
				}

				nP.onCategoryChildrenLoaded &&
				nP.onCategoryChildrenLoaded(cache.categoryChildren)
			} else if (cache) {
				// check for cache
				nP.onCategoryChildrenLoaded &&
				nP.onCategoryChildrenLoaded(cache.categoryChildren)
			} else {
				return null
			}

			if (nP.categoryId) {
				return {
					isResettable: nS.isResettable,
					brandCategoryIds: cache.categoryChildren[nP.categoryId] || [nP.categoryId],
					sizes: [cache.sizes.find(size => {
						return size.id === cache.categorySize[nP.categoryId]
					})],
				}
			} else {
				return {
					isResettable: nS.isResettable,
					brandCategoryIds: [],
					sizes: cache.sizes,
				}
			}
		}

		static defaultProps = {
			filter: {},
		}

		constructor(p) {
			super(p, {
				sizes: [],
				brandCategoryIds: [],
			})
		}

		onChange = (type, value) => {
			this.props.onChange &&
			this.props.onChange(type, value)
		}

		viewOnLoading() {
			return (
				<FilterBoxLego title="Filter by" style={this.props.style} isLoading />
			)
		}

		view() {
			return (
				<FilterBoxLego title="Filter by" style={this.props.style} contentStyle={Styles.container}>
					<PricePart key="price" price={ this.props.filter.price } onChange={this.onChange.bind(this, 'price')} />
					<ColorPart key="color" colorIds={ this.props.filter.colorIds } onChange={this.onChange.bind(this, 'colorIds')} />
					<BrandPart key="brand" categoryIds={this.state.brandCategoryIds} brandIds={ this.props.filter.brandIds } onChange={this.onChange.bind(this, 'brandIds')} />
					<SizePart key={this.state.sizes.map(s => s.id).join(',')} sizeIds={ this.props.filter.sizeIds } sizes={this.state.sizes} onChange={this.onChange.bind(this, 'sizeIds')} />

					<ButtonBit
						title="Reset All Filters"
						weight="normal"
						theme={ButtonBit.TYPES.THEMES.BLACK}
						state={ this.props.isResettable ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
						onPress={this.props.onReset}
					/>
				</FilterBoxLego>
			)
		}
	}
)
