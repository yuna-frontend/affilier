import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'

import ItemService from 'app/services/item';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class ResultItemPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				search: PropTypes.string,

				onSelect: PropTypes.func,
				onHide: PropTypes.func,
				style: PropTypes.object,
			}
		}

		static defaultProps = {
			search: '',
		}

		static TYPES = {
			MATCHBOX: 'MATCHBOX',
			VARIANT: 'VARIANT',
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				hoverIndex: undefined,
				items: [],
			}, [
				// 'onPress',
				'resultRenderer',
			])
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP) {
			if(pP.search !== this.props.search) {
				this.getData()
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				ItemService.getAllItem({
					search: this.props.search,
				}, this.props.token).then(res => {
					this.setState({
						isLoading: false,
						items: res,
					})
				}).catch(err => {
					this.warn(err)
					this.props.utilities.notification.show({
						title: 'Oops ... ',
						message: 'Something went wrong',
					})

					this.setState({
						isLoading: false,
						items: [],
					})
				})

			})
		}

		loadingRenderer() {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={[Styles.darkGrey60, Styles.padderRight]}>
						Searching...
					</GeomanistBit>
					<LoaderBit simple size={20} />
				</BoxBit>
			);
		}

		emptyRenderer() {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.darkGrey60}>
						Currently have no data recorded.
					</GeomanistBit>
				</BoxBit>
			);
		}

		onPress(val) {
			this.props.onSelect &&
			this.props.onSelect(val, true,  val.title)

			this.props.onHide &&
			this.props.onHide()
		}

		onMouseEnter(i) {
			this.setState({
				hoverIndex: i,
			})
		}

		onMouseLeave() {
			this.setState({
				hoverIndex: undefined,
			})
		}

		resultRenderer(content, i) {
			return (
				<TouchableBit key={i} unflex row
					onMouseEnter={ this.onMouseEnter.bind(this, i) }
					onMouseLeave={ this.onMouseLeave.bind(this, i) }
					style={[i === this.state.hoverIndex
						? Styles.isHovered
						: Styles.listContent,
					Styles.list,
					]}
					onPress={ this.onPress.bind(this, content)}
				>
					<BoxBit unflex row style={Styles.desc}>
						<GeomanistBit ellipsis
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={i === this.state.hoverIndex
								? Styles.white
								: Styles.darkGrey
							}
						>
							{ content.title }
						</GeomanistBit>
					</BoxBit>

					{ i === this.state.hoverIndex && (
						<TouchableBit unflex style={Styles.select}>
							<GeomanistBit
								type={GeomanistBit.TYPES.NOTE_1}
								weight="medium"
								style={Styles.white}
							>
								Select
							</GeomanistBit>
						</TouchableBit>
					) }
				</TouchableBit>
			)
		}

		view() {
			return this.state.isLoading ? this.loadingRenderer() : (
				<BoxBit unflex>
					<ScrollViewBit style={ Styles.searchBar }>
						{ this.state.items.map(this.resultRenderer) }
					</ScrollViewBit>
				</BoxBit>
			)
		}
	}
)
