import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'
import Colors from 'coeur/constants/color'
import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon'
import GeomanistBit from 'modules/bits/geomanist'
import TextInputBit from 'modules/bits/text.input'
import TouchableBit from 'modules/bits/touchable'

import ListLego from 'modules/legos/list'
import ModalPagelet from 'modules/pagelets/modal'

import Styles from './style'

const cache = []

export default ConnectHelper(
	class SearchPart extends StatefulModel {
		
		static propTypes(PropTypes) {
			return {
				onSearch: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {})
		}

		onCloseModal = () => {
			this.props.utilities.alert.close()
		}

		setCache = (val) => {
			if(cache.length === 0) return cache.push(val)
			
			const findIndex = cache.findIndex(v => v === val)

			if(cache.length > 8) {
				if(findIndex === -1) {
					cache.splice(cache.length - 1)
					cache.unshift(val)
				} else {
					cache.splice(cache[findIndex], 1)
					cache.unshift(val)
				}
			} else {
				if(findIndex === -1) {
					cache.unshift(val)
				} else {
					cache.splice(cache[findIndex], 1)
					cache.unshift(val)
				}
			}
		}

		onSubmit = (val) => {
			this.setCache(val)

			this.props.utilities.alert.close()

			this.props.onSearch &&
			this.props.onSearch(val)
		}

		headerRenderer = () => {
			return (
				<BoxBit unflex row style={ Styles.header }>
					<BoxBit>
						<TextInputBit
							prefix={ <IconBit name="search" size={ 16 } /> }
							onSubmitEditing={ this.onSubmit }
							style={ Styles.containerInput }
							inputStyle={ Styles.input }
						/>
					</BoxBit>
					<TouchableBit unflex onPress={ this.onCloseModal } style={ Styles.cancel }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold">
							Batal
						</GeomanistBit>
					</TouchableBit>
				</BoxBit>
			)
		}

		subheaderRenderer = ({
			title,
			hapus,
		}, i) => {
			return (
				<BoxBit key={ i } unflex style={ Styles.subheader }>
					<BoxBit row>
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 }>
								{ title }
							</GeomanistBit>
						</BoxBit>
						{ hapus && (
							<TouchableBit unflex style={ Styles.delete }>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold" style={ Styles.greyBase }>
									Hapus
								</GeomanistBit>
							</TouchableBit>
						) }
					</BoxBit>
				</BoxBit>
			)
		}

		listRenderer = (title, i) => {
			return (
				<ListLego key={ i }
					title={ title }
					prefix={<IconBit name={ 'time' } size={ 16 } color={ Colors.grey.palette(12) } style={ Styles.icon } />}
					onPress={ this.onSubmit.bind(this, title) }
					style={ Styles.list }
				/>
			)
		}

		view() {
			return (
				<ModalPagelet
					header={ this.headerRenderer() }
					style={ Styles.container }
					contentContainerStyle={ Styles.noRadius }
				>
					{ this.subheaderRenderer({title: 'Pencarian Terakhir', hapus: false}) }
					{ cache.map(this.listRenderer) }
					
					{/* NOT SUPPORTED YET
						{ this.subheaderRenderer({title: 'Pencarian Popular'}) }
					*/}
				</ModalPagelet>
			)
		}
	}
)
