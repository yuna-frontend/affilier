import React from 'react';
import ConnectedStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import PurchaseService from 'app/services/purchase';

import LoaderLego from 'modules/legos/loader';
import RowImageLego from 'modules/legos/row.image';

import Styles from './style'


export default ConnectHelper(
	class RowImagePurchaseOrderLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				addable: PropTypes.bool,
				reorder: PropTypes.bool,
				purchaseOrderId: PropTypes.id,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.purchaseOrderId ? PurchaseService.getAssets(oP.purchaseOrderId, state.me.token).catch(() => []) : Promise.resolve([])
		}

		static defaultProps = {
			reorder: false,
			addable: true,
		}

		uploader = (image, index) => {
			return PurchaseService.createAsset(this.props.purchaseOrderId, image, index, this.props.token)
		}

		remover = id => {
			return PurchaseService.deleteAsset(
				this.props.purchaseOrderId,
				id,
				this.props.token,
			)
		}

		orderer = images => {
			return PurchaseService.reorderAssets(this.props.purchaseOrderId, images.map(image => image.id), this.props.token)
		}

		viewOnLoading() {
			return (
				<LoaderLego simple style={ Styles.empty } />
			)
		}

		view() {
			return (
				<RowImageLego
					title={ this.props.title }
					addable={ this.props.addable }
					reorder={ this.props.reorder }
					images={ this.props.data }
					uploader={ this.uploader }
					remover={ this.remover }
					orderer={ this.orderer }
					onUpdate={ this.props.onUpdate }
				/>
			)
		}
	}
)
