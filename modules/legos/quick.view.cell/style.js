import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingTop: Sizes.margin.default,
		paddingLeft: 8,
		paddingRight: 8,
		paddingBottom: Sizes.margin.default,
	},

	title: {
		color: Colors.black.palette(2, .7),
		marginTop: 8,
	},

	center: {
		alignItems: 'center',
	},

	icon: {
		marginLeft: 4,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

})
