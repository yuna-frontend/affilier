import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	button: {
		marginTop: 16,
	},
	inputDate: {
		width: 0,
	},
	text: {
		color: Colors.primary,
	},
})
