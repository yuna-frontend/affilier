import DictionaryModel from 'coeur/models/dictionary'

import PickupPointRecord from 'app/records/pickup.point';

import BrandService from 'app/services/brand';
import PickupPointService from 'app/services/pickup.point';

import MeManager from './me';


class PickupPointManager extends DictionaryModel {

	static displayName = 'pickuppoints'

	constructor() {
		super(PickupPointRecord, {
			expiricy: 1000 * 60 * 60,
		})
	}

	dataGetter(id) {
		return PickupPointService.getPickupPoint(id, MeManager.state.token).then(pickupPoint => {
			return this.updateLater(pickupPoint)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return PickupPointService.getPickupPoints(ids, MeManager.state.token).then(pickupPoints => {
			return this.updateLater(pickupPoints)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	searchByBrandId({
		brandId,
	}) {
		return BrandService.getPickupPoints(brandId, MeManager.state.token).then(pickupPoints => {
			return this.updateBulk(pickupPoints)
		})
	}

	createPickupPoint({
		title,
		addressId,
		description,
	}) {
		// TODO
		return PickupPointService.addPickupPoints([{
			title,
			addressId,
			description,
		}], MeManager.state.token).then(pickupPoints => {
			return this.updateBulk(pickupPoints)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updatePickupPoint(update) {
		// NOTE: MUST CONTAIN ID
		return PickupPointService.updatePickupPoints([update], MeManager.state.token).then(pickupPoints => {
			return this.updateBulk(pickupPoints)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new PickupPointManager()
