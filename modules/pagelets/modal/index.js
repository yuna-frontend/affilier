/* eslint-disable no-nested-ternary */
import React from 'react'
import StatefulModel from 'coeur/models/stateful'
import ConnectHelper from 'coeur/helpers/connect'
import UtilitiesContext from 'coeur/contexts/utilities'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'
import Defaults from 'coeur/constants/default'

import Animated from 'coeur/libs/animated'

import BoxBit from 'modules/bits/box'
import HeaderBit from 'modules/bits/header'
import ScrollViewBit from 'modules/bits/scroll.view'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'


export default ConnectHelper(
	class ModalPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				header: PropTypes.node,
				unclosable: PropTypes.bool,
				unflex: PropTypes.bool,
				shorten: PropTypes.bool,
				slide: PropTypes.bool,
				children: PropTypes.node.isRequired,
				onClose: PropTypes.func,
				footer: PropTypes.node,
				centering: PropTypes.bool,
				headerless: PropTypes.bool,
				containerStyle: PropTypes.style,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
				pageStyle: PropTypes.style,

				// title: PropTypes.string,
				// small: PropTypes.bool,
				// subheader: PropTypes.node,
			}
		}

		static defaultProps = {
			slide: false,
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				slide: p.slide && (Defaults.PLATFORM === 'web' ? Defaults.OS === 'web' : true),
			})
		}

		header = {
			rightActions: [{
				type: HeaderBit.TYPES.CLOSE_CIRCLE,
				onPress: () => {
					this.onClose()
				},
				data: {
					color: Colors.white.primary,
				},
			}],
			background: Colors.transparent,
		}

		animation = {
			value: new Animated.Value(0),
			timing: null,
		}

		style = {
			transform: [{
				translateY: this.animation.value,
			}],
			opacity: this.animation.value.interpolate({
				inputRange: [0, 200, Sizes.app.height],
				outputRange: [1, 1, 0],
			}),
		}

		responder = {
			onMoveShouldSetPanResponder: (e, gS) => {
				if (gS.dy >= 0) {
					return true
				}

				return false
			},
			onPanResponderGrant: () => {
				if (this.animation.timing) {
					this.animation.timing.stop()
				}
			},
			onPanResponderMove: (e, gS) => {
				if (this.animation.timing) {
					this.animation.timing.stop()
				}

				this.animation.value.setValue(gS.dy)
			},
			onPanResponderRelease: (e, gS) => {
				if (this.animation.timing) {
					this.animation.timing.stop()
				}

				if (gS.dy > 100 || gS.vy > .5) {
					// To bottom
					if (Defaults.PLATFORM === 'web') {
						this.animation.timing = Animated.timing(this.animation.value, {
							toValue: Sizes.app.height,
							duration: Math.min(300 / gS.vy, 300),
						})
					} else {
						// TODO
						this.animation.timing = Animated.timing(this.animation.value, {
							toValue: Sizes.app.height,
							duration: Math.min(300 / gS.vy, 300),
							useNativeDriver: true,
						})
					}

					this.animation.timing.start(this.onClose)
				} else {
					if (Defaults.PLATFORM === 'web') {
						this.animation.timing = Animated.timing(this.animation.value, {
							toValue: 0,
							duration: Math.min(200 / Math.abs(gS.vy), 200),
							useNativeDriver: true,
						})
					} else {
						this.animation.timing = Animated.spring(this.animation.value, {
							toValue: 0,
							velocity: gS.vy,
							useNativeDriver: true,
						})
					}

					this.animation.timing.start()
				}
			},
			onPanResponderTerminate: (e, gS) => {
				if (this.animation.timing) {
					this.animation.timing.stop()
				}

				if (Defaults.PLATFORM === 'web') {
					this.animation.timing = Animated.decay(this.animation.value, {
						toValue: 0,
						duration: Math.min(200 / Math.abs(gS.vy), 200),
						useNativeDriver: true,
					})
				} else {
					// TODO
					this.animation.timing = Animated.spring(this.animation.value, {
						toValue: 0,
						velocity: gS.vy,
						useNativeDriver: true,
					})
				}

				this.animation.timing.start()
			},
			onPanResponderTerminationRequest: () => false,
		}

		onClose = () => {
			if (this.props.onClose) {
				this.props.onClose()
			} else {
				this.props.utilities.alert.close()
			}
		}

		view() {
			return (
				<BoxBit unflex centering style={[Styles.page, this.props.pageStyle]}>
					<TouchableBit unflex onPress={this.onClose} style={Styles.touchable} />
					<BoxBit animated unflex centering={this.props.centering} onLayout={ this.props.onLayout } style={[ Styles.modal, this.style, this.props.containerStyle]}>
						{ this.props.shorten ? this.state.slide && (
							<TouchableBit onPress={ this.onClose } />
						) || (
							<BoxBit />
						) : false }
						{ this.state.slide ? (
							<TouchableBit unflex onPress={ this.onClose } style={ Styles.header } />
						) : this.props.headerless
							? undefined
							: this.props.header ? this.props.header : (
								<HeaderBit { ...this.header } rightActions={ this.props.unclosable ? undefined : this.header.rightActions } style={Styles.headerContainer} />
							) }
						{ this.state.slide ? (
							<BoxBit unflex { ...this.responder } style={ Styles.grabber }>
								<BoxBit unflex style={ Styles.handle } />
							</BoxBit>
						) : false }
						<ScrollViewBit unflex={ this.props.unflex } scrollEventThrottle={ 16 } style={[ this.state.slide ? Styles.slideContainer : [Styles.container, this.props.contentContainerStyle], this.props.shorten ? Styles.shorten : undefined ]} contentContainerStyle={[ this.state.slide ? Styles.slideContent : Styles.content, this.props.style ]}>
							{ this.props.children }
							{ this.props.shorten ? (
								<BoxBit />
							) : false }
						</ScrollViewBit>
						{ this.props.footer ? this.props.footer : false }
					</BoxBit>
				</BoxBit>
			)
		}

		// view() {
		// 	this.log('AAAA ==>>> ' + this.props.title )
		// 	return (
		// 		<BoxBit style={[Styles.container, this.props.small && Styles.small, this.props.style]}>
		// 			<BoxLego
		// 				unflex={ false }
		// 				small={ this.props.small }
		// 				title={ this.props.title }
		// 				header={ this.props.header }
		// 				style={[Styles.contentContainer, !this.props.footer ? Styles.contentContainerFull : {}]}
		// 				contentContainerStyle={ Styles.content }
		// 			>
		// 				{ this.props.subheader }
		// 				<ScrollViewBit contentContainerStyle={[Styles.scroller, this.props.contentContainerStyle]}>
		// 					{ this.props.children }
		// 				</ScrollViewBit>
		// 			</BoxLego>
		// 			{ this.props.footer ? (
		// 				<BoxBit row unflex style={Styles.footer}>
		// 					{ this.props.footer }
		// 				</BoxBit>
		// 			) : false }
		// 		</BoxBit>
		// 	)
		// }
	}
)

