import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import RefService from 'app/services/ref';

import BoxBit from 'modules/bits/box';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import LoaderBit from 'modules/bits/loader';

import Styles from './style';

import { capitalize, isEmpty } from 'lodash'

let cache = null

export default ConnectHelper(
	class SelectCouponValidityLego extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,

				value: PropTypes.array,

				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache ? RefService.getRefAvailableCouponValidity() : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache = nP.data
			} else if (cache) {
				// check for cache
			} else {
				return null
			}


			return {
				couponValidity: cache,
			}
		}

		static defaultProps = {
			cache: true,
			disabled: false,
			unflex: false,
		}

		constructor(p) {
			super(p, {
				couponValidity: [],
				selected: p.value || [],
			}, [])
		}

		onChangeItem = title => {
			let newArr = this.state.selected.slice()
			const index = newArr.indexOf(title)


			if(index > -1) {
				newArr.splice(index, 1)
			} else {
				newArr = [...newArr, title]
			}

			this.setState({
				selected: newArr,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.state.selected)
			})
		}

		checkboxRenderer = title => {
			return (
				<InputCheckboxBit key={ title } dumb value={ this.state.selected.indexOf(title) > -1 } title={ capitalize(title) } onChange={ this.onChangeItem.bind(this, title) } style={ Styles.item } />
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering accessible={false} style={Styles.loading}>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit accessible={!this.props.disabled}>
					{ this.state.couponValidity.map(this.checkboxRenderer) }
				</BoxBit>
			)
		}
	}
)
