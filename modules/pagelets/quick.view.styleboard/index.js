import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import StyleboardService from 'app/services/style.board';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollViewBit from 'modules/bits/scroll.view';

import ColorLego from 'modules/legos/color';
import InitialLego from 'modules/legos/initial';
import LoaderLego from 'modules/legos/loader';
import QuickViewCellLego from 'modules/legos/quick.view.cell';
import QuickViewImagesMosaicLego from 'modules/legos/quick.view.images.mosaic';
import QuickViewOverviewLego from 'modules/legos/quick.view.overview';
import QuickViewTableLego from 'modules/legos/quick.view.table';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';

import Styles from './style';

import { capitalize } from 'lodash';


export default ConnectHelper(
	class QuickViewStyleboardPagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				mayPublish: PropTypes.bool,
				onClose: PropTypes.func,
				onNavigate: PropTypes.func,
			}
		}

		static propsToPromise(state, p) {
			return StyleboardService.getStyleBoardDetail(p.id, state.me.token).then(styleboard => {
				return {
					...styleboard,
					variants: styleboard.stylecards.map(stylecard => stylecard.variants).reduce((sum, a) => sum.concat(a), []),
				}
			})
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
			})
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onNavigateToClient = () => {
			this.onClose()

			this.props.onNavigate &&
			this.props.onNavigate('client/' + this.props.data.client_id)
		}

		onNavigateToStyleboard = () => {
			this.onClose()

			this.props.onNavigate &&
			this.props.onNavigate('styleboard/' + this.props.id)
		}

		onPublishStyleboard = () => {
			this.setState({
				isLoading: true,
			}, () => {
				StyleboardService.publish(this.props.id, undefined, this.props.token)
					.then(() => {
						this.props.utilities.notification.show({
							title: 'Yeay!',
							message: 'Success publishing styleboard.',
							type: 'SUCCESS',
						})

						this.props.data.status = 'PUBLISHED'

						this.setState({
							isLoading: false,
						})
					}).catch(err => {
						this.warn(err)

						this.props.utilities.notification.show({
							title: 'Oops…',
							message: err ? err.detail && (err.detail.detail || err.detail.message || err.detail) || err.message || err : 'Something went wrong with your request.',
						})

						this.setState({
							isLoading: false,
						})
					})
			})
		}

		colorRenderer = image => {
			const inventory = this.props.data.variants.find(i => i.image === image)

			return (
				<React.Fragment>
					<BoxBit unflex centering style={ Styles.id }>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.text }>#SI-{ inventory.id }</GeomanistBit>
					</BoxBit>
					<BoxBit />
					<ColorLego colors={ inventory.colors } border={ 2 } size={ 28 } radius={ 14 } style={ Styles.color } />
				</React.Fragment>
			)
		}

		stylecardRenderer = stylecard => {
			return (
				<BoxBit unflex style={ Styles.stylesheet }>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.name }>#SC-{ stylecard.id }</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 }>{ stylecard.title || `Style Card Nº ${ stylecard.id }` }</GeomanistBit>
					{ stylecard.stylist_note ? (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>“{ stylecard.stylist_note }”</GeomanistBit>
					) : false }
					<QuickViewTableLego
						data={ stylecard.variants.map(this.variantRenderer) }
						style={ Styles.table }
					/>
				</BoxBit>
			)
		}

		variantRenderer = variant => {
			return {
				subheader: `#SV-${ variant.id }`,
				title: `${ variant.title }`,
				description: `${ variant.category } – ${ variant.size }`,
				status: variant.status === 'UNAVAILABLE' ? 'PACKED' : variant.status,
			}
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalQuickViewPagelet
					title="Styleboard Preview"
					layout="3-way"
					onClose={ this.props.onClose }
					onEdit={ this.onNavigateToStyleboard }
				>
					<QuickViewOverviewLego
						id={ `#SB-${ this.props.id }` }
						subheader={ this.props.data.stylist ? (
							<BoxBit unflex row style={ Styles.stylist }>
								<InitialLego
									source={ this.props.data.stylist.image }
									size={ 20 }
									name={ this.props.data.stylist.name || this.props.data.stylist.full_name }
									style={ Styles.initial }
								/>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_2 } style={ Styles.name }>
									{ `${ this.props.data.stylist.name || this.props.data.stylist.full_name }'s` }
								</GeomanistBit>
							</BoxBit>
						) : undefined }
						title={ (this.props.data.title || `Styleboard Nº ${ this.props.id }`).toUpperCase() }
						mark={ this.props.data.service.toUpperCase() }
						price={ this.props.data.variants.length ? this.props.data.variants.map(i => i.price).reduce((sum, c) => sum + c, 0) : 0 }
						retailPrice={ this.props.data.variants.length ? this.props.data.variants.map(i => i.retail_price).reduce((sum, c) => sum + c, 0) : 0 }
						description={ this.props.data.stylist_note }
					/>
					<QuickViewImagesMosaicLego row
						max={ 8 }
						images={ this.props.data.variants.map(i => i.image) }
						childRenderer={ this.colorRenderer }
					/>
					<ScrollViewBit contentContainerStyle={ Styles.detail }>
						<QuickViewCellLego
							title="CLIENT"
							content={ this.props.data.user.name }
							onPress={ this.onNavigateToClient }
							description={ `on ${ TimeHelper.format(this.props.data.order.created_at, 'dddd, MMMM Do YYYY') }` }
						/>
						<BoxBit row unflex>
							<QuickViewCellLego unflex={ false }
								title="ATTIRE"
								content={ this.props.data.attire }
							/>
							<QuickViewCellLego unflex={ false }
								title="STATUS"
								content={ capitalize(this.props.data.status) }
							/>
						</BoxBit>
						<BoxBit row unflex>
							<QuickViewCellLego unflex={ false }
								title="STYLE CARDS"
								content={ `${ this.props.data.stylecards.filter(s => s.status === 'PUBLISHED').length }/${ this.props.data.stylecards.length }` }
							>
								{ this.props.data.stylecards.filter(s => s.status === 'PUBLISHED').map(this.stylecardRenderer) }
							</QuickViewCellLego>
						</BoxBit>
						<BoxBit />
						{ this.props.mayPublish ? (
							<ButtonBit
								title="PUBLISH STYLEBOARD"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								state={ this.props.data.status === 'STYLING' ? this.state.isLoading && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onPublishStyleboard }
								style={ Styles.button }
							/>
						) : false }
					</ScrollViewBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
