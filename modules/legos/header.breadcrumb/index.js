import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import BreadcrumbLego from 'modules/legos/breadcrumb';
// import InitialLego from '../initial';

import Styles from './style';


export default ConnectHelper(
	class HeaderBreadcrumbLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				paths: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string.isRequired,
					onPress: PropTypes.func,
				})),
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			paths: [],
		}

		static getDerivedStateFromProps(nP) {
			const paths = nP.paths.slice()

			return {
				current: paths.pop() || {},
				paths: paths,
			}
		}

		constructor(p) {
			super(p, {
				current: {},
				paths: [],
			}, [
			])
		}

		view() {
			return (
				<BoxBit row unflex style={[Styles.container, this.props.style]}>
					<BoxBit unflex style={Styles.content}>
						<BreadcrumbLego
							paths={this.state.paths}
							style={Styles.breadcrumb}
						/>
						<GeomanistBit
							type={GeomanistBit.TYPES.HEADER_4}
							style={Styles.title}>
							{this.state.current.title}
						</GeomanistBit>
					</BoxBit>
					{ this.props.children }
				</BoxBit>
			);
		}
	}
)
