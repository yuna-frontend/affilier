import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	note: {
		color: Colors.black.palette(2, .6),
	},

	warning: {
		color: Colors.yellow.palette(2),
	},

	alert: {
		color: Colors.red.palette(7),
	},

})
