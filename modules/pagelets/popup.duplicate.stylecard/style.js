import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		width: 948,
	},

	content: {
		paddingTop: 8,
		paddingLeft: 12,
		paddingRight: 12,
		paddingBottom: 16,
		backgroundColor: Colors.grey.palette(2),
		flexDirection: 'row',
		minHeight: 307,
	},

	wrapper: {
		flexWrap: 'wrap',
	},

	duplicating: {
		flexWrap: 'wrap',
		opacity: .3,
	},

	header: {
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingBottom: 8,
		marginBottom: 0,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	input: {
		height: 34,
	},

	search: {
		borderRadius: 2,
		overflow: 'hidden',
		marginTop: 8,
	},

	icon: {
		marginRight: 8,
	},

	stylesheet: {
		marginLeft: 4,
		marginRight: 4,
		marginTop: 8,
	},

	stylist: {
		marginTop: 8,
		height: 36,
		minHeight: 36,
	},

})
