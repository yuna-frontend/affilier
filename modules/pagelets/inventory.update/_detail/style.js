import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		paddingLeft: 16,
		paddingRight: 16,
		marginTop: 3,
		paddingBottom: 16,
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},
	idr: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 16,
		paddingRight: 16,
		backgroundColor: Colors.solid.grey.palette(1),
		marginRight: 4,
	},
	input: {
		width: 0,
	},
})
