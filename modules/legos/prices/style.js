import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
	},

	column: {
		marginRight: Sizes.margin.default,
		flexGrow: 1.38,
	},

	row: {
		paddingTop: 6,
		paddingBottom: 6,
	},

	title: {
		color: Colors.black.palette(2),
	},

	price: {
		flexGrow: 1,
		color: Colors.black.palette(2),
	},

	header: {
		marginTop: 0,
		paddingTop: 6,
		paddingBottom: 6,
	},

	total: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		paddingTop: 12,
		paddingBottom: 12,
	},

})
