import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginBottom: 0,
		height: 88,
	},
	title: {
		marginTop: 20,
		marginBottom: 4,
		color: Colors.black.palette(2),
	},
	input: {
		marginRight: 6,
	},
	unit: {
		marginTop: 44,
	},
	trash: {
		width: 44,
	},
})
