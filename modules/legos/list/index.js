import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import TouchableBit from 'modules/bits/touchable';
import GeomanistBit from 'modules/bits/geomanist'

import Styles from './style';


export default ConnectHelper(
	class ListLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				accessible: PropTypes.bool,
				note: PropTypes.string,
				desc: PropTypes.string,
				isFirst: PropTypes.bool,
				isLast: PropTypes.bool,
				prefix: PropTypes.node,
				ellipsis: PropTypes.bool,
				children: PropTypes.node,
				onPress: PropTypes.func,
				weight: PropTypes.string,

				contentContainerStyle: PropTypes.style,
				titleStyle: PropTypes.style,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			accessible: true,
			isFirst: false,
			isLast: false,
		}

		view() {
			return (
				<TouchableBit accessible={this.props.accessible} unflex row onPress={this.props.onPress} style={[Styles.container, this.props.isFirst && Styles.head, this.props.isLast && Styles.noBorder, this.props.style]}>
					<BoxBit row style={[Styles.center, this.props.contentContainerStyle]}>
						{ this.props.prefix }
						<BoxBit>
							<GeomanistBit
								ellipsis={ this.props.ellipsis }
								type={GeomanistBit.TYPES.PARAGRAPH_2}
								style={[Styles.text, this.props.titleStyle]}
								weight={this.props.weight}
							>
								{ this.props.title }
							</GeomanistBit>
							{ !!this.props.note && (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{ this.props.note }
								</GeomanistBit>
							) }
							{ !!this.props.desc && (
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.blueGrey}>
									{ this.props.desc }
								</GeomanistBit>
							)}
						</BoxBit>
					</BoxBit>
					{ this.props.children }
				</TouchableBit>
			)
		}
	}
)
